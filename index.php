<?php
/**
 * ENV
 */
$ip = $_SERVER['SERVER_ADDR'];
if (in_array($ip, ['127.0.0.1', '10.71.42.70','103.198.193.43'])) {
    define('ENV', 'dev');
} else {
    define('ENV', 'prod');
}
define('ENV_DEV', ENV === 'dev' ? true : false);

if (ENV_DEV) {
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', 1);
    header('Access-Control-Allow-Origin:*'); //测试环境允许跨域
}

//当前项目名
define("PROJECT_NAME", 'A02');
//开启调试模式
define("APP_DEBUG", ENV_DEV);
//网站当前路径
define('SITE_PATH', dirname(__FILE__)."/");
//项目路径，不可更改
define('APP_PATH', SITE_PATH . 'application/');
//项目相对路径，不可更改
define('SPAPP_PATH',   SITE_PATH.'simplewind/');
//
define('SPAPP',   './application/');
//项目资源目录，不可更改
define('SPSTATIC',   SITE_PATH.'statics/');
//定义缓存存放路径
define("RUNTIME_PATH", SITE_PATH . "data/runtime/");
//静态缓存目录
define("HTML_PATH", SITE_PATH . "data/runtime/Html/");
//版本号
define("SIMPLEWIND_CMF_VERSION", 'X2.1.0');

define("THINKCMF_CORE_TAGLIBS", 'cx,Common\Lib\Taglib\TagLibSpadmin,Common\Lib\Taglib\TagLibHome');

//uc client root
define("UC_CLIENT_ROOT", './api/uc_client/');

if(file_exists(UC_CLIENT_ROOT."config.inc.php")){
	include UC_CLIENT_ROOT."config.inc.php";
}

//载入框架核心文件
require SPAPP_PATH.'Core/ThinkPHP.php';

