
//引入http模块
var socketio = require('socket.io'),
	fs     = require('fs'),
	https     = require('https'),
	domain   = require('domain'),
	redis    = require('redis'),
    redisio  = require('socket.io-redis'),
    request  = require('request'),
    config   = require('./config.js');

var d = domain.create();
d.on("error", function(err) {
	console.log(err);
}); 
var options = {
  key: fs.readFileSync('/usr/local/nginx/conf/ssl/test.yunbao888.com.key'),
  cert: fs.readFileSync('/usr/local/nginx/conf/ssl/test.yunbao888.com.crt')
};
var numscount=0;// 在线人数统计
var sockets = {};
var chat_history={};
var chat_interval={};
var field=[];
// redis 链接
var clientRedis  = redis.createClient(config['REDISPORT'],config['REDISHOST']);
clientRedis.auth(config['REDISPASS']);
var server = https.createServer(options,function(req, res) {
	res.writeHead(200, {
		'Content-type': 'text/html;charset=utf-8'
	});
   //res.write("人数: " + numscount );
	res.end();
}).listen(19968, function() {
	console.log('服务开启19967');
	
});

var io = socketio.listen(server,{
	pingTimeout: 60000,
  	pingInterval: 25000
});
/* var pub = redis.createClient(config['REDISPORT'], config['REDISHOST'], { auth_pass: config['REDISPASS'] });
var sub = redis.createClient(config['REDISPORT'], config['REDISHOST'], { auth_pass: config['REDISPASS'] });
io.adapter(redisio({ pubClient: pub, subClient: sub })); */
setInterval(function(){
  //global.gc();
  //console.log('GC done')
}, 1000*30); 

io.on('connection', function(socket) {
	console.log('连接成功');
	numscount++;

	clientRedis.set("totalNums",numscount);
							
	var interval;
	//console.log("staaaaaaart");
	/* 获取服务端配置 */
	request(config['WEBADDRESS']+"?service=Home.getFilterField",function(error, response, body){
		if(error){

			//console.log(error);
			return;

		} ;

		if(!body){
			//console.log(body);
			return;
		} ;
		var res = evalJson(body);
		//console.log(body);
		//console.log(res.data.info);
		//console.log("sssss");
		field = res.data.info;
	});

	//console.log("ennnnnnnnnnd");

	//进入房间
	socket.on('conn', function(data) {
		
		if(!data || !data.token){
				return !1;
		}
		
		userid=data.uid;
		old_socket = sockets[userid];
		if (old_socket && old_socket != socket) {
			//console.log("删除老的socket"+old_socket.id);
			if(data.uid== data.roomnum && data.stream==old_socket.stream){
				old_socket.reusing = 1;
				//console.log("重用");
			}
			old_socket.disconnect()
		}
		
		clientRedis.get(data.token,function(error,res){
			if(error){
				return;
			}else if(res==null){
				//console.log("[获取token失败]"+data.uid);
			}else{
				if(res != null){
					
					var userInfo = evalJson(res);
					//if(userInfo['userid'] == data.uid && userInfo['nowroom'] == data.roomnum){
					if(userInfo['id'] == data.uid ){
						console.log("[初始化验证成功]--"+data.uid+"---"+data.roomnum+'---'+data.stream);
						//获取验证token
						socket.token   = data.token; 
						socket.sign    = userInfo['sign'];
						socket.roomnum = data.roomnum;
						socket.stream = data.stream;
						socket.nicename = userInfo['user_nicename'];
						socket.uType   = parseInt(userInfo['userType']);
						socket.uid     = data.uid;
						socket.reusing = 0;

						socket.join(data.roomnum);
						sockets[userid] = socket;
						socket.emit('conn',['ok']);
						if( socket.roomnum!=socket.uid && socket.uid >0 ){
							var data_str="{\"msg\":[{\"_method_\":\"SendMsg\",\"action\":\"0\",\"ct\":{\"id\":\""+userInfo['id']+"\",\"user_nicename\":\""+userInfo['user_nicename']+"\",\"avatar\":\""+userInfo['avatar']+"\",\"level\":\""+userInfo['level']+"\"},\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
							process_msg(io,socket.roomnum,data_str);
							if(socket.stream){
								//console.log(res);
								clientRedis.hset('userlist_'+socket.stream,socket.sign,res);
							}
						}						
						 
						sendSystemMsg(socket,"直播内容包含任何低俗、暴露和涉黄内容，账号会被封禁；安全部门会24小时巡查哦～");
						return;
					}else{
						//console.log("[初始化验证失败]");
						socket.disconnect();
					}
				}
			}
			
			socket.emit('conn',['no']);
		});
        
		
	});

	socket.on('broadcast',function(data){



		    if(socket.token != undefined){
		    	var dataObj  = typeof data == 'object'?data:evalJson(data);

		    	console.log(socket.token);
			    var msg      = dataObj['msg'][0]; 
			    var token    = dataObj['token'];
			    var action   = msg['_method_'];
			    var data_str =  typeof data == 'object'?JSON.stringify(data):data;
			    switch(action){
			    	case 'SendMsg':{     //聊天

			    		//console.log(dataObj['msg'][0]);

			    		if(dataObj['msg'][0]['isfilter']){
			    			
			    		}else{
			    			dataObj['msg'][0]['isfilter']=0;
			    		}

			    		if(dataObj['msg'][0]['isfilter']=='0'){
		    				//console.log(dataObj['msg'][0]['isfilter']);
		    				dataObj['msg'][0]['ct']=filter(dataObj['msg'][0]['ct']);
		    				//console.log(dataObj['msg'][0]['ct']);
			    		}

						

						clientRedis.hget( "super",socket.uid,function(error,res){
							if(error) return;
							if(res != null){
								var data_str='{"msg":[{"_method_":"SystemNot","action":"1","ct":"'+ dataObj['msg'][0]['ct'] +'","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}';
								process_msg(io,socket.roomnum,data_str);
		    				}else{
								data_str=JSON.stringify(dataObj);
								clientRedis.hget(socket.roomnum + "shutup",socket.uid,function(error,res){
									if(error) return;
									if(res != null){
										var time = Date.parse(new Date())/1000;
										//console.log(socket.roomnum+'---'+socket.uid+"---now time:" + time + "--shutuptime:" + res +　"= " +(time - parseInt(res)))
										if((time < parseInt(res))){
											var newData  = dataObj;
											newData['retcode'] = '409002';

											socket.emit('broadcastingListen',[JSON.stringify(newData)]);
										}else{//解除禁言
											clientRedis.del(socket.roomnum + "shutup",socket.uid);
											process_msg(io,socket.roomnum,data_str);
										}										
									}else{
										process_msg(io,socket.roomnum,data_str);
									}	
								});
		    				}							
						});
			    		break;
			    	}
			    	case 'SendGift':{    //送礼物
						var gifToken = dataObj['msg'][0]['ct'];
						//console.log('gifToken='+gifToken);
			    		clientRedis.get(gifToken,function(error,res){
			    			if(!error&&res != null){
			    				//console.log(res);
								//console.log('送礼物');
								//console.log([JSON.stringify(dataObj)]);//打印礼物信息
			    				var resObj = evalJson(res);
			    				dataObj['msg'][0]['ct'] = resObj;
								io.sockets.in(socket.roomnum).emit('broadcastingListen',[JSON.stringify(dataObj)]);
								console.log(resObj);
								//var data_str=JSON.stringify(dataObj);
								//process_msg(io,socket.roomnum,data_str);
			    				clientRedis.del(gifToken);
			    			}
			    		});
			    		break;
			    	}
						
					case 'SendBarrage':{    //弹幕
						var barragetoken = dataObj['msg'][0]['ct'];
						//console.log('barragetoken='+barragetoken);
						
			    		clientRedis.get(barragetoken,function(error,res){
			    			if(!error&&res != null){
			    				//console.log(res); 
			    				var resObj = evalJson(filter(res));
			    				dataObj['msg'][0]['ct'] = resObj;
								//console.log(JSON.stringify(dataObj));
								var data_str=JSON.stringify(dataObj);
								process_msg(io,socket.roomnum,data_str);
			    				clientRedis.del(barragetoken);
			    			}	
			    		});
			    		break;
			    	}
			    	case 'SendFly' :{    //飞屏
					     
			    		clientRedis.get(socket.uid + 'SendFly',function(error,res){
			    			if(!error&&res == '1'){
								process_msg(io,socket.roomnum,data_str);
			    				clientRedis.del(socket.uid + 'SendFly');
			    			}else{

			    			}
			    			
			    		});
	                    break;
			    	}

			    	


			    	case 'clearScreen' :{    //清屏

						process_msg(io,socket.roomnum,data_str);
			    				
	                    break;
			    	}


			    	case 'fetch_sofa' :{ //抢座
			    		clientRedis.get(socket.uid + 'fetch_sofa',function(error,res){
			    			if(!error&&res == '1'){
								process_msg(io,socket.roomnum,data_str);
			    				clientRedis.del(socket.uid + 'fetch_sofa');
			    			}else{

			    			}
			    			
			    		});
	                    break;
			    	}
					case 'ConnectVideo' :{ //连麦
						//console.log("连麦");
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
					case 'CloseVideo' :{ //下麦
						//console.log('下麦');
							clientRedis.hget('ShowVideo',socket.roomnum,function(error,res){
								//console.log(res);
								//console.log(socket.uid);
								//console.log(socket.roomnum);
								if(!error && (socket.uid==res || socket.uid==socket.roomnum) ){
									clientRedis.hdel('ShowVideo',socket.roomnum);
									process_msg(io,socket.roomnum,data_str);									
								}							 
							});
							
	                    break;
			    	}
					case 'ShowVideo' :{ //上麦显示
						//console.log("上麦");
						clientRedis.hget('ShowVideo',socket.roomnum,function(error,res){
							 // console.log("上麦："+res);
							if(!error && socket.uid==res){
								// console.log("上麦：广播");
								process_msg(io,socket.roomnum,data_str);									
							}							 
						});
	                    break;
			    	}
			    	case 'SendTietiao' :{ //贴条
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'SendHb' :{      //送红包
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'VodSong' :{     //点歌
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
					case 'light' :{     //点亮
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'SendRedPack':{//发红包
			    		//console.log(data_str);
			    		console.log(dataObj);
			    		if(dataObj.roomnum){
			    			process_msg(io,data.roomnum,data_str);
			    		}else{
			    			process_msg(io,socket.roomnum,data_str);
			    		}
			    		break;
			    	}
			    	case 'giftPK':{//礼物PK
			    		//console.log(data_str);

			    		process_msg(io,socket.roomnum,data_str);
			    		
			    		break;
			    	}

			    	case 'GrabBench':{//抢板凳
			    		process_msg(io,socket.roomnum,data_str);
			    		break;
			    	}

			    	case 'Carouse':{//转盘游戏
			    		process_msg(io,socket.roomnum,data_str);
			    		break;
			    	}

			    	case 'lianmai':{//PC端连麦
			    		console.log(data_str);
			    		process_msg(io,socket.roomnum,data_str);
			    		break;
			    	}

			    	case 'SetBackground' :{//设置背景
			    		if(socket.uType == 50){
							process_msg(io,socket.roomnum,data_str);
			    		}
	                    break;
			    	}
			    	case 'CancelBackground' :{//取消背景
			    		if(socket.uType == 50){
							process_msg(io,socket.roomnum,data_str);
			    	    }
	                    break;
			    	}
			    	case 'AgreeSong' :{//同意点歌
			    		if(socket.uType == 50){
							process_msg(io,socket.roomnum,data_str);
			    	    }
	                    break;
			    	}
			    	case 'SubmitBroadcast' :{//广播
			    		clientRedis.get(socket.uid + 'SubmitBroadcast',function(error,res){
			    			if(!error&&res == '1'){
								process_msg(io,socket.roomnum,data_str);
			    				clientRedis.del(socket.uid + 'SubmitBroadcast');
			    			}
			    		});
	                    break;
			    	}
			    	case 'MoveRoom' :{//转移房间
			    		if(socket.uType == 50 || socket.uType == 40){
							process_msg(io,socket.roomnum,data_str);
			    	    }
	                    break;
			    	}
			    	case 'SetchatPublic' :{//开启关闭公聊
			    		if(socket.uType == 50 || socket.uType == 40){
							process_msg(io,socket.roomnum,data_str);
						}
	                    break;
			    	}
			    	case 'CloseLive' :{//关闭直播
			    		if(socket.uType == 50 || socket.uType == 40){
							process_msg(io,socket.roomnum,data_str);
			    	    }
	                    break;
			    	}
			    	case 'SetBulletin' :{//房间公告
	                    if(socket.uType == 50 || socket.uType == 40){
							process_msg(io,socket.roomnum,data_str);
			    		}
	                    break;
			    	}
			    	case 'NoticeMsg' :{//全站礼物
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'KickUser' :{//踢人
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'ShutUpUser' :{//禁言
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'channel_gap':{//直播间禁言
			    		process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'changeVest':{//更换马甲
			    		process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
					case 'stopLive' :{//超管关播
						clientRedis.hget( "super",socket.uid,function(error,res){
							if(error) return;
							if(res != null){
								process_msg(io,socket.roomnum,'stopplay');								
		    				}							
						});
						break;
			    	}
			    	case 'SendPrvMsg' :{//私聊
						process_msg(io,socket.roomnum,data_str);
	                    break;
			    	}
			    	case 'ResumeUser' :{//恢复发言
			    		if(socket.uType == 50 || socket.uType == 40){
							process_msg(io,socket.roomnum,data_str);
			    	    }
			    	    break;
			    	} 
			    	case 'StartEndLive':{ 
			    		//console.log('开始关闭直播');
			    		if(socket.uType == 50 ){
			    		   socket.broadcast.to(socket.roomnum).emit('broadcastingListen',[data_str]);
			    	    }else{
			    	    	clientRedis.get("LiveAuthority" + socket.uid,function(error,res){
			    	    		if(error) return;
			    	    		if(parseInt(res) == 5 ||parseInt(res) == 1 || parseInt(res) == 2){
		    	    				socket.broadcast.to(socket.roomnum).emit('broadcastingListen',[data_str]);
		    	    			}
			    	    	})
			    	    }
			    	    break;

			    	}
					case 'showuserinfo':{//在线用户
						process_msg(io,socket.roomnum,data_str);
						break;
					}
			    	case 'RowMike':{//排麦
						process_msg(io,socket.roomnum,data_str);
			    		break;
			    	}
			    	case 'OpenGuard':{//购买守护
						process_msg(io,socket.roomnum,data_str);
			    		break;
			    	}
			    	case 'SystemNot':{//系统通知
						process_msg(io,socket.roomnum,data_str);
			    		break;
			    	}
					case 'startGame':{//炸金花游戏
						process_msg(io,socket.roomnum,data_str);
						var action=msg['action'];
						if(action==4)
						{
							var time=msg['time']*1000;
							var gameid=msg['gameid'];
							setTimeout(function() {//定时发送结果
								var token   = msg['token'];
								clientRedis.get(token+"_Game",function(error,res){
									if(!error&&res != null){
										var resObj = JSON.parse(res);
										dataObj['msg'][0]['ct'] = resObj;
										dataObj['msg'][0]['_method_'] = "startGame";
										dataObj['msg'][0]['action']="6";
										var data_str=JSON.stringify(dataObj);
										process_msg(io,socket.roomnum,data_str);
										/* 	clientRedis.del(Jinhuatoken+"_Game");  */
										request(config['WEBADDRESS']+"?service=Game.endGame&liveuid="+socket.uid + "&token=" + socket.token+ "&gameid=" + gameid+"&type=1",function(error, response, body){});
									}	
								});
							}, time);
						}
						break;
					}
					case 'startRotationGame':{//转盘
						process_msg(io,socket.roomnum,data_str);
						var action=msg['action'];
						if(action==4)
						{
							var time=msg['time']*1000;
							var gameid=msg['gameid'];
							setTimeout(function() {//定时发送结果
								var token   = msg['token'];
								clientRedis.get(token+"_Game",function(error,res){
									if(!error&&res != null){
										var resObj = JSON.parse(res);
										dataObj['msg'][0]['ct'] = resObj;
										dataObj['msg'][0]['_method_'] = "startRotationGame";
										dataObj['msg'][0]['action']="6";
										var data_str=JSON.stringify(dataObj);
										process_msg(io,socket.roomnum,data_str);
										/* 	clientRedis.del(token+"_Game");  */
										request(config['WEBADDRESS']+"?service=Game.endGame&liveuid="+socket.uid + "&token=" + socket.token+ "&gameid=" + gameid+"&type=1",function(error, response, body){});
									}	
								});
							}, time);
						}
						break;
					}
					case 'startCattleGame':{//开心牛仔
						process_msg(io,socket.roomnum,data_str);
						var action=msg['action'];
						if(action==4)
						{
							var time=msg['time']*1000;
							var gameid=msg['gameid'];
							setTimeout(function() {//定时发送结果
								var token   = msg['token'];
								clientRedis.get(token+"_Game",function(error,res){
									if(!error&&res != null){
										var resObj = JSON.parse(res);
										dataObj['msg'][0]['ct'] = resObj;
										dataObj['msg'][0]['_method_'] = "startCattleGame";
										dataObj['msg'][0]['action']="6";
										var data_str=JSON.stringify(dataObj);
										process_msg(io,socket.roomnum,data_str);
										/* 	clientRedis.del(token+"_Game");  */
										request(config['WEBADDRESS']+"?service=Game.Cowboy_end&liveuid="+socket.uid + "&token=" + socket.token+ "&gameid=" + gameid+"&type=1",function(error, response, body){});
									}	
								});
							}, time);
						}
						break;
					}
					case 'startLodumaniGame':{//海盗船长
						process_msg(io,socket.roomnum,data_str);
						var action=msg['action'];
						if(action==4)
						{
							var time=msg['time']*1000;
							var gameid=msg['gameid'];
							setTimeout(function() {//定时发送结果
								var token   = msg['token'];
								clientRedis.get(token+"_Game",function(error,res){
									if(!error&&res != null){
										var resObj = JSON.parse(res);
										dataObj['msg'][0]['ct'] = resObj;
										dataObj['msg'][0]['_method_'] = "startLodumaniGame";
										dataObj['msg'][0]['action']="6";
										var data_str=JSON.stringify(dataObj);
										process_msg(io,socket.roomnum,data_str);
										/* 	clientRedis.del(token+"_Game");  */
										request(config['WEBADDRESS']+"?service=Game.Cowboy_end&liveuid="+socket.uid + "&token=" + socket.token+ "&gameid=" + gameid+"&type=1",function(error, response, body){});
									}	
								});
							}, time);
						}
						break;
					}
					case 'startShellGame':{//二八贝
						process_msg(io,socket.roomnum,data_str);
						var action=msg['action'];
						if(action==4)
						{
							var time=msg['time']*1000;
							var gameid=msg['gameid'];
							setTimeout(function() {//定时发送结果
								var token   = msg['token'];
								clientRedis.get(token+"_Game",function(error,res){
									if(!error&&res != null){
										var resObj = JSON.parse(res);
										dataObj['msg'][0]['ct'] = resObj;
										dataObj['msg'][0]['_method_'] = "startShellGame";
										dataObj['msg'][0]['action']="6";
										var data_str=JSON.stringify(dataObj);
										process_msg(io,socket.roomnum,data_str);
										/* 	clientRedis.del(token+"_Game");  */
										request(config['WEBADDRESS']+"?service=Game.Cowboy_end&liveuid="+socket.uid + "&token=" + socket.token+ "&gameid=" + gameid+"&type=1",function(error, response, body){});
									}	
								});
							}, time);
						}
						break;
					}
			    	case 'requestFans':{
							//console.log(socket.stream);
							//console.log(socket.uid);
							request(config['WEBADDRESS']+"?service=Live.getZombie&stream=" + socket.stream+"&uid=" + socket.uid,function(error, response, body){
								//console.log(error);
								//console.log("*****************************");
								//console.log(body);

								//console.log("*****************************");
								//console.log(body);
								if(error) return;
								var res = evalJson(body);
								//console.log(res);
								if( response.statusCode == 200 && res.data.code == 0){
									//console.log('发送了');
									var data_str="{\"msg\":[{\"_method_\":\"requestFans\",\"action\":\"3\",\"ct\": "+ body + ",\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
									process_msg(io,socket.roomnum,data_str);
								}
							});

			    	}
						
			    }
		    }
		    
	});
	
	socket.on('superadminaction',function(data){
		//console.log("监控关闭房间");
    	if(data['token'] == config['TOKEN']){
			process_msg(io,data['roomnum'],'stopplay');
    	}
    });

	/*系统管理员发送红包*/

    socket.on('superRedPacket',function(data){
    	//console.log(data);
    	var dataObj  = typeof data == 'object'?data:evalJson(data);
    	 var data_str =  typeof data == 'object'?JSON.stringify(data):data;
    	//console.log(dataObj);
    	//console.log(dataObj.msg[0]);
    	process_msg(io,dataObj.msg[0].roomnum,data_str);
    });


	socket.on('virtualenterRoom',function(data){ 
		//取用僵尸粉用户信息data.virtualinfo
		//取出哪个房间的，获取房间流名data.stream
		
		if(data['token'] == config['TOKEN']){
			var data_str="{\"msg\":[{\"_method_\":\"SendMsg\",\"action\":\"0\",\"ct\":{\"id\":\""+data['virtualinfo']['id']+"\",\"user_nicename\":\""+data['virtualinfo']['user_nicename']+"\",\"avatar\":\""+data['virtualinfo']['avatar']+"\",\"level\":\""+data['virtualinfo']['level']+"\"},\"msgtype\":\"0\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
			process_msg(io,data['roomnum'],data_str);
		
			//将用户信息加到当前房间的缓存中
			/*if(data.stream){
				clientRedis.hset('userlist_'+data.stream,data.sign,data['virtualinfo']);	
			}*/
		}
				
    });
	/* 系统信息 */
	socket.on('systemadmin',function(data){
    	if(data['token'] == config['TOKEN']){
			//console.log("后台系统信息");
    		io.emit('broadcastingListen',['{"msg":[{"_method_":"SystemNot","action":"1","ct":"'+ data.content +'","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}']);
    	}
    });

    /* 系统直播间广告 */
	socket.on('systemadvertise',function(data){
    	if(data['token'] == config['TOKEN']){
			//console.log("后台系统信息");
			
    		io.emit('broadcastingListen',['{"msg":[{"_method_":"SystemAdvertise","action":"1","ct":"'+ data.content +'","url":"'+data.url+'","msgtype":"4"}],"retcode":"000000","retmsg":"OK"}']);
    	}
    });
	
    //资源释放
	socket.on('disconnect', function() { 
			 numscount--; 
            if(numscount<0){
				numscount=0;
			}

			clientRedis.set("totalNums",numscount);
          			
			if(socket.roomnum ==null || socket.token==null){
				return !1;
			}
				
			d.run(function() {
				/* 连麦 */
				clientRedis.hget('ShowVideo',socket.roomnum,function(error,res){
					if(!error && (socket.uid==res || socket.uid==socket.roomnum) ){
						//console.log("清除 ShowVideo");
						clientRedis.hdel('ShowVideo',socket.roomnum);		
						var data_str="{\"msg\":[{\"_method_\":\"CloseVideo\",\"action\":\"0\",\"msgtype\":\"20\",\"uid\":\""+socket.uid+"\",\"uname\":\""+socket.nicename+"\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
						process_msg(io,socket.roomnum,data_str);
					}							 
				});
				
				
				if(socket.roomnum==socket.uid){
					/* 主播 */ 
					console.log('时间：'+FormatNowDate());
					console.log(socket.reusing);
					if(socket.reusing==0){

						request(config['WEBADDRESS']+"?service=Live.stopRoom&uid="+socket.uid + "&token=" + socket.token+ "&stream=" + socket.stream,function(error, response, body){});
						var data_str='{"retmsg":"ok","retcode":"000000","msg":[{"msgtype":"1","_method_":"StartEndLive","action":"18","ct":"直播关闭"}]}';
						process_msg(io,socket.roomnum,data_str);
					}
				}else{
					/* 观众 */
					clientRedis.hget('userlist_'+socket.stream,socket.sign,function(error,res){
						if(error) return;
						if(res != null){
							var user = JSON.parse(res);
							var data_str="{\"msg\":[{\"_method_\":\"disconnect\",\"action\":\"1\",\"ct\":{\"id\":\""+user['id']+"\",\"user_nicename\":\""+user['user_nicename']+"\",\"avatar\":\""+user['avatar']+"\",\"level\":\""+user['level']+"\"},\"msgtype\":\"0\",\"uid\":\""+socket.uid+"\",\"uname\":\""+user.user_nicename+"\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}";
							process_msg(io,socket.roomnum,data_str);	
						}
						clientRedis.hdel('userlist_'+socket.stream,socket.sign);
					});
					
				}
				console.log(socket.roomnum+"==="+socket.token+"===="+socket.uid+"======"+socket.stream);
				
				socket.leave(socket.roomnum);
				delete io.sockets.sockets[socket.id];
				sockets[socket.uid] = null;
				delete sockets[socket.uid];
				//console.log("离开广播"+socket.uid);

			});
	});

});
function sendSystemMsg(socket,msg){
	socket.emit('broadcastingListen',["{\"msg\":[{\"_method_\":\"SystemNot\",\"action\":\"1\",\"ct\":\""+ msg +"\",\"msgtype\":\"4\"}],\"retcode\":\"000000\",\"retmsg\":\"OK\"}"]);
						
}
function evalJson(data){
	return eval("("+data+")");
}

function process_msg(io,roomnum,data){
	if(!chat_history[roomnum]){
		chat_history[roomnum]=[];
	}
	chat_history[roomnum].push(data);
	chat_interval[roomnum] || (chat_interval[roomnum]=setInterval(function(){
		if(chat_history[roomnum].length>0){
			send_msg(io,roomnum);
		}else{
			clearInterval(chat_interval[roomnum]);
			chat_interval[roomnum]=null;
		}
	},200));
}

function send_msg(io,roomnum){
	var data=chat_history[roomnum].splice(0,chat_history[roomnum].length);
    io.sockets.in(roomnum).emit("broadcastingListen", data);
}

//时间格式化
function FormatNowDate(){
	var mDate = new Date();
	var H = mDate.getHours();
	var i = mDate.getMinutes();
	var s = mDate.getSeconds();
	return H + ':' + i + ':' + s;
}

function filter(str) {
 
  // 多个敏感词，这里直接以数组的形式展示出来
  var arrMg = field;

  //console.log(arrMg);
 
  // 显示的内容--showContent
  var showContent = str;
//console.log(showContent);

 
  // 正则表达式
  // \d 匹配数字 
 
  for (var i = 0; i < arrMg.length; i++) {
 
    // 创建一个正则表达式
    var r = new RegExp(arrMg[i], "ig");
	var re='';
	for(var n=0;n<arrMg[i].length;n++){
		re+='*';
	}
    showContent = showContent.replace(r, re);
  }
  // 显示的内容--showInput
	//console.log(showContent);
	return showContent;
}
