<?php
/**
 * 分库分表的自定义数据库路由配置
 */

//['local'=>'127.0.0.1', 'zhibo-test-ip'=>'10.71.42.70', 'a02-test-ip'=>'103.198.193.43',]
$ip = @$_SERVER['SERVER_ADDR'];
if (in_array($ip, ['127.0.0.1', '10.71.42.70',])) {
    return [
        /**
         * DB数据库服务器集群
         */
        'servers' => [
            'db_appapi' => [                         //服务器标记
                'host' => 'db.xxx.xxx',             //数据库域名
                'name' => 'a02',               //数据库名字
                'user' => 'phonelive',                  //数据库用户名
                'password' => 'tieweishivps',                        //数据库密码
                'port' => '3306',                  //数据库端口
                'charset' => 'UTF8',                  //数据库字符集
            ],
        ],

        /**
         * 自定义路由表
         */
        'tables' => [
            //通用路由
            '__default__' => [
                'prefix' => 'cmf_',
                'key' => 'id',
                'map' => [
                    ['db' => 'db_appapi'],
                ],
            ],

            /**
             * 'demo' => array(                                                //表名
             * 'prefix' => 'pa_',                                         //表名前缀
             * 'key' => 'id',                                              //表主键名
             * 'map' => array(                                             //表路由配置
             * array('db' => 'db_appapi'),                               //单表配置：array('db' => 服务器标记)
             * array('start' => 0, 'end' => 2, 'db' => 'db_appapi'),     //分表配置：array('start' => 开始下标, 'end' => 结束下标, 'db' => 服务器标记)
             * ),
             * ),
             */
        ],
    ];
} else {
    return [
        /**
         * DB数据库服务器集群
         */
        'servers' => [
            'db_appapi' => [                         //服务器标记
                'host' => 'db.xxx.xxx',             //数据库域名
                'name' => 'phonelive',               //数据库名字
                'user' => 'phonelive',                  //数据库用户名
                'password' => 'tieweishivps',                        //数据库密码
                'port' => '3306',                  //数据库端口
                'charset' => 'UTF8',                  //数据库字符集
            ],
        ],

        /**
         * 自定义路由表
         */
        'tables' => [
            //通用路由
            '__default__' => [
                'prefix' => 'cmf_',
                'key' => 'id',
                'map' => [
                    ['db' => 'db_appapi'],
                ],
            ],

            /**
             * 'demo' => array(                                                //表名
             * 'prefix' => 'pa_',                                         //表名前缀
             * 'key' => 'id',                                              //表主键名
             * 'map' => array(                                             //表路由配置
             * array('db' => 'db_appapi'),                               //单表配置：array('db' => 服务器标记)
             * array('start' => 0, 'end' => 2, 'db' => 'db_appapi'),     //分表配置：array('start' => 开始下标, 'end' => 结束下标, 'db' => 服务器标记)
             * ),
             * ),
             */
        ],
    ];
}
