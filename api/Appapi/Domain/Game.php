<?php
class Domain_Game {

	public function record($liveuid,$stream,$token,$action,$time,$result) {
		$rs = array();
		$model = new Model_Game();
		$rs = $model->record($liveuid,$stream,$token,$action,$time,$result);
		return $rs;
	}
	public function endGame($liveuid,$gameid,$type) {
		$rs = array();
		$model = new Model_Game();
		$rs = $model->endGame($liveuid,$gameid,$type);
		return $rs;
  }
	public function gameBet($uid,$gameid,$coin,$action,$grade)
	{
		$rs = array();
		$model = new Model_Game();
		$rs = $model->gameBet($uid,$gameid,$coin,$action,$grade);
		return $rs;
	}
	public function settleGame($uid,$gameid)
	{
		$rs = array();
		$model = new Model_Game();
		$rs = $model->settleGame($uid,$gameid);
		return $rs;
	}
	public function checkGame($liveuid,$stream)
	{
		$rs = array();
		$model = new Model_Game();
		$rs = $model->checkGame($liveuid,$stream);
		return $rs;
	}
}