<?php

class Domain_Live {

	public function createRoom($uid,$data) {
		$rs = array();

		$model = new Model_Live();

		
		$rs = $model->createRoom($uid,$data);
		return $rs;
	}
	
	public function getFansIds($touid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->getFansIds($touid);
		return $rs;
	}
	
	public function changeLive($uid,$stream,$status) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->changeLive($uid,$stream,$status);
		return $rs;
	}

	public function stopRoom($uid,$stream) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->stopRoom($uid,$stream);
		return $rs;
	}
	
	public function stopInfo($stream) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->stopInfo($stream);
		return $rs;
	}
	
	public function checkLive($uid,$liveuid,$stream) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->checkLive($uid,$liveuid,$stream);
		return $rs;
	}
	
	public function roomCharge($uid,$token,$liveuid,$stream) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->roomCharge($uid,$token,$liveuid,$stream);
		return $rs;
	}
	
	public function isZombie($uid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->isZombie($uid);
		return $rs;
	}
	
	public function getZombie($stream,$where) {
        $rs = array();
				
        $model = new Model_Live();
        $rs = $model->getZombie($stream,$where);

        return $rs;
    }	

	public function getPop($touid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->getPop($touid);
		return $rs;
	}

	public function getGiftList() {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->getGiftList();
		return $rs;
	}
	
	public function sendGift($uid,$liveuid,$stream,$giftid,$giftcount,$giftGroupNum,$type) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->sendGift($uid,$liveuid,$stream,$giftid,$giftcount,$giftGroupNum,$type);
		return $rs;
	}

	public function sendBarrage($uid,$liveuid,$stream,$giftid,$giftcount,$content) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->sendBarrage($uid,$liveuid,$stream,$giftid,$giftcount,$content);
		return $rs;
	}
	
	public function setAdmin($liveuid,$touid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->setAdmin($liveuid,$touid);
		return $rs;
	}
	
	public function getAdminList($liveuid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->getAdminList($liveuid);
		return $rs;
	}
	
	public function getUserHome($uid,$touid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->getUserHome($uid,$touid);
		return $rs;
	}

	public function setReport($uid,$touid,$content) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->setReport($uid,$touid,$content);
		return $rs;
	}

	public function getVotes($liveuid) {
		$rs = array();

		$model = new Model_Live();
		$rs = $model->getVotes($liveuid);
		return $rs;
	}
	public function superStopRoom($uid,$token,$liveuid,$type) {
		$rs = array();
		$model = new Model_Live();
		$rs = $model->superStopRoom($uid,$token,$liveuid,$type);
		return $rs;
	}	
	public function getFirstLive() {
		$rs = array();
		$model = new Model_Live();
		$rs = $model->getFirstLive();
		return $rs;
	}	
	public function getChannelLive($p) {
		$rs = array();
		$model = new Model_Live();
		$rs = $model->getChannelLive($p);
		return $rs;
	}

	
	public function checkManager($liveuid,$uid){
		$rs=array();
		$model=new Model_Live();
		$rs=$model->checkManager($liveuid,$uid);
		return $rs;
	}

	public function sendRedPackets($uid,$liveuid,$num,$totalMoney,$title,$type){
		$rs=array();
		$model=new Model_Live();
		$rs=$model->sendRedPackets($uid,$liveuid,$num,$totalMoney,$title,$type);
		return $rs;
	}

	public function robRedPackets($uid,$liveuid,$sendRedUid,$sendtime,$type,$title){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->robRedPackets($uid,$liveuid,$sendRedUid,$sendtime,$type,$title);
		return $rs;
	}

	public function giftGroupLists(){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->giftGroupLists();

		return $rs;
	}

	public function giftPK($uid,$guestID,$effectiveTime,$masterGiftID,$guestGiftID){
		$rs=array();
		$model=new Model_Live();
		$rs=$model->giftPK($uid,$guestID,$effectiveTime,$masterGiftID,$guestGiftID);

		return $rs;

	}

	public function stopPK($uid,$pkID){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->stopPK($uid,$pkID);
		return $rs;
	}


	public function createGrabBench($uid,$effectiveTime,$winNums){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->createGrabBench($uid,$effectiveTime,$winNums);
		return $rs;
	}

	public function grabBench($uid,$liveuid,$grabbenchID){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->grabBench($uid,$liveuid,$grabbenchID);
		return $rs;
	}

	public function stopGrabBench($uid,$grabbenchID){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->stopGrabBench($uid,$grabbenchID);
		return $rs;
	}

	public function getCarouse(){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->getCarouse();
		return $rs;
	}

	public function getBuyGuard($uid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->getBuyGuard($uid);
		return $rs;
	}

	public function buyGuard($uid,$liveuid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->buyGuard($uid,$liveuid);

		return $rs;
	}

	

	public function checkIsGuard($uid,$liveuid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->checkIsGuard($uid,$liveuid);
		return $rs;
	}

	public function checkisSuper($uid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->checkisSuper($uid);
		return $rs;
	}

	public function getVestLists(){
		$model=new Model_Live();
		$rs=$model->getVestLists();
		return $rs;
	}

	

	public function showVestOption($uid,$touid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->showVestOption($uid,$touid);
		return $rs;
	}

	public function changeVest($uid,$touid,$vestid,$roomid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->changeVest($uid,$touid,$vestid,$roomid);
		return $rs;
	}

	public function channelGag($uid,$roomid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->channelGag($uid,$roomid);
		return $rs;
	}

	public function delChannelGap($uid,$roomid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->delChannelGap($uid,$roomid);
		return $rs;
	}


	/*测试*/

	public function getredisInfo($uid){
		$rs=array();

		$model=new Model_Live();
		$rs=$model->getredisInfo($uid);
		return $rs;
	}


	

	
}
