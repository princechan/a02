<?php

class Domain_Common {

	public function getConfigPub() {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->getConfigPub();
		return $rs;
	}
	
	public function getConfigPri() {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->getConfigPri();
		return $rs;
	}
	
	public function checkToken($uid,$token) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->checkToken($uid,$token);
		return $rs;
	}

	public function getUserInfo($uid) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->getUserInfo($uid);
		return $rs;
	}
	
	public function isAttention($uid,$touid) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->isAttention($uid,$touid);
		return $rs;
	}
	
	public function isBlack($uid,$touid) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->isBlack($uid,$touid);
		return $rs;
	}

	public function isAdmin($uid,$liveuid) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->isAdmin($uid,$liveuid);
		return $rs;
	}
	public function getLevel($experience) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->getLevel($experience);
		return $rs;
	}
	
	public function isBan($uid) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->isBan($uid);
		return $rs;
	}
	public function isAuth($uid) {
		$rs = array();
		$model = new Model_Common();
		$rs = $model->isAuth($uid);
		return $rs;
	}
	
	public function LoginBonus($uid,$token){
		$rs = array();
		$model = new Model_Common();
		$rs = $model->LoginBonus($uid,$token);
		return $rs;

	}

	public function getGiftInfo($giftid){
		$rs = array();
		$model = new Model_Common();
		$rs = $model->getGiftInfo($giftid);
		return $rs;
	}

	public function checkIsGuard($uid,$liveuid){
		$rs = array();
		$model = new Model_Common();
		$rs = $model->checkIsGuard($uid,$liveuid);
		return $rs;
	}
	public function islimitIP($uid){
		$rs=array();
		$model=new Model_Common();
		$rs=$model->islimitIP($uid);
		return $rs;
	}

	public function getLiveChatMsg($liveid){
		$rs=array();
		$model=new Model_Common();
		$rs=$model->getLiveChatMsg($liveid);
		return $rs;
	}


	public function getVestID($uid,$liveid){
		$rs=array();
		$model=new Model_Common();
		$rs=$model->getVestID($uid,$liveid);
		return $rs;
	}


	public function getUserIdentity($uid,$touid,$liveid){
		$rs=array();
		$model=new Model_Common();
		$rs=$model->getUserIdentity($uid,$touid,$liveid);
		return $rs;
	}

	public function getVestInfoByID($vestID){
		$rs=array();
		$model=new Model_Common();
		$rs=$model->getVestInfoByID($vestID);
		return $rs;
	}

	public function getPrivateChatByUID($uid,$roomid){
		$rs=array();

		$model=new Model_Common();
		$rs=$model->getPrivateChatByUID($uid,$roomid);
		
		return $rs;
	}


	
}
