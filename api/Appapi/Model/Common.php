<?php

class Model_Common extends PhalApi_Model_NotORM {

    const KEY_PUBLIC_CONFIG  = 'publicConfig';
    const KEY_PRIVATE_CONFIG = 'privateConfig';
    const KEY_EXTEND_USERS   = 'cache:extend:users';

    /**
     * 获取用户拓展数据公用key
     * @return string
     */
    private function getExtUsersKey()
    {
        return self::KEY_EXTEND_USERS;
    }

    /**
     * 写入用户拓展数据
     * @param $uid
     * @param $data
     * @return int
     */
    public function setExtUsers($uid, $data)
    {
        if(!is_array($data)){
            return false;
        }

        $ext_info = $this->getExtUserInfo($uid);
        if($ext_info){
            $data = array_merge($ext_info, $data);
        }
        return DI()->redis->hSet($this->getExtUsersKey(), $uid, json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    /**
     * 判断是否存在
     * @param $uid
     * @return bool
     */
    public function isExistExtUsers($uid)
    {
        return DI()->redis->hExists($this->getExtUsersKey(), $uid);
    }

    /**
     * 获取所有用户拓展数据
     * @return array
     */
    public function getExtUsers()
    {
        return DI()->redis->hGetAll($this->getExtUsersKey());
    }

    /**
     * 获取当个用户拓展数据
     * @param $uid
     * @return mixed|null
     */
    public function getExtUserInfo($uid)
    {
        $ext_info = DI()->redis->hget($this->getExtUsersKey(), $uid);
        return $ext_info ? json_decode($ext_info, true) : null;
    }


	/* 设置缓存 */
	public function setcache($key,$info){
		$config=$this->getConfigPri();
		if($config['cache_switch']!=1){
			return 1;
		}

		DI()->redis->set($key,json_encode($info));
		DI()->redis->setTimeout($key, $config['cache_time']); 

		return 1;
	}	
	/* 设置缓存 可自定义时间*/
	public function setcaches($key,$info,$time){
		DI()->redis->set($key,json_encode($info));
		DI()->redis->setTimeout($key, $time); 
		return 1;
	}
	/* 获取缓存 */
	public function getcache($key){
		$config=$this->getConfigPri();

		if($config['cache_switch']!=1){
			$isexist=false;
		}else{
			$isexist=DI()->redis->Get($key);
		}

		return json_decode($isexist,true);
	}		
	/* 获取缓存 不判断后台设置 */
	public function getcaches($key){

		$isexist=DI()->redis->Get($key);
		
		return json_decode($isexist,true);
	}
	/* 删除缓存 */
	public function delcache($key){
		$isexist=DI()->redis->delete($key);
		return 1;
	}	
	/* 同系统函数 array_column   php版本低于5.5.0 时用  */
	public function array_column2($input, $columnKey, $indexKey = NULL){
		$columnKeyIsNumber = (is_numeric($columnKey)) ? TRUE : FALSE;
		$indexKeyIsNull = (is_null($indexKey)) ? TRUE : FALSE;
		$indexKeyIsNumber = (is_numeric($indexKey)) ? TRUE : FALSE;
		$result = array();
 
		foreach ((array)$input AS $key => $row){ 
			if ($columnKeyIsNumber){
				$tmp = array_slice($row, $columnKey, 1);
				$tmp = (is_array($tmp) && !empty($tmp)) ? current($tmp) : NULL;
			}else{
				$tmp = isset($row[$columnKey]) ? $row[$columnKey] : NULL;
			}
			if (!$indexKeyIsNull){
				if ($indexKeyIsNumber){
					$key = array_slice($row, $indexKey, 1);
					$key = (is_array($key) && ! empty($key)) ? current($key) : NULL;
					$key = is_null($key) ? 0 : $key;
				}else{
					$key = isset($row[$indexKey]) ? $row[$indexKey] : 0;
				}
			}
			$result[$key] = $tmp;
		}
		return $result;
	}
	
	/* 密码检查 */
	public function passcheck($user_pass) {
		$num = preg_match("/^[a-zA-Z]+$/",$user_pass);
		$word = preg_match("/^[0-9]+$/",$user_pass);
		$check = preg_match("/^[a-zA-Z0-9]{6,12}$/",$user_pass);
		if($num || $word ){
			return 2;
		}else if(!$check){
			return 0;
		}		
		return 1;
	}
	
	/* 密码加密 */
	public function setPass($pass){
		$authcode='rCt52pF2cnnKNB3Hkp';
		$pass="###".md5(md5($authcode.$pass));
		return $pass;
	}	
	
	/* 公共配置 */
	public function getConfigPub() {
		$key='getConfigPub';
		$config=$this->getcaches($key);
		$config=false;
		if(!$config){
			$config= DI()->notorm->config
					->select('*')
					->where(" id ='1'")
					->fetchOne();
			$this->setcaches($key,$config,60);
		}
        
		return 	$config;
	}		
	
	/* 私密配置 */
	public function getConfigPri() {

	    self::KEY_PUBLIC_CONFIG;
	    self::KEY_PRIVATE_CONFIG;

		$key='getConfigPri';
		$config=$this->getcaches($key);
		$config=false;
		if(!$config){
			$config= DI()->notorm->config_private
					->select('*')
					->where(" id ='1'")
					->fetchOne();
			$this->setcaches($key,$config,60);
		}
		return 	$config;
	}		
	
	/**
	 * 返回带协议的域名
	 */
	public function get_host(){
		//$host=$_SERVER["HTTP_HOST"];
	//	$protocol=$this->is_ssl()?"https://":"http://";
		//return $protocol.$host;
		$config=$this->getConfigPub();
		return $config['site'];
	}	
	
	/**
	 * 转化数据库保存的文件路径，为可以访问的url
	 */
	public function get_upload_path($file){
		if(strpos($file,"http")===0){
			return $file;
		}else if(strpos($file,"/")===0){
			$filepath= $this->get_host().$file;

			return $filepath;
		}else{
			$space_host= DI()->config->get('app.Qiniu.space_host');
			$filepath=$space_host."/".$file;
			return $filepath;
		}
	}
	
	/* 判断是否关注 */
	public function isAttention($uid,$touid) {
		$isexist=DI()->notorm->users_attention
					->select("*")
					->where('uid=? and touid=?',$uid,$touid)
					->fetchOne();
		if($isexist){
			return  1;
		}else{
			return  0;
		}			 
	}
	/* 是否黑名单 */
	public function isBlack($uid,$touid) {	
		$isexist=DI()->notorm->users_black
				->select("*")
				->where('uid=? and touid=?',$uid,$touid)
				->fetchOne();
		if($isexist){
			return 1;
		}else{
			return 0;					
		}
	}	
	
	/* 判断权限 */
	public function isAdmin($uid,$liveuid) {
		if($uid==$liveuid){//主播
			return 50;
		}
		$isuper=$this->isSuper($uid);

		if($isuper){//超管
			return 60;
		}


		$isexist=DI()->notorm->users_livemanager
					->select("*")
					->where('uid=? and liveuid=?',$uid,$liveuid)
					->fetchOne();
		if($isexist){
			return  40;
		}

		

		
		return  30;



			
	}







	/* 判断账号是否超管 */
	public function isSuper($uid){
		$isexist=DI()->notorm->users_super
					->select("*")
					->where('uid=?',$uid)
					->fetchOne();
		if($isexist){
			return 1;
		}			
		return 0;
	}	
	/* 判断token */
	public function checkToken($uid,$token) {
		$userinfo=$this->getCache("token_".$uid);
		if(!$userinfo){
			$userinfo=DI()->notorm->users
						->select('token,expiretime')
						->where('id = ? and user_type="2"', $uid)
						->fetchOne();	
			$this->setCache("token_".$uid,$userinfo);								
		}

		if($userinfo['token']!=$token || $userinfo['expiretime']<time()){
			return 700;				
		}else{
			return 	0;				
		} 		
	}	
	
	/* 用户基本信息 */
	public function getUserInfo($uid) {
		$info=$this->getCache("userinfo_".$uid);
		if(!$info){
			$info=DI()->notorm->users
					->select('id,user_nicename,user_login,user_pass,avatar,coin,avatar_thumb,sex,signature,consumption,votestotal,province,city,birthday,issuper,isauth,iszombie,livecoin,iswhite,chat_num,chat_frequency,send_url')
					->where('id=? and user_type="2"',$uid)
					->fetchOne();	
			if($info){
				$info['level']=$this->getLevel($info['consumption']);
				$info['avatar']=$this->get_upload_path($info['avatar']);
				$info['avatar_thumb']=$this->get_upload_path($info['avatar_thumb']);

				$info['user_nicename']=(string)$info['user_nicename'];
				$info['signature']=(string)$info['signature'];

				$info['coin']=$this->getApiUserCoin($info['user_login'],$info['user_pass']);

				$this->setCache("userinfo_".$uid,$info);						
			}					
		}
		return 	$info;		
	}


	/* 用户基本信息 */
	/*public function getUserInfo($uid) {
		$info=$this->getCache("userinfo_".$uid);
		if(!$info){
			$info=DI()->notorm->users
					->select('id,user_nicename,avatar,coin,avatar_thumb,sex,signature,consumption,votestotal,province,city,birthday,issuper,isauth,iszombie,livecoin,iswhite,chat_num,chat_frequency,send_url')
					->where('id=? and user_type="2"',$uid)
					->fetchOne();	
			if($info){
				$info['level']=$this->getLevel($info['consumption']);
				$info['avatar']=$this->get_upload_path($info['avatar']);
				$info['avatar_thumb']=$this->get_upload_path($info['avatar_thumb']);

				$info['user_nicename']=(string)$info['user_nicename'];
				$info['signature']=(string)$info['signature'];

				$this->setCache("userinfo_".$uid,$info);						
			}					
		}
		return 	$info;		
	}*/

	
	/* 会员等级 */
	public function getLevel($experience){
		$levelid=1;
		$key='level';
		$level=$this->getCache($key);
		if(!$level){
			$level=DI()->notorm->experlevel
					->select("levelid,level_up")
					->order("level_up asc")
					->fetchAll();
			$this->setCache($key,$level);			 
		}

		foreach($level as $k=>$v){
			if( $v['level_up']>=$experience){
				$levelid=$v['levelid'];
				break;
			}
		}
		return $levelid;
	}
	
	/* 等级区间限额 */
	public function getLevelSection($level){
		$key='experlevel_limit';
		$experlevel_limit=$this->getCache($key);
		if(!$experlevel_limit){
			$experlevel_limit=DI()->notorm->experlevel_limit
						 ->select("withdraw,level_up")
						 ->order("level_up asc")
						 ->fetchAll();
			$this->setCache($key,$experlevel_limit);			 
		}
		
		foreach($experlevel_limit as $k=>$v){
			if($v['level_up']>=$level){
				$withdraw=$v['withdraw'];
				break;
			}
			
		}
		return $withdraw;		 
	}	
	/* 统计 直播 */
	public function getLives($uid) {
		/* 直播中 */
		$count1=DI()->notorm->users_live
				->where('uid=? and islive="1"',$uid)
				->count();
		/* 回放 */
		$count2=DI()->notorm->users_liverecord
					->where('uid=? ',$uid)
					->count();
		return 	$count1+$count2;
	}		
	
	/* 统计 关注 */
	public function getFollows($uid) {
		$count=DI()->notorm->users_attention
				->where('uid=? ',$uid)
				->count();
		return 	$count;
	}			
	
	/* 统计 粉丝 */
	public function getFans($uid) {
		$count=DI()->notorm->users_attention
				->where('touid=? ',$uid)
				->count();
		return 	$count;
	}		
	/**
	*  @desc 根据两点间的经纬度计算距离
	*  @param float $lat 纬度值
	*  @param float $lng 经度值
	*/
	public function getDistance($lat1, $lng1, $lat2, $lng2){
		$earthRadius = 6371000; //近似地球半径 单位 米
		 /*
		   Convert these degrees to radians
		   to work with the formula
		 */

		$lat1 = ($lat1 * pi() ) / 180;
		$lng1 = ($lng1 * pi() ) / 180;

		$lat2 = ($lat2 * pi() ) / 180;
		$lng2 = ($lng2 * pi() ) / 180;


		$calcLongitude = $lng2 - $lng1;
		$calcLatitude = $lat2 - $lat1;
		$stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
		$calculatedDistance = $earthRadius * $stepTwo;
		
		$distance=round($calculatedDistance)/1000;

		return $distance.'km';
	}
	/* 判断账号是否禁用 */
	public function isBan($uid){
		$status=DI()->notorm->users
					->select("user_status")
					->where('id=?',$uid)
					->fetchOne();
		if(!$status || $status['user_status']==0){
			return 0;
		}
		return 1;
	}
	/* 是否认证 */
	public function isAuth($uid){
		$status=DI()->notorm->users_auth
					->select("status")
					->where('uid=?',$uid)
					->fetchOne();
		if(!$status || $status['status']!=1){
			return 0;
		}
		return 1;
	}
	/* 过滤字符 */
	public function filterField($field){
		
		$configpri=$this->getConfigPri();
		
		$sensitive_field=$configpri['sensitive_field'];
		
		$sensitive=explode(",",$sensitive_field);
		$replace=array();
		$preg=array();
		foreach($sensitive as $k=>$v){
			if($v){
				$re='';
				$num=mb_strlen($v);
				for($i=0;$i<$num;$i++){
					$re.='*';
				}
				$replace[$k]=$re;
				$preg[$k]='/'.$v.'/i';
			}else{
				unset($sensitive[$k]);
			}
		}
		
		return preg_replace($preg,$replace,$field);
	}



	/* 时间差计算 */
	public function datetime($time){
		$cha=time()-$time;
		$iz=floor($cha/60);
		$hz=floor($iz/60);
		$dz=floor($hz/24);
		/* 秒 */
		$s=$cha%60;
		/* 分 */
		$i=floor($iz%60);
		/* 时 */
		$h=floor($hz/24);
		/* 天 */
		
		if($cha<60){
			return $cha.'秒前';
		}else if($iz<60){
			return $iz.'分钟前';
		}else if($hz<24){
			return $hz.'小时'.$i.'分钟前';
		}else if($dz<30){
			return $dz.'天前';
		}else{
			return date("Y-m-d",$time);
		}
	}		
	/* 时长格式化 */
	public function getSeconds($cha){		 
		$iz=floor($cha/60);
		$hz=floor($iz/60);
		$dz=floor($hz/24);
		/* 秒 */
		$s=$cha%60;
		/* 分 */
		$i=floor($iz%60);
		/* 时 */
		$h=floor($hz/24);
		/* 天 */
		
		if($cha<60){
			return $cha.'秒';
		}else if($iz<60){
			return $iz.'分'.$s.'秒';
		}else if($hz<24){
			return $hz.'小时'.$i.'分'.$s.'秒';
		}else if($dz<30){
			return $dz.'天'.$h.'小时'.$i.'分'.$s.'秒';
		}
	}	
	
	/* 数字格式化 */
	public function NumberFormat($num){
		if($num<10000){

		}else if($num<1000000){
			$num=round($num/10000,2).'万';
		}else if($num<100000000){
			$num=round($num/10000,1).'万';
		}else if($num<10000000000){
			$num=round($num/100000000,2).'亿';
		}else{
			$num=round($num/100000000,1).'亿';
		}
		return $num;
	}

	/**
	*  @desc 获取推拉流地址
	*  @param string $host 协议，如:http、rtmp
	*  @param string $stream 流名,如有则包含 .flv、.m3u8
	*  @param int $type 类型，0表示播流，1表示推流
	*/
	public function PrivateKeyA($host,$stream,$type){
		$configpri=$this->getConfigPri();
		$cdn_switch=$configpri['cdn_switch'];
		//$cdn_switch=3;
		switch($cdn_switch){
			case '1':
				$url=$this->PrivateKey_ali($host,$stream,$type);
				break;
			case '2':
				$url=$this->PrivateKey_tx($host,$stream,$type);
				break;
			case '3':
				$url=$this->PrivateKey_qn($host,$stream,$type);
				break;
			case '4':
				$url=$this->PrivateKey_ws($host,$stream,$type);
				break;
		}

		
		return $url;
	}
	
	/**
	*  @desc 阿里云直播A类鉴权
	*  @param string $host 协议，如:http、rtmp
	*  @param string $stream 流名,如有则包含 .flv、.m3u8
	*  @param int $type 类型，0表示播流，1表示推流
	*/
	public function PrivateKey_ali($host,$stream,$type){
		$configpri=$this->getConfigPri();
		$key=$configpri['auth_key'];
		if($type==1){
			$domain=$host.'://'.$configpri['push_url'];
			$time=time() +60*60*10;
		}else{
			$domain=$host.'://'.$configpri['pull_url'];
			$time=time() - 60*30 + $configpri['auth_length'];
		}
		
		$filename="/5showcam/".$stream;
		if($key!=''){
			$sstring = $filename."-".$time."-0-0-".$key;
			$md5=md5($sstring);
			$auth_key="auth_key=".$time."-0-0-".$md5;
		}
		if($type==1){
			if($auth_key){
				$auth_key='&'.$auth_key;
			}
			$url=$domain.$filename.'?vhost='.$configpri['pull_url'].$auth_key;
		}else{
			if($auth_key){
				$auth_key='?'.$auth_key;
			}
			$url=$domain.$filename.$auth_key;
		}
		
		return $url;
	}
	
	/**
	*  @desc 腾讯云推拉流地址
	*  @param string $host 协议，如:http、rtmp
	*  @param string $stream 流名,如有则包含 .flv、.m3u8
	*  @param int $type 类型，0表示播流，1表示推流
	*/
	public function PrivateKey_tx($host,$stream,$type){
		$configpri=$this->getConfigPri();
		$bizid=$configpri['tx_bizid'];
		$push_url_key=$configpri['tx_push_key'];
		
		$stream_a=explode('.',$stream);
		$streamKey = $stream_a[0];
		$ext = $stream_a[1];
		
		$live_code = $bizid . "_" .$streamKey;      	
		$now_time = time() + 3*60*60;
		$txTime = dechex($now_time);

		$txSecret = md5($push_url_key . $live_code . $txTime);
		$safe_url = "&txSecret=" .$txSecret."&txTime=" .$txTime;		

		if($type==1){
			//$push_url = "rtmp://" . $bizid . ".livepush2.myqcloud.com/live/" .  $live_code . "?bizid=" . $bizid . "&record=flv" .$safe_url;	可录像
			$url = "rtmp://" . $bizid .".livepush2.myqcloud.com/live/" . $live_code . "?bizid=" . $bizid . "" .$safe_url;	
		}else{
			$url = 'http://'. $bizid .".liveplay.myqcloud.com/live/" . $live_code . ".flv";
		}
		
		return $url;
	}

	/**
	*  @desc 七牛云直播
	*  @param string $host 协议，如:http、rtmp
	*  @param string $stream 流名,如有则包含 .flv、.m3u8
	*  @param int $type 类型，0表示播流，1表示推流
	*/
	public function PrivateKey_qn($host,$stream,$type){
		
		$configpri=$this->getConfigPri();
		$ak=$configpri['qn_ak'];
		$sk=$configpri['qn_sk'];
		$hubName=$configpri['qn_hname'];
		$push=$configpri['qn_push'];
		$pull=$configpri['qn_pull'];
		$stream_a=explode('.',$stream);
		$streamKey = $stream_a[0];
		$ext = $stream_a[1];

		if($type==1){
			$time=time() +60*60*10;
			//RTMP 推流地址
			$url = \Qiniu\Pili\RTMPPublishURL($push, $hubName, $streamKey, $time, $ak, $sk);
		}else{
			if($ext=='flv'){
				$pull=str_replace('pili-live-rtmp','pili-live-hdl',$pull);
				//HDL 直播地址
				$url = \Qiniu\Pili\HDLPlayURL($pull, $hubName, $streamKey);
			}else if($ext=='m3u8'){
				$pull=str_replace('pili-live-rtmp','pili-live-hls',$pull);
				//HLS 直播地址
				$url = \Qiniu\Pili\HLSPlayURL($pull, $hubName, $streamKey);
			}else{
				//RTMP 直播放址
				$url = \Qiniu\Pili\RTMPPlayURL($pull, $hubName, $streamKey);
			}
		}
				
		return $url;
	}
	
	/**
	*  @desc 网宿推拉流
	*  @param string $host 协议，如:http、rtmp
	*  @param string $stream 流名,如有则包含 .flv、.m3u8
	*  @param int $type 类型，0表示播流，1表示推流
	*/
	public function PrivateKey_ws($host,$stream,$type){
		$configpri=$this->getConfigPri();
		if($type==1){
			$domain=$host.'://'.$configpri['ws_push'];
			//$time=time() +60*60*10;
		}else{
			$domain=$host.'://'.$configpri['ws_pull'];
			//$time=time() - 60*30 + $configpri['auth_length'];
		}
		
		$filename="/".$configpri['ws_apn']."/".$stream;

		$url=$domain.$filename;
		
		return $url;
	}
	
	/**
	*  @desc 登录奖励
	*/
	public function LoginBonus($uid,$token){
		$rs=array(
			'bonus_switch'=>'0',
			'bonus_day'=>'0',
			'bonus_list'=>array(),
		);
		$configpri=$this->getConfigPri();
		if(!$configpri['bonus_switch']){
			return $rs;
		}
		$rs['bonus_switch']=$configpri['bonus_switch'];
		$iftoken=$this->checkToken($uid,$token);
		if($iftoken){
			return $iftoken;
		}
		
		/* 获取登录设置 */
		$list=DI()->notorm->loginbonus
					->select("day,coin")
					->fetchAll();
		$rs['bonus_list']=$list;
		$bonus_coin=array();
		foreach($list as $k=>$v){
			$bonus_coin[$v['day']]=$v['coin'];
		}
		
		/* 登录奖励 */
		$userinfo=DI()->notorm->users
					->select("bonus_day,bonus_time")
					->where('id=?',$uid)
					->fetchOne();
		$nowtime=time();
		if($nowtime>$userinfo['bonus_time']){
			//更新
			$bonus_time=strtotime(date("Ymd",$nowtime))+60*60*24;
			$bonus_day=$userinfo['bonus_day'];
			if($bonus_day>6){
				$bonus_day=0;
			}
			$bonus_day++;
			
			$rs['bonus_day']=$bonus_day;
			$coin=$bonus_coin[$bonus_day];
			DI()->notorm->users
				->where('id=?',$uid)
				->update(array("bonus_time"=>$bonus_time,"bonus_day"=>$bonus_day,"coin"=>new NotORM_Literal("coin + {$coin}") ));
			/* 记录 */
			$insert=array("type"=>'income',"action"=>'loginbonus',"uid"=>$uid,"touid"=>$uid,"giftid"=>$bonus_day,"giftcount"=>'0',"totalcoin"=>$coin,"showid"=>'0',"addtime"=>$nowtime );
			$isup=DI()->notorm->users_coinrecord->insert($insert);
		}
		
		return $rs;
		
	}


	/**
	 * 获取礼物信息
	 */
	public function getGiftInfo($giftid){
		$info=DI()->notorm->gift
		->where("id=?",$giftid)
		->fetchOne();
		
			$info['gifticon']=$this->get_upload_path($info['gifticon']);

		return $info;

	}

	/*
	 *判断是否是守护
	 */
	public function checkIsGuard($uid,$liveuid){
		$now=time();
		$rs=array(
			'isGuard'=>'0',
			'guard_level'=>'1',
		);

		$guardinfo=DI()->notorm->users_guard_lists
			->select("*")
			->where("uid=? and liveuid=? and effectivetime >?",$uid,$liveuid,$now)
			->fetchOne();
		if($guardinfo){
			$rs['isGuard']='1';
			if($guardinfo['level_endtime']<$now){

				$cha=ceil( ($now-$guardinfo['level_endtime'])/30 );
				$guard_level=$guardinfo['guard_level']+$cha;

				$level_endtime=$guardinfo['level_endtime']+60*60*24*30*$cha;

				DI()->notorm->users_guard_lists->where("uid=? and liveuid=? ",$uid,$liveuid)->update( array('guard_level'=>$guard_level,'level_endtime'=>$level_endtime) );

				$rs['guard_level']=(string)$guard_level;

			}else{
				$rs['guard_level']=(string)$guardinfo['guard_level'];
			}
		}


		return $rs;
	}

	/**
	 * 通过三方接口获取用户余额
	 */

	public function getApiUserCoin($user_login,$user_pass){

		$data=array(
			'username'=>$user_login,
			'password'=>$user_pass,
		);

		$url = self::getApiHost() . '/api-get-zhibo-balance.htm';
		$response=$this->curl_post($data,$url);

		$response=json_decode($response,1);
		
		if($response['status']==0){
			$coin=$response['data']['balance'];
		}else{
			return 0;//查询失败
		}

		return $coin;
	}

	/**
	 * 通过三方接口扣除用户消费
	 * 
	 */
	public function removeApiUserCoin($user_login,$user_pass,$total){

		
		$data=array(
			'username'=>$user_login,
			'password'=>$user_pass,
			'amount'=>$total,
		);

		//$url='http://api.k8531.com/api-transfer-zhibo-balance.htm';
		$url = self::getApiHost() . '/api-transfer-zhibo-balance.htm';
		$response=$this->curl_post($data,$url);
		$response=json_decode($response,1);


		if($response['status']==-3){ //余额不足
			
			return 1501;
		}else if($response['status']==1){//操作失败

			return 1502;
		}else if($response['status']==999){//操作异常

			return 1503;
		}




	}

    private static function getApiHost()
    {
        return ENV_DEV ? 'http://www.a02phpapi.com' : 'http://api.k8531.com';
    }

    function curl_post($curlPost,$url){


		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,  false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,  false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
		$return_str = curl_exec($curl);
		curl_close($curl);
		return $return_str;
	}
	
	function curl_get($url){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,  false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,  false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		$return_str = curl_exec($curl);
		curl_close($curl);
		return $return_str;
	}

	/*判断用户是否被封禁IP*/
	
	function islimitIP($uid){

		$count=DI()->notorm->users_disable_ips
					->select("id")
					->where('uid=?',$uid)
					->count('id');

		if($count>0){//有可能当前IP被封禁
			//获取当前的IP
			$currentIP=$_SERVER['REMOTE_ADDR'];

			
			$lists=DI()->notorm->users_disable_ips
				->where("uid={$uid}")
				->fetchAll();

			$isLimitIP=0;

			foreach ($lists as $k => $v) {
				if($currentIP==$v['begin_ip']){//符合单独IP被限制
					$isLimitIP=1;
					break;
				}


				$info=DI()->notorm->users_disable_ips
					->where("uid={$uid} and inet_aton('{$currentIP}') between inet_aton('{$v["begin_ip"]}') and inet_aton('{$v["end_ip"]}')")
					->count();

				$count1=intval($info);
				if($count1>0){
					$isLimitIP=1;
					break;
				}
			}


			if($isLimitIP==1){
				return 1;//被限制IP
			}

		}else{
			return 0;//未被限制IP
		}
		
		return 1;//被限制IP
	}

	/*获取直播间设置的发言字数和频率*/

	public function getLiveChatMsg($liveid){
		
		$arr=array();

		$info=DI()->notorm->users_live
		->where("uid={$liveid} and islive=1")
		->fetchOne();


		if($info){
			$arr['chat_num']=$info['chat_num'];
			$arr['chat_frequency']=$info['chat_frequency'];
		}else{
			$arr['chat_num']="";
			$arr['chat_frequency']="";
		}

		return $arr;
	}

	/*根据用户id获取马甲信息*/
	public function getVestID($uid,$liveuid){

		$vestID="1";

		$userinfo=DI()->notorm->users
		->select("vest_id")
		->where("id=?",$uid)
		->fetchOne();

		
		

		if($userinfo['vest_id']>1){ //紫马或者黑马

			$vestID=$userinfo['vest_id'];

		}else{
			//从房间马甲记录表中查询
			$vestInfo=DI()->notorm->users_vest_lists
			->where("uid=? and liveid=?",$uid,$liveuid)
			->fetchOne();


			if($vestInfo){

				$vestID=$vestInfo['vestid'];

			}else{

				$vestID="1";
			}
		}


		return $vestID;

	}




	public  function getUserIdentity($uid,$touid,$liveid){

		//11：白马 12：绿马 13：蓝马 14：黄马 15：橙马 16：紫马 17：黑马 18：主播 19 超管

		$rs=array(
			'uidIdentity'=>"0",
			'touidIdentity'=>"0",
		);
		$info=DI()->notorm->users
		->select("issuper,vest_id")
		->where("id=?",$uid)
		->fetchOne();

		if($info['issuper']==1){

			$rs['uidIdentity']="19";//超管

		}else if($uid==$liveid){

			$rs['uidIdentity']="18";//主播

		}else{

			if($info['vest_id']>1){
				if($info['vest_id']==7){//黑马
					$rs['uidIdentity']="17";
				}

				if($info['vest_id']==6){//紫马
					$rs['uidIdentity']="16";
				}
			}else{

				$vestInfo=DI()->notorm->users_vest_lists
				->where("uid=? and liveid=?",$uid,$liveid)
				->fetchOne();

				if($vestInfo){
					switch($vestInfo['vestid']){
						case 2:
						$rs['uidIdentity']="12";//绿马
						break;
						case 3:
						$rs['uidIdentity']="13";//蓝马
						break;
						case 4:
						$rs['uidIdentity']="14";//黄马
						break;
						case 5:
						$rs['uidIdentity']="15";//橙马
						break;
						default:
						$rs['uidIdentity']="11";//白马

					}
				}else{
					$rs['uidIdentity']="11";//默认白马
				}

			}
		}

		$info1=DI()->notorm->users
		->select("issuper,vest_id")
		->where("id=?",$touid)
		->fetchOne();





		if($info1['issuper']==1){
			$rs['touidIdentity']="19";//超管
		}else if($touid==$liveid){
			$rs['touidIdentity']="18";//主播
		}else{

			if($info1['vest_id']>1){
				if($info1['vest_id']==7){//黑马
					$rs['touidIdentity']="17";
				}

				if($info1['vest_id']==6){//紫马
					$rs['touidIdentity']="16";
				}
			}else{

				$vestInfo=DI()->notorm->users_vest_lists
				->where("uid=? and liveid=?",$touid,$liveid)
				->fetchOne();

				if($vestInfo){
					switch($vestInfo['vestid']){
						case 2:
						$rs['touidIdentity']="12";//绿马
						break;
						case 3:
						$rs['touidIdentity']="13";//蓝马
						break;
						case 4:
						$rs['touidIdentity']="14";//黄马
						break;
						case 5:
						$rs['touidIdentity']="15";//橙马
						break;
						default:
						$rs['touidIdentity']="11";//白马

					}
				}else{
					$rs['touidIdentity']="11";//默认白马
				}



			}	
		}


		return $rs;
	}


	/*根据马甲ID获取马甲信息*/

	public function getVestInfoByID($vestID){

		$info=DI()->notorm->users_vest
		->where("id=?",$vestID)
		->fetchOne();
		

		return $info;

	}

	/*根据用户id获取私聊权限*/

	public function getPrivateChatByUID($uid,$roomid){

		
		

		$userInfo=DI()->notorm->users->where("id=?",$uid)->fetchOne();

		

		if($userInfo){

			//判断用户是否是超管
			if($userInfo['issuper']==1){

				return 0;

			}else if($uid==$roomid){//主播

				return 0;

			}else{

				if($userInfo['vest_id']>1){//紫马或黑马

					$vestInfo=DI()->notorm->users_vest
					->where("id=?",$userInfo['vest_id'])
					->fetchOne();

					if($vestInfo){
						if($vestInfo['private_chat']==1){
							return 0;
						}else{
							return 1001;
						}
					}else{
						return 1001;
					}

				}else{//从房间马甲列表查找

					$vestListInfo=DI()->notorm->users_vest_lists
					->where("uid=? and liveid=?",$uid,$roomid)
					->fetchOne();

					if($vestListInfo){
						$vestInfo=DI()->notorm->users_vest->where("id=?",$vestListInfo['vestid'])->fetchOne();
						if($vestInfo['private_chat']==1){
							return 0;
						}else{
							return 1001;
						}
					}else{
						return 1001;
					}
				}
			}
		}else{
			return 1001;
		}
	}

    /*
     * 多维数组指定多字段排序
     * example Func:;multiaArraySort($arr, 'id', SORT_DESC, 'num', SORT_DESC);
     * 排序：SORT_ASC升序 , SORT_DESC降序
     * @return array
     */
    public function multiaArraySort()
    {
        $funcArgs = func_get_args();
        if(empty($funcArgs)){
            return null;
        }
        $arr = array_shift($funcArgs);
        if(!is_array($arr)){
            return false;
        }
        foreach($funcArgs as $key => $value){
            if(is_string($value)){
                $tempArr = array();
                foreach($arr as $k=> $v){
                    $tempArr[$k] = $v[$value];
                }
                $funcArgs[$key] = $tempArr;
            }
        }
        $funcArgs[] = &$arr;
        call_user_func_array('array_multisort', $funcArgs);
        return array_pop($funcArgs);
    }

	
	
	
}
