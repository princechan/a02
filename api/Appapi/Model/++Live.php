<?php

class Model_Live extends Model_Common {
	/* 创建房间 */
	public function createRoom($uid,$data) {
		$isexist=DI()->notorm->users_live
					->select("uid")
					->where('uid=?',$uid)
					->fetchOne();
		if($isexist){
			/* 更新 */
			$rs=DI()->notorm->users_live->where('uid = ?', $uid)->update($data);
		}else{
			/* 加入 */
			$rs=DI()->notorm->users_live->insert($data);
		}
		if(!$rs){
			return $rs;
		}
		return 1;
	}
	
	/* 主播粉丝 */
    public function getFansIds($touid) {
		$fansids=DI()->notorm->users_attention
					->select("uid")
					->where('touid=?',$touid)
					->fetchAll();
        return $fansids;
    }	
	
	/* 修改直播状态 */
	public function changeLive($uid,$stream,$status){

		if($status==1){
			return DI()->notorm->users_live
					->where('uid=? and stream=?',$uid,$stream)
					->update(array("islive"=>1));
		}else{
			$this->stopRoom($uid,$stream);
			return 1;
		}
	}
	
	/* 关播 */
	public function stopRoom($uid,$stream) {

		$info=DI()->notorm->users_live
				->select("uid,showid,starttime,title,province,city,stream,lng,lat,type,type_val")
				->where('uid=? and stream=? and islive="1"',$uid,$stream)
				->fetchOne();
		if($info){
			DI()->notorm->users_live
				->where('uid=?',$uid)
				->delete();
			$nowtime=time();
			$info['endtime']=$nowtime;
			$stream2=explode('_',$stream);
			$starttime=$stream2[1];
			$votes=DI()->notorm->users_coinrecord
				->where('touid=? and showid=?',$uid,$starttime)
				->sum('totalcoin');
			$info['votes']=0;
			if($votes){
				$info['votes']=$votes;
			}
			$nums=DI()->redis->hlen('userlist_'.$stream);			
			DI()->redis->hDel("livelist",$uid);
			DI()->redis->delete($uid.'_zombie');
			DI()->redis->delete($uid.'_zombie_uid');
			DI()->redis->delete('attention_'.$uid);
			DI()->redis->delete('userlist_'.$stream);
			$game=DI()->notorm->game
				->select("*")
				->where('stream=? and liveuid=? and state=?',$stream,$uid,"0")
				->fetchOne();
			$total=array();
			if($game)
			{
				$coin="coin_".$game['result'];
				$sql = "select uid,sum(".$coin.") as gamecoin from cmf_users_gamerecord where gameid=:gameid group by uid";
				$params = array(':gameid' => $game['id']);   
				$total=DI()->notorm->user->queryAll($sql, $params);
				foreach( $total as $k=>$v){
					DI()->notorm->users
						->where('id = ?', $v['uid'])
						->update(array('coin' => new NotORM_Literal("coin + {$v['gamecoin']}")));
				}
				DI()->notorm->game
					->where('id = ? and liveuid =?', $game['id'],$uid)
					->update(array('state' =>'3','endtime' => time() ) );
				$brandToken=$stream."_".$game["action"]."_".$game['starttime']."_Game";
				DI()->redis->delete($brandToken);
			}
			$info['nums']=$nums;			
			$result=DI()->notorm->users_liverecord->insert($info);	
		}					
		return 1;
	}
	/* 关播信息 */
	public function stopInfo($stream){
		
		$rs=array(
			'nums'=>0,
			'length'=>0,
			'votes'=>0,
		);
		
		$stream2=explode('_',$stream);
		$liveuid=$stream2[0];
		$starttime=$stream2[1];
		$liveinfo=DI()->notorm->users_liverecord
					->select("starttime,endtime,nums,votes")
					->where('uid=? and starttime=?',$liveuid,$starttime)
					->fetchOne();
		if($liveinfo){
			$rs['length']=$this->getSeconds($liveinfo['endtime'] - $liveinfo['starttime']);
			$rs['nums']=$liveinfo['nums'];
		}
		if($liveinfo['votes']){
			$rs['votes']=$liveinfo['votes'];
		}
		return $rs;
	}
	
	/* 直播状态 */
	public function checkLive($uid,$liveuid,$stream){
		$islive=DI()->notorm->users_live
					->select("islive,type,type_val,starttime")
					->where('uid=? and stream=?',$liveuid,$stream)
					->fetchOne();
					
		if(!$islive || $islive['islive']==0){
			return 1005;
		}
		$rs['type']=$islive['type'];
		$rs['type_msg']='';
		
		if($islive['type']==1){
			$rs['type_msg']=md5($islive['type_val']);
		}else if($islive['type']==2){
			$rs['type_msg']='本房间为收费房间，需支付'.$islive['type_val'].'钻石';
			$isexist=DI()->notorm->users_coinrecord
						->select('id')
						->where('uid=? and touid=? and showid=? and action="roomcharge" and type="expend"',$uid,$liveuid,$islive['starttime'])
						->fetchOne();
			if($isexist){
				$rs['type']='0';
				$rs['type_msg']='';
			}
		}else if($islive['type']==3){
			$rs['type_msg']='本房间为计时房间，每分钟支付需支付'.$islive['type_val'].'钻石';
		}
		
		return $rs;
		
	}
	/* 房间扣费 */
	public function roomCharge($uid,$token,$liveuid,$stream){
		$islive=DI()->notorm->users_live
					->select("islive,type,type_val,starttime")
					->where('uid=? and stream=?',$liveuid,$stream)
					->fetchOne();
		if(!$islive || $islive['islive']==0){
			return 1005;
		}
		
		if($islive['type']==0 || $islive['type']==1 ){
			return 1006;
		}
		
		$userinfo=DI()->notorm->users
					->select("token,expiretime,coin")
					->where('id=?',$uid)
					->fetchOne();
		if($userinfo['token']!=$token || $userinfo['expiretime']<time()){
			return 700;				
		}
		
		$total=$islive['type_val'];
		if($total<=0){
			return 1007;
		}
		if($userinfo['coin'] < $total){
			return 1008;
		}
		$action='roomcharge';
		if($islive['type']==2){
			$action='roomcharge';
		}
		
		$giftid=0;
		$giftcount=0;
		$showid=$islive['starttime'];
		$addtime=time();
		/* 更新用户余额 消费 */
		DI()->notorm->users
				->where('id = ?', $uid)
				->update(array('coin' => new NotORM_Literal("coin - {$total}"),'consumption' => new NotORM_Literal("consumption + {$total}")) );

		/* 更新直播 映票 累计映票 */
		DI()->notorm->users
				->where('id = ?', $liveuid)
				->update( array('votes' => new NotORM_Literal("votes + {$total}"),'votestotal' => new NotORM_Literal("votestotal + {$total}") ));

		/* 更新直播 映票 累计映票 */
		DI()->notorm->users_coinrecord
				->insert(array("type"=>'expend',"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime ));	
				
		$userinfo2=DI()->notorm->users
					->select('coin')
					->where('id = ?', $uid)
					->fetchOne();	
		$rs['coin']=$userinfo2['coin'];
		return $rs;
		
	}
	
	/* 判断是否僵尸粉 */
	public function isZombie($uid) {
        $userinfo=DI()->notorm->users
					->select("iszombie")
					->where("id='{$uid}'")
					->fetchOne();
		
		return $userinfo['iszombie'];				
    }
	
	/* 僵尸粉 */
    public function getZombie($stream,$where) {
		$ids= DI()->notorm->users_zombie
            ->select('uid')
            ->where("uid not in ({$where})")
			->limit(0,2)
            ->fetchAll();	

		$info=array();

		if($ids){
			$ids2=$this->array_column2($ids,'uid');
			$ids=implode(",",$ids2);
			
			$stream2=explode('_',$stream);
			$showid=$stream2[1];

			$info= DI()->notorm->users
				->select('id,user_nicename,avatar,sex,consumption,city')
				->where("id in ({$ids}) ")
				->fetchAll();	
			foreach( $info as $k=>$v){
				$v['avatar']=$this->get_upload_path($v['avatar']);
				$info[$k]['avatar']=$v['avatar'];
				$level=$this->getLevel($v['consumption']);	
				$v['level']=$level;						
				$info[$k]['level']=$level;						
				$sign = md5($showid.'_'.$v['id']);		
				DI()->redis -> hSet('userlist_'.$stream,$sign,json_encode($v));					
			}		
				 
			$num=count($info);
		}				
		return 	$info;		
    }		

	
	/* 弹窗 */
	public function getPop($touid){
		$info=$this->getUserInfo($touid);
		if(!$info){
			return $info;
		}
		$info['follows']=$this->getFollows($touid);
		$info['fans']=$this->getFans($touid);
		
		$info['consumption']=$this->NumberFormat($info['consumption']);
		$info['votestotal']=$this->NumberFormat($info['votestotal']);
		$info['follows']=$this->NumberFormat($info['follows']);
		$info['fans']=$this->NumberFormat($info['fans']);
		unset($info['province']);
		unset($info['birthday']);
		unset($info['issuper']);
		return $info;
	}
	
	/* 礼物列表 */
	public function getGiftList(){

		$rs=DI()->notorm->gift
			->select("id,type,giftname,needcoin,gifticon")
			->order("orderno asc")
			->fetchAll();
		foreach($rs as $k=>$v){
			$rs[$k]['gifticon']=$this->get_upload_path($v['gifticon']);
		}
		
		return $rs;
	}
	
	/* 赠送礼物 */
	public function sendGift($uid,$liveuid,$stream,$giftid,$giftcount,$giftGroupNum) {

		$userinfo=DI()->notorm->users
					->select('coin')
					->where('id = ?', $uid)
					->fetchOne();	

			/* 礼物信息 */
		$giftinfo=DI()->notorm->gift
					->select("giftname,gifticon,needcoin")
					->where('id=?',$giftid)
					->fetchOne();
		if(!$giftinfo){
			/* 礼物信息不存在 */
			return 1002;
		}							 
				
		$total= $giftinfo['needcoin']*$giftcount*$giftGroupNum;
		 
		$addtime=time();
		$type='expend';
		$action='sendgift';
		if($userinfo['coin'] < $total){
			/* 余额不足 */
			return 1001;
		}	




		/*******判断中奖礼物start********/


		//判断该礼物是不是中奖礼物
		
		if(intval($giftinfo['is_win_gift'])==1){

			//获取后台配置信息
			$configPub=$this->getConfigPub();
			//获取后台配置的中奖几率
			$gift_win_percent=$configPub['gift_win_percent'];
			//获取中奖倍率
			$gift_win_multiple=$configPub['gift_win_multiple'];
			

			if($gift_win_percent>=100){
				$gift_win_percent=100;
			}else if($gift_win_percent<0){
				$gift_win_percent=0;
			}

			if($gift_win_percent>0 && $gift_win_percent<100){

				$randNum=rand(1,99);
				if($randNum<=$gift_win_percent){//中奖了
					//计算中奖的金额
					$winCoin=$gift_win_multiple*$total;
					$lastCoin=$total-$winCoin;

					//向中奖纪录表里添加数据
					$data=array(
						'uid'=>$uid,
						'liveuid'=>$liveuid,
						'stream'=>$stream,
						'giftid'=>$giftid,
						'gift_coin'=>intval($giftinfo['needcoin']),
						'gift_win_multiple'=>floatval($gift_win_multiple),
						'win_coin'=>$winCoin,
						'addtime'=>time()
					);

					
					
					DI()->notorm->users_sendgift_win->insert($data);
				}

			}else if($gift_win_percent==100){//直接中奖
				
				//计算中奖的金额
				$winCoin=$gift_win_multiple*$total;
				$lastCoin=$total-$winCoin;

				//向中奖纪录表里添加数据
					$data=array(
						'uid'=>$uid,
						'liveuid'=>$liveuid,
						'stream'=>$stream,
						'giftid'=>$giftid,
						'gift_coin'=>intval($giftinfo['needcoin']),
						'gift_win_multiple'=>floatval($gift_win_multiple),
						'win_coin'=>$winCoin,
						'addtime'=>time()
					);

					
					
					DI()->notorm->users_sendgift_win->insert($data);
			}


			if(is_null($lastCoin)){
				$lastCoin=0;
			}

			

		}
		
		/*******判断中奖礼物end********/


	

		/* 更新用户余额 消费 */
		$isuid =DI()->notorm->users
				->where('id = ?', $uid)
				->update(array('coin' => new NotORM_Literal("coin - {$total}"),'consumption' => new NotORM_Literal("consumption + {$total}") ) );

		/* 更新直播 魅力值 累计魅力值 */
		$istouid =DI()->notorm->users
					->where('id = ?', $liveuid)
					->update( array('votes' => new NotORM_Literal("votes + {$total}"),'votestotal' => new NotORM_Literal("votestotal + {$total}") ));
		
		$stream2=explode('_',$stream);
		$showid=$stream2[1];

		/* 写入记录 或更新 */
		/* $unique=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"showid"=>$showid);
		$insert=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime );
		$update= array('giftcount' => new NotORM_Literal("giftcount + {$giftcount}"),'totalcoin' => new NotORM_Literal("totalcoin + {$total}"));

		$isexit=DI()->notorm->users_coinrecord
				->select("id")
				->where($unique)
				->fetchOne();
		if($isexit){
			$isup=DI()->notorm->users_coinrecord->where('id=?',$isexit['id'])->update($update);
		}else{
			$isup=DI()->notorm->users_coinrecord->insert($insert);
		}	 */		
		$insert=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime );
		$isup=DI()->notorm->users_coinrecord->insert($insert);

		$userinfo2 =DI()->notorm->users
				->select('consumption,coin')
				->where('id = ?', $uid)
				->fetchOne();	
			 
		$level=$this->getLevel($userinfo2['consumption']);			
		
		/* 清除缓存 */
		$this->delCache("userinfo_".$uid); 
		$this->delCache("userinfo_".$liveuid); 
	
		$votestotal=$this->getVotes($liveuid);
		
		$gifttoken=md5(md5($action.$uid.$liveuid.$giftid.$giftcount.$total.$showid.$addtime.rand(100,999)));
		
		$result=array("uid"=>$uid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"giftname"=>$giftinfo['giftname'],"gifticon"=>$this->get_upload_path($giftinfo['gifticon']),"level"=>$level,"coin"=>$userinfo2['coin'],"votestotal"=>$votestotal,"gifttoken"=>$gifttoken);
					
		return $result;
	}		
	
	/* 发送弹幕 */
	public function sendBarrage($uid,$liveuid,$stream,$giftid,$giftcount,$content) {

		$userinfo=DI()->notorm->users
					->select('coin')
					->where('id = ?', $uid)
					->fetchOne();	
		$configpri=$this->getConfigPri();
					 
		$giftinfo=array(
			"giftname"=>'弹幕',
			"gifticon"=>'',
			"needcoin"=>$configpri['barrage_fee'],
		);		
		
		$total= $giftinfo['needcoin']*$giftcount;
		 
		$addtime=time();
		$type='expend';
		$action='sendbarrage';
		if($userinfo['coin'] < $total){
			/* 余额不足 */
			return 1001;
		}		

		/* 更新用户余额 消费 */
		$isuid =DI()->notorm->users
				->where('id = ?', $uid)
				->update(array('coin' => new NotORM_Literal("coin - {$total}"),'consumption' => new NotORM_Literal("consumption + {$total}") ) );

		/* 更新直播 魅力值 累计魅力值 */
		$istouid =DI()->notorm->users
				->where('id = ?', $liveuid)
				->update( array('votes' => new NotORM_Literal("votes + {$total}"),'votestotal' => new NotORM_Literal("votestotal + {$total}") ));
				
		$stream2=explode('_',$stream);
		$showid=$stream2[1];

		/* 写入记录 或更新 */
		/* $unique=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"showid"=>$showid);
		$insert=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime );
		$update= array('giftcount' => new NotORM_Literal("giftcount + {$giftcount}"),'totalcoin' => new NotORM_Literal("totalcoin + {$total}"));

		
		$isexit=DI()->notorm->users_coinrecord
					->select("id")
					->where($unique)
					->fetchOne();
		if($isexit){
			$isup=DI()->notorm->users_coinrecord->where('id=?',$isexit['id'])->update($update);
		}else{
			$isup=DI()->notorm->users_coinrecord->insert($insert);
		} */
		$insert=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime );
		$isup=DI()->notorm->users_coinrecord->insert($insert);
					 
		$userinfo2 =DI()->notorm->users
				->select('consumption,coin')
				->where('id = ?', $uid)
				->fetchOne();	
			 
		$level=$this->getLevel($userinfo2['consumption']);			
		
		/* 清除缓存 */
		$this->delCache("userinfo_".$uid); 
		$this->delCache("userinfo_".$liveuid); 
		
		$votestotal=$this->getVotes($liveuid);
		
		$barragetoken=md5(md5($action.$uid.$liveuid.$giftid.$giftcount.$total.$showid.$addtime.rand(100,999)));
		 
		$result=array("uid"=>$uid,"content"=>$content,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"giftname"=>$giftinfo['giftname'],"gifticon"=>$giftinfo['gifticon'],"level"=>$level,"coin"=>$userinfo2['coin'],"votestotal"=>$votestotal,"barragetoken"=>$barragetoken);
		
		return $result;
	}			
	
	/* 设置/取消 管理员 */
	public function setAdmin($liveuid,$touid){
					
		$isexist=DI()->notorm->users_livemanager
					->select("*")
					->where('uid=? and  liveuid=?',$touid,$liveuid)
					->fetchOne();			
		if(!$isexist){
			$count =DI()->notorm->users_livemanager
						->where('liveuid=?',$liveuid)
						->count();	
			if($count>=5){
				return 1004;
			}		
			$rs=DI()->notorm->users_livemanager
					->insert(array("uid"=>$touid,"liveuid"=>$liveuid) );	
			if($rs!==false){
				return 1;
			}else{
				return 1003;
			}				
			
		}else{
			$rs=DI()->notorm->users_livemanager
				->where('uid=? and  liveuid=?',$touid,$liveuid)
				->delete();		
			if($rs!==false){
				return 0;
			}else{
				return 1003;
			}						
		}
	}
	
	/* 管理员列表 */
	public function getAdminList($liveuid){
		$rs=DI()->notorm->users_livemanager
						->select("uid")
						->where('liveuid=?',$liveuid)
						->fetchAll();	
		foreach($rs as $k=>$v){
			$rs[$k]=$this->getUserInfo($v['uid']);
		}				
		return $rs;
	}
	
	/* 举报 */
	public function setReport($uid,$touid,$content){
		return  DI()->notorm->users_report
				->insert(array("uid"=>$uid,"touid"=>$touid,'content'=>$content,'addtime'=>time() ) );	
	}
	
	/* 主播总映票 */
	public function getVotes($liveuid){
		$userinfo=DI()->notorm->users
					->select("votestotal")
					->where('id=?',$liveuid)
					->fetchOne();	
		return $userinfo['votestotal'];					
	}
	
	/* 超管关闭直播间 */
	public function superStopRoom($uid,$token,$liveuid,$type){
		
		$userinfo=DI()->notorm->users
					->select("token,expiretime,issuper")
					->where('id=? ',$uid)
					->fetchOne();
		if($userinfo['token']!=$token || $userinfo['expiretime']<time()){
			return 700;				
		} 	
		
		if($userinfo['issuper']==0){
			return 1001;
		}
		
		if($type==1){
			/* 关闭并禁用 */
			DI()->notorm->users->where('id=? ',$liveuid)->update(array('user_status'=>0));
		}
		
	
		$info=DI()->notorm->users_live
				->select("uid,showid,starttime,title,province,city,stream,lng,lat,type,type_val")
				->where('uid=? and islive="1"',$liveuid)
				->fetchOne();
		if($info){
			$nowtime=time();
			$stream=$info['stream'];
			$info['endtime']=$nowtime;
			
			$nums=DI()->redis->hlen('userlist_'.$stream);
			DI()->redis->hDel("livelist",$liveuid);
			DI()->redis->delete($liveuid.'_zombie');
			DI()->redis->delete($liveuid.'_zombie_uid');
			DI()->redis->delete('attention_'.$liveuid);
			DI()->redis->delete('userlist_'.$stream);

			$info['nums']=$nums;			
			$result=DI()->notorm->users_liverecord->insert($info);	
		}
		DI()->notorm->users_live->where('uid=?',$liveuid)->delete();
		
		return 0;
		
	}
	
	/* 频道第一条 */
	public function getFirstLive(){
		$info=array();
		$liveid=DI()->notorm->channel
				->select("liveid")
				->order('orderno asc')
				->fetchOne();
		if($liveid){
			$info=DI()->notorm->users_live
				->select("uid,avatar,avatar_thumb,user_nicename,title,city,stream,lng,lat,pull,thumb,ispc")
				->where("islive=1 and uid={$liveid['liveid']}")
				->order("starttime desc")
				->fetchOne();
			
		}
		if(!$info){
			$info=DI()->notorm->users_live
				->select("uid,avatar,avatar_thumb,user_nicename,title,city,stream,lng,lat,pull,thumb,ispc")
				->where("islive=1")
				->order("starttime desc")
				->fetchOne();
			
		}
		
		if(!$info){
			return 1001;
		}
		
		$nums=DI()->redis->hlen('userlist_'.$info['stream']);
		$info['nums']=(string)$nums;
		
		if(!$info['thumb']){
			$info['thumb']=$info['avatar'];
		}
		$info['pull']=$this->PrivateKeyA('rtmp',$info['stream'],0);
		
		return $info;
	}
	
	/* 频道列表 */
	public function getChannelLive($p){
		
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" l.islive= '1'";
		$prefix= DI()->config->get('dbs.tables.__default__.prefix');
		
		$result=DI()->notorm->users_live
					->queryAll("select l.uid,l.avatar,l.avatar_thumb,l.user_nicename,l.title,l.city,l.stream,l.pull,l.thumb,l.ispc from {$prefix}channel c left join {$prefix}users_live l on l.uid=c.liveid where {$where} order by c.orderno asc limit {$start},{$pnum}");
					
		foreach($result as $k=>$v){
			$nums=DI()->redis->hlen('userlist_'.$v['stream']);

			$result[$k]['nums']=(string)$nums;
			
			if(!$v['thumb']){
				$result[$k]['thumb']=$v['avatar'];
			}
			$result[$k]['pull']=$this->PrivateKeyA('rtmp',$v['stream'],0);
		}	
		
		return $result;
		
	}

	

	/*判断用户是否是主播的管理员*/
	public function checkManager($liveuid,$uid){
			$result=DI()->notorm->users_livemanager
			->select("*")
			->where("uid=? and liveuid=?",$uid,$liveuid)
			->fetchOne();

			if($result){
				return 1;
			}else{
				return 0;
			}

	}

	
	/*发红包*/
	public function sendRedPackets($uid,$liveuid,$num,$totalMoney,$title,$type){


		
		//判断用户的钻石数
		$userinfo=DI()->notorm->users
		->select("coin")
		->where("id=?",$uid)
		->fetchOne();



		if($userinfo['coin']<$totalMoney){
			return 1003;
		}

		//减去用户的钻石数
		DI()->notorm->users
		->where("id=?",$uid)
		->update(array('coin' => new NotORM_Literal("coin - {$totalMoney}"),'consumption' => new NotORM_Literal("consumption + {$totalMoney}")));

		$now=time();
		$rs=array();

		//var_dump($now);

		//判断红包的类型
		 if($type==1){ //口令红包

			//将口令记录到redis里
			 DI()->redis -> hSet('redpacketstitle_'.$liveuid.'_'.$uid,$now,$title);
		 }


		//$title11=DI()->redis -> hGet('redpacketstitle_'.$liveuid.'_'.$uid,$now);
		/*var_dump($now);
		var_dump($title11);
*/
		//die;

		$total=$totalMoney;//红包总金额
		$min=0.01;//每个人最少能收到0.01元 
		for ($i=1;$i<$num;$i++) { 
			 $safe_total=($total-($num-$i)*$min)/($num-$i);//随机安全上限 
			 $money=mt_rand($min*100,$safe_total*100)/100; 
			 $total=$total-$money; 
			 //echo '第'.$i.'个红包：'.$money.' 元，余额：'.$total.' 元 '; 
			 $rs[]=$money;
		}
		//echo '第'.$num.'个红包：'.$total.' 元，余额：0 元';
		 $rs[]=$total;

		 //红包信息存到redis里
		  DI()->redis -> hSet('redpackets_'.$liveuid.'_'.$uid,$now,json_encode($rs));
		 
		/* var_dump(DI()->redis -> hGet('redpackets_'.$liveuid.'_'.$uid,$now));

		 die;*/


		//DI()->redis->delete('redpackets_'.$liveuid.'_'.$uid);
		
		//向数据库里写入发红包记录
		DI()->notorm->users_send_redpackets
		->insert(array('uid'=>$uid,'roomid'=>$liveuid,'num'=>$num,'total_money'=>$totalMoney,'title'=>$title,'type'=>$type,'addtime'=>$now));


		 return strval($now);
		
	}


	/*抢红包*/

	public function robRedPackets($uid,$liveuid,$sendRedUid,$sendtime,$type,$title){

		$result=array();

		//获取后台配置的钻石和直播券兑换比例
		$configPri=$this->getConfigPri();
		$live_coin_percent=$configPri['live_coin_percent'];



		//判断红包类型
		if($type==0){ //******普通红包*********


			//判断用户是否已经抢过红包
			
			$robMsg=DI()->notorm->users_rob_redpackets
			->where('uid=? and senduid=? and liveuid=? and sendtime=? and type=?',$uid,$sendRedUid,$liveuid,$sendtime,0)
			->fetchOne();

			if($robMsg){ //记录存在

				return 1003; //已经抢过红包了

			}


			//从redis里获取红包信息
			$redPackets=DI()->redis -> hGet('redpackets_'.$liveuid.'_'.$sendRedUid,$sendtime);


			if($redPackets){ //有红包
				 
					$packetArr=json_decode($redPackets,true);//将json转为数组


					//判断数组的长度
				 	$packetCount=count($packetArr);

				 	if($packetCount==1){//最后一个红包


					 	$val=$packetArr[0];

					 	//计算该用户抢得的直播券
					 	$liveCoin=$val*$live_coin_percent;

					 	//给用户加上该直播券
					 	DI()->notorm->users
					 	->where('id=?',$uid)
					 	->update(array('livecoin'=>new NotORM_Literal("livecoin + {$liveCoin}")));

					 	//向抢红包记录里添加一条数据
					 	DI()->notorm->users_rob_redpackets
					 	->insert(array('uid'=>$uid,'senduid'=>$sendRedUid,'coin'=>$val,'livecoin'=>$liveCoin,'live_coin_percent'=>$live_coin_percent,'type'=>0,'liveuid'=>$liveuid,'sendtime'=>$sendtime,'title'=>$title,'addtime'=>time() ) );

					 	//将该红包从redis里整个删除掉
					 	
					 	DI()->redis->hDel('redpackets_'.$liveuid.'_'.$sendRedUid,$sendtime);

					 	

					 	$result['end']=1; //该红包被抢完
				 

					}else{ //还有超过2个红包

						

					 	$sub=array_rand($packetArr,1); //取得数组的随机下标

					 	
					 	
						$val=$packetArr[$sub]; //获取数组下标对应的值
						
						$liveCoin=$val*$live_coin_percent; //计算该用户抢得的直播券



						//给用户加上该直播券
					 	DI()->notorm->users
					 	->where('id=?',$uid)
					 	->update(array('livecoin'=>new NotORM_Literal("livecoin + {$liveCoin}")));

					 	//向抢红包记录里添加一条数据
					 	DI()->notorm->users_rob_redpackets
					 	->insert(array('uid'=>$uid,'senduid'=>$sendRedUid,'coin'=>$val,'livecoin'=>$liveCoin,'live_coin_percent'=>$live_coin_percent,'type'=>0,'liveuid'=>$liveuid,'sendtime'=>$sendtime,'title'=>$title,'addtime'=>time() ) );

					 	//将redis数组对应下标的值删除掉
					 	unset($packetArr[$sub]);
					 	

					 	//将数组重新排序
					 	$packetArr=array_merge($packetArr);

					 

					 	//重新给redis赋值
					 	
					 	$redPacketsInfo=json_encode($packetArr); //数组重新打包

					 	//重新给redis赋值
					 	DI()->redis->hSet('redpackets_'.$liveuid.'_'.$sendRedUid,$sendtime,$redPacketsInfo);

					 	$result['end']=0;

					}

					 		$result['liveCoin']=$liveCoin;
				
							return $result; //返回数据



				
				 	
				 	


			}else{ //没有红包

				return 1001; //红包不存在
			}



		}else if($type==1){  //如果是口令红包

			//从抢红包记录里查询该用户是否抢过红包
				$robMsg=DI()->notorm->users_rob_redpackets
				->where('uid=? and senduid=? and liveuid=? and sendtime=? and type=?',$uid,$sendRedUid,$liveuid,$sendtime,1)
				->fetchOne();


				if($robMsg){ //抢到红包了

					return 1003;
				}



			//从redis里获取红包信息
			$redPackets=DI()->redis -> hGet('redpackets_'.$liveuid.'_'.$sendRedUid,$sendtime);
			// $redisPacketTitle=DI()->redis->hGet('redpacketstitle_'.$liveuid.'_'.$sendRedUid,$sendtime);
			/*var_dump("dfgh");
			var_dump($redPackets);
			var_dump("asdf");
			die;
*/
			if($redPackets){ //如果红包存在

				//从redis里取出口令
				$redisPacketTitle=DI()->redis->hGet('redpacketstitle_'.$liveuid.'_'.$sendRedUid,$sendtime);

				/*var_dump($redisPacketTitle);
				die;*/
					
					if($redisPacketTitle==$title){ //判断口令是否一致

						

						$packetArr=json_decode($redPackets,true);//将json转为数组
						


						//判断数组的长度
				 		$packetCount=count($packetArr);



				 		if($packetCount==1){ //最后一个红包

					 			$val=$packetArr[0];

					 			
					 			//计算该用户抢得的直播券
					 			$liveCoin=$val*$live_coin_percent;


					 			//给用户加上该直播券
							 	DI()->notorm->users
							 	->where('id=?',$uid)
							 	->update(array('livecoin'=>new NotORM_Literal("livecoin + {$liveCoin}")));

							 	//向抢红包记录里添加一条数据
							 	DI()->notorm->users_rob_redpackets
							 	->insert(array('uid'=>$uid,'senduid'=>$sendRedUid,'coin'=>$val,'livecoin'=>$liveCoin,'live_coin_percent'=>$live_coin_percent,'type'=>1,'liveuid'=>$liveuid,'sendtime'=>$sendtime,'title'=>$title,'addtime'=>time() ) );

							 	//将该红包从redis里整个删除掉
						 		DI()->redis->hDel('redpackets_'.$liveuid.'_'.$sendRedUid,$sendtime);

							 	//将口令从redis里删除掉
							 	DI()->redis->hDel('redpacketstitle_'.$liveuid.'_'.$sendRedUid,$sendtime);

							 	$result['end']=1;


			 			}else{ //红包超过2个

			 				

			 				$sub=array_rand($packetArr,1); //取得数组的随机下标

					 	
							$val=$packetArr[$sub]; //获取数组下标对应的值

							
							
							$liveCoin=$val*$live_coin_percent; //计算该用户抢得的直播券



							//给用户加上该直播券
						 	$rs=DI()->notorm->users
						 	->where('id=?',$uid)
						 	->update(array('livecoin'=>new NotORM_Literal("livecoin + {$liveCoin}")));

						 	

						 	//向抢红包记录里添加一条数据
						 	DI()->notorm->users_rob_redpackets
						 	->insert(array('uid'=>$uid,'senduid'=>$sendRedUid,'coin'=>$val,'livecoin'=>$liveCoin,'live_coin_percent'=>$live_coin_percent,'type'=>1,'liveuid'=>$liveuid,'sendtime'=>$sendtime,'title'=>$title,'addtime'=>time() ) );
						 	//将redis取出的数组对应下标的值删除掉
						 	unset($packetArr[$sub]);

						 	
						 	//将数组重新排序
						 	$packetArr=array_merge($packetArr);

						 	//重新给redis赋值
						 	$redPacketsInfo=json_encode($packetArr); //数组重新打包
					
						 	
						 	//重新给redis赋值
						 	DI()->redis->hSet('redpackets_'.$liveuid.'_'.$sendRedUid,$sendtime,$redPacketsInfo);
						 	

						 	$result['end']=0;

				 			}

				 			$result['liveCoin']=$liveCoin;
				
							return $result; //返回数据


					}else{  //口令不一致

						
						
						return 1002;
					}


			}else{//红包不存在
				return 1001;
			}


		}
	

	}


	/*获取礼物组别列表*/

	public function giftGroupLists(){
		$rs=DI()->notorm->gift_group
		->fetchAll();

		return $rs;

	}

	/*礼物PK*/

	public function giftPK($uid,$guestID,$effectiveTime,$masterGiftID,$guestGiftID){

		//将PK信息写入数据库
		$data=array(
			'uid'=>$uid,
			'guest_id'=>$guestID,
			'master_giftid'=>$masterGiftID,
			'guest_giftid'=>$guestGiftID,
			'effective_time'=>$effectiveTime,
			'addtime'=>time()

		);
		$rs=DI()->notorm->gift_pk->insert($data);

		//将信息存入redis里
		
		$data1=array(
			'uid'=>$uid,
			'guestID'=>$guestID,
			'masterGiftID'=>$masterGiftID,
			'masterGiftNum'=>0,
			'masterGiftTotalCoin'=>0,
			'guestGiftID'=>$guestGiftID,
			'guestGiftNum'=>0,
			'guestGiftTotalCoin'=>0,
			'isEnd'=>0
		)

		//DI()->redis->set('giftPK_'.$uid,json_encode($data1));

		if($rs){
			return 1;
		}else{
			return 0;
		}

	}




	public function getredisInfo($uid,$liveuid,$sendtime){

		$info=DI()->redis -> hGet('redpackets_'.$liveuid.'_'.$uid,$sendtime);
		//var_dump(DI()->redis -> hGet('redpackets_'.$liveuid.'_'.$uid,$sendtime));
		return $info;
	}



}
