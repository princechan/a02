<?php

class Api_Live extends Api_Common {

	public function getRules() {
		return array(
			'createRoom' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'user_nicename' => array('name' => 'user_nicename', 'type' => 'string', 'require' => true, 'desc' => '用户昵称 url编码'),
				'avatar' => array('name' => 'avatar', 'type' => 'string',  'require' => true, 'desc' => '用户头像 url编码'),
				'avatar_thumb' => array('name' => 'avatar_thumb', 'type' => 'string',  'require' => true, 'desc' => '用户小头像 url编码'),
				'title' => array('name' => 'title', 'type' => 'string','default'=>'', 'desc' => '直播标题 url编码'),
				'province' => array('name' => 'province', 'type' => 'string', 'default'=>'', 'desc' => '省份'),
				'city' => array('name' => 'city', 'type' => 'string', 'default'=>'', 'desc' => '城市'),
				'lng' => array('name' => 'lng', 'type' => 'string', 'default'=>'0', 'desc' => '经度值'),
				'lat' => array('name' => 'lat', 'type' => 'string', 'default'=>'0', 'desc' => '纬度值'),
				'type' => array('name' => 'type', 'type' => 'int', 'default'=>'0', 'desc' => '直播类型，0是一般直播，1是私密直播，2是收费直播，3是计时直播'),
				'type_val' => array('name' => 'type_val', 'type' => 'string', 'default'=>'', 'desc' => '类型值'),
				'chat_num'=>array('name' => 'chat_num', 'type' => 'string','default'=>'30', 'desc' => '直播间发言字数'),
				'chat_frequency'=>array('name' => 'chat_frequency', 'type' => 'string', 'desc' => '直播间发言频率'),
			),
			'changeLive' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
				'status' => array('name' => 'status', 'type' => 'int', 'require' => true, 'desc' => '直播状态 0关闭 1直播'),
			),
			'stopRoom' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
			),
			
			'stopInfo' => array(
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
			),
			
			'checkLive' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
			),
			
			'roomCharge' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
			),
			
			'enterRoom' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
				'city' => array('name' => 'city', 'type' => 'string','default'=>'', 'desc' => '城市'),
			),
			
			'showVideo' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
                'touid' => array('name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '上麦会员ID'),
            ),
			
			'getZombie' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
								'stream' => array('name' => 'stream', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '流名'),
            ),

			'getUserLists' => array(
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
				'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1,'desc' => '页数'),
			),
			
			'getPop' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'),
			),
			
			'getGiftList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			
			'sendGift' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
				'giftid' => array('name' => 'giftid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物ID'),
				'giftcount' => array('name' => 'giftcount', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物数量'),
				'giftGroupNum'=>array('name'=>'giftGroupNum','type'=>'int','min'=>1,'require'=>true,'desc'=>'礼物组别数量'),
				'type'=>array('name'=>'type','type'=>'int','require'=>true,'desc'=>'赠送礼物的类型，0没有提示直播券不足，1提示直播券不足'),
			),
			
			'sendBarrage' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'stream' => array('name' => 'stream', 'type' => 'string', 'require' => true, 'desc' => '流名'),
				'giftid' => array('name' => 'giftid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物ID 弹幕为1'),
				'giftcount' => array('name' => 'giftcount', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '礼物数量'),
				'content' => array('name' => 'content', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '弹幕内容'),
			),
			
			'setAdmin' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'),
			),
			
			'getAdminList' => array(
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
			),
			
			'setReport' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'),
				'content' => array('name' => 'content', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '举报内容'),
			),
			
			'getVotes' => array(
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
			),
			
			'setShutUp' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '用户token'),
                'touid' => array('name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '禁言用户ID'),
                'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
                'minutes'=>array('name' => 'minutes', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '禁言时长（分钟）'),
            ),
			
			'kicking' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
				'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '对方ID'),
			),
			
			'superStopRoom' => array(
            	'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '会员ID'),
                'token' => array('name' => 'token', 'require' => true, 'min' => 1, 'desc' => '会员token'),
                'liveuid' => array('name' => 'liveuid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '主播ID'),
				'type' => array('name' => 'type', 'type' => 'int','default'=>0, 'desc' => '关播类型 0表示关闭当前直播 1表示关闭当前直播并禁用账号'),
            ),
			'searchMusic' => array(
				'key' => array('name' => 'key', 'type' => 'string','require' => true,'desc' => '关键词'),
				'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1,'desc' => '页数'),
            ),
			
			'getDownurl' => array(
				'audio_id' => array('name' => 'audio_id', 'type' => 'int','require' => true,'desc' => '歌曲ID'),
            ),
			
			'getAuthKs' => array(
				'uid' => array('name' => 'uid', 'type' => 'int','require' => true,'desc' => '用户ID'),
            ),
			
			'getChannelLive' => array(
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			'sendRedPackets'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'liveuid'=>array('name'=>'liveuid','type'=>'int','require'=>true,'min'=>'1','desc'=>'主播id'),
				'num'=>array('name'=>'num','type'=>'int','require'=>true,'desc'=>'红包个数'),
				'totalMoney'=>array('name'=>'totalMoney','type'=>'int','require'=>true,'desc'=>'红包总金额'),
				'title'=>array('name'=>'title','type'=>'string','require'=>true,'desc'=>'标题或者口令'),
				'type'=>array('name'=>'type','type'=>'int','min'=>'0','require'=>true,'desc'=>'红包类型，0普通红包，1口令红包'),
			),

			'robRedPackets'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'sendRedUid'=>array('name'=>'sendRedUid','type'=>'int','require'=>true,'min'=>'1','desc'=>'发送红包用户的ID'),
				'liveuid'=>array('name'=>'liveuid','type'=>'int','require'=>true,'min'=>'1','desc'=>'主播id'),
				'sendtime'=>array('name'=>'sendtime','type'=>'string','require'=>true,'desc'=>'发红包时间'),
				'type'=>array('name'=>'type','type'=>'int','require'=>true,'desc'=>'红包类型'),
				'title'=>array('name'=>'title','type'=>'string','require'=>true,'desc'=>'红包标题或口令')
			),

			'giftGroupLists'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
			),

			'giftPK'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'guestID'=>array('name'=>'guestID','type'=>'int','require'=>true,'min'=>1,'desc'=>'客队人ID'),
				'effectiveTime'=>array('name'=>'effectiveTime','type'=>'int','require'=>true,'desc'=>'PK有效时间'),
				'masterGiftID'=>array('name'=>"masterGiftID",'type'=>'int','require'=>true,'desc'=>'主队礼物ID'),
				'guestGiftID'=>array('name'=>'guestGiftID','type'=>'int','require'=>true,'desc'=>'客队礼物ID'),
			),

			'stopPK'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'pkID'=>array('name'=>'pkID','type'=>'int','require'=>true,'min'=>'1','desc'=>'pkID'),
			),

			'createGrabBench'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'effectiveTime'=>array('name'=>'effectiveTime','type'=>'int','require'=>true,'desc'=>'抢板凳有效时间'),
				'winNums'=>array('name'=>'winNums','type'=>'string','require'=>true,'desc'=>'抢板凳的中奖号码'),
			),

			'grabBench'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'liveuid'=>array('name' => 'liveuid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '直播间ID'),
				'grabbenchID'=>array('name'=>'grabbenchID','type'=>'int','min'=>'1','require'=>true,'desc'=>'创建抢板凳游戏的ID'),
			),

			'stopGrabBench'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'grabbenchID'=>array('name'=>'grabbenchID','type'=>'int','min'=>'1','require'=>true,'desc'=>'创建抢板凳游戏的ID'),
			),

			'getCarouse'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'startGameUid'=>array('name'=>'startGameUid','type'=>'int','require'=>true,'desc'=>'开启游戏人ID'),
				'stream'=>array('name'=>'stream','type'=>'string','require'=>true,'desc'=>'流地址'),
			),

			'getBuyGuard'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
			),

			'buyGuard'=>array(
				'uid'=>array('name' => 'uid', 'type' => 'int', 'min' => '1', 'require' => true, 'desc' => '会员ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'liveuid'=>array('name'=>'liveuid','type'=>'int','require'=>true,'min'=>'1','desc'=>'主播ID'),
			),

			'checkisSuper'=>array(
				'uid'=>array('name'=>'uid','type'=>'int','min' => '1','require'=>true,'desc'=>'用户ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
			),

			'showVestOption'=>array(
				'uid'=>array('name'=>'uid','type'=>'int','min' => '1','require'=>true,'desc'=>'用户ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'touid'=>array('name'=>'touid','type'=>'int','min' => '1','require'=>true,'desc'=>'对方ID'),

			),

			'changeVest'=>array(

				'uid'=>array('name'=>'uid','type'=>'int','min' => '1','require'=>true,'desc'=>'用户ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'touid'=>array('name'=>'touid','type'=>'int','min' => '1','require'=>true,'desc'=>'对方ID'),
				'vestid'=>array('name'=>'vestid','type'=>'int','min' => '1','require'=>true,'desc'=>'新卡的马甲ID'),
				'roomid'=>array('name'=>'roomid','type'=>'int','min' => '1','require'=>true,'desc'=>'房间ID'),
			),

			'channelGag'=>array(
				'uid'=>array('name'=>'uid','type'=>'int','min' => '1','require'=>true,'desc'=>'用户ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'roomid'=>array('name'=>'roomid','type'=>'int','min' => '1','require'=>true,'desc'=>'房间ID'),

			),

			'delChannelGap'=>array(
				'uid'=>array('name'=>'uid','type'=>'int','min' => '1','require'=>true,'desc'=>'用户ID'),
				'token'=>array('name'=>'token','type'=>'string','require'=>true,'min'=>'1','desc'=>'用户token'),
				'roomid'=>array('name'=>'roomid','type'=>'int','min' => '1','require'=>true,'desc'=>'房间ID'),
			),

			


			'getredisInfo'=>array(
				'uid'=>array('name'=>'uid','type'=>'int','require'=>true,'desc'=>'用户ID'),
				
			),

			
			
			
		);
	}

	/**
	 * 创建开播
	 * @desc 用于用户开播生成记录
	 * @return int code 操作码，0表示成功
	 * @return array info
	 * @return string info[0].userlist_time 用户列表请求间隔
	 * @return string info[0].barrage_fee 弹幕价格
	 * @return string info[0].votestotal 主播映票
	 * @return string info[0].stream 流名
	 * @return string info[0].push 推流地址
	 * @return string info[0].chatserver socket地址
	 * @return string info[0].pull_wheat 连麦播流地址
	 * @return string msg 提示信息
	 */
	public function createRoom() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$uid = $this->uid;
		$token=$this->checkNull($this->token);
		$configpub=$this->getConfigPub();
		if($configpub['maintain_switch']==1){
			$rs['code']=1002;
			$rs['msg']=$configpub['maintain_tips'];
			return $rs;

		}
		$isban = $this->isBan($uid);
		if(!$isban){
			$rs['code']=1001;
			$rs['msg']='该账号已被禁用';
			return $rs;
		}

		

		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}
		$configpri=$this->getConfigPri();
		if($configpri['auth_islimit']==1){
			$isauth=$this->isAuth($uid);
			if(!$isauth){
				$rs['code']=1002;
				$rs['msg']='请先进行身份认证或等待审核';
				return $rs;
			}
		}
		$userinfo=$this->getUserInfo($uid);

		if($userinfo['iswhite']==0){

			$islimitIP=$this->islimitIP($uid);
		
			if($islimitIP){//IP被限制
				$rs['code']=1001;
				$rs['msg']='您的IP已被封禁';
				return $rs;
			}
		}

		
		if($configpri['level_islimit']==1){
			if( $userinfo['level'] < $configpri['level_limit'] ){
				$rs['code']=1003;
				$rs['msg']='等级小于'.$configpri['level_limit'].'级，不能直播';
				return $rs;
			}
		}
				
		$nowtime=time();
		
		$user_nicename=$this->checkNull($this->user_nicename);
		$avatar=$this->checkNull($this->avatar);
		$avatar_thumb=$this->checkNull($this->avatar_thumb);
		$showid=$nowtime;
		$starttime=$nowtime;
		$title=$this->checkNull($this->title);
		$province=$this->checkNull($this->province);
		$city=$this->checkNull($this->city);
		$lng=$this->checkNull($this->lng);
		$lat=$this->checkNull($this->lat);
		$type=$this->checkNull($this->type);
		$type_val=$this->checkNull($this->type_val);
		$chat_num=$this->chat_num;
		$chat_frequency=$this->chat_frequency;
		$stream=$uid.'_'.$nowtime;
		//$pull='rtmp://'.$configpri['pull_url'].'/5showcam/'.$stream;
		//$push='rtmp://'.$configpri['push_url'].'/5showcam/'.$stream.'?vhost='.$configpri['pull_url'];
		$pull='';
		$push=$this->PrivateKeyA('rtmp',$stream,1);


		if(!$city){
			$city='好像在火星';
		}
		if(!$lng){
			$lng=0;
		}
		if(!$lat){
			$lat=0;
		}
		if(($type==1 && $type_val=='') || ($type > 1 && $type_val<=0 ) ){
			$rs['code']=1002;
			$rs['msg']='房间类型参数错误';
			return $rs;
		}

		

		if($chat_num=='0'){
			$rs['code']=1002;
				$rs['msg']='发言字数限制不能少于4个字';
				return $rs;
		}



		if($chat_num==""){
			$chat_num=30;
		}	

		if($chat_num!=""){
			if($chat_num<4){
				$rs['code']=1002;
				$rs['msg']='发言字数限制不能少于4个字';
				return $rs;
			}
		}

		if($chat_frequency!=""){
			if($chat_frequency<1){
				$rs['code']=1002;
				$rs['msg']='发言频率限制不能少于1秒';
				return $rs;
			}

			if($chat_frequency>60){
				$rs['code']=1002;
				$rs['msg']='发言频率限制不能大于60秒';
				return $rs;
			}

		}
		
		$thumb='';
		if($_FILES){
			if ($_FILES["file"]["error"] > 0) {
				$rs['code'] = 1003;
				$rs['msg'] = T('failed to upload file with error: {error}', array('error' => $_FILES['file']['error']));
				DI()->logger->debug('failed to upload file with error: ' . $_FILES['file']['error']);
				return $rs;
			}
			
			if(!$this->checkExt($_FILES["file"]['name'])){
				$rs['code']=1004;
				$rs['msg']='图片仅能上传 jpg,png,jpeg';
				return $rs;
			}

			$uptype=DI()->config->get('app.uptype');
			if($uptype==1){
				//七牛
				$url = DI()->qiniu->uploadFile($_FILES['file']['tmp_name']);

				if (!empty($url)) {
					$thumb=  $url.'?imageView2/2/w/600/h/600'; //600 X 600
				}
			}else if($uptype==2){
				//本地上传
				//设置上传路径 设置方法参考3.2
				DI()->ucloud->set('save_path','thumb/'.date("Ymd"));

				//新增修改文件名设置上传的文件名称
			   // DI()->ucloud->set('file_name', $this->uid);

				//上传表单名
				$res = DI()->ucloud->upfile($_FILES['file']);
				
				$files='../upload/'.$res['file'];
				$PhalApi_Image = new Image_Lite();
				//打开图片
				$PhalApi_Image->open($files);
				/**
				 * 可以支持其他类型的缩略图生成，设置包括下列常量或者对应的数字：
				 * IMAGE_THUMB_SCALING      //常量，标识缩略图等比例缩放类型
				 * IMAGE_THUMB_FILLED       //常量，标识缩略图缩放后填充类型
				 * IMAGE_THUMB_CENTER       //常量，标识缩略图居中裁剪类型
				 * IMAGE_THUMB_NORTHWEST    //常量，标识缩略图左上角裁剪类型
				 * IMAGE_THUMB_SOUTHEAST    //常量，标识缩略图右下角裁剪类型
				 * IMAGE_THUMB_FIXED        //常量，标识缩略图固定尺寸缩放类型
				 */
				$PhalApi_Image->thumb(660, 660, IMAGE_THUMB_SCALING);
				$PhalApi_Image->save($files);							
				
				$thumb=$res['url'];
			}
			
			@unlink($_FILES['file']['tmp_name']);			
		}
		
		$dataroom=array(
			"uid"=>$uid,
			"user_nicename"=>$user_nicename,
			"avatar"=>$avatar,
			"avatar_thumb"=>$avatar_thumb,
			"showid"=>$showid,
			"starttime"=>$starttime,
			"title"=>$title,
			"province"=>$province,
			"city"=>$city,
			"stream"=>$stream,
			"thumb"=>$thumb,
			"pull"=>$pull,
			"lng"=>$lng,
			"lat"=>$lat,
			"type"=>$type,
			"type_val"=>$type_val,
			"chat_num"=>$chat_num,
			"chat_frequency"=>$chat_frequency

		);	



		$domain = new Domain_Live();

		
		$result = $domain->createRoom($uid,$dataroom);
		if($result===false){
			$rs['code'] = 1011;
			$rs['msg'] = '开播失败，请重试';
			return $rs;
		}
		$data=array('city'=>$city);


		$domain2 = new Domain_User();
		$info2 = $domain2->userUpdate($uid,$data);
		
		$userinfo['city']=$city;
		$userinfo['sign'] = md5($uid.'_'.$uid);
		$userinfo['userType']=50;



		DI()->redis  -> set($token,json_encode($userinfo));

		$votestotal=$domain->getVotes($uid);


		
		$info['userlist_time']=$configpri['userlist_time'];
		$info['barrage_fee']=$configpri['barrage_fee'];
		$info['chatserver']=$configpri['chatserver'];
		$info['votestotal']=$votestotal;
		$info['stream']=$stream;
		$info['push']=$push;
		$info['pull_wheat']='rtmp://'.$configpri['pull_url'].'/5showcam/';
		if($chat_num!=""){
			$info['chat_num']=$chat_num;
		}else{
			$info['chat_num']="";
		}

		if($chat_frequency!=""){
			$info['chat_frequency']=$chat_frequency;
		}else{
			$info['chat_frequency']="";
		}



		$info['iswhite']=0;  //用于主播在房间内发言时，判断是否匹配敏感词过滤（如果要求主播不过滤敏感词的话，将这个值改为1即可）
		

		$rs['info'][0] = $info;
		/* 极光推送 */
		/* $app_key = $configpri['jpush_key'];
		$master_secret = $configpri['jpush_secret'];
		if($app_key && $master_secret && $type==0){
			require './JPush/autoload.php';

			// 初始化
			$client = new \JPush\Client($app_key, $master_secret,null);
			
			$anthorinfo=array(
				"uid"=>$dataroom['uid'],
				"avatar"=>$dataroom['avatar'],
				"avatar_thumb"=>$dataroom['avatar_thumb'],
				"user_nicename"=>$dataroom['user_nicename'],
				"title"=>$dataroom['title'],
				"city"=>$dataroom['city'],
				"stream"=>$dataroom['stream'],
				"pull"=>$dataroom['pull'],
				"thumb"=>$dataroom['thumb'],
			);
			$fansids = $domain->getFansIds($uid); 
			$uids=$this->array_column2($fansids,'uid');
			$nums=count($uids);	
		
			for($i=0;$i<$nums;){
				$alias=array();
				for($n=0;$n<1000;$n++,$i++){
					if($uids[$i]){
						$alias[]=$uids[$i].'PUSH';			 
					}else{
						continue;
					}
				}	 
				try{	
					$result = $client->push()
							->setPlatform('all')
							->addAlias($alias)
							->setNotificationAlert('你的好友：'.$anthorinfo['user_nicename'].'正在直播，邀请你一起')
							->iosNotification('你的好友：'.$anthorinfo['user_nicename'].'正在直播，邀请你一起', array(
								'sound' => 'sound.caf',
								'category' => 'jiguang',
								'extras' => array(
									'userinfo' => $anthorinfo
								),
							))
							->androidNotification('你的好友：'.$anthorinfo['user_nicename'].'正在直播，邀请你一起', array(
								'extras' => array(
									'userinfo' => $anthorinfo
								),
							))
							->options(array(
								'sendno' => 100,
								'time_to_live' => 0,
								'apns_production' =>  $configpri['jpush_sandbox'],
							))
							->send();
							
				} catch (Exception $e) {   
					//file_put_contents('./jpush.txt',date('y-m-d h:i:s').'提交参数信息 设备名:'.json_encode($alias)."\r\n",FILE_APPEND);
					//file_put_contents('./jpush.txt',date('y-m-d h:i:s').'提交参数信息:'.$e."\r\n",FILE_APPEND);
				}					
			}			
		} */
		/* 极光推送 */

		return $rs;
	}
	

	/**
	 * 修改直播状态
	 * @desc 用于主播修改直播状态
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].msg 成功提示信息
	 * @return string msg 提示信息
	 */
	public function changeLive() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid = $this->uid;
		$token=$this->checkNull($this->token);
		$stream=$this->checkNull($this->stream);
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}
		
		$domain = new Domain_Live();
		$info=$domain->changeLive($uid,$stream,$this->status);

		$rs['info'][0]['msg']='成功';
		return $rs;
	}	

	
	/**
	 * 关闭直播
	 * @desc 用于用户结束直播
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].msg 成功提示信息
	 * @return string msg 提示信息
	 */
	public function stopRoom() { 
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid = $this->uid;
		$token=$this->checkNull($this->token);
		$stream=$this->checkNull($this->stream);

        $checkToken=$this->checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = 'Token错误或已过期，请重新登录';
            return $rs;
        }

        $domain = new Domain_Live();
        $info=$domain->stopRoom($uid,$stream);

		$rs['info'][0]['msg']='关播成功';
		return $rs;
	}	
	
	/**
	 * 直播结束信息
	 * @desc 用于直播结束页面信息展示
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].nums 人数
	 * @return string info[0].length 时长
	 * @return string info[0].votes 映票数
	 * @return string msg 提示信息
	 */
	public function stopInfo() { 
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$stream=$this->checkNull($this->stream);
		
		$domain = new Domain_Live();
		$info=$domain->stopInfo($stream);

		$rs['info'][0]=$info;
		return $rs;
	}		
	
	/**
	 * 检查直播
	 * @desc 用于用户进房间时检查直播
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].type 房间类型	
	 * @return string info[0].type_msg 提示信息
	 * @return string msg 提示信息
	 */
	public function checkLive() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->checkNull($this->token);
		$liveuid=$this->liveuid;
		$stream=$this->checkNull($this->stream);
		
		$configpub=$this->getConfigPub();
		if($configpub['maintain_switch']==1){
			$rs['code']=1002;
			$rs['msg']=$configpub['maintain_tips'];
			return $rs;

		}
		$isban = $this->isBan($uid);
		if(!$isban){
			$rs['code']=1001;
			$rs['msg']='该账号已被禁用';
			return $rs;
		}

		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}


		$userinfo=$this->getUserInfo($uid);

		if($userinfo['iswhite']==0){
			$islimitIP=$this->islimitIP($uid);
		
			if($islimitIP){//IP被限制
				$rs['code']=1001;
				$rs['msg']='您的IP已被封禁';
				return $rs;
			}
		}
		




		$iskick=DI()->redis  -> hGet($liveuid.'kick',$uid);
		$nowtime=time();
		if($iskick>$nowtime){
			$surplus=$iskick-$nowtime;
			$rs['code']=1004;
			$rs['msg']='您已被踢出房间，剩余'.$surplus.'秒';
		}else{
			DI()->redis  -> hdel($liveuid.'kick',$uid);
		}
		
		
		$domain = new Domain_Live();
		$info=$domain->checkLive($uid,$liveuid,$stream);
		
		if($info==1005){
			$rs['code'] = 1005;
			$rs['msg'] = '直播已结束';
			return $rs;
		}
		$rs['info'][0]=$info;
		
		
		return $rs;
	}
	
	/**
	 * 房间扣费
	 * @desc 用于房间扣费
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].coin 用户余额
	 * @return string msg 提示信息
	 */
	public function roomCharge() { 
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->checkNull($this->token);
		$liveuid=$this->liveuid;
		$stream=$this->checkNull($this->stream);
		

		
		$domain = new Domain_Live();
		$info=$domain->roomCharge($uid,$token,$liveuid,$stream);
		
		if($info==700){
			$rs['code'] = 700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}else if($info==1005){
			$rs['code'] = 1005;
			$rs['msg'] = '直播已结束';
			return $rs;
		}else if($info==1006){
			$rs['code'] = 1006;
			$rs['msg'] = '该房间非扣费房间';
			return $rs;
		}else if($info==1007){
			$rs['code'] = 1007;
			$rs['msg'] = '房间费用有误';
			return $rs;
		}else if($info==1008){
			$rs['code'] = 1008;
			$rs['msg'] = '余额不足';
			return $rs;
		}
		$rs['info'][0]['coin']=$info['coin'];
	
		return $rs;
	}			

	/**
	 * 进入直播间
	 * @desc 用于用户进入直播
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].votestotal 直播映票
	 * @return string info[0].barrage_fee 弹幕价格
	 * @return string info[0].userlist_time 用户列表获取间隔
	 * @return string info[0].chatserver socket地址
	 * @return string info[0].isattention 是否关注主播，0表示未关注，1表示已关注
	 * @return string info[0].nums 房间人数
	 * @return string info[0].push_url 推流地址
	 * @return string info[0].pull_url 播流地址
	 * @return string info[0].showvideo 连麦用户ID，0表示未连麦
	 * @return string info[0].showvideo_url 连麦播流地址
	 * @return array info[0].userlists 用户列表
	 * @return array info[0].game 押注信息
	 * @return string info[0].gametime 游戏剩余时间
	 * @return string info[0].gameid 游戏记录ID
	 * @return string info[0].gameaction 游戏类型，1表示炸金花，2表示牛牛，3表示转盘
	 * @return string info[0].chat_num 房间聊天字数
	 * @return string info[0].chat_frequency 房间聊天频率
	 * @return string msg 提示信息
	 */
	public function enterRoom() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->checkNull($this->token);
		$liveuid=$this->liveuid;
		$city=$this->checkNull($this->city);
		$stream=$this->checkNull($this->stream);
		$isban = $this->isBan($uid);
		
		if(!$isban){
			$rs['code']=1001;
			$rs['msg']='该账号已被禁用';
			return $rs;
		}



		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}


		
		$userinfo=$this->getUserInfo($uid);
		

		if($userinfo['issuper']==1){
			DI()->redis  -> hset('super',$userinfo['id'],'1');
		}else{
			DI()->redis  -> hDel('super',$userinfo['id']);
		}
		if(!$city){
			$city='好像在火星';
		}
		
		$data=array('city'=>$city);
		$domain2 = new Domain_User();
		$info = $domain2->userUpdate($uid,$data);
		$userinfo['city']=$city;
		$userinfo['sign'] = md5($liveuid.'_'.$userinfo['id']);
		$userinfo['userType'] = 30;
		
		//unset($userinfo['issuper']);
		
		DI()->redis  -> set($token,json_encode($userinfo));
		
		
		$lists=array();
		$times=20;
		$pnum=20;

		$key="getUserLists_".$liveuid.'_'.$times;

		$isexist=$this->getcache($key);
		if(!$isexist){ 

			$list=DI()->redis -> hVals('userlist_'.$stream);

			$com_model = new Model_Common();
            //赋予标识：1
            $extUsers = $com_model->getExtUsers();

			foreach($list as $v){
				$redisuser=json_decode($v,true);
				$guard=$this->checkIsGuard($redisuser['id'],$liveuid);
				$redisuser['guard']=$guard;
				$redisuser['isguard']=$guard['isGuard'];

				//获取用户的马甲
				$vestID=$this->getVestID($redisuser['id'],$liveuid);
				$redisuser['vestID']=$vestID;

                //赋予标识：2
                if(isset($extUsers[$redisuser['id']])){
                    $ext_arr   = json_decode($extUsers[$redisuser['id']], true);
                    $redisuser = array_merge($redisuser, $ext_arr);
                }

				$lists[]=$redisuser;
			}

            //从数组中取出指定的列 //将数组$lists按照vestID倒序排列
            $lists = $com_model->multiaArraySort($lists, 'vestid', SORT_DESC, 'level', SORT_DESC);

			$newArr=array_chunk($lists,20);//将数组20个分割

     		$lists=$newArr[0];//返回前20个

     		if(!$lists){
     			$lists=array();
     		}
		}else{
			$lists=$isexist;
		}
		
		$nums=DI()->redis->hlen('userlist_'.$stream);
		$showvideo='0';
		$ishowVideo=DI()->redis  -> hGet('ShowVideo',$liveuid);
		
		if($ishowVideo){
			$showvideo=$ishowVideo;
		}

		$domain = new Domain_Live();
		$configpri=$this->getConfigPri();
		$domain3 = new Domain_Game();
		$game = $domain3->checkGame($liveuid,$stream);

		$ismanager=$domain->checkManager($liveuid,$uid);
		
		$push_url='rtmp://'.$configpri['push_url'].'/5showcam/';
		$pull_url='?vhost='.$configpri['pull_url'];

		//获取直播间设置的发言字数和发言频率
		$chatArr=$this->getLiveChatMsg($liveuid);

		if($chatArr['chat_num']==""){
			$chatArr['chat_num']=30;
		}

		if($chatArr['chat_frequency']==""){
			$chatArr['chat_frequency']=0;
		}



		$guard=$this->checkIsGuard($uid,$liveuid);


		
	    $info=array(
			'votestotal'=>$domain->getVotes($liveuid),
			'barrage_fee'=>$configpri['barrage_fee'],
			'userlist_time'=>$configpri['userlist_time'],
			'chatserver'=>$configpri['chatserver'],
			'push_url'=>$push_url,
			'pull_url'=>$pull_url,
			'showvideo'=>$showvideo,
			'showvideo_url'=>'rtmp://'.$configpri['pull_url'].'/5showcam/',
			'nums'=>$nums,
			'game'=>$game['brand'],
			'gametime'=>$game['time'],
			'gameid'=>$game['id'],
			'gameaction'=>$game['action'],
			'coin'=>$userinfo['coin'],
			'isauth'=>$userinfo['isauth'],//用户是否认证
			'issuper'=>$userinfo['issuper'],//用户是否是超管
			'ismanager'=>strval($ismanager),//用户是否是主播的管理员
			'chat_num'=>$chatArr['chat_num'],//房间发言字数
			'chat_frequency'=>$chatArr['chat_frequency'],//房间发言频率
			'isGuard'=>$guard['isGuard'],//用户是否是主播的守护
			'guard_level'=>$guard['guard_level'],//用户守护等级
			'iswhite'=>$userinfo['iswhite'],//用户是否在白名单
			'personal_chat_num'=>$userinfo['chat_num'], //用户个人发言字数
			'personal_chat_frequency'=>$userinfo['chat_frequency'], //用户个人发言频率
			'personal_send_url'=>$userinfo['send_url'], //用户是否可以发送网址

		);
		$info['isattention']=(string)$this->isAttention($uid,$liveuid);
		$info['userlists']=$lists;


		/*******获取礼物PK的redis信息start********/

		$giftPkInfo=DI()->redis->get("giftPK_".$liveuid);

		$giftPkArr=array();

		if($giftPkInfo){

			$giftPkArr=json_decode($giftPkInfo,true);//转换为数组


			if($giftPkArr['isEnd']==0){
				//到PK结束的剩余时间
				$effectiveTime=$giftPkArr['lastStopTime']-time();

				if($effectiveTime<=0){//主播未正常停止游戏造成时间失效
					DI()->redis->delete("giftPK_".$liveuid);

					$info['giftpk']=array();

				}else{
					$giftPkArr['effectiveTime']=$effectiveTime;

					$guestUserInfo=$this->getUserInfo($giftPkArr['guestID']);
					$giftPkArr['guestAvatar']=$guestUserInfo['avatar'];//客队头像
					$giftPkArr['guestName']=$guestUserInfo['user_nicename'];//客队昵称

					$masterUserInfo=$this->getUserInfo($giftPkArr['uid']);
					$giftPkArr['masterAvatar']=$masterUserInfo['avatar'];//主队头像
					$giftPkArr['masterName']=$masterUserInfo['user_nicename'];//主队昵称

					$guestGiftInfo=$this->getGiftInfo($giftPkArr['guestGiftID']);
					$giftPkArr['guestGiftImg']=$guestGiftInfo['gifticon'];//客队礼物图片

					$masterGiftInfo=$this->getGiftInfo($giftPkArr['masterGiftID']);
					$giftPkArr['masterGiftImg']=$masterGiftInfo['gifticon'];//主队礼物图片

					unset($giftPkArr['uid']);
					unset($giftPkArr['guestID']);
					$info['giftpk'][]=$giftPkArr;

				}

				

				
			}
			

		}else{
			$giftPkArr=array();
			$info['giftpk']=$giftPkArr;
		}

		/*******获取礼物PK的redis信息end********/


		/*******获取抢板凳的redis信息start********/

		$grabBenchInfo=DI()->redis->get("grabbench_".$liveuid);

		$grabBenchArr=array();

		if($grabBenchInfo){
			
			$grabBenchArr=json_decode($grabBenchInfo,true);//解析成数组

			if($grabBenchArr['isEnd']==1){//活动已经结束
				DI()->redis->delete("grabbench_".$liveuid);//将redis删除

				$info['grabbench']=array();

			}else{

				$grabbenchID=$grabBenchArr['grabbenchID'];

				//获取后台配置的连续点击时间间隔
				$hits_space=$configpri['grabbench_hits_space'];
				$effectiveTime=$grabBenchArr['effectiveTime']-time();//到活动结束的剩余时间

				if($effectiveTime<=0){

					DI()->redis->delete("grabbench_".$liveuid);
					$grabBenchArr=array();


				}else{

					$grabBenchArr=array();
					$grabBenchArr['hits_space']=$hits_space;
					$grabBenchArr['effectiveTime']=strval($effectiveTime);
					$grabBenchArr['grabbenchID']=$grabbenchID;

					$info['grabbench'][]=$grabBenchArr;
				}

				
			}


		}else{

			$info['grabbench']=$grabBenchArr;
		}

		/*******获取抢板凳的redis信息end********/


		//获取房间是否被禁言
		
		$ischannelShutUp="0";

		$arr=DI()->redis->hKeys('channelShutUp');
		foreach ($arr as $k => $v) {
			if($v==$liveuid){
				$ischannelShutUp="1";
				break;
			}
		}
		$info['ischannelShutUp']=$ischannelShutUp;


		//获取用户的马甲信息
		$vestID=$this->getVestID($uid,$liveuid);
		$vestInfo=$this->getVestInfoByID($vestID);

		$vestInfo['vest_man_url']=$this->get_upload_path($vestInfo['vest_man_url']);
		$vestInfo['vest_woman_url']=$this->get_upload_path($vestInfo['vest_woman_url']);

		$info['vestInfo']=$vestInfo;

		$rs['info'][0]=$info;


		return $rs;
	}	
	
    /**
     * 连麦信息
     * @desc 用于主播同意连麦 写入redis
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
		 
    public function showVideo() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->checkNull($this->token);
		$liveuid=$this->liveuid;
		$touid=$this->touid;

        if($uid!=$liveuid){
			$rs['code']=1001;
			$rs['info']='您不是主播';
			$rs['msg']='您不是主播';
			return $rs;
		}
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}
		
		DI()->redis  -> hset('ShowVideo',$liveuid,$touid);
					
        return $rs;
    }		

	
    /**
     * 获取僵尸粉
     * @desc 用于获取僵尸粉
     * @return int code 操作码，0表示成功
     * @return array info 僵尸粉信息
     * @return string msg 提示信息
     */
		 
    public function getZombie() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$stream=$this->checkNull($this->stream);
		
		$stream2=explode('_',$stream);
		$liveuid=$stream2[0];
			
	
		$domain = new Domain_Live();
		
		$iszombie=$domain->isZombie($liveuid);
		
		if($iszombie==0){
			$rs['code']=1001;
			$rs['info']='未开启僵尸粉';
			$rs['msg']='未开启僵尸粉';
			return $rs;
			
		}

		/* 判断用户是否进入过 */
		$isvisit=DI()->redis ->sIsMember($liveuid.'_zombie_uid',$uid);

		if($isvisit){
			$rs['code']=1003;
			$rs['info']='用户已访问';
			$rs['msg']='用户已访问';
			return $rs;
			
		}
	
		$times=DI()->redis  -> get($liveuid.'_zombie');
		
		if($times && $times>10){
			$rs['code']=1002;
			$rs['info']='次数已满';
			$rs['msg']='次数已满';
			return $rs;
		}else if($times){
			$times=$times+1;
			
		}else{
			$times=0;
		}
	
		DI()->redis  -> set($liveuid.'_zombie',$times);
		DI()->redis  -> sAdd($liveuid.'_zombie_uid',$uid);
		
		/* 用户列表 */ 

		$uidlist=array();
		$list=DI()->redis -> hVals('userlist_'.$stream);
		foreach($list as $v){
			$uidlist[]=json_decode($v,true);
		}
	
		$uids=$this->array_column2($uidlist,'id');		
		$uid=implode(",",$uids);

		$where='0';
		if($uid){
			$where.=','.$uid;
		} 
		$where=str_replace(",,",',',$where);
		$where=trim($where, ",");
		$rs['info'][0]['list'] = $domain->getZombie($stream,$where);

		$nums=DI()->redis->hlen('userlist_'.$stream);

		$rs['info'][0]['nums']=(string)$nums;

		file_put_contents("zom.txt", "aaaaa");
		
        return $rs;
    }	
	/**
	 * 用户列表 
	 * @desc 用于直播间获取用户列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].userlist 用户列表
	 * @return string info[0].nums 房间人数
	 * @return string info[0].votestotal 主播映票
	 * @return string msg 提示信息
	 */
	public function getUserLists()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $liveuid=$this->liveuid;
        $stream=$this->checkNull($this->stream);
        $p=$this->p;

        /* 用户列表 */
        $pnum=20;
        $start=($p-1)*$pnum;
        $times=$p*$pnum;

        $lists=array();

        $key="getUserLists_".$liveuid.'_'.$times;

        $isexist=$this->getcache($key);


        if(!$isexist){

            $list=DI()->redis -> hVals('userlist_'.$stream);

            $com_model = new Model_Common();

            //赋予标识：1
            $extUsers = $com_model->getExtUsers();

            foreach($list as $v){

                $redisuser=json_decode($v,true);

                $guard=$this->checkIsGuard($redisuser['id'],$liveuid);
                $redisuser['guard']=$guard;
                $redisuser['isguard']=$guard['isGuard'];


                //获取用户的马甲
                $vestID=$this->getVestID($redisuser['id'],$liveuid);
                $redisuser['vestID']=$vestID;

                //赋予标识：2
                if(isset($extUsers[$redisuser['id']])){
                    $ext_arr   = json_decode($extUsers[$redisuser['id']], true);
                    $redisuser = array_merge($redisuser, $ext_arr);
                }

                $lists[]=$redisuser;
            }

            //从数组中取出指定的列 //将数组$lists按照vestID倒序排列
            $lists = $com_model->multiaArraySort($lists, 'vestid', SORT_DESC, 'level', SORT_DESC);

            $newArr=array_chunk($lists,20);//将数组20个分割

            $lists=$newArr[0];//返回前20个

            if(!$lists){
                $lists=array();
            }
        }else{
            $lists=$isexist;
        }


        $nums=DI()->redis->hlen('userlist_'.$stream);

        $rs['info'][0]['userlist']=$lists;
        $rs['info'][0]['nums']=(string)$nums;

        /* 主播信息 */
        $domain = new Domain_Live();
        $rs['info'][0]['votestotal']=$domain->getVotes($liveuid);


        return $rs;
	}				
		

		
	/**
	 * 弹窗 
	 * @desc 用于直播间弹窗信息
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return int status 当前用户点击对方用户时返回的状态，0:选的比自己马甲等级低 1：同级或比自己大【举报功能】 2：超管对主播 【有关播，停播功能】 3：超管对超管【不举报】
	 * @return string info[0].consumption 消费总数
	 * @return string info[0].votestotal 票总数
	 * @return string info[0].follows 关注数
	 * @return string info[0].fans 粉丝数
	 * @return string info[0].isattention 是否关注，0未关注，1已关注
	 * @return string info[0].action 
	 * 操作显示，0表示自己，30表示普通用户，40表示管理员，501表示主播设置管理员，502表示主播取消管理员，503表示主播操作超管，60表示超管管理主播，601表示超管操作普通用户，602表示超管操作管理员
	 * 特别注意: 503 601 602只是后来添加的action用来做标识没有实际操作(都是为私信服务的)
	 * @return string msg 提示信息
	 * @return string info[1].uidIdentity 当前用户的身份 11：白马 12：绿马 13：蓝马 14：黄马 15：橙马 16：紫马 17：黑马 18：主播 19：超管
	 * @return string info[1].touidIdentity 被选用户的身份 11：白马 12：绿马 13：蓝马 14：黄马 15：橙马 16：紫马 17：黑马 18：主播 19：超管
	 */
	public function getPop() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$liveuid=$this->liveuid;
		$touid=$this->touid;
		
		$domain = new Domain_Live();
		$info=$domain->getPop($touid);
		if(!$info){
			$rs['code']=1002;
			$rs['msg']='用户信息不存在';
			return $rs;
		}
		$info['isattention']=(string)$this->isAttention($uid,$touid);
		if($uid==$touid){
			$info['action']='0';
		}else{
			$uid_admin=$this->isAdmin($uid,$liveuid);
			$touid_admin=$this->isAdmin($touid,$liveuid);

			if($uid_admin==40 && $touid_admin==30){
				$info['action']='40';
			}else if($uid_admin==50 && $touid_admin==30){
				$info['action']='501';
			}else if($uid_admin==50 && $touid_admin==40){
				$info['action']='502';
			}else if($uid_admin==50 && $touid_admin==60){
				$info['action']='503';
			}else if($uid_admin==60 && $touid_admin==30){
				$info['action']='601';
			}else if($uid_admin==60 && $touid_admin==40){
				$info['action']='602';
			}else if($uid_admin==60 && $touid_admin==50){
				$info['action']='60';
			}else{
				$info['action']='30';
			}
			
		}

		

			//判断用户的身份
			
			$uid_identity=$this->getUserIdentity($uid,$touid,$liveuid);//返回数组

			$status="0";



			$vestOption=array();

			if($uid==$touid){

				$status="4";
				$vestOption['set_admin']="0";
				$vestOption['away_user']="0";
				$vestOption['away_admin']="0";
				$vestOption['gap']="0";
				$vestOption['private_chat']="0";

			}else{


				if($uid_identity['uidIdentity']==19){//如果当前用户是超管
				
					if($uid_identity['touidIdentity']==18){//对方是主播
						$status="2";

						$vestOption['set_admin']="0";
						$vestOption['away_user']="0";
						$vestOption['away_admin']="0";
						$vestOption['gap']="0";
						$vestOption['private_chat']="1";
						
					}

					if($uid_identity['touidIdentity']==19){//对方是超管
						$status="3";

						$vestOption['set_admin']="0";
						$vestOption['away_user']="0";
						$vestOption['away_admin']="0";
						$vestOption['gap']="0";
						$vestOption['private_chat']="1";
						
					}


					if($uid_identity['touidIdentity']>=11&&$uid_identity['touidIdentity']<16){//普通马甲

						$vestOption['set_admin']="1";
						$vestOption['away_user']="1";
						$vestOption['away_admin']="1";
						$vestOption['gap']="1";
						$vestOption['private_chat']="1";


					}





					if($uid_identity['touidIdentity']==16||$uid_identity['touidIdentity']==17){//紫马或黑马

						$vestOption['set_admin']="0";
						$vestOption['away_user']="1";
						$vestOption['away_admin']="1";
						$vestOption['gap']="1";
						$vestOption['private_chat']="1";
					}




				}else if($uid_identity['uidIdentity']==18){//当前用户是主播


					if($uid_identity['touidIdentity']==19){//对方是超管
						$status="3";

						$vestOption['set_admin']="0";
						$vestOption['away_user']="0";
						$vestOption['away_admin']="0";
						$vestOption['gap']="0";
						$vestOption['private_chat']="1";
						
					}


					if($uid_identity['touidIdentity']==16||$uid_identity['touidIdentity']==17){//对方是紫马或黑马

						$vestOption['set_admin']="0";
						$vestOption['away_user']="1";
						$vestOption['away_admin']="1";
						$vestOption['gap']="1";
						$vestOption['private_chat']="1";
					}


					if($uid_identity['touidIdentity']>=11&&$uid_identity['touidIdentity']<16){//对方是普通马甲

						$vestOption['set_admin']="1";
						$vestOption['away_user']="1";
						$vestOption['away_admin']="1";
						$vestOption['gap']="1";
						$vestOption['private_chat']="1";


					}



				}else{

					

					//判断当前马甲和对方马甲的大小
					
					if($uid_identity['uidIdentity']>$uid_identity['touidIdentity']){//当前马甲大于对方的马甲

						//读取当前用户的马甲权限
						
						$vestOption=$this->getVestInfoByID($uid_identity['uidIdentity']-10);

						
						if($uid_identity['uidIdentity']==12){
							$vestOption['set_admin']="0";
						}

						unset($vestOption['id']);
						unset($vestOption['vest_name']);
						unset($vestOption['vest_man_url']);
						unset($vestOption['vest_woman_url']);
						unset($vestOption['gap_all']);



					}else{//同级或比对方小

						$vestOption=$this->getVestInfoByID($uid_identity['uidIdentity']-10);
						$vestOption['set_admin']="0";
						unset($vestOption['id']);
						unset($vestOption['vest_name']);
						unset($vestOption['vest_man_url']);
						unset($vestOption['vest_woman_url']);
						unset($vestOption['gap_all']);

						$status="1";

					}



				}


			} 


		
		$rs['info'][0]=$info;
		$rs['info'][0]['status']=$status;
		$rs['info'][0]['vestInfo']=$uid_identity;
		$rs['info'][0]['vestOption']=$vestOption;

		return $rs;
	}				

	/**
	 * 礼物列表 
	 * @desc 用于获取礼物列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].coin 余额
	 * @return array info[0].giftlist 礼物列表
	 * @return string info[0].giftlist[].id 礼物ID
	 * @return string info[0].giftlist[].type 礼物类型
	 * @return string info[0].giftlist[].giftname 礼物名称
	 * @return string info[0].giftlist[].needcoin 礼物价格
	 * @return string info[0].giftlist[].gifticon 礼物图片
	 * @return string info[0].giftlist[].livecoin 礼物直播券
	 * @return string msg 提示信息
	 */
	public function getGiftList() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->checkNull($this->token);
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}
		
		$key='getGiftList';
		$giftlist=$this->getcache($key);
		if(!$giftlist){
			$domain = new Domain_Live();
			$giftlist=$domain->getGiftList();
			$this->setcache($key,$giftlist);
		}
		
		$domain2 = new Domain_User();
		$coin=$domain2->getBalance($uid);
		$livecoin=$domain2->getLivecoin($uid);
		$rs['info'][0]['giftlist']=$giftlist;
		
		$rs['info'][0]['coin']=$coin;
		$rs['info'][0]['livecoin']=$livecoin;
		return $rs;
	}		

	/**
	 * 赠送礼物 
	 * @desc 用于赠送礼物
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].gifttoken 礼物token
	 * @return string info[0].level 用户等级
	 * @return string info[0].coin 用户余额
	 * @return string info[0].livecoin 用户直播券
	 * @return string msg 提示信息
	 */
	public function sendGift() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$stream=$this->checkNull($this->stream);
		$giftid=$this->giftid;
		$giftcount=$this->giftcount;
		$giftGroupNum=$this->giftGroupNum;
		$type=$this->type;
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		} 

		$configpub=$this->getConfigPub();
		
		$domain = new Domain_Live();
		$result=$domain->sendGift($uid,$liveuid,$stream,$giftid,$giftcount,$giftGroupNum,$type);
		
		if($result==1001){
			$rs['code']=1001;
			$rs['msg']='余额不足';
			return $rs;
		}else if($result==1002){
			$rs['code']=1002;
			$rs['msg']='礼物信息不存在';
			return $rs;
		}else if($result==1003){
			$rs['code']=1003;
			$rs['msg']='您的直播券不足，是否使用'.$configpub['name_coin'].'支付？';
			return $rs;
		}

		
		
		$rs['info'][0]['gifttoken']=$result['gifttoken'];
        $rs['info'][0]['level']=$result['level'];
        $rs['info'][0]['coin']=$result['coin'];
        $rs['info'][0]['iswin']=strval($result['iswin']);
        $rs['info'][0]['wincoin']=strval($result['winCoin']);
        $rs['info'][0]['livecoin']=strval($result['livecoin']);
        $rs['info'][0]['giftname']=strval($result['giftname']);
		$rs['info'][0]['winMsg']="恭喜您中得".$result['winLiveCoin']."直播券！";

		unset($result['gifttoken']);

		DI()->redis  -> set($rs['info'][0]['gifttoken'],json_encode($result));
		
		
		return $rs;
	}		
	
	/**
	 * 发送弹幕 
	 * @desc 用于发送弹幕
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].barragetoken 礼物token
	 * @return string info[0].level 用户等级
	 * @return string info[0].coin 用户余额
	 * @return string msg 提示信息
	 */
	public function sendBarrage() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$stream=$this->checkNull($this->stream);
		$giftid=$this->giftid;
		$giftcount=$this->giftcount;
		
		$content=$this->checkNull($this->content);
		if($content==''){
			$rs['code'] = 1003;
			$rs['msg'] = '弹幕内容不能为空';
			return $rs;
		}
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		} 
		
		$domain = new Domain_Live();
		$result=$domain->sendBarrage($uid,$liveuid,$stream,$giftid,$giftcount,$content);
		
		if($result==1001){
			$rs['code']=1001;
			$rs['msg']='余额不足';
			return $rs;
		}else if($result==1002){
			$rs['code']=1002;
			$rs['msg']='礼物信息不存在';
			return $rs;
		}
		
		$rs['info'][0]['barragetoken']=$result['barragetoken'];
        $rs['info'][0]['level']=$result['level'];
        $rs['info'][0]['coin']=$result['coin'];
		
		unset($result['barragetoken']);

		DI()->redis -> set($rs['info'][0]['barragetoken'],json_encode($result));

		return $rs;
	}			

	/**
	 * 设置/取消管理员 
	 * @desc 用于获取礼物列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].isadmin 是否是管理员，0表示不是管理员，1表示是管理员
	 * @return string msg 提示信息
	 */
	public function setAdmin() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$touid=$this->touid;
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		} 
		
		if($uid!=$liveuid){
			$rs['code'] = 1001;
			$rs['msg'] = '你不是该房间主播，无权操作';
			return $rs;
		}
		
		$domain = new Domain_Live();
		$info=$domain->setAdmin($liveuid,$touid);
		
		if($info==1004){
			$rs['code'] = 1004;
			$rs['msg'] = '最多设置5个管理员';
			return $rs;
		}else if($info==1003){
			$rs['code'] = 1003;
			$rs['msg'] = '操作失败，请重试';
			return $rs;
		}
		
		$rs['info'][0]['isadmin']=$info;
		return $rs;
	}		
	
	/**
	 * 管理员列表 
	 * @desc 用于获取管理员列表
	 * @return int code 操作码，0表示成功
	 * @return array info 管理员列表
	 * @return array info[].userinfo 用户信息
	 * @return string msg 提示信息
	 */
	public function getAdminList() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$domain = new Domain_Live();
		$info=$domain->getAdminList($this->liveuid);
		
		$rs['info']=$info;
		return $rs;
	}			
	
	/**
	 * 用户举报 
	 * @desc 用于用户举报
	 * @return int code 操作码，0表示成功
	 * @return array info 礼物列表
	 * @return string info[0].msg 举报成功
	 * @return string msg 提示信息
	 */
	public function setReport() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->checkNull($this->token);
		$touid=$this->touid;
		$content=$this->checkNull($this->content);
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		} 
		
		if(!$content){
			$rs['code'] = 1001;
			$rs['msg'] = '举报内容不能为空';
			return $rs;
		}
		
		$domain = new Domain_Live();
		$info=$domain->setReport($uid,$touid,$content);
		if($info===false){
			$rs['code'] = 1002;
			$rs['msg'] = '举报失败，请重试';
			return $rs;
		}
		
		$rs['info'][0]['msg']="举报成功";
		return $rs;
	}			
	
	/**
	 * 主播映票 
	 * @desc 用于获取主播映票
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].votestotal 用户总数
	 * @return string msg 提示信息
	 */
	public function getVotes() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$domain = new Domain_Live();
		$info=$domain->getVotes($this->liveuid);
		
		$rs['info'][0]=$info;
		return $rs;
	}		
	
    /**
     * 禁言
     * @desc 用于 禁言操作
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
		 
    public function setShutUp() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$touid=$this->touid;
		$minutes=$this->minutes;


		$checkToken = $this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg']='token已过期，请重新登陆';
			return $rs;
		}

		
						
        /*$uidtype = $this->isAdmin($uid,$liveuid);

		if($uidtype==30 ){
			$rs["code"]=1001;
			$rs["msg"]='无权操作';
			return $rs;									
		}*/				
				
        /*$touidtype = $this->isAdmin($touid,$liveuid);
		
		if($touidtype==60){
			$rs["code"]=1001;
			$rs["msg"]='对方是超管，不能禁言';
			return $rs;	
		}

		if( $touidtype==50){
			$rs["code"]=1002;
			$rs["msg"]='对方是主播，不能禁言';
			return $rs;		
		}

		if($uidtype==40){
			if( $touidtype==50){
				$rs["code"]=1002;
				$rs["msg"]='对方是主播，不能禁言';
				return $rs;		
			}	
			if($touidtype==40 ){
				$rs["code"]=1002;
				$rs["msg"]='对方是管理员，不能禁言';
				return $rs;		
			}	
			
		}*/




		/*判断马甲*/

		$UserIdentity=$this->getUserIdentity($uid,$touid,$liveuid);



		$uidIdentity=$UserIdentity['uidIdentity'];//当前用户身份
		$touidIdentity=$UserIdentity['touidIdentity'];//对方用户身份



		if($uidIdentity==19){   //超管

			if($touidIdentity==19){ //超管对超管
				$rs["code"]=1002;
				$rs["msg"]='对方是超管，不能禁言';
				return $rs;

			}else if($touidIdentity==18){ //超管对主播

				$rs["code"]=1002;
				$rs["msg"]='对方是主播，不能禁言';
				return $rs;

			}else{

			    $nowtime=time();	
		        $result=DI()->redis -> hGet($liveuid . 'shutup',$touid);
				if($result){
					if($nowtime<=$result){
						$rs["code"]=1003;
						$rs["msg"]='对方已被禁言';
						return $rs;	
					}
				}		
				$time=$nowtime + $minutes*60;
		        DI()->redis -> hSet($liveuid . 'shutup',$touid,$time);	  
						
		        return $rs;	


			}



		}else if($uidIdentity==18){    //主播

			if($touidIdentity==19){ //主播对超管
				$rs["code"]=1002;
				$rs["msg"]='对方是超管，不能禁言';
				return $rs;

			}else if($touidIdentity==18){ //主播对自己

				$rs["code"]=1002;
				$rs["msg"]='不能对自己禁言';
				return $rs;

			}else{

			    $nowtime=time();	
		        $result=DI()->redis -> hGet($liveuid . 'shutup',$touid);
				if($result){
					if($nowtime<=$result){
						$rs["code"]=1003;
						$rs["msg"]='对方已被禁言';
						return $rs;	
					}
				}		
				$time=$nowtime + $minutes*60;
		        DI()->redis -> hSet($liveuid . 'shutup',$touid,$time);	  
						
		        return $rs;	


			}


		}else{   //普通马甲用户



			if($uidIdentity<=$touidIdentity){
				$rs["code"]=1002;
				$rs["msg"]='等级不够，不能对其禁言';
				return $rs;
			}

			//判断马甲权限
		
			$vestInfo=$this->getVestInfoByID($uidIdentity-10); //获取当前用户的马甲信息（状态码-10就是马甲id）


			//判断权限
			if($vestInfo['gap']==0){
				$rs["code"]=1002;
				$rs["msg"]='您没有禁言权限';
				return $rs;

			}else{

				//判断对方是否在白名单里
				$userinfo=$this->getUserInfo($touid);



				if($userinfo['iswhite']==1){//用户在白名单
					$rs["code"]=1002;
					$rs["msg"]='对方在白名单，无法被禁言';
					return $rs;
				}

			

				 $nowtime=time();	
		        $result=DI()->redis -> hGet($liveuid . 'shutup',$touid);
				if($result){
					if($nowtime<=$result){
						$rs["code"]=1003;
						$rs["msg"]='对方已被禁言';
						return $rs;	
					}
				}		
				$time=$nowtime + $minutes*60;
		        DI()->redis -> hSet($liveuid . 'shutup',$touid,$time);	  
						
		        return $rs;
			}




		}

		
		

		

		

		
    }	
	
	/**
	 * 踢人 
	 * @desc 用于直播间踢人
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].msg 踢出成功
	 * @return string msg 提示信息
	 */
	public function kicking() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$touid=$this->touid;
		
		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		} 


		/*$admin_uid=$this->isAdmin($uid,$liveuid);
		if($admin_uid==30){
			$rs['code']=1001;
			$rs['msg']='无权操作';
			return $rs;
		}
		$admin_touid=$this->isAdmin($touid,$liveuid);
		
		if($admin_touid==60){
			$rs["code"]=1002;
			$rs["msg"]='对方是超管，不能被踢出';
			return $rs;
		}
		
		if($admin_uid!=60){
			if($admin_touid==50 ){
				$rs['code']=1001;
				$rs['msg']='对方是主播，不能被踢出';
				return $rs;
			}else if($admin_touid==40 ){
				$rs['code']=1002;
				$rs['msg']='对方是管理员，不能被踢出';
				return $rs;
			}				
		}*/	

		/*判断马甲*/

		$UserIdentity=$this->getUserIdentity($uid,$touid,$liveuid);



		$uidIdentity=$UserIdentity['uidIdentity'];//当前用户身份
		$touidIdentity=$UserIdentity['touidIdentity'];//对方用户身份


		if($uidIdentity==19){   //超管

			if($touidIdentity==19){ //超管对超管
				$rs["code"]=1002;
				$rs["msg"]='对方是超管，不能被踢出';
				return $rs;

			}else if($touidIdentity==18){ //超管对主播

				$rs["code"]=1002;
				$rs["msg"]='对方是主播，不能被踢出';
				return $rs;

			}else{

			   

				$nowtime=time();
		
				$time=$nowtime+60*0.5;
				
				$result=DI()->redis->hset($liveuid.'kick',$touid,$time);


				$rs['info'][0]['msg']='踢出成功';
				return $rs;	


			}



		}else if($uidIdentity==18){    //主播

			if($touidIdentity==19){ //主播对超管
				$rs["code"]=1002;
				$rs["msg"]='对方是超管，不能被踢出';
				return $rs;

			}else if($touidIdentity==18){ //主播对自己

				$rs["code"]=1002;
				$rs["msg"]='不能将自己踢出';
				return $rs;

			}else{

			   $nowtime=time();
		
				$time=$nowtime+60*0.5;
				
				$result=DI()->redis->hset($liveuid.'kick',$touid,$time);


				$rs['info'][0]['msg']='踢出成功';
				return $rs;	



			}


		}else{   //普通马甲用户

			if($uidIdentity<=$touidIdentity){
				$rs["code"]=1002;
				$rs["msg"]='等级不够，不能将其踢出';
				return $rs;
			}

			//判断马甲权限
		
			$vestInfo=$this->getVestInfoByID($uidIdentity-10); //获取当前用户的马甲信息（状态码-10就是马甲id）

			//判断权限
			if($vestInfo['away_user']==0){
				$rs["code"]=1002;
				$rs["msg"]='您没有踢人权限';
				return $rs;

			}else{

				//判断对方是否在白名单里
				$userinfo=$this->getUserInfo($touid);

				if($userinfo['iswhite']==1){//用户在白名单
					$rs["code"]=1002;
					$rs["msg"]='对方在白名单，无法被踢出';
					return $rs;
				}

				$nowtime=time();
		
				$time=$nowtime+60*0.5;
				
				$result=DI()->redis->hset($liveuid.'kick',$touid,$time);


				$rs['info'][0]['msg']='踢出成功';
				return $rs;
			}




		}


		
	}		
	
	/**
     * 超管关播
     * @desc 用于超管关播
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].msg 提示信息 
     * @return string msg 提示信息
     */
		
	public function superStopRoom(){

		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		
		$domain = new Domain_Live();
		
		$result = $domain->superStopRoom($this->uid,$this->token,$this->liveuid,$this->type);
		if($result==700){
			$rs['code'] = 700;
            $rs['msg'] = 'token已过期，请重新登陆';
            return $rs;
		}else if($result==1001){
			$rs['code'] = 1001;
            $rs['msg'] = '你不是超管，无权操作';
            return $rs;
		}
		$rs['info'][0]['msg']='关闭成功';
 
    	return $rs;
	}	

	/**
     * 歌曲查询
     * @desc 用于搜索歌曲（百度音乐）
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[].audio_id 歌曲ID 
     * @return string info[].audio_name 歌曲名称 
     * @return string info[].artist_name 歌手 
     * @return string msg 提示信息
     */
		
	public function searchMusic(){

		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		
		$key=$this->checkNull($this->key);
		if($key!=''){
			$url='http://musicmini.baidu.com/app/search/searchList.php?qword='.$key.'&ie=utf-8&page='.$p;
			
			$ch = curl_init(); 
			$timeout = 10; 
			curl_setopt ($ch, CURLOPT_URL, $url); 
			curl_setopt ($ch, CURLOPT_HEADER, 0); 
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 

			$contents = curl_exec($ch);
			curl_close($ch);

			$p="/<table[^>]+>(.+)<\/table>/sU"; //表格部分代码
			preg_match_all($p,$contents,$img); 

			$p2='/<tr.*>.*<td class="sName.*key="(.+)".*>.*<a href="javascript:;" onclick="playSong\((.+),.*\)".*>.*<\/a>.*<\/td>.*<td class="uName.*key="(.+)".*>.*<\/td>.*<\/tr>/sU';
			preg_match_all($p2,$img[0][0],$m,PREG_SET_ORDER);
			foreach($m as $k=>$v){
				$info[$k]['audio_id']=str_replace("&#039;",'',$v[2]);
				$info[$k]['audio_name']=$v[1];
				$info[$k]['artist_name']=$v[3];
			}
			if($info){
				$rs['info']=$info;
			}
			
		}
		
 
    	return $rs;
	}	

	/**
     * 歌曲信息
     * @desc 用于获取歌曲详细信息（百度音乐）
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].audio_link 歌曲下载链接 
     * @return string info[0].audio_ext 文件后缀 
     * @return string info[0].time_len 时长 
     * @return string info[0].audio_size 大小 
     * @return string info[0].lrcLink 歌词链接 
     * @return string msg 提示信息
     */
		
	public function getDownurl(){

		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		$info=array();

		$url='http://music.baidu.com/data/music/links?songIds='.$this->audio_id;
		
		$ch = curl_init(); 
		$timeout = 10; 
		curl_setopt ($ch, CURLOPT_URL, $url); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 

		$contents = curl_exec($ch);
		curl_close($ch);
		
		$body=json_decode($contents,true);
		$errorCode=$body['errorCode'];
		$songList=$body['data']['songList'];

		if($errorCode!=22000 || !$songList){
			$rs['code'] = 1001;
            $rs['msg'] = '歌曲不存在';
            return $rs;
		}


		$info['audio_link']=$songList[0]['songLink'];
		$info['audio_ext']=$songList[0]['format'];
		$info['time_len']=$songList[0]['time'];
		$info['audio_size']=$songList[0]['size'];
		$info['lrcLink']=$songList[0]['lrcLink'];

			

		$rs['info'][0]=$info;
 
    	return $rs;
	}	
	/**
     * 金山云鉴权
     * @desc 用于获取金山云鉴权信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].audio_link 歌曲下载链接
     * @return string msg 提示信息
     */
	
	public function getAuthKs(){
		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		$uid=$this->uid;
		$configpri=$this->getConfigPri();
		
		$ak=$configpri['ks_ak'];
		$sk=$configpri['ks_sk'];
		$uniqname=$configpri['ks_uniqname'];
		
		$time=time()+60*60*5;
		$nonce=md5('yunbao'.time());
		$vdoid='yunbao';
		$resource="nonce={$nonce}&uid={$uid}&uniqname={$uniqname}&vdoid={$vdoid}";
		$signature=base64_encode(hash_hmac('sha1', "GET\n{$time}\n{$resource}", $sk, true));
		$info['url']=$configpri['ks_url'];
		$info['uniqname']=$uniqname;
		$info['authString']="accesskey={$ak}&expire={$time}&{$resource}&signature={$signature}";
		$rs['info'][]=$info;
		return $rs;
	}
	
	/**
     * 获取频道第一条直播信息
     * @desc 用于获取频道第一条直播信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].uid 主播id
     * @return string info[0].avatar 主播头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].user_nicename 直播昵称
     * @return string info[0].title 直播标题
     * @return string info[0].city 主播位置
     * @return string info[0].stream 流名
     * @return string info[0].pull 播流地址
     * @return string info[0].nums 人数
     * @return string info[0].thumb 直播封面
     * @return string info[0].chat_num 房间发言字数
     * @return string info[0].chat_frequency 房间发言频率
     * @return string msg 提示信息
     */
	
	public function getFirstLive(){
		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		
		$domain = new Domain_Live();
        $info = $domain->getFirstLive();
		
		if($info==1001){
			$rs['code'] = 1001;
            $rs['msg'] = '暂无直播';
            return $rs;
		}

		$res=$this->getLiveChatMsg($info['uid']);
		$info['chat_num']=$res['chat_num'];
		$info['chat_frequency']=$res['chat_frequency'];
		$rs['info'][]=$info;
		return $rs;
	}

	/**
     * 获取频道直播列表
     * @desc 用于获取频道直播列表
     * @return int code 操作码，0表示成功
     * @return array info 直播列表
     * @return string info[].uid 主播id
     * @return string info[].avatar 主播头像
     * @return string info[].avatar_thumb 头像缩略图
     * @return string info[].user_nicename 直播昵称
     * @return string info[].title 直播标题
     * @return string info[].city 主播位置
     * @return string info[].stream 流名
     * @return string info[].pull 播流地址
     * @return string info[].nums 人数
     * @return string info[].thumb 直播封面
     * @return string info[].ispc 是否PC开播
     * @return string info[].chat_num 直播间聊天字数
     * @return string info[].chat_frequency 直播间聊天频率
     * @return string msg 提示信息
     */
	
	public function getChannelLive(){
		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		
		$key="getChannelLive_".$this->p;
		$list=$this->getcaches($key);
		// $list=false;
		if(!$list){
			$domain = new Domain_Live();
			$list = $domain->getChannelLive($this->p);
			$this->setCaches($key,$list,2); 
		}
		
		$rs['info']=$list;
		return $rs;
	}

	/**
	 * 发红包
	 * @desc 
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array userinfo 用户信息
	 * @return string info[0].user_nicename 用户昵称
	 * @return string info[0].avatar 用户头像
	 * @return int info[0].senduid 发送红包人的id
	 * @return string info[0].title 红包标题
	 * @return string info[0].sendtime 红包发送时间
	 * @return int info[0].redPacketType 红包类型
	 * @return string userinfo['redPacketType'] 红包类型
	 * @return string userinfo['livecoin'] 用户直播券
	 * 
	 */

	public function sendRedPackets(){
		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$num=$this->num;
		$totalMoney=$this->totalMoney;
		$title=$this->title;
		$type=$this->type;


		if($num>10000||$num<1){
			$rs['code']=1004;
			$rs['msg']="红包数量错误";
			return $rs;
		}

		if($totalMoney<1){
			$rs['code']=1005;
			$rs['msg']="红包金额错误";
			return $rs;
		}

		

		if(is_float($totalMoney)){
			$rs['code']=1005;
			$rs['msg']="请填写整数";
			return $rs;
		}



		$checkToken=$this->checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		if(0.01*$num>$totalMoney){
			$rs['code'] = 1002;
			$rs['msg'] = '单个红包金额最少0.01元，请重新选择';
			return $rs;
		}



		$domain=new Domain_Live();
		
		$info=$domain->sendRedPackets($uid,$liveuid,$num,$totalMoney,$title,$type);
		$userinfo=$this->getUserInfo($uid);
		if($info==1003){
			$rs['code']=1003;
			$rs['msg']='金额不足，无法发送红包';
			return $rs;
		}

		$userinfo['liveuid']=strval($liveuid);
		$userinfo['uid']=$userinfo['id'];
		$userinfo['sendtime']=$info;
		$userinfo['title']=$title;
		$userinfo['sendRedUid']=$userinfo['id'];
		unset($userinfo['id']);//将用户的id清除
		$userinfo['redPacketType']=strval($type);
		$rs['info'][0]=$userinfo;
		return $rs;


	}



	/**
	 * 抢红包
	 * @desc 直播间用户抢红包
	 * @return int code 状态码，0表示成功
	 * @return float info[0].liveCoin 用户抢到的直播券
	 * @return int info[0].end 是否是最后一个红包
	 * @return string msg 提示信息
	 */
	
	public function robRedPackets(){
		$uid=$this->uid;
		$token=$this->token;
		$sendRedUid=$this->sendRedUid;
		$liveuid=$this->liveuid;
		$sendtime=$this->sendtime;
		$type=$this->type;
		$title=$this->title;
		


		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		if($sendRedUid==""||$sendRedUid<1){
			$rs['code']=1001;
			$rs['msg']='无法抢红包';
			return $rs;
		}

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$domain=new Domain_Live();
		$info=$domain->robRedPackets($uid,$liveuid,$sendRedUid,$sendtime,$type,$title);

		if($info==1001){//红包不存在
			$rs['code']=1001;
			$rs['msg']='红包已抢完';
			return $rs;
		}

		if($info==1002){
			$rs['code']=1002;
			$rs['msg']='口令错误';
			return $rs;
		}

		if($info==1003){
			$rs['code']=1003;
			$rs['msg']='你已经抢到红包了';
			return $rs;
		}

		$rs['info'][0]=$info;

		return $rs;
	
	}

	/**
	 * 直播间礼物列表显示分组
	 * @desc 直播间礼物列表显示分组
	 * @return int code 状态码，0表示成功
	 * @return array info 分组列表
	 * @return string info['group_name'] 分组名称
	 * @return int info['group_val'] 分组值
	 * @return string msg 提示信息
	 */
	
	public function giftGroupLists(){

		$rs=array('code'=>0,'msg'=>'','info'=>array());
		
		$uid=$this->uid;
		$token=$this->token;

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->giftGroupLists();

		
		$rs['info']=$info;

		return $rs;

	}



	/**
	 * 直播间礼物PK
	 * @desc 用于直播间礼物PK功能
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 */
	
	public function giftPK(){

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$uid=$this->uid;
		$token=$this->token;
		$guestID=$this->guestID;
		$effectiveTime=$this->effectiveTime;
		$masterGiftID=$this->masterGiftID;
		$guestGiftID=$this->guestGiftID;


		//获取config信息
		$config=$this->getConfigPub();

		

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		if($effectiveTime<30){
			$rs['code']=1001;
			$rs['msg']="有效时间应该大于30秒";
			return $rs;
		}


		if($effectiveTime>3600){
			$rs['code']=1005;
			$rs['msg']="有效时间应该小于3600秒";
			return $rs;
		}

		if($uid==$guestID){
			$rs['code']=1003;
			$rs['msg']="客队ID不能跟主队一样";
			return $rs;
		}

		if($masterGiftID==$guestGiftID){
			$rs['code']=1002;
			$rs['msg']="主队和客队礼物不能相同";
			return $rs;
		}

		//获取主队人的信息
		$masterUserInfo=$this->getUserInfo($uid);

		//获取客队人的信息
		$guestUserInfo=$this->getUserInfo($guestID);

		//判断客队人信息
		if(!$guestUserInfo){
			$rs['code']=1004;
			$rs['msg']="客队主播不存在";
			return $rs;
		}


		$domain=new Domain_Live();
		$info=$domain->giftPK($uid,$guestID,$effectiveTime,$masterGiftID,$guestGiftID);

		
		if($info!=0){//返回成功
			$rs['msg']="成功发起礼物PK";
		}else{
			$rs['code']=1003;
			$rs['msg']="发起礼物PK失败";

			return $rs;
		}


		//获取主队礼物信息
		$masterGiftInfo=$this->getGiftInfo($masterGiftID);
		//获取客队礼物信息
		$guestGiftInfo=$this->getGiftInfo($guestGiftID);
		

		$rs['info'][0]['guestName']=$guestUserInfo['user_nicename'];//客队昵称

		$rs['info'][0]['guestAvatar']=$guestUserInfo['avatar'];//客队头像
		$rs['info'][0]['masterGiftImg']=$masterGiftInfo['gifticon'];//主队礼物图片
		$rs['info'][0]['guestGiftImg']=$guestGiftInfo['gifticon'];//主队礼物图片
		$rs['info'][0]['masterAvatar']=$masterUserInfo['avatar'];//主队头像
		$rs['info'][0]['masterName']=$masterUserInfo['user_nicename'];//主队昵称
		$rs['info'][0]['masterGiftID']=strval($masterGiftID);//主队礼物id
		$rs['info'][0]['guestGiftID']=strval($guestGiftID);//客队礼物id
		$rs['info'][0]['pkID']=strval($info);//PK记录id
		$rs['info'][0]['effectiveTime']=strval($effectiveTime);//PK有效时间

		return $rs;
	}

	/**
	 * 直播间礼物PK结束
	 * @desc 用于直播间礼物pk结束
	 * @return string info['masterUserNicename'] 主队用户昵称
	 * @return string info['masterFirstUserNicename'] 主队送礼最多人的昵称
	 * @return int info['masterFirstUserCoin'] 主队送礼最多人的赠送总数
	 * @return int info['guestUserNicename'] 客队用户昵称
	 * @return string info['guestFirstUserNicename'] 客队送礼最多人的昵称
	 * @return int info['guestFirstUserCoin'] 客队送礼最多人的赠送总数
	 * @return int info['winType'] 双方PK的结果，0代表平局，1代表主队胜，2代表客队胜
	 */

	public function stopPK(){
		$uid=$this->uid;
		$token=$this->token;
		$pkID=$this->pkID;

		$rs=array('code'=>0,'msg'=>'','info'=>array());
		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->stopPK($uid,$pkID);
		
		$rs['info']=$info;

		return $rs;

	}

	/**
	 * 主播创建抢板凳游戏
	 * @desc 用于主播创建抢板凳游戏
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return int grabbenchID 抢板凳记录ID
	 * @return int hits_space 同一用户连续两次点击时间间隔
	 */
	public function createGrabBench(){
		$uid=$this->uid;
		$token=$this->token;
		$effectiveTime=$this->effectiveTime;
		$winNums=$this->winNums;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		if($effectiveTime<30){
			$rs['code']=1001;
			$rs['msg']="游戏有效时间要大于30秒";
			return $rs;
		}


		if($effectiveTime>3600){
			$rs['code']=1002;
			$rs['msg']="游戏有效时间要小于3600秒";
			return $rs;
		}


		$domain=new Domain_Live();
		$info=$domain->createGrabBench($uid,$effectiveTime,$winNums);

		if($info==0){//创建失败
			$rs['code']=1003;
			$rs['msg']="游戏创建失败";
			return $rs;

		}

		$rs['info']=$info;

		return $rs;

	}


	/**
	 * 用户点击抢板凳活动
	 * @desc 用于用户点击抢板凳活动
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return string info[0] 抢到的号码
	 */
	
	public function grabBench(){
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;
		$grabbenchID=$this->grabbenchID;

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$domain=new Domain_Live();

		$info=$domain->grabBench($uid,$liveuid,$grabbenchID);

		if($info==-1){//抢板凳redis信息不存在
			$rs['code']=1001;
			$rs['msg']='抢板凳游戏已经结束';
			return $rs;
		}

		if($info==0){//该用户已经中奖
			$rs['code']=1002;
			$rs['msg']='您已中奖，不能再抢';
			return $rs;
		}

		if($info>0){
			$arr=array();
			$arr['num']=strval($info);
			$rs['info'][0]=$arr;
		}

		return $rs;
	}

	/**
	 * 抢板凳活动结束
	 * @desc 用于抢板凳活动结束
	 * @return int code 状态码，0表示成功
	 * @return msg string 提示信息
	 * @return array info 中奖号码和中奖人昵称
	 * @return int info.num 中奖号码
	 * @return string info.user_nicename 中奖人昵称
	 */
	public function  stopGrabBench(){
		$uid=$this->uid;
		$token=$this->token;
		$grabbenchID=$this->grabbenchID;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();

		$info=$domain->stopGrabBench($uid,$grabbenchID);

		if($info==-1){
			
			$rs['info']=array();
		}else{
			$rs['info']=$info;
		}

		return $rs;

	}


	/**
	 * @desc 用于转盘游戏
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array info 数组信息
	 * @return int info.nums 转盘总分类数
	 * @return int info.randNum 随机的中奖号码
	 * @return string info.imgUrl 后台配置的转盘图片
	 */

	public function getCarouse(){
		$uid=$this->uid;
		$token=$this->token;
		$startGameUid=$this->startGameUid;
		$stream=$this->stream;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		if($startGameUid==""){
			$rs['code']=1001;
			$rs['msg'] = '请填写开启游戏用户ID';
			return $rs;
		}


		if($uid==$startGameUid){
			$rs['code']=1001;
			$rs['msg'] = '用户不能是自己';
			return $rs;
		}



		$startUserInfo=$this->getUserInfo($startGameUid);



		if($startUserInfo){

			//判断该用户是否是僵尸粉
			/*if($startUserInfo['iszombie']==1){
				$rs['msg']='该用户无法开启游戏';
				$rs['code']=1003;
				return $rs;
			}else{//判断本用户是否在此房间*/
				$list=DI()->redis -> hVals('userlist_'.$stream);
				//var_dump($list);

				//判断该用户是否在本房间内
				$inRoom=0;
				foreach ($list as $k => $v) {

					$arr=json_decode($v,true);

					if(intval($arr['id'])==$startGameUid){
						$inRoom=1;
						break;
					}
				}


				if($inRoom==1){ //在房间内
					$domain=new Domain_Live();

					$info=$domain->getCarouse();

					if($info==-1){
						$rs['msg']='暂无转盘游戏';
						$rs['code']=1001;
						return $rs;
					}


				}else{ //不在房间内

					$rs['code']=1001;
					$rs['msg']="该用户不在本房间";

					return $rs;


				}
			//}

		}else{
			$rs['msg']='该用户不存在';
			$rs['code']=1002;
			return $rs;
		}


		
		
		$info['startGameUid']=strval($startGameUid);
		$rs['info'][]=$info;

		return $rs;



	}

	/**
	 * 购买守护时返回信息
	 * @desc 用于点击购买守护时返回信息
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return info[0].guardCoin 后台
	 */

	public function getBuyGuard(){
		$uid=$this->uid;
		$token=$this->token;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->getBuyGuard($uid);

		$rs['info'][0]=$info;

		return $rs;




	}

	/**
	 * 用户购买守护
	 * @desc 用于用户购买守护
	 * @return int code 状态码，0表示成功
	 * @return string info 提示信息
	 * @return array info 返回信息
	 */

	public function buyGuard(){
		$uid=$this->uid;
		$token=$this->token;
		$liveuid=$this->liveuid;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	

		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->buyGuard($uid,$liveuid);

		if($info==1001){
			$rs['code']=1001;
			$rs['msg']='余额不足';
			return $rs;
		}

		if($info==1002){
			$rs['code']=1002;
			$rs['msg']='购买守护失败';
			return $rs;
		}

		$rs['msg']="购买成功";
		$rs['info'][0]['coin']=$info['coin'];
		$rs['info'][0]['guardInfo']=$info['guardInfo'];

		return $rs;


	}

	/**
	 * 检测用户是否是超管
	 * @desc 用于检测用户是否是超管
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array info 返回信息
	 */

	public function checkisSuper(){
		$uid=$this->uid;
		$token=$this->token;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->checkisSuper($uid);

		$rs['info']=$info;

		return $rs;

	}

	/**
	 * 获取马甲列表
	 * @desc 用户获取马甲列表
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array info 马甲列表
	 */

	public function getVestLists(){

		$rs=array('code'=>0,'msg'=>'','info'=>'');
		$domain=new Domain_Live();
		$info=$domain->getVestLists();

		$rs['info'][0]=$info;

		return $rs;
	}



	/**
	 * 
	 */
	public function showVestOption(){

		$uid=$this->uid;
		$token=$this->token;
		$touid=$this->touid;

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->showVestOption($uid,$touid);




	}


	/**
	 * 更换马甲【即设置管理】
	 * @desc 用户更换马甲
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array info 返回信息
	 */

	public function changeVest(){

		$rs=array('code'=>0,'msg'=>'','info'=>'');

		$uid=$this->uid;
		$token=$this->token;
		$touid=$this->touid;
		$vestid=$this->vestid;
		$roomid=$this->roomid;

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->changeVest($uid,$touid,$vestid,$roomid);

		if($info==1001){

			$rs['code']=1001;
			$rs['msg']="对方是超管，无法设置马甲";
			return $rs;
		}

		if($info==1002){
			$rs['code']=1002;
			$rs['msg']="对方是主播，无法设置马甲";
			return $rs;
		}

		if($info==1003){
			$rs['code']=1003;
			$rs['msg']="对方是黑马或紫马，无法设置";
			return $rs;
		}
		if($info==1004){
			$rs['code']=1004;
			$rs['msg']="马甲相同，无法设置";
			return $rs;
		}

		if($info==1005){
			$rs['code']=1005;
			$rs['msg']="马甲设置失败";
			return $rs;
		}

		$vestInfo=$this->getVestInfoByID($vestid);
		$vestInfo['vest_man_url']=$this->get_upload_path($vestInfo['vest_man_url']);
		$vestInfo['vest_woman_url']=$this->get_upload_path($vestInfo['vest_woman_url']);

		$rs['msg']="更换马甲成功";
		$rs['info'][0]=$vestInfo;

		return $rs;

	}

	/**
	 * 频道禁言
	 * @desc 用于频道禁言
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array info 返回信息
	 */
	public function channelGag(){

		$rs=array('code'=>0,'msg'=>'','info'=>'');

		$uid=$this->uid;
		$token=$this->token;
		$roomid=$this->roomid;

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}

		$domain=new Domain_Live();
		$info=$domain->channelGag($uid,$roomid);

		if($info==1001){
			$rs['code']=1001;
			$rs['msg']="该房间已被禁言";
			return $rs;
		}

		if($info==1002){
			$rs['code']=1002;
			$rs['msg']="您没有房间禁言权限";
			return $rs;
		}

		$rs['msg']=$info."将房间禁言啦";

		return $rs;
	}


	/**
	 * 房间解除禁言
	 * @desc 用于房间解除禁言
	 * @return int code 状态码，0表示成功
	 * @return string msg 提示信息
	 * @return array info 返回信息
	 */

	public function delChannelGap(){
		$rs=array('code'=>0,'msg'=>'','info'=>'');

		$uid=$this->uid;
		$token=$this->token;
		$roomid=$this->roomid;

		$checkToken=$this->checkToken($uid,$token);	
		if($checkToken==700){
			$rs['code']=700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			return $rs;
		}
		$domain=new Domain_Live();
		$info=$domain->delChannelGap($uid,$roomid);

		if($info==1001){
			$rs['code']=1001;
			$rs['msg']="该房间未被禁言";
			return $rs;
		}

		if($info==1002){
			$rs['code']=1002;
			$rs['msg']="您的等级不够,无法解除禁言";
			return $rs;
		}

		if($info==1003){
			$rs['code']=1003;
			$rs['msg']="您没有解除禁言权限";
			return $rs;
		}

		$rs['msg']=$info."将频道解除禁言";

		return $rs;


	}

	

	public function getredisInfo(){

		$rs=array('code'=>0,'msg'=>'','info'=>array());
		$uid=$this->uid;
		
		$domain=new Domain_Live();
		$info=$domain->getredisInfo($uid);

		$rs['info']=$info;
		return $rs;


	}







	
	
}
