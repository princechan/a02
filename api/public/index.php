<?php
/**
 * ENV
 */
if (in_array($_SERVER['SERVER_ADDR'], ['127.0.0.1', '10.71.42.70',])) {
    define('ENV', 'dev');
} else {
    define('ENV', 'prod');
}
define('ENV_DEV', ENV === 'dev' ? true : false);

/**
 * $APP_NAME 统一入口
 */
require_once dirname(__FILE__) . '/init.php';
require_once dirname(__FILE__) . '/qiniucdn/Pili_v2.php';
//装载你的接口
DI()->loader->addDirs('Appapi');


/** ---------------- 响应接口请求 ---------------- **/

$api = new PhalApi();
$rs = $api->response();
$rs->output();
