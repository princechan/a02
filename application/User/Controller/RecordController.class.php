<?php
/**
 * 消费和充值记录
 */
namespace User\Controller;
use Common\Controller\HomebaseController;
class RecordController extends HomebaseController {
	

	function spend(){
		$uid=I("uid");
		$start=I("start");
		$end=I("end");
		$token=I("token");
		$Spend=M("users_coinrecord");

		//判断用户是否登录
		$userInfo=M("users")
		->where("id={$uid} and token='{$token}'")
		->select();

		if($userInfo){


				$spendLists=$Spend->query("select * from cmf_users_coinrecord where uid={$uid}  order by addtime desc");
				
				$count=count($spendLists);


				$spendList=$Spend->query("select * from cmf_users_coinrecord where uid={$uid}  order by addtime desc LIMIT ".$start.",".$end."");

				$Car=M('car');
				$gift=M('gift');

				foreach ($spendList as $k => $v) {
					$spendList[$k]['userinfo']=getUserInfo($v['touid']);

					$action=$v['action'];

					if(substr($action,0,6)=="buycar"){//买座驾

						$spendList[$k]['giftName']=$Car->where("id={$v['giftid']}")->getField('name');
						$spendList[$k]['use']="购买座驾";
					}

					if($action=="sendbighorn"){//大喇叭
						$spendList[$k]['giftName']="大喇叭";
						$spendList[$k]['use']="购买喇叭";
					}

					if($action=="sendbarrage"){//发弹幕
						$spendList[$k]['giftName']="发弹幕";
						$spendList[$k]['use']="发弹幕";
					}

					if($action=="buyguard"){//购买守护
						$spendList[$k]['giftName']="购买守护";
						$spendList[$k]['use']="购买守护";
					}

					if($action=="roomcharge"){//收费房间进入
						$spendList[$k]['giftName']="付费房间";
						$spendList[$k]['use']="付费房间";
					}

					if($action=="sendgift"){//送礼物
						$spendList[$k]['giftName']=$gift->where("id={$v['giftid']}")->getField("giftname");
						$spendList[$k]['use']="送礼物";
					}

				}


				$this->assign("spendList",$spendList);
				$this->assign("uid",$uid);
				$this->assign("start",$start);
				$this->assign("end",$end);
				$this->assign("count",$count);
				$this->assign("token",$token);

				$this->display();
				exit;
	

		}else{

			$this->assign("woringMsg","用户信息验证失败，请重新登录");
			$this->display();
			exit;

		}

		
	}




	public function charge(){
		$uid=I("uid");
		$start=I("start");
		$end=I("end");
		$token=I("token");
		$Charge=M("users_charge");

		//判断用户是否登录
		$userInfo=M("users")
		->where("id={$uid} and token='{$token}'")
		->select();

		if($userInfo){

				$chargeLists=$Charge->query("select * from cmf_users_charge where uid={$uid} and status=1  order by addtime desc");
				
				$count=count($chargeLists);

				$chargeList=$Charge->query("select * from cmf_users_charge where uid={$uid} and status=1 order by addtime desc LIMIT ".$start.",".$end."");


				$this->assign("chargeList",$chargeList);
				$this->assign("uid",$uid);
				$this->assign("start",$start);
				$this->assign("end",$end);
				$this->assign("count",$count);
				$this->assign("token",$token);

				$this->display();
				exit;

		}else{

			$this->assign("woringMsg","用户信息验证失败，请重新登录");
			$this->display();
			exit;
		}
	}



	function sendpacket(){
		$uid=I("uid");
		$start=I("start");
		$end=I("end");
		$token=I("token");

		$Spend=M("users_send_redpackets");

		//判断用户是否登录
		$userInfo=M("users")
		->where("id={$uid} and token='{$token}'")
		->select();

		if($userInfo){


				$spendLists=$Spend->query("select * from cmf_users_send_redpackets where uid={$uid}  order by addtime desc");
				
				$count=count($spendLists);

				$User=M("users");

				$spendList=$Spend->query("select * from cmf_users_send_redpackets where uid={$uid}  order by addtime desc LIMIT ".$start.",".$end."");

				foreach ($spendList as $k => $v) {
					$spendList[$k]['liveUserName']=	$User->where("id='{$v['roomid']}'")->getField("user_nicename");
				}



				$this->assign("spendList",$spendList);
				$this->assign("uid",$uid);
				$this->assign("start",$start);
				$this->assign("end",$end);
				$this->assign("count",$count);
				$this->assign("token",$token);

				$this->display();
				exit;
	

		}else{

			$this->assign("woringMsg","用户信息验证失败，请重新登录");
			$this->display();
			exit;

		}

		
	}


	function getpacket(){
		$uid=I("uid");
		$start=I("start");
		$end=I("end");
		$token=I("token");

		$Spend=M("users_rob_redpackets");

		//判断用户是否登录
		$userInfo=M("users")
		->where("id={$uid} and token='{$token}'")
		->select();

		if($userInfo){


				$spendLists=$Spend->query("select * from cmf_users_rob_redpackets where uid={$uid}  order by addtime desc");
				
				$count=count($spendLists);

				$User=M("users");

				$getList=$Spend->query("select * from cmf_users_rob_redpackets where uid={$uid}  order by addtime desc LIMIT ".$start.",".$end."");

				foreach ($getList as $k => $v) {
					$getList[$k]['liveUserName']=	$User->where("id='{$v['liveuid']}'")->getField("user_nicename");
					$getList[$k]['sendUserName']=	$User->where("id='{$v['senduid']}'")->getField("user_nicename");
				}



				$this->assign("getList",$getList);
				$this->assign("uid",$uid);
				$this->assign("start",$start);
				$this->assign("end",$end);
				$this->assign("count",$count);
				$this->assign("token",$token);

				$this->display();
				exit;
	

		}else{

			$this->assign("woringMsg","用户信息验证失败，请重新登录");
			$this->display();
			exit;

		}

		
	}


	function wingift(){
		$uid=I("uid");
		$start=I("start");
		$end=I("end");
		$token=I("token");

		$Spend=M("users_sendgift_win");

		//判断用户是否登录
		$userInfo=M("users")
		->where("id={$uid} and token='{$token}'")
		->select();

		if($userInfo){


				$spendLists=$Spend->query("select * from cmf_users_sendgift_win where uid={$uid}  order by addtime desc");
				
				$count=count($spendLists);

				$User=M("users");
				$Gift=M("gift");

				$winList=$Spend->query("select * from cmf_users_sendgift_win where uid={$uid}  order by addtime desc LIMIT ".$start.",".$end."");

				foreach ($winList as $k => $v) {
					$winList[$k]['liveUserName']=	$User->where("id='{$v['liveuid']}'")->getField("user_nicename");
					$winList[$k]['giftName']=	$Gift->where("id='{$v['giftid']}'")->getField("giftname");
				}



				$this->assign("winList",$winList);
				$this->assign("uid",$uid);
				$this->assign("start",$start);
				$this->assign("end",$end);
				$this->assign("count",$count);
				$this->assign("token",$token);

				$this->display();
				exit;
	

		}else{

			$this->assign("woringMsg","用户信息验证失败，请重新登录");
			$this->display();
			exit;

		}

		
	}


	function backpacket(){
		$uid=I("uid");
		$start=I("start");
		$end=I("end");
		$token=I("token");

		$Spend=M("redpcakets_back");

		//判断用户是否登录
		$userInfo=M("users")
		->where("id={$uid} and token='{$token}'")
		->select();

		if($userInfo){


				$spendLists=$Spend->query("select * from cmf_redpcakets_back where uid={$uid}  order by addtime desc");
				
				$count=count($spendLists);

				$User=M("users");
				$Redpacket=M("users_send_redpackets");

				$backList=$Spend->query("select * from cmf_redpcakets_back where uid={$uid}  order by addtime desc LIMIT ".$start.",".$end."");

				foreach ($backList as $k => $v) {
					$backList[$k]['packetInfo']=	$Redpacket->where("id='{$v['packet_id']}'")->find();
					$backList[$k]['liveUserName']=$User->where("id='{$backList[$k]['packetInfo']['roomid']}'")->getField("user_nicename");
				}

				

				$this->assign("backList",$backList);
				$this->assign("uid",$uid);
				$this->assign("start",$start);
				$this->assign("end",$end);
				$this->assign("count",$count);
				$this->assign("token",$token);

				$this->display();
				exit;
	

		}else{

			$this->assign("woringMsg","用户信息验证失败，请重新登录");
			$this->display();
			exit;

		}

		
	}



	
}