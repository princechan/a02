<?php

/**
 * 举报
 */
namespace Admin\Controller;
use Common\Controller\AdminbaseController;
class ReportController extends AdminbaseController {
    function index(){

					if($_REQUEST['status']!=''){
						  $map['status']=$_REQUEST['status'];
							$_GET['status']=$_REQUEST['status'];
					 }
				   if($_REQUEST['start_time']!=''){
						  $map['addtime']=array("gt",strtotime($_REQUEST['start_time']));
							$_GET['start_time']=$_REQUEST['start_time'];
					 }
					 
					 if($_REQUEST['end_time']!=''){
						 
						   $map['addtime']=array("lt",strtotime($_REQUEST['end_time']));
							 $_GET['end_time']=$_REQUEST['end_time'];
					 }
					 if($_REQUEST['start_time']!='' && $_REQUEST['end_time']!='' ){
						 
						 $map['addtime']=array("between",array(strtotime($_REQUEST['start_time']),strtotime($_REQUEST['end_time'])));
						 $_GET['start_time']=$_REQUEST['start_time'];
						 $_GET['end_time']=$_REQUEST['end_time'];
					 }
 
					 if($_REQUEST['keyword']!=''){
						 $map['uid']=array("like","%".$_REQUEST['keyword']."%"); 
						 $_GET['keyword']=$_REQUEST['keyword'];
					 }		
			
    	$Report=M("users_report");
    	$count=$Report->where($map)->count();
    	$page = $this->page($count, 20);
    	$lists = $Report
    	->where($map)
    	->order("addtime DESC")
    	->limit($page->firstRow . ',' . $page->listRows)
    	->select();
			
			foreach($lists as $k=>$v){
					$lists[$k]['userinfo']= M("users")->where("id='{$v[uid]}'")->find();
				  $lists[$k]['touserinfo']= M("users")->where("id='{$v[touid]}'")->find();
			}

			//var_dump($lists[0]['touserinfo']);
			
    	$this->assign('lists', $lists);
    	$this->assign('formget', $_GET);
    	$this->assign("page", $page->show('Admin'));
    	
    	$this->display();
    }
		
		function setstatus(){
			 	$id=intval($_GET['id']);
					if($id){
						 $data['status']=1;
						 $data['uptime']=time();
						$result=M("users_report")->where("id='{$id}'")->save($data);				
							if($result){
									$this->success('标记成功');
							 }else{
									$this->error('标记失败');
							 }			
					}else{				
						$this->error('数据传入失败！');
					}								  		
		}		
		
		function del(){
			 	$id=intval($_GET['id']);
					if($id){
						$result=M("users_report")->delete($id);				
							if($result){
									$this->success('删除成功');
							 }else{
									$this->error('删除失败');
							 }			
					}else{				
						$this->error('数据传入失败！');
					}								  
		}		

		
		function edit(){
			 	$touid=intval($_GET['id']);
					if($touid){
					$userinfo=M("users")->where("id={$touid}")->find();
					$this->assign("userinfo",$userinfo);
					$lists=M("users_disable_ips")->where("uid={$touid}")->select();
					$this->assign("lists",$lists);
					$this->assign("touid",$touid);


					}else{				
						$this->error('数据传入失败！');
					}								  
					$this->display();				
		}
		
		

		 function ban(){
    	$id=intval($_GET['id']);
    	if ($id) {
    		$rst = M("Users")->where(array("id"=>$id,"user_type"=>2))->setField('user_status','0');
    		if ($rst!==false) {
				$nowtime=time();
				$redis = connectionRedis();
				$time=$nowtime + 60*60*1;
				$live=M("users_live")->field("uid")->where("islive='1'")->select();
				foreach($live as $k=>$v){
					$redis -> hSet($v['uid'] . 'shutup',$id,$time);
				}
				$redis -> close();	
    			$this->success("会员拉黑成功！",U("Report/index"));
    		} else {
    			$this->error('会员拉黑失败！');
    		}
    	} else {
    		$this->error('数据传入失败！');
    	}
    }
    
    function cancelban(){ 
    	$id=intval($_GET['id']);
    	if ($id) {
    		$rst = M("Users")->where(array("id"=>$id,"user_type"=>2))->setField('user_status','1');
    		if ($rst!==false) {
    			$this->success("会员启用成功！");
    		} else {
    			$this->error('会员启用失败！');
    		}
    	} else {
    		$this->error('数据传入失败！');
    	}
    }  


     function sendimg(){ 
        $id=intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(array("id"=>$id,"user_type"=>2))->setField('issendimg','1');
            if ($rst!==false) {
                $this->success("成功开启发图功能！");
            } else {
                $this->error('开启发图功能失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    function cancelsendimg(){ 
        $id=intval($_GET['id']);
        if ($id) {
            $rst = M("Users")->where(array("id"=>$id,"user_type"=>2))->setField('issendimg','0');
            if ($rst!==false) {
                $this->success("成功关闭发图功能！");
            } else {
                $this->error('关闭发图功能失败！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }


    function delIps(){
    	$id=I("id");
    	$result=M("users_disable_ips")->where("id={$id}")->delete();
    	if($result!==false){
    		$data=array('code'=>0,'msg'=>'删除成功');
    	}else{
    		$data=array('code'=>1001,'msg'=>'删除失败');
    	}	

    	echo json_encode($data);
    	exit;
    }


    function addIps(){
    	$uid=I("uid");
    	$ipsStart=I("ipsStart");
    	$ipsEnd=I("ipsEnd");

    	$users_obj= M("Users");
    	$id=$_SESSION['ADMIN_ID'];
    	$user=$users_obj->where("id=$id")->find();

    	//判断当前IP是否已经被禁用
    	$info=M("users_disable_ips")->where("begin_ip='{$ipsStart}' and end_ip='{$ipsEnd}'")->find();

    	if($info){
    		$data=array(
    			'code'=>1002,
    			'msg'=>'该IP段已经被限制'
    		);
    		echo json_encode($data);
	    	exit;
    	}else{

    		$arr=array(
    		'uid'=>$uid,
    		'begin_ip'=>$ipsStart,
    		'end_ip'=>$ipsEnd,
    		'admin_name'=>$user['user_login'],
    		'addtime'=>time()

	    	);
	    	$rs=M("users_disable_ips")->add($arr);
	    	if($rs){
	    		$data=array('code'=>0,'msg'=>'添加成功');
	    	}else{
	    		$data=array('code'=>1001,'msg'=>'添加失败');
	    	}

	    	echo json_encode($data);
	    	exit;


    	}

    	
    }



    function addIP(){

    	$users_obj= M("Users");
    	$id=$_SESSION['ADMIN_ID'];
    	$user=$users_obj->where("id=$id")->find();

    	$uid=I("uid");

    	$currentIP=$_SERVER["REMOTE_ADDR"];

    	//判断当前IP是否已经被禁用
    	$info=M("users_disable_ips")->where("begin_ip='{$currentIP}'")->find();

    	if($info){
    		$data=array(
    			'code'=>1002,
    			'msg'=>'该IP已经被限制'
    		);
    		echo json_encode($data);
    		exit;	
    	}else{
    		$arr=array(
    		'uid'=>$uid,
    		'begin_ip'=>$currentIP,
    		'admin_name'=>$user['user_login'],
    		'addtime'=>time()
	    	);

	    	$rs=M("users_disable_ips")->add($arr);

	    	if($rs){
	    		$data=array('code'=>0,'msg'=>'添加成功');
	    	}else{
	    		$data=array('code'=>1001,'msg'=>'添加失败');
	    	}

	    	echo json_encode($data);
	    	exit;	
    	}

    	

    }


    
}
