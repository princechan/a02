<?php

namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Activity\Activity;
use Common\Lib\Helpers\Func;

class ActivityController extends AdminbaseController
{
    const ONE_MONTH = 3600*24*30;
    /**
     * 活动管理
     */
    public function index()
    {
        $map = [];
        if ($_REQUEST['name'] != '') {
            $map['name'] = ["like", "%" . $_REQUEST['name'] . "%"];
            $_GET['name'] = $_REQUEST['name'];
        }

        $count = Activity::getInstance()->getCount($map);
        $page = $this->page($count, 20);
        $list = Activity::getInstance()->getAdminList($map, $page->firstRow, $page->listRows) ?: [];

        $this->assign([
            'list' => $list,
            'formget' => $_GET,
            'page' => $page->show('Admin'),
        ]);

        $this->display();
    }

    /**
     * 添加/编辑
     * @return mixed
     */
    public function edit()
    {
        if (IS_POST) {
            $id = (int)$this->getPost('id');
            $name = trim($this->getPost('name'));
            $start_at = trim($this->getPost('start_at'));
            $question_list_id = (int)$this->getPost('question_list_id');
            $base_bonus_amount = (float)$this->getPost('base_bonus_amount');
            $extra_bonus_type = (int)$this->getPost('extra_bonus_type');
            $extra_bonus_amount = (float)$this->getPost('extra_bonus_amount');
            $extra_bonus_by_ranking = trim($this->getPost('extra_bonus_by_ranking'));
            $desct = trim($this->getPost('desct'));
            $remind = (int)$this->getPost('remind');
            $live_id = (int)$this->getPost('live_id');
            $location_live_id = trim($this->getPost('location_live_id'));
            $countdown_at = (int)$this->getPost('countdown_at');
            $answer_countdown_at = (int)$this->getPost('answer_countdown_at');
            $status = (int)$this->getPost('status');
            $icon = trim($this->getPost('icon'));

            if (!$name || !$start_at || !$question_list_id || !$base_bonus_amount || !$desct || !$live_id || !$countdown_at || !$answer_countdown_at) {
                return $this->renderError('有未填信息');
            }

            ###针对A02项目业务做的区间判断 start
            $join_requirements = $this->getPost('join_requirements');
            if(isset($join_requirements['cash_flow_limit_type']) && $join_requirements['cash_flow_limit_type']){
                $cash_start_at = $cash_end_at = 0;
                if(isset($join_requirements['cash_flow_limit_begintime']) && $join_requirements['cash_flow_limit_begintime']){
                    $cash_start_at = strtotime($join_requirements['cash_flow_limit_begintime']);
                }
                if(isset($join_requirements['cash_flow_limit_endtime']) && $join_requirements['cash_flow_limit_endtime']){
                    $cash_end_at = strtotime($join_requirements['cash_flow_limit_endtime']);
                }
                if(!((self::ONE_MONTH > ($cash_end_at-$cash_start_at)) && (0 < ($cash_end_at-$cash_start_at)))){
                    return $this->renderError('流水查询区间必须在一个月内');
                }
            }

            if(isset($join_requirements['recharge_limit_type']) && $join_requirements['recharge_limit_type']){
                $recharge_start_at = $recharge_end_at = 0;
                if(isset($join_requirements['recharge_limit_begintime']) && $join_requirements['recharge_limit_begintime']){
                    $recharge_start_at = strtotime($join_requirements['recharge_limit_begintime']);
                }
                if(isset($join_requirements['recharge_limit_endtime']) && $join_requirements['recharge_limit_endtime']){
                    $recharge_end_at = strtotime($join_requirements['recharge_limit_endtime']);
                }
                if(!((self::ONE_MONTH > ($recharge_end_at-$recharge_start_at)) && (0 < ($recharge_end_at-$recharge_start_at)))){
                    return $this->renderError('充值查询区间必须在一个月内');
                }
            }
            ###针对A02项目业务做的区间判断 end

            $data = [
                'id' => $id,
                'name' => $name,
                'start_at' => strtotime($start_at),
                'question_list_id' => $question_list_id,
                'base_bonus_amount' => $base_bonus_amount,
                'extra_bonus_type' => $extra_bonus_type,
                'extra_bonus_amount' => $extra_bonus_amount,
                'extra_bonus_by_ranking' => implode(',', explode(PHP_EOL, $extra_bonus_by_ranking)),
                'desct' => $desct,
                'remind' => $remind,
                'live_id' => $live_id,
                'location_live_id' => $location_live_id,
                'countdown_at' => $countdown_at,
                'answer_countdown_at' => $answer_countdown_at,
                'status' => $status,
                'icon' => $icon,
                'admin_id' => $this->adminId,
                'join_requirements' => json_encode((array)$this->getPost('join_requirements')),
            ];

            $rs = Activity::getInstance()->saveActivity($data);
            if (false === $rs) {
                return $this->renderError('保存失败');
            }

            return $this->renderObject(Activity::getInstance()->getActivityBaseInfo($id));
        }

        $id = (int)I('id');
        $data = Activity::getInstance()->getActivity($id);
        if ($data) {
            $data['start_at'] = date('Y-m-d H:i:s', $data['start_at']);
            $data['extra_bonus_by_ranking'] = str_replace(',', PHP_EOL, $data['extra_bonus_by_ranking']);
        }

        $this->assign([
            'data' => $data,
            'statusMap' => Activity::$statusMap,
            'questionList' => Activity::getInstance()->getQuestionList(),
            'config' => Func::getPrivateConfig(),
        ]);

        $this->display();
    }

    /**
     * 删除活动
     */
    public function delete()
    {
        $id = (int)$this->getGet('id');

        $rs = Activity::getInstance()->delActivity($id);

        if (false === $rs) {
            $this->error(Activity::getInstance()->getLastErrMsg());
        } else {
            $this->success('删除成功！');
        }
    }

    /**
     * 答题记录
     */
    public function activityAnswerLogs()
    {
        $id = (int)$this->getGet('id');
        $rs = Activity::getInstance()->getAnswerLogs($id);

        $this->assign('data', $rs);
        $this->display('answer_logs');
    }

    /**
     * 导出排行榜
     */
    public function export()
    {
        $activity_id = (int)$this->getGet('activity_id');
        $question_id = (int)$this->getGet('question_id');
        $type        = $this->getGet('type');

        if(!$activity_id || !$type){
            $this->error('非法请求, 参数为空');
        }

        if( !in_array($type, ['rank', 'question'])){
            $this->error('非法请求, 类型错误');
        }

        $data = [];
        $lib = Activity::getInstance();

        if('rank' == $type){
            $data = $lib->getAnswerLogsRank($activity_id);
            if(!$data){
                $this->error('无数据');
            }

            //排行榜导出
            $xlsName = "排行榜";
            $xlsData = $data;
            $cellName = ['A', 'B', 'C', 'D', 'E'];
            $xlsCell = [
                ['rank', '名次'],
                ['user_nicename', '昵称'],
                ['time', '答题用时'],
                ['base_bonus_amount', '平分奖金'],
                ['ranking_bonus_amount', '加奖奖金'],
            ];
        }elseif('question' == $type){
            if(!$question_id){
                $this->error('非法请求, 参数为空');
            }

            $data = $lib->getPrizeRankAll($activity_id, $question_id);
            if(!$data){
                $this->error('无数据');
            }
            //问题导出
            $xlsName = '第' . $question_id . '题排行榜';
            $xlsData = $data;
            $cellName = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
            $xlsCell = [
                ['rank', '名次'],
                ['user_nickname', '昵称'],
                ['score', '答题用时'],
                ['status', '状态'],
            ];
        }
        exportExcel($xlsName, $xlsCell, $xlsData, $cellName);
    }

    /**
     * 获取某道题玩家的答题日志
     */
    public function activityAnswerUserLog()
    {
        $activity_id = $this->getPost('activity_id');
        $question_id = $this->getPost('question_id');

        $lib = Activity::getInstance();
        $rs  = $lib->getAnswerUserLogs($activity_id, $question_id);

        if(false == $rs){
            return $this->renderError($lib->getLastErrMsg());
        }

        $this->renderArray((array)$rs);
    }

    /**
     * 活动进行中控制答题正确某个玩家是否答题正确的展示
     * @return mixed
     */
    public function filterPlayerStatus()
    {
        $activity_id = (int)$this->getPost('activity_id');
        $question_id = (int)$this->getPost('question_id');
        $uid         = (int)$this->getPost('uid');
        $type        = $this->getPost('type');

        $lib = Activity::getInstance();
        $rs = $lib->filterPlayerStatus($activity_id, $question_id, $uid, $type);

        if(false == $rs){
            return $this->renderError($lib->getLastErrMsg());
        }
        $this->renderArray('成功');
    }
}
