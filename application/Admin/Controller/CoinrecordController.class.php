<?php

/**
 * 消费记录
 */
namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\Users\UsersCoinrecord;
use Common\Lib\Helpers\Func;


class CoinrecordController extends AdminbaseController {

    function index(){

        $type       = $this->getPost('type');
        $action     = $this->getPost('action');
        $spend_type = $this->getPost('spend_type');
        $start_time = $this->getPost('start_time');
        $end_time   = $this->getPost('end_time');
        $uid        = $this->getPost('uid');
        $to_uid     = $this->getPost('touid');
        $is_export  = $this->getPost('is_export');

        $form_get = $map = [];
        if($type){
            $form_get['type'] = $map['type'] = $type;
        }
        if($action){
            $form_get['action'] = $map['action'] = $action;
        }
        if($spend_type){
            $form_get['spend_type'] = $map['spend_type'] = $spend_type;
        }
        if($start_time){
            $map['addtime']         = ["gt", strtotime($start_time)];
            $form_get['start_time'] = $start_time;
        }
        if($end_time){
            $map['addtime']         = ["lt", strtotime($end_time)];
            $form_get['end_time'] = $end_time;
        }
        if($start_time && $end_time){
            $map['addtime']         = ["between", [strtotime($start_time),strtotime($end_time)]];
            $form_get['start_time'] = $start_time;
            $form_get['end_time']   = $end_time;
        }
        if($uid){
            $form_get['uid'] = $map['uid'] = $uid;
        }
        if($to_uid){
            $form_get['touid'] = $map['touid'] = $to_uid;
        }

        //收支类型
        $type = [
            "income" => "收入",
            "expend" => "支出"
        ];
        //货币类型
        $spend_type = [
            1 => 'CNY/钻石',
            2 => '直播券',
        ];
        //收支行为
        $action = [
            'loginbonus'    => '登录奖励',
            'roomcharge'    => '付费房间',
            'sendgift'      => '赠送礼物',
            'buyguard'      => '购买守护',
            'sendbarrage'   => '发送弹幕',
            'sendbighorn'   => '购买喇叭',
        ];

        if($is_export){
            $this->export($map);
        }

        $rs = UsersCoinrecord::getInstance()->getPageList($map, 40, 'Admin');

    	$this->assign('type', $type);
    	$this->assign('spend_type', $spend_type);
    	$this->assign('action', $action);
    	$this->assign('rs', $rs);
    	$this->assign('formget', $form_get);
    	$this->display();
    }

    /**
     * 导出数据到excel
     */
    private function export($map)
    {
        $data = UsersCoinrecord::getInstance()->export($map);

        $xls_name  = "玩家消费记录";
        $xls_data  = $data['lists'];
        $cell_name = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
        $xls_cell  = [
            ['id', 'ID'],
            ['type', '收支类型'],
            ['spend_type', '货币类型'],
            ['action', '收支行为'],
            ['userinfo', '会员 (ID)'],
            ['touserinfo', '主播 (ID)'],
            ['giftinfo', '礼物 (ID)'],
            ['giftcount', '礼物数量'],
            ['total_price', '礼物总价'],
            ['showid', '直播id'],
            ['addtime', '赠送时间'],
        ];

        //拓展参数
        $extend = [];
        if($data['total_spend'] || $data['single_spend']){
            //拓展
            $extend = [
                [
                    'key' => '全部消费总额',
                    'val' => $data['total_spend']
                ]
            ];
        }

        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        Func::exportExcel($xls_name, $xls_cell, $xls_data, $cell_name, $extend);
    }
		
    function del(){
            $id=intval($_GET['id']);
                if($id){
                    $result=M("users_coinrecord")->delete($id);
                        if($result){
                                $this->success('删除成功');
                         }else{
                                $this->error('删除失败');
                         }
                }else{
                    $this->error('数据传入失败！');
                }
                $this->display();
    }

    	
}
