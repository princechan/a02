<?php

/**
 * 系统消息
 */
namespace Admin\Controller;

use Common\Controller\AdminbaseController;
use Common\Lib\System\LiveSysMsg;

class SystemController extends AdminbaseController
{
    /**
     * 系统消息编辑
     */
    function index(){

        $config=M("config_private")->where("id='1'")->find();
        $this->assign('config', $config);
        $this->display("edit");
    }

    /**
     * 发送系统消息(单条系统消息)
     */
    function send()
    {
        $content=I("content");

        if(!$content){
            $data=array(
                "error"=>10001,
                "data"=>'',
                "msg"=>'内容不能为空'
            );
        }

        /*
        系统消息日志
        $id = $this->admin_id;
        日志入库
        */

        $data=array(
            "error"=>0,
            "data"=>'',
            "msg"=>'success!'
        );
        echo json_encode($data);
    }


    /**
     * 公告管理(多条系统消息)---定时发送广播
     */
    public function sysLiveMsgList()
    {
        $ret_arr = LiveSysMsg::getInstance()->getList();
        $lists   = $ret_arr['lists'] ? $ret_arr['lists'] : [];

        $this->assign("lists", $lists);
        $this->assign("page", $ret_arr['page']);
        $this->display();
    }

    /**
     * 创建公告
     */
    public function setSysLiveMsg()
    {

        if(IS_POST){
            $id             = $this->getPost('id');
            $live_room_ids  = $this->getPost('live_room_id');
            $content        = $this->getPost('content');
            $link_url       = $this->getPost('link_url');
            $start_at       = $this->getPost('start_at');
            $end_at         = $this->getPost('end_at');
            $frequency      = $this->getPost('frequency');

            if(!$live_room_ids){
                $this->renderError('直播间ID不能为空');
            }
            if(!$start_at){
                $this->renderError('请设置开始时间');
            }
            if(!$frequency){
                $this->renderError('请设置发送频率');
            }

            $time = time();
            if($live_room_ids){
                $live_room_ids = json_encode(array_unique(array_filter(explode(',', $live_room_ids))), JSON_UNESCAPED_UNICODE);
            }
            $start_at = $start_at ? strtotime($start_at) : 0;
            $end_at   = $end_at ? strtotime($end_at) : 0;
            $link_url = $link_url ? trim($link_url) : '';


            $arr = [
                'live_room_id'  => $live_room_ids,
                'content'       => $content,
                'link_url'      => $link_url,
                'start_at'      => $start_at,
                'end_at'        => $end_at,
                'frequency'     => $frequency,
                'admin_id'      => $this->admin_id,
                'updated_at'    => $time,
            ];

            if(!$id){
                $arr['created_at'] = $time;
            }

            $rs = LiveSysMsg::getInstance()->save($arr, $id, true);
            return $rs ? $this->renderArray('保存成功') : $this->renderError('保存失败');
        }else{

            $id     = $this->getGet('id');
            $info   = LiveSysMsg::getInstance()->getInfo($id);

            $this->assign('data', $info);
            $this->display();
        }
    }

    /**
     * 删除公告(单条)
     */
    public function delSysLiveMsgSingle()
    {
        $id = $this->getGet("id");

        $lib = LiveSysMsg::getInstance();
        $rs  = $lib->delSysLiveMsgSingle($id);
        if(false == $rs){
            $this->error($lib->getLastErrMsg());
        }
        $this->success('删除成功');
    }

    /**
     * 删除公告(多条)
     */
    public function delSysLiveMsg()
    {
        $ids = $this->getPost("ids");
        $lib = LiveSysMsg::getInstance();
        $rs  = $lib->delSysLiveMsg($ids);
        if(false == $rs){
            $this->renderError($lib->getLastErrMsg());
        }
        $this->renderArray('success!');
    }

    /**
     * 脚本控制
     */
    public function setStatus()
    {
        $id     = (int)$this->getPost('id');
        $status = trim($this->getPost('status'));

        $rs = LiveSysMsg::getInstance()->setStartOrEnd($id, $status, true);
        if(false == $rs){
            $this->renderError(LiveSysMsg::getInstance()->getLastErrMsg());
        }
        $this->renderArray('success!');
    }




}