<?php
namespace Admin\Controller;
use Common\Controller\AdminbaseController;
use Common\Lib\Users\User;

class UservirtualController extends AdminbaseController{
	protected $users_model,$usersvirtual_model;
 
	function _initialize() {
		parent::_initialize();
		$this->users_model = D("Common/Users");
		$this->usersvirtual_model = D("Common/users_virtual");
	}
	function index(){
		$map=array();
			$map['user_type']=2;
			$map['isvirtual']=1;
			 if($_REQUEST['start_time']!=''){
				$map['create_time']=array("gt",$_REQUEST['start_time']);
				$_GET['start_time']=$_REQUEST['start_time'];
			 }
			 
			 if($_REQUEST['end_time']!=''){
				$map['create_time']=array("lt",$_REQUEST['end_time']);
				$_GET['end_time']=$_REQUEST['end_time'];
			 }
			 if($_REQUEST['start_time']!='' && $_REQUEST['end_time']!='' ){
				$map['create_time']=array("between",array($_REQUEST['start_time'],$_REQUEST['end_time']));
				$_GET['start_time']=$_REQUEST['start_time'];
				$_GET['end_time']=$_REQUEST['end_time'];
			 }

			 if($_REQUEST['keyword']!=''){
				$where['id|user_nicename']	=array("like","%".$_REQUEST['keyword']."%");
				$where['_logic']	="or";
				$map['_complex']=$where;
				
				$_GET['keyword']=$_REQUEST['keyword'];
			 }
			

    	$users_model=$this->users_model;
    	$count=$users_model->where($map)->count();
    	$page = $this->page($count, 20);
    	$lists = $users_model
    	->where($map)
    	->order("id DESC")
    	->limit($page->firstRow . ',' . $page->listRows)
    	->select();

			
    	$this->assign('lists', $lists);
    	$this->assign('formget', $_GET);
    	$this->assign("page", $page->show("Admin"));
    	
    	$this->display();
	}
	
	function add(){
		$this->display();				
	}
	
	function add_post(){
		if(IS_POST){			
			$user=$this->users_model;
			if( $user->create()){
                $user->user_login = User::getInstance()->encryptMD5($this->getPost('user_login'));
				$user->user_type=2;
				$user->user_status=1;
				$user->isvirtual=1;
				$user->user_pass=sp_password($_POST['user_pass']);

				if($_POST['user_nicename']==""){
					$this->error('请填写昵称');
				}

				$user->user_nicename=$_POST['user_nicename'];

				if($_POST['sex']==""){
					$user->sex=1;
				}else{
					$user->sex=$_POST['sex'];
				}

				$avatar=$_POST['avatar'];

				if($avatar==''){
					$user->avatar= '/default.jpg'; 
					$user->avatar_thumb= '/default_thumb.jpg'; 
				}else if(strpos($avatar,'http')===0){
					/* 绝对路径 */
					$user->avatar=  $avatar; 
					$user->avatar_thumb=  $avatar;
				}else if(strpos($avatar,'/')===0){
					/* 本地图片 */
					$user->avatar=  $avatar;
					$user->avatar_thumb=  $avatar; 
				}else{
					/* 七牛 */
					//$user->avatar=  $avatar.'?imageView2/2/w/600/h/600'; //600 X 600
					//$user->avatar_thumb=  $avatar.'?imageView2/2/w/200/h/200'; // 200 X 200
				}


				$user->signature=$_POST['signature'];


				$user->last_login_time=date('Y-m-d H:i:s',time());
				$user->create_time=date('Y-m-d H:i:s',time());
				$result=$user->add();
				if($result!==false){
					$this->success('添加成功');
				}else{
					$this->error('添加失败');
				}					 
				 
			}else{
				$this->error($this->users_model->getError());
			}
			//echo $_GET['callback']."({'status':0,'data':{},'info':''})";
			exit;	

		}			
	}		
		function edit(){
			 	$id=intval($_GET['id']);
					if($id){
						$userinfo=M("users")->find($id);
						$this->assign('userinfo', $userinfo);						
					}else{				
						$this->error('数据传入失败！');
					}								  
					$this->display();				
		}
		
		function edit_post(){
			if(IS_POST){			
				$user=M("users");
				$user->create();
				$avatar=$_POST['avatar'];
				
				if($avatar==''){
					$user->avatar= '/default.jpg'; 
					$user->avatar_thumb= '/default_thumb.jpg'; 
				}else if(strpos($avatar,'http')===0){
					/* 绝对路径 */
					$user->avatar=  $avatar; 
					$user->avatar_thumb=  $avatar;
				}else if(strpos($avatar,'/')===0){
					/* 本地图片 */
					$user->avatar=  $avatar; 
					$user->avatar_thumb=  $avatar; 
				}else{
					/* 七牛 */
					//$user->avatar=  $avatar.'?imageView2/2/w/600/h/600'; //600 X 600
					//$user->avatar_thumb=  $avatar.'?imageView2/2/w/200/h/200'; // 200 X 200
				}
				 $result=$user->save(); 
				 if($result!==false){
					  $this->success('修改成功');
				 }else{
					  $this->error('修改失败');
				 }
			}			
		}
	//批量导入
	function importuser(){
		$this->display();				
	}
	
	public function excel_import() { 
		   
		$file=$_FILES['file'];

		if (IS_POST) {
			$savepath=date('Ymd').'/';
						//上传处理类
			$config=array(
					'rootPath' => './'.C("UPLOADPATH"),
					'savePath' => $savepath,
					'maxSize' => 11048576,
					'saveName'   =>    array('uniqid',''),
					'exts'       =>    array('xls', 'xlsx'),
					'autoSub'    =>    false,
			);
			$upload = new \Think\Upload($config);// 
			$info=$upload->upload();
		
			//开始上传
			if ($info) {
					//上传成功
					//写入附件数据库信息
					$first=array_shift($info);
					if(!empty($first['url'])){
						$url=$first['url'];
						
					}else{
						$url=C("TMPL_PARSE_STRING.__UPLOAD__").$savepath.$first['savename'];
							
					}

				Vendor("PHPExcel.PHPExcel.IOFactory");
		
				if(substr($url,-5)==".xlsx"){
					$objReader = \PHPExcel_IOFactory::createReader('Excel2007');

				}else{
					$objReader = \PHPExcel_IOFactory::createReader('Excel5');

				}
			
				//$objReader = PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format
				$objPHPExcel = $objReader->load('./'.C("UPLOADPATH").$savepath.$first['savename']); //$filename可以是上传的文件，或者是指定的文件
		
				$sheet = $objPHPExcel->getSheet(0);
				$highestRow = $sheet->getHighestRow(); // 取得总行数
				$highestColumn = $sheet->getHighestColumn(); // 取得总列数
					 

				$data=array();
				$time=time();
				//循环读取excel文件,读取一条,插入一条
				for($num=2,$isi=0,$isn=0;$num<=$highestRow;$num++)
				{
					$user_login = $objPHPExcel->getActiveSheet()->getCell("A".$num)->getValue();//获取A列的值:登录名称
					$user_pass = $objPHPExcel->getActiveSheet()->getCell("B".$num)->getValue();//获取B列的值：登录密码// "123456"
					$avatar = $objPHPExcel->getActiveSheet()->getCell("C".$num)->getValue();//获取C列的值：头像
					$sex = $objPHPExcel->getActiveSheet()->getCell("D".$num)->getValue();//获取D列的值：性别
					$user_status = $objPHPExcel->getActiveSheet()->getCell("E".$num)->getValue();//获取E列的值：用户状态 0：禁用； 1：正常 ；2：未验证
					$user_type = $objPHPExcel->getActiveSheet()->getCell("F".$num)->getValue();//获取F列的值：用户类型，1:admin ;2:会员
					
					
					$userinfo=array(
						'user_login'=>User::getInstance()->encryptMD5($user_login),
						'user_pass'=>sp_password($user_pass),
						'user_nicename'=>"user_".substr($user_login,-4),//$time.$user_login,//昵称
						'avatar'=>$avatar,
						'sex'=>$sex,
						'user_status'=>$user_status,
						'user_type'=>$user_type,
						'create_time'=>date("Y-m-d H:i:s",$time),
						'isvirtual'=>1,
					);
					 $isexist=M("users")->where("user_login='$user_login'")->find();
					 
					 if(!$isexist){
						 $data[$isi]=$userinfo;
						 $isi++;
					 }else{
						 $isn++;
					 }	 
				}
				$result=M("users")->addAll($data);			
				if ($result) {
						$this->success("导入成功！有{$isn}条重复信息未导入");
				} else {
						$this->error("导入失败！所有手机号已存在");
				}
			} else {
					//上传失败，返回错误
					$this->error($upload->getError());
			}
		} 		 
			
	}
	function del(){
    	$id=intval($_GET['id']);
    	if ($id) {
    		$rst = M("Users")->where(array("id"=>$id,"user_type"=>2))->delete();
    		if ($rst!==false) {
					/* 删除直播记录 */
					M("users_liverecord")->where("uid='{$id}'")->delete();
					/* 删除房间管理员 */
					M("users_livemanager")->where("uid='{$id}' or liveuid='{$id}'")->delete();
					/* 删除兑换记录 */
					M("users_exchange")->where("uid='{$id}'")->delete();
					/* 删除消费记录 */
					M("users_coinrecord")->where("uid='{$id}' or touid='{$id}'")->delete();
					/*  删除黑名单*/
					M("users_black")->where("uid='{$id}' or touid='{$id}'")->delete();
					/* 删除关注记录 */
					M("users_attention")->where("uid='{$id}' or touid='{$id}'")->delete();
					/* 删除手动充值记录 */
					M("users_charge_admin")->where("touid='{$id}'")->delete();
					/* 删除举报记录记录 */
					M("users_report")->where("uid='{$id}' or touid='{$id}'")->delete();
					/* 删除反馈记录 */
					M("feedback")->where("uid='{$id}'")->delete();
					/* 删除僵尸 */
					M("users_zombie")->where("uid='{$id}'")->delete();
					/* 删除超管 */
					M("users_super")->where("uid='{$id}'")->delete();
					/*删除虚拟用户列表*/
					M("users_virtual")->where("uid='{$id}' or virtualid='{$id}'")->delete();
			
    			$this->success("会员删除成功！");
    		} else {
    			$this->error('会员删除失败！');
    		}
    	} else {
    		$this->error('数据传入失败！');
    	}
    }
	//设置僵尸粉
	function setfans(){

		$virtualid=intval($_GET['id']);
		$virtualname=intval($_GET['user_nicename']);
		if($virtualid){
			
			$oldvirtual = M("users_virtual")->where("virtualid=".$virtualid)->find();
			
			if($oldvirtual){
				$userinfolive=M("users_live")->field("uid,user_nicename")->where("uid=".$oldvirtual['uid'])->find();
			}
			$this->assign('userinfolive', $userinfolive);						
			$this->assign('virtualid', $virtualid);						
			$this->assign('virtualname', $virtualname);						
			$this->assign('oldvirtual', $oldvirtual);						
		}else{				
			$this->error('数据传入失败！');
		}						
		$config=M("config_private")->where("id='1'")->find();
		$this->assign('config', $config);
					
		$this->display();
	}
	function setfans_post(){

		$virtualid=I('virtualid');

		$virtualname=I('virtualname');
		$uid=I('userid');//直播间id

		$userinfolive=M("users_live")->where("islive=1 and uid='{$uid}'")->find();

		//file_put_contents("a.txt", var_export($userinfolive,true));

		//僵尸粉用户信息
		$userinfo=getUserInfo($virtualid);

		$userinfo['avatar']=get_upload_path($userinfo['avatar']);//用户头像匹配地址

		$rs=array('status'=> 0,'info'=>'','stream'=>'',	'data'=>$virtualname,'sign'=>'','id'=>$virtualid,'uid'=>$uid,'virtualinfo'=>$userinfo);

		if($userinfolive){

			$rs['stream']=$userinfolive['stream'];
			$rs['sign']=md5($uid.'_'.$virtualid);
			 $id=I('id');
			 if($id){
				 $oldusersvirtual=M("users_virtual");
				 $oldusersvirtual->uid= $uid;

				 $data['uid'] =$uid;
				 $data['virtualid'] = $virtualid;

				 $result= $oldusersvirtual->where('id='.$id)->save($data); // 根据条件更新记录

				 if($result!==false){
					$rs['status']=0;
					$rs['info']='更新设置成功';
					//将用户存入redis中
					$redis = connectionRedis();
					
					$redis-> hSet('userlist_'.$userinfolive['stream'],md5($uid.'_'.$virtualid),json_encode($userinfo));
					$arr=$redis-> hGet('userlist_'.$userinfolive['stream'],md5($uid.'_'.$virtualid));




				 }else{
					$rs['status']=1;
					$rs['info']='更新设置失败';					
				 } 

			 }else{

				$usersvirtual=$this->usersvirtual_model;

				$data['uid'] =$uid;
				$data['virtualid'] = $virtualid;

				$result=$usersvirtual->data($data)->add();
				if($result!==false){
					$rs['status']=0;
					$rs['info']='设置成功';

				}else{	
					$rs['status']=1;
					$rs['info']='设置失败';
				}
			 } 
		}else{
			$rs['status']=1;
			$rs['info']='当前用户未开启直播';
		}
		echo  json_encode($rs);
	}




	 /* redis链接 */
    function connectionRedis(){
        $REDIS_HOST= C('REDIS_HOST');
        $REDIS_AUTH= C('REDIS_AUTH');
        $REDIS_PORT= C('REDIS_PORT');
        $redis = new \Redis();
        $redis -> pconnect($REDIS_HOST,$REDIS_PORT);
        $redis -> auth($REDIS_AUTH);

        return $redis;
    }
}