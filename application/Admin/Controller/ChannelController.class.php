<?php

/**
 * 频道
 */

namespace Admin\Controller;

use Common\Controller\AdminbaseController;

class ChannelController extends AdminbaseController
{
    function index()
    {
        $channel = M("channel");
        $count = $channel->count();
        $page = $this->page($count, 20);
        $lists = $channel
            ->where()
            ->order("orderno asc")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();
        $this->assign('lists', $lists);
        $this->assign("page", $page->show('Admin'));

        $this->display();
    }

    function del()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $result = M("channel")->delete($id);
            if ($result) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    public function listorders()
    {
        $ids = $_POST['listorders'];
        foreach ($ids as $key => $r) {
            $data['orderno'] = $r;
            M("channel")->where(['id' => $key])->save($data);
        }

        $status = true;
        if ($status) {
            $this->success("排序更新成功！");
        } else {
            $this->error("排序更新失败！");
        }
    }

    function add()
    {
        $this->display();
    }

    function add_post()
    {
        if (IS_POST) {
            $channel = M("channel");
            $liveid = $_POST['liveid'];
            $isexist = $channel->where("liveid='{$liveid}'")->find();
            if ($isexist) {
                $this->error('该直播间已存在');
            }
            $channel->create();
            $result = $channel->add();
            if ($result) {
                $this->success('添加成功');
            } else {
                $this->error('添加失败');
            }
        }
    }

    function edit()
    {
        $id = intval($_GET['id']);
        if ($id) {
            $channel = M("channel")->find($id);
            $this->assign('channel', $channel);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    function edit_post()
    {
        if (IS_POST) {
            $channel = M("channel");
            $id = $_POST['id'];
            $liveid = $_POST['liveid'];
            $isexist = $channel->where("liveid='{$liveid}' and id!={$id}")->find();
            if ($isexist) {
                $this->error('该直播间已存在');
            }
            $channel->create();
            $result = $channel->save();
            if ($result !== false) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }
    }

    /**
     * 直播切换列表
     */
    public function liveSwitch()
    {
        $liveId = (int)I('liveid');
        if (!$liveId) {
            $this->error('缺liveid参数');
        }

        $count = M('live_switch')->count();
        $page = $this->page($count, 20);
        $lists = M('live_switch')->where(['liveid' => $liveId])->order("orderno asc")
            ->limit($page->firstRow . ',' . $page->listRows)
            ->select();

        $this->assign([
            'liveId' => $liveId,
            'lists' => $lists,
            "page" => $page->show('Admin'),
        ]);

        $this->display();
    }

    public function addLiveSwitch()
    {
        if (IS_POST) {
            $liveId = (int)I('liveid');
            $childLiveId = (int)I('child_liveid');
            $orderno = (int)I('orderno');
            if (!$liveId || !$childLiveId || !$orderno) {
                $this->error('信息未填写完整');
            }

            $switch = M('live_switch');
            if ($switch->where(['liveid' => $liveId, 'child_liveid' => $childLiveId])->find()) {
                $this->error('该直播间已存在');
            }

            $switch->create();
            $result = $switch->add();
            if ($result) {
                $this->success('添加成功');
            } else {
                $this->error('添加失败');
            }
        }

        $liveId = (int)I('liveid');
        if (!$liveId) {
            $this->error('缺liveid参数');
        }

        $this->assign([
            'liveId' => $liveId,
        ]);

        $this->display();
    }

    public function editLiveSwitch()
    {
        if (IS_POST) {
            $id = (int)I('id');
            $liveId = (int)I('liveid');
            $childLiveId = (int)I('child_liveid');
            $orderno = (int)I('orderno');
            if (!$id || !$liveId || !$childLiveId || !$orderno) {
                $this->error('信息未填写完整');
            }

            $channel = M('live_switch');
            $isexist = $channel->where("liveid='{$liveId}' and child_liveid='{$childLiveId}' and id!={$id}")->find();
            if ($isexist) {
                $this->error('该直播间已存在');
            }

            $channel->create();
            $result = $channel->save();
            if ($result !== false) {
                $this->success('修改成功');
            } else {
                $this->error('修改失败');
            }
        }

        $id = intval($_GET['id']);
        if ($id) {
            $channel = M('live_switch')->find($id);
            $this->assign([
                'channel' => $channel,
                'liveId' => $channel['liveid'],
            ]);
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    public function delLiveSwitch()
    {
        $id = (int)I('id');
        if ($id) {
            $result = M('live_switch')->delete($id);
            if ($result) {
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('数据传入失败！');
        }
        $this->display();
    }

    public function editLiveSwitchOrder()
    {
        $ids = $_POST['listorders'];
        $status = false;
        foreach ($ids as $key => $value) {
            M('live_switch')->where(['id' => $key])->save(['orderno' => $value]);
            $status = true;
        }

        if ($status) {
            $this->success("排序更新成功！");
        } else {
            $this->error("排序更新失败！");
        }
    }

}
