<?php
/**
 * 直播切换业务逻辑类
 */
namespace Common\Lib\Live;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class LiveSwitch extends Service
{
    /**
     * 获取列表
     */
    public function getList($ids=[])
    {
        if($ids){
            if(!is_array($ids)){
                $ids = (array)$ids;
            }

            $live_switch_list = M('live_switch')->where(['liveid' => ['in', $ids]])->order('orderno')->select() ?: [];
        }else{
            $live_switch_list = M('live_switch')->order('orderno')->select() ?: [];
        }
        return $live_switch_list ? Func::index($live_switch_list, 'id') : [];
    }

    /**
     * 获取单个信息
     */
    public function getInfo($id)
    {
        $lists = $this->getList($id);
        return $lists ? $lists[$id] : [];
    }






}