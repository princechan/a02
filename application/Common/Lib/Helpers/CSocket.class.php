<?php

namespace Common\Lib\Helpers;

use ElephantIO\Client, ElephantIO\Engine\SocketIO\Version2X;
require SITE_PATH . 'vendor/autoload.php';
set_time_limit(0);

class CSocket
{
    const NODE_DEFAULT = 1;
    private static $node;
    private $client;

    public function __construct($host)
    {
        $this->client = new Client(new Version2X($host, [
            'params' => [
                "verify_peer"       => false,
                "verify_peer_name"  => false,
            ],
        //'header' => [],
        ]
        ));
        return $this->client->initialize();
    }

    public function emit($action, $data)
    {
        return $this->client->emit($action, $data);
    }

    public function __destruct()
    {
        return $this->client->close();
    }


}


