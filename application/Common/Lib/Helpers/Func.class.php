<?php
/**
 * Created by PhpStorm.
 * User: kobi.h
 */

namespace Common\Lib\Helpers;

class Func
{
    const KEY_PUBLIC_CONFIG = 'publicConfig';
    const KEY_PRIVATE_CONFIG = 'privateConfig';

    /**
     * 返回带协议的域名
     */
    public static function getHost()
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol = is_ssl() ? "https://" : "http://";
        return $protocol . $host;
    }

    public static function getRequestURL()
    {
        return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .
            $_SERVER['DOCUMENT_URI'] . '?' . $_SERVER['QUERY_STRING'];
    }

    /**
     * 转化数据库保存的文件路径，为可以访问的url
     * @param $file
     * @param int $defaultType
     * @return string
     */
    public static function getUrl($file = '', $defaultType = 1)
    {

        if (!$file) {
            switch ($defaultType) {
                case 2:
                    $file = self::getSingleVideoDefaultImage();
                    break;
                case 3:
                    $file = self::getYouKeVestIcon();
                    break;
                case 1:
                default:
                    $file = self::getUserDefaultImage();
            }
            return $file;
        }

        if (strpos($file, 'http') === 0) {
            return $file;
        } else if (strpos($file, '/') === 0) {
            return self::getHost() . $file;
        } else {
            return $file;
        }
    }

    private static function getUserDefaultImage()
    {
        return self::getUrl('/default.jpg');
    }

    private static function getSingleVideoDefaultImage()
    {
        return self::getUrl('/public/images/home/video-default-image.jpg');
    }

    private static function getYouKeVestIcon()
    {
        return self::getUrl('/public/images/user/vest-icon-youke.png');
    }

    /**
     * index data
     * @param array $array
     * @param string $key
     * @param string $sort
     * @return array
     */
    public static function index(array $array, $key = 'id', $sort = 'asc')
    {
        $result = [];
        foreach ($array as $v) {
            if (isset($v[$key]) && $v) {
                $result[$v[$key]] = $v;
            }
        }
        $array = null;
        if ($sort == 'desc') {
            krsort($result);
        } else {
            ksort($result);
        }

        return $result;
    }

    public static function usort(&$arr, $field, $sorting = SORT_ASC)
    {
        usort($arr, function ($a, $b) use ($field, $sorting) {
            if ($sorting == SORT_ASC) {
                if ($a[$field] > $b[$field]) return 1;
                if ($a[$field] < $b[$field]) return -1;
                return 0;
            } else {
                if ($b[$field] > $a[$field]) return 1;
                if ($b[$field] < $a[$field]) return -1;
                return 0;
            }
        });
    }

    public static function usortByIds(&$arr, $field, $ids)
    {
        usort($arr, function ($a, $b) use ($field, $ids) {
            if (array_search($a[$field], $ids) > array_search($b[$field], $ids))
                return 1;
            if (array_search($a[$field], $ids) < array_search($b[$field], $ids))
                return -1;
            return 0;
        });
    }

    /**
     * 静态资源版本号
     * @param bool $update
     * @return false|string
     */
    public static function getSetStaticVersion($update = false)
    {
        $key = md5(__METHOD__);
        $redis = CRedis::getInstance();
        $defaultVersion = date('mdHi');
        $redisVersion = $redis->get($key);
        $version = $update ? $defaultVersion : ($redisVersion ?: $defaultVersion);
        if ($update || empty($redisVersion)) {
            $redis->set($key, $version);
        }

        return $version;
    }

    /**
     * 清除缓存
     */
    public static function clearCache()
    {
        sp_clear_cache();
        @exec('rm -rf ' . RUNTIME_PATH . '/*');
        self::getSetStaticVersion(true);
    }

    public static function getPublicConfig($update = false)
    {
        $key = self::KEY_PUBLIC_CONFIG;
        $redis = CRedis::getInstance();
        $config = json_decode($redis->get($key), true);
        if (!$config || $update) {
            $config = M('config')->where(['id' => 1])->find();
            $redis->set($key, json_encode($config), 300);
        }

        return $config;
    }

    public static function getPrivateConfig($update = false)
    {
        $key = self::KEY_PRIVATE_CONFIG;
        $redis = CRedis::getInstance();
        $config = json_decode($redis->get($key), true);
        if (!$config || $update) {
            $config = M('config_private')->where(['id' => 1])->find();
            $redis->set($key, json_encode($config), 300);
        }

        return $config;
    }

    /**
     * 过滤关键词
     * @param $field
     * @return null|string|string[]
     */
    public static function filterWord($field)
    {
        $configpri = self::getPrivateConfig();
        $sensitive_field = $configpri['sensitive_field'];
        $sensitive = explode(',', $sensitive_field);
        $replace = [];
        $preg = [];
        foreach ($sensitive as $k => $v) {
            if ($v) {
                $re = '';
                $num = mb_strlen($v);
                for ($i = 0; $i < $num; $i++) {
                    $re .= '*';
                }
                $replace[$k] = $re;
                $preg[$k] = '/' . $v . '/i';
            } else {
                unset($sensitive[$k]);
            }
        }

        return preg_replace($preg, $replace, $field);
    }

    /**
     * @param $cha
     * @return string
     */
    public static function formatTime($cha)
    {
        $iz = floor($cha / 60);
        $hz = floor($iz / 60);
        $s = $cha % 60;
        $i = floor($iz % 60);
        if ($s < 10) {
            $s = '0' . $s;
        }
        if ($i < 10) {
            $i = '0' . $i;
        }
        if ($hz < 10) {
            $hz = '0' . $hz;
        }

        return $hz . ':' . $i . ':' . $s;
    }


    ##########A02 FunC#####################################

    /**
     * xml转array
     * @param $xml
     * @return mixed
     */
    public static function Xml2Array($xml){
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if(preg_match_all($reg, $xml, $matches)){
            $count = count($matches[0]);
            for($i = 0; $i < $count; $i++){
                $subxml= $matches[2][$i];
                $key = $matches[1][$i];
                if(preg_match( $reg, $subxml )){
                    $arr[$key] = self::Xml2Array( $subxml );
                }else{
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }

    /**
     * 获取hash码
     * @param int $length
     * @param int $numeric
     * @return string
     */
    static function random($length = 6 , $numeric = 0) {
        PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
        if($numeric) {
            $hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
        } else {
            $hash = '';
            $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
            $max = strlen($chars) - 1;
            for($i = 0; $i < $length; $i++) {
                $hash .= $chars[mt_rand(0, $max)];
            }
        }
        return $hash;
    }

    /**
     * 导出Excel 表格
     * @param $expTitle 名称
     * @param $expCellName 参数
     * @param $expTableData 内容
     * @param $extend   拓展数组
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    static function exportExcel($expTitle,$expCellName,$expTableData,$cellName, $extend=[])
    {
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
        $fileName = $xlsTitle . '_' . date('YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
        vendor("PHPExcel.PHPExcel");
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'1', $expCellName[$i][1]);
        }
        for($i=0;$i<$dataNum;$i++){
            for($j=0;$j<$cellNum;$j++){
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+2), $expTableData[$i][$expCellName[$j][0]]);
            }
        }

        if($extend){
            $extendDataNum = count($extend);
            for($n=0; $n<$extendDataNum; $n++){
                $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($dataNum+$n+4), $extend[$n]['key']);
                $objPHPExcel->getActiveSheet(0)->setCellValue('B' . ($dataNum+$n+4), $extend[$n]['val']);
            }
        }

        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');//Excel5为xls格式，excel2007为xlsx格式
        $objWriter->save('php://output');
        exit;
    }

    /*
     * 多维数组指定多字段排序
     * example Func:;multiaArraySort($arr, 'id', SORT_DESC, 'num', SORT_DESC);
     * 排序：SORT_ASC升序 , SORT_DESC降序
     * @return array
     */
    static function multiaArraySort()
    {
        $funcArgs = func_get_args();
        if(empty($funcArgs)){
            return null;
        }
        $arr = array_shift($funcArgs);
        if(!is_array($arr)){
            return false;
        }
        foreach($funcArgs as $key => $value){
            if(is_string($value)){
                $tempArr = array();
                foreach($arr as $k=> $v){
                    $tempArr[$k] = $v[$value];
                }
                $funcArgs[$key] = $tempArr;
            }
        }
        $funcArgs[] = &$arr;
        call_user_func_array('array_multisort', $funcArgs);
        return array_pop($funcArgs);
    }

    /**
     *  @desc 获取推拉流地址
     *  @param string $host 协议，如:http、rtmp
     *  @param string $stream 流名,如有则包含 .flv、.m3u8
     *  @param int $type 类型，0表示播流，1表示推流
     */
    static function PrivateKeyA($host,$stream,$type){
        $configpri  = self::getPrivateConfig();
        $cdn_switch = $configpri['cdn_switch'];

        switch($cdn_switch){
            case '1':
                $url = self::PrivateKey_ali($host,$stream,$type);
                break;
            case '2':
                $url = self::PrivateKey_tx($host,$stream,$type);
                break;
            case '3':
                $url = self::PrivateKey_qn($host,$stream,$type);
                break;
            case '4':
                $url = self::PrivateKey_ws($host,$stream,$type);
                break;
        }
        return $url;
    }

    /**
     *  @desc 阿里云直播A类鉴权
     *  @param string $host 协议，如:http、rtmp
     *  @param string $stream 流名,如有则包含 .flv、.m3u8
     *  @param int $type 类型，0表示播流，1表示推流
     */
    static function PrivateKey_ali($host,$stream,$type)
    {
        $configpri  = self::getPrivateConfig();
        $key        = $configpri['auth_key'];

        if($type==1){
            $domain = $host.'://'.$configpri['push_url'];
            $time   = time() +60*60*10;
        }else{
            $domain = $host.'://'.$configpri['pull_url'];
            $time   = time() - 60*30 + $configpri['auth_length'];
        }

        $filename = "/5showcam/".$stream;
        if($key!=''){
            $sstring    = $filename."-".$time."-0-0-".$key;
            $md5        = md5($sstring);
            $auth_key   = "auth_key=".$time."-0-0-".$md5;
        }
        if($type==1){
            if($auth_key){
                $auth_key ='&'.$auth_key;
            }

            $url=array(
                'cdn'       => urlencode($domain.'/5showcam'),
                'stream'    => urlencode($stream.'?vhost='.$configpri['pull_url'].$auth_key),
            );
        }else{
            if($auth_key){
                $auth_key = '?'.$auth_key;
            }
            $url = $domain.$filename.$auth_key;
        }
        return $url;
    }

    /**
     *  @desc 腾讯云推拉流地址
     *  @param string $host 协议，如:http、rtmp
     *  @param string $stream 流名,如有则包含 .flv、.m3u8
     *  @param int $type 类型，0表示播流，1表示推流
     */
    static function PrivateKey_tx($host,$stream,$type)
    {
        $configpri      = self::getPrivateConfig();
        $bizid          = $configpri['tx_bizid'];
        $push_url_key   = $configpri['tx_push_key'];
        $stream_a       = explode('.',$stream);
        $streamKey      = $stream_a[0];
        $ext            = $stream_a[1];
        $live_code      = $bizid . "_" .$streamKey;
        $now_time       = time() + 3*60*60;
        $txTime         = dechex($now_time);
        $txSecret       = md5($push_url_key . $live_code . $txTime);
        $safe_url       = "&txSecret=" .$txSecret."&txTime=" .$txTime;

        if($type==1){
            $url=array(
                'cdn'       => urlencode("rtmp://" . $bizid .".livepush2.myqcloud.com/live/"),
                'stream'    => urlencode($live_code."?bizid=".$bizid."".$safe_url),
            );
        }else{
            $url = 'http://'. $bizid .".liveplay.myqcloud.com/live/" . $live_code . ".flv";
        }
        return $url;
    }

    /**
     *  @desc 七牛云直播
     *  @param string $host 协议，如:http、rtmp
     *  @param string $stream 流名,如有则包含 .flv、.m3u8
     *  @param int $type 类型，0表示播流，1表示推流
     */
    static function PrivateKey_qn($host,$stream,$type)
    {
        require_once './api/public/qiniucdn/Pili_v2.php';
        $configpri      = self::getPrivateConfig();
        $ak             = $configpri['qn_ak'];
        $sk             = $configpri['qn_sk'];
        $hubName        = $configpri['qn_hname'];
        $push           = $configpri['qn_push'];
        $pull           = $configpri['qn_pull'];
        $stream_a       = explode('.',$stream);
        $streamKey      = $stream_a[0];
        $ext            = $stream_a[1];

        if($type==1){
            $time = time() +60*60*10;
            //RTMP 推流地址
            $url2   = \Qiniu\Pili\RTMPPublishURL($push, $hubName, $streamKey, $time, $ak, $sk);
            $url_a  = explode('/',$url2);
            //return $url_a;
            $url = array(
                'cdn'       => urlencode($url_a[0].'//'.$url_a[2].'/'.$url_a[3]),
                'stream'    => urlencode($url_a[4]),
            );
        }else{
            if($ext=='flv'){
                $pul = str_replace('pili-live-rtmp','pili-live-hdl',$pull);
                //HDL 直播地址
                $url = \Qiniu\Pili\HDLPlayURL($pull, $hubName, $streamKey);
            }else if($ext=='m3u8'){
                $pull = str_replace('pili-live-rtmp','pili-live-hls',$pull);
                //HLS 直播地址
                $url = \Qiniu\Pili\HLSPlayURL($pull, $hubName, $streamKey);
            }else{
                //RTMP 直播放址
                $url = \Qiniu\Pili\RTMPPlayURL($pull, $hubName, $streamKey);
            }
        }
        return $url;
    }
    /**
     *  @desc 网宿推拉流
     *  @param string $host 协议，如:http、rtmp
     *  @param string $stream 流名,如有则包含 .flv、.m3u8
     *  @param int $type 类型，0表示播流，1表示推流
     */
    static function PrivateKey_ws($host,$stream,$type)
    {
        $configpri  = self::getPrivateConfig();
        if($type==1){
            $domain     = $host.'://'.$configpri['ws_push'];
            $filename   = "/".$configpri['ws_apn'];
            $url = array(
                'cdn'       => urlencode($domain.$filename),
                'stream'    => urlencode($stream),
            );
        }else{
            $domain     = $host.'://'.$configpri['ws_pull'];
            $filename   = "/".$configpri['ws_apn']."/".$stream;
            $url        = $domain.$filename;
        }
        return $url;
    }

    /* 过滤关键词 */
    static function filterField($field)
    {
        $configpri       = self::getPrivateConfig();
        $sensitive_field = $configpri['sensitive_field'];
        $sensitive       = explode(",",$sensitive_field);
        $replace         = $preg = [];

        foreach($sensitive as $k=>$v){
            if($v){
                $re     = '';
                $num    = mb_strlen($v);
                for($i=0;$i<$num;$i++){
                    $re.='*';
                }
                $replace[$k] = $re;
                $preg[$k]    = '/'.$v.'/i';
            }else{
                unset($sensitive[$k]);
            }
        }
        return preg_replace($preg,$replace,$field);
    }

    /**
     * 当前项目名
     * @return string
     */
    static function getAppKey()
    {
        return PROJECT_NAME;
    }




}
