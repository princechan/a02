<?php
/**
 * 直播切换业务逻辑类
 */
namespace Common\Lib\System;

use Common\Lib\Service;
use Common\Lib\Helpers\Page;
use Common\Lib\Helpers\CRedis;


class LiveSysMsg extends Service
{
    const KEY_LIVE_SYS_MSG = 'cron:live_sys_msg:';
    static $status  = [
        'del'       => 0,
        'normal'    => 1,
        'start'     => 2,
        'end'       => 3,
    ];
    static $status_name  = [
        0 => '已删除',
        1 => '未开始',
        2 => '开始',
        3 => '结束',
    ];
    static $action_name = [
        'start' => '开始',
        'end'   => '结束',
    ];
    static $action_code = [
        'start' => 'start',
        'end'   => 'end',
    ];

    /**
     * 获取key
     * @param $id
     * @return string
     */
    private function getKeyLiveSysMgs($id)
    {
        return self::KEY_LIVE_SYS_MSG . $id;
    }

    /**
     * 数据入库
     */
    public function save($data, $id=0, $update_cached=false)
    {
        $model = M('system_live_msg');

        if($id){
            $model->where(['id'=>$id])->save($data);
        }else{
            $id = $model->add($data);
            $data['id'] = $id;
        }

        if($update_cached){
            CRedis::getInstance()->set($this->getKeyLiveSysMgs($id),json_encode($data, JSON_UNESCAPED_UNICODE));
        }
        return true;
    }

    /**
     * 获取单条数据
     */
    public function getInfo($id)
    {
        $rs = CRedis::getInstance()->get($this->getKeyLiveSysMgs($id));
        if($rs){
            $rs = json_decode($rs, true);
        }else{
            $rs = M('system_live_msg')->where(['id'=>$id])->find();
            if($rs){
                CRedis::getInstance()->set($this->getKeyLiveSysMgs($id), json_encode($rs,JSON_UNESCAPED_UNICODE));
            }
        }

        if($rs && isset($rs['live_room_id']) && $rs['live_room_id']){
            $rs['live_room_id'] = implode(',', json_decode($rs['live_room_id'], true));
        }
        if($rs && isset($rs['start_at']) && $rs['start_at']){
            $rs['start_at'] = date('Y-m-d H:i:s', $rs['start_at']);
        }
        if($rs && isset($rs['end_at']) && $rs['end_at']){
            $rs['end_at'] = date('Y-m-d H:i:s', $rs['end_at']);
        }
        $rs['status_name'] = self::$status_name[$rs['status']];
        $rs['action_name'] = (self::$status['normal'] == $rs['status'] || self::$status['end'] == $rs['status']) ? self::$action_name['start'] : self::$action_name['end'];
        $rs['action_code'] = (self::$status['normal'] == $rs['status'] || self::$status['end'] == $rs['status']) ? self::$action_code['start'] : self::$action_code['end'];


        return $rs ?: [];
    }

    /**
     * 获取数据列表
     */
    public function getList($map=[], $size=20)
    {
        $model = M('system_live_msg');
        $count = $model->where($map)->count();
        $Page  = Page::getInstance()->pageInit($count, $size);
        $Page->SetPager('Admin', '{first}{prev}&nbsp;{liststart}{list}{listend}&nbsp;{next}{last}', array("listlong" => "9", "first" => "首页", "last" => "尾页", "prev" => "上一页", "next" => "下一页", "list" => "*", "disabledclass" => ""));

        $ret_arr = $model->where($map)->order("id DESC")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        if ($ret_arr) {
            $admin_ids = $admin_users = [];
            foreach ($ret_arr as $v) {
                $admin_ids[] = $v['admin_id'];
            }

            if ($admin_ids) {
                $admin_strs = implode(',', array_unique($admin_ids));
                $admin_user_arr = M('users')->field('id, user_login')->where('user_type=1 AND id in(' . $admin_strs . ')')->select();

                if ($admin_user_arr) {
                    foreach ($admin_user_arr as $v) {
                        $admin_users[$v['id']] = $v['user_login'];
                    }
                }
            }
            foreach ($ret_arr as &$v) {
                if (isset($admin_users[$v['admin_id']])) {
                    $v['admin_id'] = $admin_users[$v['admin_id']];
                }
                $v['live_room_id'] = implode(',', json_decode($v['live_room_id'], true));
                $v['status_name'] = self::$status_name[$v['status']];
                $v['action_name'] = (self::$status['normal'] == $v['status'] || self::$status['end'] == $v['status']) ? self::$action_name['start'] : self::$action_name['end'];
                $v['action_code'] = (self::$status['normal'] == $v['status'] || self::$status['end'] == $v['status']) ? self::$action_code['start'] : self::$action_code['end'];
            }
        }
        return [
            'lists' => $ret_arr,
            'page' => $Page->show("Admin"),
        ];
    }

    /**
     * 设置当前脚本状态
     * status 0-删除; 1-正常; 2-开始; 3-结束;
     */
    public function setStatus($id, $status, $update_cache=false)
    {

        if(!$id){
            $this->setError('非法请求, 参数null');
            return false;
        }

        if(!in_array($status, array_values(self::$status))){
            $this->setError('非法请求, 非法参数');
            return false;
        }

        if($update_cache){
            $rs = CRedis::getInstance()->get($this->getKeyLiveSysMgs($id));
            if($rs){
                $rs = json_decode($rs, true);
                $rs['status'] = $status;
                CRedis::getInstance()->set($this->getKeyLiveSysMgs($id), json_encode($rs, JSON_UNESCAPED_UNICODE));
            }
        }
        return M('system_live_msg')->where(['id'=>$id])->setField('status', $status);
    }

    /**
     * 设置当前脚本状态
     * status 0-删除; 1-正常; 2-开始; 3-结束;
     */
    public function setStartOrEnd($id, $status, $update_cache=false)
    {

        if(!$id){
            $this->setError('非法请求, 参数null');
            return false;
        }

        if(!in_array($status, ['start', 'end'])){
            $this->setError('非法请求, 非法参数');
            return false;
        }

        $status_real = 'start' == $status ? 2 : 3;
        return $this->setStatus($id, $status_real, $update_cache);
    }

    /**
     * 删除系统公告(单条)
     */
    public function delSysLiveMsgSingle($id)
    {
        $id = (int)$id;
        if(!$id){
            $this->setError('非法提交, 参数错误');
            return false;
        }
        return $this->setStatus($id, self::$status['del'], true);
    }

    /**
     * 删除系统公告(多条)
     */
    public function delSysLiveMsg($ids)
    {

        if(!$ids || !is_array($ids)){
            $this->setError('非法提交, 参数错误');
            return false;
        }

        foreach($ids as $v){
            $this->setStatus($v, self::$status['del'], true);
        }
        return true;
    }

    /**
     * 判断时间
     */
    public function judgment($info)
    {
        $time   = time();
        $id     = $info['id'];

        switch($info['status']){
            case 0:
                //删除脚本
                return false;
                break;
            case 1:
                //判断时间
                if($time < $info['start_at']){
                    //未开始
                    return false;
                }
                if(0 < $info['end_at']){
                    if(($time >= $info['start_at']) && ($time <= $info['end_at'])){
                        //已开始
                        $this->setStatus($id, self::$status['start'], true);
                        return true;
                    }
                }else{
                   if($time >= $info['start_at']){
                       //已开始
                       $this->setStatus($id, self::$status['start'], true);
                       return true;
                   }
                }
                return false;
                break;
            case 2:
                if($time > $info['end_at']){
                    $this->setStatus($id, self::$status['end'], true);
                    return false;
                }
                return true;
                break;
            case 3:
                return false;
                break;
            default:
                return true;
        }
    }


}