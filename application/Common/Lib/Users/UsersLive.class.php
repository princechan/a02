<?php
/**
 * 在线主播业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class UsersLive extends Service
{
    /**
     * 获取所有数据
     */
    public function getList($ids=[], $orderby='uid')
    {
        $map = [
            'islive' => 1,
        ];
        if($ids){
            if(!is_array($ids)){
                $ids = (array)$ids;
            }
            $map['uid'] = ['in', $ids];
            $users_live_list = M('users_live')->where($map)->order($orderby . ' DESC')->select() ?: [];
        }else{
            $users_live_list = M('users_live')->where($map)->order('uid DESC')->select() ?: [];
        }
        return $users_live_list ? Func::index($users_live_list, 'uid') : [];
    }

    /**
     * 获取单个主播信息
     */
    public function getInfo($id,  $orderby='uid')
    {
        $live_info = $this->getList($id, $orderby);
        return $live_info ? $live_info[$id] : [];
    }
}