<?php
/**
 * 中奖纪录业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;

class UsersSendGiftWin extends Service
{

    /**
     * add
     */
    public function add($data)
    {
        if(!$data || !is_array($data)){
            $this->setError('参数错误');
            return false;
        }
        return M('users_sendgift_win')->add($data);
    }


}