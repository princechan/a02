<?php
/**
 * 守护主播业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class UsersGuardLists extends Service
{
    /**
     * 获取所有数据
     */
    public function getList($show_id)
    {
        if(! $show_id){
            $this->setError('无效参数');
            return false;
        }
        $map = [
            'liveuid'       => $show_id,
            'effectivetime' => ['gt', time()],
        ];
        $users_guard_list = M('users_guard_lists')->where($map)->select();
        return $users_guard_list ? Func::index($users_guard_list, 'uid') : [];
    }
}