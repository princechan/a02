<?php
/**
 * 消费记录业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Helpers\Page;
use Common\Lib\Service;
use Common\Lib\Users\User;
use Common\Lib\Gift\Gift;


class UsersCoinrecord extends Service
{

    /**
     * 获取消费记录
     */
    public function getPageList($map, $size=20, $page_tpl='Admin')
    {
        $single_spend = $total_spend = 0;
        $coin_record  = M('users_coinrecord');
        $total_spend  = $coin_record->where($map)->sum('totalcoin');
        $count        = $coin_record->where($map)->count();
        $Page         = Page::getInstance()->pageInit($count, $size);

        if('Admin' == $page_tpl){
            $Page->SetPager('Admin', '{first}{prev}&nbsp;{liststart}{list}{listend}&nbsp;{next}{last}', array("listlong" => "9", "first" => "首页", "last" => "尾页", "prev" => "上一页", "next" => "下一页", "list" => "*", "disabledclass" => ""));
        }

        $lists = $coin_record->where($map)->order("addtime DESC")->limit($Page->firstRow . ',' . $Page->listRows)->select();

        if($lists){
            $uids = $to_uids =  $gift_ids = [];
            array_walk($lists, function($v, $k) use (&$uids, &$gift_ids, &$to_uids){
                $uids[]     = $v['uid'];
                $to_uids[]  = $v['touid'];
                $gift_ids[] = $v['giftid'];
            });

            $uids         = array_unique($uids);
            $to_uids      = array_unique($to_uids);
            $gift_ids     = array_unique($gift_ids);
            $user_list    = User::getInstance()->getUsersInfo($uids);
            $to_user_list = User::getInstance()->getUsersInfo($to_uids);
            $gift_list    = Gift::getInstance()->getList($gift_ids);

            $spend_type = [
                1 => 'CNY/钻石',
                2 => '直播券',
            ];
            $type = [
                "income" => "收入",
                "expend" => "支出"
            ];

            foreach($lists as $k=>$v){
                $single_spend += $v['totalcoin'];

                $lists[$k]['type']          = $type[$v['type']];
                $lists[$k]['userinfo']      = $user_list[$v['uid']]['user_nicename'];
                $lists[$k]['touserinfo']    = $to_user_list[$v['touid']]['user_nicename'];
                $lists[$k]['spend_type']    = $spend_type[$v['spend_type']];
                $lists[$k]['total_price']   = $v['spend_type'] == 1 ? $v['totalcoin'] : $v['total_livecoin'];

                $act = $v['action'];
                switch ($act){
                    case 'loginbonus':
                        $lists[$k]['action']   = '登录奖励';
                        $lists[$k]['giftinfo'] = '第'.$v['giftid'].'天';
                        break;
                    case 'roomcharge':
                        $lists[$k]['action']   = '付费房间';
                        $lists[$k]['giftinfo'] = '付费房间';
                        break;
                    case 'sendgift':
                        $lists[$k]['action']   = '赠送礼物';
                        $lists[$k]['giftinfo'] = $gift_list[$v['giftid']]['giftname'];
                        break;
                    case 'buyguard':
                        $lists[$k]['action']   = '购买守护';
                        $lists[$k]['giftinfo'] = '购买守护';
                        break;
                    case 'sendbarrage':
                        $lists[$k]['action']   = '发送弹幕';
                        $lists[$k]['giftinfo'] = '发送弹幕';
                        break;
                    case 'sendbighorn':
                        $lists[$k]['action']   = '购买喇叭';
                        $lists[$k]['giftinfo'] = '大喇叭';
                        break;
                    default:
                }
            }
        }
        return [
            'single_spend'  => $single_spend,
            'total_spend'   => $total_spend,
            'lists'         => $lists,
            'page'          => $Page->show('Admin')
        ];
    }

    /**
     * 导出数据到excel
     */
    public function export($map)
    {
        $single_spend = $total_spend = 0;
        $coin_record  = M('users_coinrecord');
        $total_spend  = $coin_record->where($map)->sum('totalcoin');
        $total_spend  = $total_spend ? $total_spend : 0;
        $lists        = $coin_record->where($map)->order("addtime DESC")->select();

        if($lists){
            $uids = $to_uids =  $gift_ids = [];
            array_walk($lists, function($v, $k) use (&$uids, &$gift_ids, &$to_uids){
                $uids[]     = $v['uid'];
                $to_uids[]  = $v['touid'];
                $gift_ids[] = $v['giftid'];
            });

            $uids         = array_unique($uids);
            $to_uids      = array_unique($to_uids);
            $gift_ids     = array_unique($gift_ids);
            $user_list    = User::getInstance()->getUsersInfo($uids);
            $to_user_list = User::getInstance()->getUsersInfo($to_uids);
            $gift_list    = Gift::getInstance()->getList($gift_ids);

            $spend_type = [
                1 => 'CNY/钻石',
                2 => '直播券',
            ];
            $type = [
                "income" => "收入",
                "expend" => "支出"
            ];

            foreach($lists as $k=>$v){
                $single_spend += $v['totalcoin'];

                $lists[$k]['type']          = $type[$v['type']];
                $lists[$k]['userinfo']      = $user_list[$v['uid']]['user_nicename'] . '(' . $v['uid'] . ')';
                $lists[$k]['touserinfo']    = $to_user_list[$v['touid']]['user_nicename'] . '(' . $v['touid'] . ')';
                $lists[$k]['spend_type']    = $spend_type[$v['spend_type']];
                $lists[$k]['total_price']   = $v['spend_type'] == 1 ? $v['totalcoin'] : $v['total_livecoin'];
                $lists[$k]['addtime']       = date('Y-m-d H:i:s', $v['addtime']);

                $act = $v['action'];
                switch ($act){
                    case 'loginbonus':
                        $lists[$k]['action']   = '登录奖励';
                        $lists[$k]['giftinfo'] = '第'.$v['giftid'].'天';
                        break;
                    case 'roomcharge':
                        $lists[$k]['action']   = '付费房间';
                        $lists[$k]['giftinfo'] = '付费房间';
                        break;
                    case 'sendgift':
                        $lists[$k]['action']   = '赠送礼物';
                        $lists[$k]['giftinfo'] = $gift_list[$v['giftid']]['giftname'] . '(' . $v['giftid'] . ')';
                        break;
                    case 'buyguard':
                        $lists[$k]['action']   = '购买守护';
                        $lists[$k]['giftinfo'] = '购买守护';
                        break;
                    case 'sendbarrage':
                        $lists[$k]['action']   = '发送弹幕';
                        $lists[$k]['giftinfo'] = '发送弹幕';
                        break;
                    case 'sendbighorn':
                        $lists[$k]['action']   = '购买喇叭';
                        $lists[$k]['giftinfo'] = '大喇叭';
                        break;
                    default:
                }
            }
        }
        return [
            'single_spend'  => $single_spend,
            'total_spend'   => $total_spend,
            'lists'         => $lists,
        ];
    }

    /**
     * add
     */
    public function add($data)
    {
        if(!$data || !is_array($data)){
            $this->setError('参数错误');
            return false;
        }
        return M('users_coinrecord')->add($data);
    }


}