<?php
/**
 * 设置用户马甲业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class UsersVestList extends Service
{
    /**
     * 获取所有礼物
     */
    public function getList($show_id)
    {
        if(! $show_id){
            $this->setError('无效参数');
            return false;
        }

        $users_vest_list = M('users_vest_lists')->where(['liveid' => $show_id])->order('id DESC')->select() ?: [];
        return $users_vest_list ? Func::index($users_vest_list, 'uid') : [];
    }
}