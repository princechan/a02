<?php
/**
 * 在线主播业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;

class UsersSuper extends Service
{
    /* 判断账号是否超管 */
    function isSuper($uid)
    {
        return $this->getInfo($uid) ? true : false;
    }
    public function getInfo($uid)
    {
        $info = M("users_super")->where("uid='{$uid}'")->find();
        return $info ? : null;
    }
}