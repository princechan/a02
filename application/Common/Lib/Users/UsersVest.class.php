<?php
/**
 * 马甲业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class UsersVest extends Service
{
    /**
     * 获取所有数据
     */
    public function getList($ids=[])
    {
        if($ids){
            if(!is_array($ids)){
                $ids = (array)$ids;
            }

            $users_vest = M('users_vest')->where(['id' => ['in', $ids]])->order('id DESC')->select() ?: [];
        }else{
            $users_vest = M('users_vest')->order('id DESC')->select() ?: [];
        }
        return $users_vest ? Func::index($users_vest, 'id') : [];
    }
}