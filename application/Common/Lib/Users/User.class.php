<?php
/**
 * 用户业务逻辑类
 */

namespace Common\Lib\Users;

use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Func;
use Common\Lib\Service;
use Common\Lib\Vendor\Easemob;
use Common\Lib\Auth\UserKaifa;
use Common\Lib\Users\UsersGuardLists;
use Common\Lib\Users\UsersLive;
use Common\Lib\Users\UsersVest;
use Common\Lib\Users\UsersVestList;

class User extends Service
{
    const KEY_CACHE_USER        = 'cache:user';
    const KEY_VISITOR_SENDER    = 'vistor:senderid';
    const KEY_VISITOR_USER      = 'vistor:user:';
    const KEY_CACHE_TIME        = 10 * 60;
    const EXPIRE_TIME           = 3600;
    const TOKEN_EXPIRE_TIME     = 24 * 3600;
    const VISITOR_EXPIRE_TIME   = 7 * 24 * 3600;
    const KEY_EXTEND_USERS      = 'cache:extend:users';

    /**
     * 游客信息
     */
    const VISITOR_ID_PREFIX     = 'temp_';
    const VISITOR_COOKIE        = 'tmp_id';
    const VISITOR_NAME_PREFIX   = '游客';

    protected $_config = [];

    /**
     * 注册
     */
    public function register($username, $password)
    {
        $exist = M('users')->where(['user_login' => $username, 'user_type' => 2])->find();
        if(!$exist){
            $user_id = $this->userAdd($username, $password);
            if(!$user_id){
                $this->setError('保存失败-sql');
                return false;
            }
        }

        //调登录接口
        return $this->login($username, $password);
    }

    /**
     * 生成新用户
     * @param $username
     * @param $password
     * @return mixed
     */
    private function userAdd($username, $password)
    {
        $data = [
            'user_login'        => $username,
            'user_nicename'     => 'WEB用户' . substr($username, -4),
            'user_pass'         => $password,
            'signature'         => '这家伙很懒，什么都没留下',
            'avatar'            => '/default.jpg',
            'avatar_thumb'      => '/default_thumb.jpg',
            'create_time'       => date("Y-m-d H:i:s"),
            'last_login_time'   => date("Y-m-d H:i:s"),
            'user_status'       => 1,
            "user_type"         => 2,//会员
        ];
        return M('users')->add($data);
    }

    /**
     * 登录
     */
    public function login($username, $password)
    {
        $map = [
            'user_login'    => $username,
            'user_type'     => 2,
        ];
        $user_info = M('users')->where($map)->find();
        if(!$user_info){
            return $this->register($username, $password);
        }

        if($password != $user_info['user_pass']){
            $this->setError('密码错误');
            return false;
        }

        $user_info['level'] = $this->getLevel($user_info['consumption']);

        $token = $this->buildToken($user_info['user_login']);
        M('users')->where(['id' => $user_info['id']])->save([
            'token'         => $token,
            'expiretime'    => time() + self::TOKEN_EXPIRE_TIME,
        ]);

        $user = $this->getUserPublicInfo($user_info['id'], true);

        //添加user拓展字段  例如os 来源，默认值 pc
        $ext = [
            'os' => 'pc',
        ];
        $this->setExtUsers($user['id'], $ext);
        $this->setUserToken($token, $user);
        return $user;
    }

    /**
     * 设置token
     * @param $token
     * @param array $user
     * @return bool
     */
    private function setUserToken($token, array $user)
    {
        if (!$token || !$user) {
            return false;
        }

        //cookie
        $cookieTimeout = ($user['vest_id'] <= 1 ? 1 : 24) * self::EXPIRE_TIME;
        cookie('token', $token, $cookieTimeout);

        //session
        return CRedis::getInstance()->set($this->getUserTokenKey(), json_encode($user, JSON_UNESCAPED_UNICODE), time() + self::TOKEN_EXPIRE_TIME);
    }

    /**
     * 删除用户token
     * @return int
     */
    private function delUserToken()
    {
        return CRedis::getInstance()->del($this->getUserTokenKey());
    }

    /**
     * 获取pc token
     * @return null|string
     */
    private function getUserTokenKey()
    {
        $token = $this->getToken();
        if (!$token) {
            return null;
        }
        return $token;
    }

    public function getToken()
    {
        return cookie('token');
    }

    /**
     * 检查权限
     * @param $name string|array  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param $uid  int           认证用户的id
     * @param $relation string    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return boolean           通过验证返回true;失败返回false
     */
    public function check($uid, $name, $relation = 'or')
    {
        if (empty($uid)) {
            return false;
        }
        if ($uid == 1) {
            return true;
        }
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = [$name];
            }
        }

        $list = []; //保存验证通过的规则名
        $role_user_model = M("RoleUser");
        $role_user_join = C('DB_PREFIX') . 'role as b on a.role_id =b.id';
        $groups = $role_user_model->alias("a")->join($role_user_join)
            ->where(["a.user_id" => $uid, "b.status" => 1])->getField("role_id", true) ?: [];

        if (in_array(1, $groups)) {
            return true;
        }

        if (empty($groups)) {
            return false;
        }

        $auth_access_model = M("AuthAccess");
        $join = C('DB_PREFIX') . 'auth_rule as b on a.rule_name =b.name';
        $rules = $auth_access_model
            ->alias("a")
            ->join($join)
            ->where([
                "a.role_id" => ["in", $groups],
                "b.name" => ["in", $name],
            ])->select();

        //下面用到的变量$user
        $user = M('users')->where(['id' => $uid])->find();
        foreach ($rules as $rule) {
            if (!empty($rule['condition'])) { //根据condition进行验证
                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $list[] = strtolower($rule['name']);
                }
            } else {
                $list[] = strtolower($rule['name']);
            }
        }

        if ($relation == 'or' and !empty($list)) {
            return true;
        }
        $diff = array_diff($name, $list);
        if ($relation == 'and' and empty($diff)) {
            return true;
        }
        return false;
    }

    /**
     * 用户权限
     * @param $uid
     * @param $liveId
     * @param int $liveType 0-单 1-多
     * @return int 10-游客 30-观众 40-管理员 50-房间主播 60-超管 70-麦手
     */
    public function getUserIdentity($uid, $liveId, $liveType = 0)
    {
        if ($this->isSuper($uid)) {
            return 60;
        }

        if ($liveType == 0) {
            if ($uid == $liveId) {
                return 50;
            }

            $manager = M('users_livemanager')->where(['uid' => $uid, 'liveuid' => $liveId])->find();
            if ($manager) {
                return 40;
            }
        } else {
            $list = M('muti_showmanager')->where(['muti_id' => $liveId])->select() ?: [];
            foreach ($list as $value) {
                if ($value['uid'] == $uid) {
                    if ($value['vest_type'] == self::VEST_TYPE_DJ) {
                        return 70;
                    }
                    if ($value['vest_type'] == self::VEST_TYPE_ANCHOR) {
                        return 50;
                    }
                    break;
                }
            }
        }
        return 30;
    }

    /**
     * 判断账号是否超管
     * @param $uid
     * @return int
     */
    public function isSuper($uid)
    {
        return M('users_super')->where(['uid' => $uid])->find();
    }

    /**
     * 认证信息
     * @param $uid
     * @return int|mixed
     */
    public function getUserAuth($uid)
    {
        $auth = M('users_auth')->where(['uid' => $uid])->find();
        return $auth ?: [];
    }

    /**
     * 判断该用户是否已经认证
     * @param $uid
     * @return int
     */
    public function getAuthStatus($uid)
    {
        $auth = $this->getUserAuth($uid);
        return $auth ? $auth['status'] : 0;
    }

    /**
     * 创建easemob临时用户
     * @param $name
     * @return bool
     */
    public function createTmpEasemobUser($name)
    {
        if (!$this->isVisitor($name)) {
            return false;
        }

        if ($name != $this->genVisitorId($this->getVisitorCookie())) {
            return false;
        }

        $model = M('tmp_easemob_user');
        if ($model->where(['name' => $name])->find()) {
            return true;
        }

        return $model->add([
            'name' => $name,
            'created_at' => time(),
        ]);
    }

    /**
     * 删除环信临时用户
     * @param $name
     * @return bool
     */
    public function delTmpEasemobUser($name)
    {
        $rs = Easemob::getInstance()->deleteUser($name);
        if (empty($rs['error'])) {
            M('tmp_easemob_user')->where(['name' => $name])->delete();
        }
        return true;
    }

    /**
     * 删除2天前的环信用户数据
     */
    public function delTmpEasemobUsers()
    {
        $time = strtotime('-2 day');
        $users = M('tmp_easemob_user')->where(['created_at' => ['LT', $time]])
            ->limit(0, 10000)->order(['id' => 'ASC'])->select() ?: [];
        foreach ($users as $user) {
            $this->delTmpEasemobUser($user['name']);
        }
    }

    /**
     * @deprecated
     * @param $pass
     * @return string
     */
    public function setPass($pass)
    {
        $code = 'rCt52pF2cnnKNB3Hkp';
        return '###' . md5(md5($code . $pass));
    }

    /**
     * 密码检查
     * @param $user_pass
     * @return int
     */
    function passCheck($user_pass)
    {
        $num = preg_match("/^[a-zA-Z]+$/", $user_pass);
        $word = preg_match("/^[0-9]+$/", $user_pass);
        $check = preg_match("/^[a-zA-Z0-9]{6,12}$/", $user_pass);
        if ($num || $word) {
            return 2;
        } else if (!$check) {
            return 0;
        }
        return 1;
    }

    /**
     * 修改用户资料
     * @param $uid
     * @param $sex
     * @param $birthday
     * @param $userNicename
     * @param $signature
     * @return bool
     */
    public function editModify($uid, $sex, $birthday, $userNicename, $signature)
    {
        if (!$sex && !$birthday && !$userNicename && !$signature) {
            $this->setError('参数错误');
            return false;
        }

        if ($userNicename) {
            if (M('users')->where(['user_nicename' => $userNicename])->find()) {
                $this->setError('用户名重复');
                return false;
            }
        }

        $data = [];
        $data['id'] = $uid;
        if ($sex) {
            $data['sex'] = $sex;
        }
        if ($birthday) {
            $data['birthday'] = $birthday;
        }
        $userNicename = Func::filterWord($userNicename);
        if ($userNicename) {
            $data['user_nicename'] = $userNicename;
        }
        $signature = Func::filterWord($signature);
        if ($signature) {
            $data['signature'] = $signature;
        }

        $result = M('users')->save($data);
        if ($result) {
            if ($userNicename) {
                if (M('users_live')->where(['uid' => $uid])->find()) {
                    M('users_live')->where(['uid' => $uid])->setField('user_nicename', $userNicename);
                }
            }
            return true;
        }

        $this->setError('修改失败');
        return false;
    }

    /**
     * 生成乱码
     * @param int $num
     * @return string
     */
    private function getRandChar($num = 6)
    {
        $str = '';
        for ($i = 1; $i <= $num; $i++) {
            $str .= chr(rand(97, 122));
        }
        return $str;
    }

    /**
     * 生成nicename
     * @param $username
     * @return string
     */
    public function genUserNicename($username)
    {
        $nick = '***' . substr($username, 3);
        if (!M('users')->where(['user_nicename' => $nick])->find()) {
            return $nick;
        }

        for ($i = 1; $i < 1000; $i++) {
            $nick .= $this->getRandChar();
            if (!M('users')->where(['user_nicename' => $nick])->find()) {
                return $nick;
            }
            $nick .= date('mdHi');
        }
        return $nick;
    }

    /**
     * 判断是否关注
     * @param $uid
     * @param $touid
     * @return int
     */
    public function isAttention($uid, $touid)
    {
        $data = M('users_attention')->where(['uid' => $uid, 'touid' => $touid])->find();
        return $data ? 1 : 0;
    }

    /**
     * 关注人数
     * @param $uid
     * @return mixed
     */
    public function getFollownums($uid)
    {
        return M('users_attention')->where(['uid' => $uid])->count();
    }

    /**
     * 粉丝人数
     * @param $uid
     * @return mixed
     */
    public function getFansnums($uid)
    {
        return M('users_attention')->where(['touid' => $uid])->count();
    }

    /**
     * 判断token是否过期
     * @deprecated
     * @param $uid
     * @param $token
     * @return int
     */
    public function checkToken($uid, $token)
    {
        $userinfo = $this->getUserInfo($uid);
        if(!$userinfo){
            $userinfo = $this->getUserInfo($uid, true);
        }

        if ($userinfo['token'] != $token || $userinfo['expiretime'] < time()) {
            return 700;
        }
        return 0;
    }

    /**
     * 获取等级
     * @param $experience
     * @return int
     */
    public function getLevel($experience)
    {
        $key = 'experlevel_limit';
        $redis = CRedis::getInstance();
        $list = json_decode($redis->get($key), true) ?: [];
        if (!$list) {
            $list = M('experlevel_limit')->field('withdraw,level_up')->order('level_up asc')->select() ?: [];
            $redis->set($key, json_encode($list), 60);
        }

        $withdraw = 0;
        foreach ($list as $v) {
            if ($v['level_up'] >= $experience) {
                $withdraw = $v['withdraw'];
                break;
            }
        }
        return $withdraw;
    }

    /**
     * 判断账号是被禁用
     * @param $uid
     * @return int 0-禁用，1-未禁用
     */
    public function isBan($uid)
    {
        $status = M('users')->field('user_status')->where(['id' => $uid])->find();
        if (!$status || $status['user_status'] == 0) {
            return 0;
        }
        return 1;
    }

    /**
     * 获取指定长度的随机字符串
     * @param int $length
     * @param int $numeric
     * @return string
     */
    public function random($length = 6, $numeric = 0)
    {
        PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
        if ($numeric) {
            $hash = sprintf('%0' . $length . 'd', mt_rand(0, pow(10, $length) - 1));
        } else {
            $hash = '';
            $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
            $max = strlen($chars) - 1;
            for ($i = 0; $i < $length; $i++) {
                $hash .= $chars[mt_rand(0, $max)];
            }
        }

        return $hash;
    }

    /**
     * 获取用户信息(可公开)
     * @param $uid
     * @param bool $update
     * @return array|bool
     */
    public function getUserPublicInfo($uid, $update=false)
    {
        $info = $this->getUserInfo($uid);
        if($info){
            $arr = [
                'id'                => $info['id'],
                'user_nicename'     => $info['user_nicename'],
                'level'             => $info['level'],
                'avatar'            => $info['avatar'],
                'avatar_thumb'      => $info['avatar_thumb'],
                'sex'               => $info['sex'],
                'birthday'          => $info['birthday'],
                'signature'         => $info['signature'],
                'vest_id'           => $info['vest_id'],
                'sign'              => $info['sign'],
                'is_trial_play'     => $info['is_trial_play'],
                'isauth'            => 0,
                'logged'            => 0,
                'light'             => 0,
                'lighttime'         => 0,
            ];
            return $arr;
        }
        return false;
    }

    /**
     * 统一获取用户信息方法
     */
    public function getUserInfo($uid, $update=false)
    {
        if (!$uid) {
            $this->setError('缺参数');
            return false;
        }

        if(0 < $uid){
            if($user = $this->getVisitorInfo($uid)){
                return $user;
            }
        }

        $users = $this->getUsersInfo([$uid], $update);
        if (empty($users[$uid])) {
            $this->setError('用户不存在');
            return false;
        }

        $users[$uid]['avatar']       = Func::getUrl($users[$uid]['avatar']);
        $users[$uid]['avatar_thumb'] = Func::getUrl($users[$uid]['avatar_thumb']);
        $users[$uid]['sign']         = $users[$uid]['token']; //兼容nodejs redis
        $users[$uid]['is_trial_play']= (stristr($users[$uid]['user_login'], 'a02')) ? 1 : 0;
        $users[$uid]['levle']        = $this->getLevel($users[$uid]['consumption']);
        $users[$uid]['isauth']       = 0;
        $users[$uid]['logged']       = 0;
        $users[$uid]['light']        = 0;
        $users[$uid]['lighttime']    = 0;

        return $users[$uid];
    }

    /**
     * 用户信息
     */
    public function getUsersInfo(array $ids, $update = false)
    {
        if (!$ids) {
            return false;
        }

        $redis  = CRedis::getInstance();
        $key    = $this->getUserCacheKey($ids);
        $users  = $redis->get($key);
        if ($users && !$update) {
            return json_decode($users, true);
        }

        $users = M('users')->where(['id' => ['in', $ids]])->select() ?: [];
        $users = $users ? Func::index($users, 'id') : [];
        if ($users) {
            $redis->set($key, json_encode($users), self::KEY_CACHE_TIME);
        }
        return $users;
    }

    private function getUserCacheKey($id)
    {
        return self::KEY_CACHE_USER . (is_array($id) ? json_encode($id) : $id);
    }

    /**
     * 用户信息 含有私密信息
     * @param $uid
     * @return mixed
     */
    public function getUserPrivateInfo($uid)
    {
        $user = M('users')->where(['id' => $uid])->find();
        if ($user) {
            $user['lighttime']      = 0;
            $user['light']          = 0;
            $user['level']          = $this->getLevel($user['consumption']);
            $user['avatar']         = Func::getUrl($user['avatar']);
            $user['avatar_thumb']   = Func::getUrl($user['avatar_thumb']);

            //用户余额
            $user['coin'] = UserKaifa::getInstance()->getZhiBoBalance($user['user_login'], $user['user_pass']);

            //查询用户是否认证通过
            $user['isauth'] = $this->getAuthStatus($uid);

            //是否是登陆用户
            $user['logged'] = 1;
        }

        return $user;
    }

    private function buildToken($username)
    {
        return md5(md5($username . microtime(true) . uniqid()));
    }

    /**
     * PC获取用户ID方法
     * @return int|string
     */
    public function getUserId()
    {
        $user = $this->getUserByToken();
        if(!$user){
            return null;
        }
        return $user['id'];
    }

    /**
     * PC获取用户ID方法
     * @return int|string
     */
    public function getUserByToken()
    {
        $token = $this->getUserTokenKey();
        if(!$token){
            return false;
        }
        return json_decode(CRedis::getInstance()->get($token), true);
    }

    /**
     * PC获取用户ID方法
     * @return int|string
     */
    public function getUserToken()
    {
        $token = $this->getUserTokenKey();
        if(!$token){
            return false;
        }
        $user = json_decode(CRedis::getInstance()->get($token), true);
        return $user ? $user['token'] : null;
    }

    /**
     * 前台个人中心判断是否登录
     * @deprecated
     */
    public function checkLogin()
    {
        $uid = $this->getUserId();
        if(!$uid){
            $this->setError('未登录');
            return false;
        }
        return true;
    }

    /**
     * 注销
     */
    public function logout()
    {
        session(null);
        cookie(null);
        $this->delUserToken();
    }

    /**
     * 是否拉黑
     * @param $uid
     * @param $touid
     * @return int
     */
    public function isBlack($uid, $touid)
    {
        $black = M('users_black')->where(['uid' => $uid, 'touid' => $touid])->find();
        if ($black) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 是否开启僵尸粉
     * @param $id
     * @return mixed
     */
    public function isZombie($id)
    {
        $user_info = User::getInstance()->getUserInfo($id);
        return $user_info['iszombie'];
    }

    /**
     * 验证图片码
     */
    public function verifyCode($code, $id)
    {
        $verify = new \Think\Verify();
        $check_verify = $verify->check($code, $id);
        if(!$check_verify){
            $this->setError('图片验证码不正确', 1120);
            return false;
        }
        return true;
    }

    /**
     * md5加密(16位)
     */
    public function encryptMD5($str)
    {
        return md5($str);
    }

    /**
     * 获取在线用户列表
     */
    public function getOnlineUsers($show_id)
    {
        $live_info  = UsersLive::getInstance()->getInfo($show_id);
        $stream     = $live_info ? $live_info['stream'] : $show_id."_".$show_id;

        $redis = CRedis::getInstance();
        $list  = $redis->hVals('userlist_' . $stream);
        $nums  = $redis->hlen('userlist_' . $stream);

        $data = [
            'list' => [],
            'nums' => 0,
        ];

        $lists = $list_temp = $visitor_list = $uid_arr  = $user_list = $vest_list = [];
        if($list){
            foreach($list as $k=> $v){
                $list_temp[]=json_decode($v,true);
            }

            foreach($list_temp as $v){

                if(0 < $v['id']){
                    $lists[] = $v;
                }else{
                    //游客
                    $visitor_list[] = $v;
                }
            }

            $uid_arr            = array_column($lists,'id');
            $user_list          = $this->getUsersInfo($uid_arr);
            $vest_list          = UsersVest::getInstance()->getList();
            $user_vest_list     = UsersVestList::getInstance()->getList($show_id);
            $users_guard_list   = UsersGuardLists::getInstance()->getList($show_id);

            foreach ($lists as $k => $v) {

                //获取用户的黑马或紫马信息
                $vest_id   = $user_list[$v['id']]['vest_id'];
                $vest_info = $vest_list[$vest_id];

                //女性马甲图标 //男性马甲图标
                $lists[$k]['vestIcon'] = ($v['sex']==2) ? Func::getUrl($vest_info['vest_woman_url']) : Func::getUrl($vest_info['vest_man_url']);

                if($vest_id>0){
                    $lists[$k]['vestid'] = $vest_id;//设置马甲ID
                }else{
                    //判断该用户是否被设置马甲
                    //$vestInfo=$VestLists->where("uid={$v['id']} and liveid={$showid} and effectivetime > {$now}")->find();因为有效期未设置，此代码备用
                    $lists[$k]['vestid'] = $user_vest_list ? $user_vest_list[$v['id']]['vestid'] : 1;
                }

                //判断该用户是否是该主播的守护
                if($users_guard_list && isset($users_guard_list[$v['id']])){
                    $lists[$k]['isGuard']    = 1;
                    $lists[$k]['guardLevel'] = $users_guard_list[$v['id']]['guard_level'];
                }else{
                    $lists[$k]['isGuard'] = 0;
                }
            }


            //赋予标识：1
            $extUsers = User::getInstance()->getExtUsers();

            foreach ($lists as $k => $v) {

                //赋予标识：2
                if(isset($extUsers[$v['id']])){
                    $ext_arr   = json_decode($extUsers[$v['id']], true);
                    $lists[$k] = array_merge($lists[$k], $ext_arr);
                }

                if($show_id == $v['id']){
                    //将主播删除
                    array_splice($lists,$k,1);

                    //主播被删除，用户列表个数减1
                    $nums -= 1;
                }
            }

            $lists = Func::multiaArraySort($lists, 'vestid', SORT_DESC, 'level', SORT_DESC);
            $lists = array_merge_recursive($lists, $visitor_list);

            $data = [
                'list' => $lists,
                'nums' => $nums,
            ];
        }
        return $data;
    }

    /**
     * 用户进步直播间写入缓存 50本房间主播 60超管 40管理员 30观众 10为游客(判断当前用户身份)
     * @param $live_type
     * @param $show_id
     * @param $stream
     * @return array
     */
    public function setNodeInfo($live_type, $show_id, $stream)
    {

        $uid     = $this->getUserId();
        if(0 < $uid){
            $info = $this->getUserInfo($uid);
            $info['liveuid']    = $show_id;
            $info['sign']       = md5($show_id.'_'.$info['id']);
            $info['userType']   = ($uid==$show_id) ? 50 : 40;
        }else{
            /* 游客 */
            $new_status = 1;
            if($token = $this->getToken()){
                $info = $this->getUserByToken($token);
                if($info){
                    $new_status = 0;
                }
            }
            if(1 == $new_status){
                $random = mt_rand(1000,9999);
                $info = [
                    'id'                => '-' . $random,
                    'user_nicename'     => '游客' . $random,
                    'avatar'            => '',
                    'avatar_thumb'      => '',
                    'sex'               => 0,
                    'signature'         => 0,
                    'consumption'       => 0,
                    'votestotal'        => 0,
                    'province'          => '',
                    'city'              => '',
                    'liveuid'           => $show_id,
                    'userType'          => 0,
                    'vestid'            => 0, //游客马甲为0
                    'is_trial_play'     => 0, //游客, 非试玩账号
                    'isauth'            => 0,
                    'logged'            => 0,
                    'light'             => 0,
                    'lighttime'         => 0,
                ];
                $info['token'] = $info['sign'] = $token = md5($show_id.'_'.$random);
            }
        }

        $info['live_type']  = $live_type;
        $info['live_id']    = $show_id;
        $info['roomnum']    = $show_id;

        //判断该房间是否在直播
        $live = UsersLive::getInstance()->getInfo($show_id);
        $info['stream'] = $live ? $live['stream'] : (($uid==$show_id) ? $stream : $show_id."_".$show_id);

        //写入缓存
        $this->setUserToken($token, $info);
        if(0 > $uid){
            $this->setVisitorInfo($info['id'], $info);
        }

        /*判断改房间是否开启僵尸粉*/
        return [
            'error'     => 0,
            'userinfo'  => $info,
            'iszombie'  => $this->isZombie($show_id),
        ];

    }

    /**
     * 获取用户拓展数据公用key
     * @return string
     */
    private function getExtUsersKey()
    {
        return self::KEY_EXTEND_USERS;
    }

    /**
     * 写入用户拓展数据
     * @param $uid
     * @param $data
     * @return int
     */
    public function setExtUsers($uid, $data)
    {
        if(!is_array($data)){
            return false;
        }

        $ext_info = $this->getExtUserInfo($uid);
        if($ext_info){
            $data = array_merge($ext_info, $data);
        }
        return CRedis::getInstance()->hSet($this->getExtUsersKey(), $uid, json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    /**
     * 判断用户拓展是否存在
     * @param $uid
     * @return bool
     */
    public function isExistExtUsers($uid)
    {
        return CRedis::getInstance()->hExists($this->getExtUsersKey(), $uid);
    }

    /**
     * 获取所有用户拓展数据
     * @return array
     */
    public function getExtUsers()
    {
        return CRedis::getInstance()->hGetAll($this->getExtUsersKey());
    }

    /**
     * 获取当个用户拓展数据
     * @param $uid
     * @return mixed|null
     */
    public function getExtUserInfo($uid)
    {
        $ext_info = CRedis::getInstance()->hget($this->getExtUsersKey(), $uid);
        return $ext_info ? json_decode($ext_info, true) : null;
    }

    /**
     * 昵称是否重复
     */
    public function isUnExistNickName($nick_name)
    {
        if(false == $this->checkLogin()){
            return false;
        }
        $is_exist = M('users')->where(['user_nicename' => $nick_name])->find();
        if($is_exist){
            $this->setError('昵称已存在');
            return false;
        }
        return true;
    }

    /**
     * 修改昵称
     */
    public function modifyNickName($nick_name)
    {

        if(!$this->isUnExistNickName($nick_name)){
            return false;
        }

        $uid = $this->getUserId();
        if(M('users')->where(['id' => $uid])->setField('user_nicename', $nick_name)){
            $user = $this->getUserInfo($uid, true);
            $this->setUserToken($user['token'], $user);
            return true;
        }
        $this->setError('昵称修改失败');
        return false;
    }

    /**
     * 游客key
     */
    public function getVisitorKey($uid)
    {
        return self::KEY_VISITOR_USER . $uid;
    }

    /**
     *  自减
     */
    public function setDec($uid, $field, $field_value)
    {
        M('users')->where(['id'=>$uid])->setDec($field, $field_value);
        return $this->getUserInfo($uid, true);
    }

    /**
     * 自增
     */
    public function setInc($uid, $field, $field_value)
    {
        M('users')->where(['id'=>$uid])->setInc($field, $field_value);
        return $this->getUserInfo($uid, true);
    }

    /**
     *
     * @param $id
     * @param $data
     * @return bool
     */
    public function setVisitorInfo($id, $data)
    {
        //log
        file_put_contents('/tmp/visitor-' . Func::getAppKey() . '-' . date('Y-m-d'), json_encode([
                'id' => $id,
                'date' => date('Y-m-d H:i:s'),
            ]) . PHP_EOL, FILE_APPEND);

        return CRedis::getInstance()->set($this->getVisitorKey($id), json_encode($data, JSON_UNESCAPED_UNICODE), self::VISITOR_EXPIRE_TIME);
    }

    /**
     * 游客信息
     * @param $uid
     * @return mixed
     */
    public function getVisitorInfo($uid)
    {
        return json_decode(CRedis::getInstance()->get($this->getVisitorKey($uid)), true);
    }


















}
