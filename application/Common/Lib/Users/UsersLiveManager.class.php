<?php
/**
 * 马甲业务逻辑类
 */
namespace Common\Lib\Users;

use Common\Lib\Service;
use Common\Lib\Users\UsersSuper;

class UsersLiveManager extends Service
{
    /* 房间管理员 */
    function getIsAdmin($uid,$show_id){
        if($uid==$show_id){
            return 50;
        }

        if(UsersSuper::getInstance()->isSuper($uid)){
            return 60;
        }

        if($this->getInfo($uid, $show_id))	{
            return 40;
        }
        return 30;
    }

    public function getInfo($uid, $show_id)
    {
        $info = M("users_livemanager")->where("uid = '$uid' and liveuid = '$show_id'")->find();
        return $info ?: null;
    }


}