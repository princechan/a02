<?php
/**
 * 凯发A02 对接接口
 */
namespace Common\Lib\Auth;

use Common\Lib\Service;
use Common\Lib\Helpers\Curl;



class UserKaifa extends Service
{

    //项目ID
    private static $product_id = 'A02';
    private static $timestamp;

    //测试账号密码
    private static $debugUsername = 'ckf09080002';
    private static $debugPassword = 'd8be69dda476a353bc3e554aec6dc0a3';

    public function __construct()
    {
        $this->setTime();
    }

    public function setTime()
    {
        self::$timestamp = time();
    }

    /**
     * base url
     * @return string
     */
    private static function getApiHost()
    {
        return ENV_DEV ? 'http://www.a02phpapi.com' : 'http://api.k8531.com';
    }

    /**
     * 获取当前请求uri
     * @param $uri
     */
    private static function getApi($uri)
    {
        switch ($uri){
            case 'transfer_balance':
                $url = self::getApiHost() . '/api-transfer-zhibo-balance.htm';
                break;
            case 'get_balance':
                $url = self::getApiHost() . '/api-get-zhibo-balance.htm';
                break;
            case 'check_join_requirements':
                $url = self::getApiHost() . '/liveBookApi.htm';
                break;
            default:
        }
        return $url;
    }

    /**
     * 加密串
     */
    private function buildToken($username)
    {
        $key = trim(file_get_contents('/data/config/ybkey.ini'));
        return md5(self::$product_id . $username . self::$timestamp . $key);
    }

    /**
     * 扣款
     * @param $username
     * @param $password
     * @param $amount
     * @return mixed
     */
    private function transferZhiBoBalance($username, $password, $amount)
    {
        if(!empty($_REQUEST['debug'])) {
            $username = self::$debugUsername;
            $password = self::$debugPassword;
        }

        $data = [
            'username'  => $username,
            'password'  => $password,
            'timestamp' => self::$timestamp,
            'amount'    => $amount,
            'token'     => '',//self::buildToken($username),
        ];
        $response = Curl::getInstance()->post(self::getApi('transfer_balance'), $data);
        return $response;
    }

    /**
     * 查询余额
     * @param $username
     * @param $password
     * @param bool $debug
     * @return float|mixed
     */
    public function getZhiBoBalance($username, $password, $debug = false)
    {
        if(!empty($_REQUEST['debug'])) {
            $username = self::$debugUsername;
            $password = self::$debugPassword;
        }

        $data = [
            'username'  => $username,
            'password'  => $password,
            'timestamp' => self::$timestamp,
            'token'     => '',//self::buildToken($username),
        ];

        $response = Curl::getInstance()->post(self::getApi('get_balance'), $data);
        return  $debug ? $response : @(float)$response['data']['balance'];
    }

    public function testBalance() {
        $username=I('username');
        $user = M('users')->where(['user_login'=>$username])->find();
        $password = $user['user_pass'];
        echo '<pre>';var_dump($user,$this->getZhiBoBalance($username, $password, true));
    }

    /**
     * 预约限制
     * @param $data
     * @return bool
     */
    public function checkJoinRequirements($data)
    {
        $arr = [
            'login_name' => $data['login_name'],
            'timestamp'  => time(),
            'token'      => $this->buildToken($data['login_name']),
        ];

        if(isset($data['flow_limit']) && $data['flow_limit']){
            $arr['flow_limit'] = json_encode($data['flow_limit'], JSON_UNESCAPED_UNICODE);
        }
        if(isset($data['recharge_limit']) && $data['recharge_limit']){
            $arr['recharge_limit'] = json_encode($data['recharge_limit'], JSON_UNESCAPED_UNICODE);
        }

        $response = Curl::getInstance()->post(self::getApi('check_join_requirements'), $arr);
        if($response && (200 == $response['status']) && (true == $response['result'])){
            return true;
        }
        return false;
    }



}