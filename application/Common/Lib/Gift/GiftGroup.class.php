<?php
/**
 * 礼品组管理业务逻辑类
 */
namespace Common\Lib\Gift;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class GiftGroup extends Service
{
    /**
     * 获取所有礼物
     */
    public function getList($ids=[])
    {
        if($ids){
            if(!is_array($ids)){
                $ids = (array)$ids;
            }

            $gift_group_list = M('gift_group')->where(['id' => ['in', $ids]])->order('id DESC')->select() ?: [];
        }else{
            $gift_group_list = M('gift_group')->order('orderno')->select() ?: [];
        }
        return $gift_group_list ? Func::index($gift_group_list, 'id') : [];
    }





}