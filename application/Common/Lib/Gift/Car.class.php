<?php
/**
 * 豪车业务逻辑类
 */
namespace Common\Lib\Gift;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class Car extends Service
{
    /**
     * 获取所有礼物
     */
    public function getList($ids=[])
    {
        if($ids){
            if(!is_array($ids)){
                $ids = (array)$ids;
            }

            $car_list = M('car')->where(['id' => ['in', $ids]])->order('id DESC')->select() ?: [];
        }else{
            $car_list = M('car')->order('id DESC')->select() ?: [];
        }
        return $car_list ? Func::index($car_list, 'id') : [];
    }





}