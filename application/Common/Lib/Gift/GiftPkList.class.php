<?php
/**
 * 礼物pk纪录业务逻辑类
 */
namespace Common\Lib\Gift;

use Common\Lib\Service;

class GiftPkList extends Service
{
    /**
     * add
     */
    public function add($data)
    {
        if(!$data || !is_array($data)){
            $this->setError('参数错误');
            return false;
        }
        return M('giftpk_lists')->add($data);
    }





}