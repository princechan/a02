<?php
/**
 * 礼品管理业务逻辑类
 */
namespace Common\Lib\Gift;

use Common\Lib\Service;
use Common\Lib\Helpers\Func;

class Gift extends Service
{
    /**
     * 获取所有礼物
     */
    public function getList($ids=[])
    {
        if($ids){
            if(!is_array($ids)){
                $ids = (array)$ids;
            }

            $gift_list = M('gift')->where(['id' => ['in', $ids]])->order('id DESC')->select() ?: [];
        }else{
            $gift_list = M('gift')->order('id DESC')->select() ?: [];
        }
        return $gift_list ? Func::index($gift_list, 'id') : [];
    }

    /**
     * 获取单个主播信息
     */
    public function getInfo($id)
    {
        $gift_info = $this->getList($id);
        return $gift_info ? $gift_info[$id] : [];
    }





}