<?php

namespace Common\Controller;

trait ControllerTrait
{
    /**
     * @param array $data
     * @param int $code 200-正常，非200-异常
     * @param string $msg
     * @return mixed
     */
    public function renderJson($data=[], $code = 200, $msg = '')
    {
        $value = [
            'code' => (int)$code,
            'msg'  => "$msg",
            'data' => $data,
        ];

        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode($value, JSON_UNESCAPED_UNICODE));
    }

    protected function renderObject($info=[])
    {
        if (empty($info)) {
            $info = (object)[];
        }

        return $this->renderJson((object)$info);
    }

    protected function renderArray($info=[])
    {
        /*
        if (empty($info)) {
            $info = [];
        }
        */
        return $this->renderJson((array)$info);
    }

    protected function renderError($msg, $code = 400)
    {
        return $this->renderJson([], $code, $msg);
    }

    ######A02###############

    /**
     * jsonpSuccess
     * @param $code
     * @param $msg
     * @param $data
     */
    protected function jsonpSuccess($data=[], $msg, $code=0)
    {
        $arr = [
            'errno' => $code,
            'errmsg'=> $msg,
            'data'  => $data,
        ];
        // 返回JSON数据格式到客户端 包含状态信息
        header('Content-Type:application/json; charset=utf-8');
        $handler  =   isset($_GET[C('VAR_JSONP_HANDLER')]) ? $_GET[C('VAR_JSONP_HANDLER')] : C('DEFAULT_JSONP_HANDLER');
        exit($handler.'('.json_encode($arr,JSON_UNESCAPED_UNICODE).');');
    }

    /**
     * jsonpError
     * @param $code
     * @param $msg
     * @param $data
     */
    protected function jsonpError($code=400, $msg, $data=[])
    {
        $arr = [
            'errno' => $code,
            'errmsg'=> $msg,
            'data'  => $data,
        ];
        // 返回JSON数据格式到客户端 包含状态信息
        header('Content-Type:application/json; charset=utf-8');
        $handler  =   isset($_GET[C('VAR_JSONP_HANDLER')]) ? $_GET[C('VAR_JSONP_HANDLER')] : C('DEFAULT_JSONP_HANDLER');
        exit($handler.'('.json_encode($arr,JSON_UNESCAPED_UNICODE).');');
    }

    public function jsonFormat($data)
    {
        exit(json_encode($data, JSON_UNESCAPED_UNICODE));
    }


}
