<?php
namespace Cli\Controller;

use Think\Controller;
use Common\Lib\Helpers\CSocket;
use Common\Lib\Helpers\Func;
use Common\Lib\System\LiveSysMsg;

set_time_limit(0);
ini_set('memory_limit', '2048M');

/**
 * 后台系统系统消息脚本控制器
 * Class UserController
 * @package Cli\Controller
 */
class LiveSysMsgController extends Controller
{
    static function getUrl()
    {
        return Func::getHost() . '/index.php?g=Cli&m=LiveSysMsg&a=sendSysLiveMsg&id=';
    }

    public function index()
    {
        $mins = date('i', time());
        $rs = M('system_live_msg')->field(['id', 'frequency'])->where(['status'=>2, 'frequency'=>['gt', 0]])->select();

        if($rs){
            set_time_limit(0);
            foreach($rs as $v){
                if(0 == ($mins%$v['frequency'])){
                    file_get_contents(self::getUrl() . $v['id']);
                }
            }
        }
    }

    /**
     * 发送系统消息接口
     */
    public function sendSysLiveMsg()
    {

        $id     = (int)I('id');
        $info   = LiveSysMsg::getInstance()->getInfo($id);
        if(!$info || (0 == $info['status'])){
            exit;
        }

        //检查时间, 并重置状态或脚本
        $info['start_at'] = strtotime($info['start_at']);
        $info['end_at']   = strtotime($info['end_at']);
        if(false == LiveSysMsg::getInstance()->judgment($info)){
            exit;
        }

        $rs             = [];
        $token          = '1234567';
        $live_room_arr  = array_unique(array_filter(explode(',', $info['live_room_id'])));
        if($live_room_arr){

            $content    = $info['link_url'] ? "<a href='{$info["link_url"]}' target='_blank'>" . $info['content'] . "</a>" : $info['content'];
            $config     = Func::getPrivateConfig();
            $client     = new CSocket($config['chatserver']);
            foreach($live_room_arr as $live_room_id){
                $data = [
                    'token'        => $token,
                    'live_room_id' => $live_room_id,
                    'content'      => $content,
                ];
                $client->emit('systemlivemsg', $data);
            }
        }
    }


}
