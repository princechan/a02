<?php
namespace Cli\Controller;

use Think\Controller;
use Common\Lib\Users\User;
use Common\Lib\Helpers\CRedis;


set_time_limit(0);
ini_set('memory_limit', '2048M');

/**
 * 系统脚本控制器
 * Class UserController
 * @package Cli\Controller
 */
class SysController extends Controller
{

    /**
     * redis -db0 清除
     */
    public function flushAll()
    {
        $redis_lib = CRedis::getInstance();
        $redis_lib->select(0);
        $redis_lib->flushAll();

        $users = M('users')->where(['user_type'=>2])->order('id asc')->select();
        foreach ($users as $user) {
            User::getInstance()->getUserInfo($user['id'], true);
        }

        echo 'success!';
    }
}
