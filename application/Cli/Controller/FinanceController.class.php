<?php
namespace Cli\Controller;

use Think\Controller;


/**
 * 玩家财富记录脚本控制器
 * Class UserController
 * @package Cli\Controller
 */
class FinanceController extends Controller
{
    /**
     * 修改消费记录表结构，并将原数据经计算重置,原数据不变
     */
    public function changeColumn()
    {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');
        $sql = 'ALTER TABLE `cmf_users_coinrecord` ADD COLUMN spend_type TINYINT(1) NOT NULL DEFAULT "1" COMMENT "消费类型：1->钻石;2->直播券" AFTER `giftgroup_num`';
        $rs = M()->execute($sql);
        if(0 == $rs){
            $rt = M('users_coinrecord')->where(['total_livecoin' => ['gt', 0]])->save(['spend_type' => 2]);
            if($rt){
                echo 'success!';
            }
        }
    }
}
