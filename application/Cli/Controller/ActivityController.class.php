<?php
namespace Cli\Controller;

use Think\Controller;
use Common\Lib\Activity\Activity;

/**
 * 活动定时器
 * Class ActivityController
 * @package Cli\Controller
 */
class ActivityController extends Controller
{
    /**
     * 每天凌晨1点添加活动当天玩家详情日志入库
     */
    public function setActivityPlayerDetailLog()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        Activity::getInstance()->setActivityPlayerDetailLog();
    }

    /**
     * 每天凌晨2点添加当天活动玩家详情日志入库
     */
    public function setActivityPlayerTotalLog()
    {
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        Activity::getInstance()->setActivityPlayerTotalLog();
    }
}
