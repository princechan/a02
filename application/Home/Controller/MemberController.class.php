<?php

namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Users\User;
use Common\Lib\Users\UsersLive;
use Common\Lib\Helpers\Func;
use Common\Lib\Gift\GiftGroup;
use Common\Lib\Live\LiveSwitch;
use Common\Lib\Activity\Activity;



/**
 * 会员登录
 */
class MemberController extends HomebaseController
{
    const SIDE_BAR_ROUTE = '/index.php?g=home&m=member&a=sidebar&init=1';

    public function login()
    {
        $username   = I('username');
        $password   = I('password');
        $level      = I('level');
        $roomid     = I('roomid');
        $token      = I('token');
        $redirect   = trim(I('redirect'));
        $source     = trim(I('source'));

        $key = trim(@file_get_contents('/data/config/ybkey.ini'));
        $sign = md5("username={$username}password={$password}level={$level}" . $key);

        $rs = ['status' => 0, 'livestream_url' => ''];
        if ($sign != $token) {
            //$this->jsonFormat($rs);
        }

        //测试用
        $password = '###1568c2f160324913af8f52581eeff079';

        //用户登录
        $user = User::getInstance()->login($username, $password);
        if($user){
            //增加用户来源拓展参数
            $extend = [
                'source' => $source,
            ];

            User::getInstance()->setExtUsers($user['id'], $extend);
        }

        //侧边栏特殊处理
        if (!$roomid) {
            if ($redirect == 'side_bar') {
                if (ENV_DEV) {
                    $roomid = 279;
                } else {
                    $roomid = 5;
                }
            } else {
                $Live = M('users_live');
                $liveid = M("channel")->field("liveid")->order('orderno asc')->find();
                if ($liveid) {
                    $info = $Live
                        ->field("uid")
                        ->where("islive=1 and uid={$liveid['liveid']}")
                        ->order("starttime desc")
                        ->find();

                }
                if (!$info) {
                    $info = $Live
                        ->field("uid")
                        ->where("islive=1")
                        ->order("starttime desc")
                        ->find();

                }
                if ($info) {
                    $roomid = $info['uid'];
                } else {
                    $roomid = $liveid['liveid'];
                }
            }
        }

        $configpub = getConfigPub();
        $href = $configpub['site'] . '/' . $roomid;
        if ($redirect == 'side_bar') {
            $forceSsl = (bool)I('ssl');
            $href = getHost($forceSsl) . self::SIDE_BAR_ROUTE . "&roomid={$roomid}";
        }

        header("Location:{$href}");
        exit;
    }

    /**
     * 登出
     */
    public function logout()
    {
        $url = I("url");
        User::getInstance()->logout();
        header("Location:" . $url);
    }

    /**
     * 侧边栏
     * @link http://www.a02zhibo.com/index.php?g=home&m=member&a=sidebar&roomid=12
     */
    public function sidebar()
    {
        $anchorid = (int)I('roomid');
        if (!$anchorid) {
            $this->error('房间ID不存在');
        }

        $user_lib   = User::getInstance();
        $uid        = $user_lib->getUserId();
        $token      = $user_lib->getUserToken();
        $config     = $this->config;

        $getConfigPub = Func::getPublicConfig();
        if ($getConfigPub['maintain_switch'] == 1) {
            $this->assign('jumpUrl', __APP__);
            $this->error(nl2br($getConfigPub['maintain_tips']));
        }

        $getConfigPri = Func::getPrivateConfig();
        $this->assign("configj", json_encode($config));
        $this->assign("ConfigPub", json_encode($getConfigPub));
        $this->assign("getConfigPri", $getConfigPri);
        $this->assign("getConfigPub", $getConfigPub);

        $giftGroupLists = GiftGroup::getInstance()->getList();
        $this->assign("giftGroupLists", json_encode($giftGroupLists));

        //切换到组里最优直播间
        $user_live_lib = UsersLive::getInstance();
        if(!$anchorid){
            $userLive = $user_live_lib->getInfo($anchorid);
            if (!$userLive) {
                $child_liveid = [];
                $switchList = LiveSwitch::getInstance()->getInfo($anchorid);
                if($switchList){
                    foreach ($switchList as $value) {
                        $child_liveid[] = $value['child_liveid'];
                    }
                    $child_liveid = array_unique($child_liveid);
                }
                $live_list = $user_live_lib->getList($child_liveid);
                if($live_list){
                    $live     = current($live_list);
                    $anchorid = $live['uid'];
                }
            }
        }

        $anchorinfo = $user_lib->getUserInfo($anchorid);
        if (!$anchorinfo) {
            $this->error("主播不存在");
        }

        $anchorinfo['level']    = $user_lib->getLevel($anchorinfo['consumption']);
        $anchorinfo['follows']  = $user_lib->getFollownums($anchorinfo['id']);
        $anchorinfo['fans']     = $user_lib->getFansnums($anchorinfo['id']);

        /*设置stream*/
        if ($uid == $anchorinfo['id']) {
            $stream = $anchorid . "_" . time();
        } else {
            $stream = $anchorid . "_" . $anchorid;
        }
        $anchorinfo['stream'] = $stream;

        $this->assign("anchorinfo", $anchorinfo);
        $this->assign("anchorinfoj", json_encode($anchorinfo));

        $liveinfo         = $user_live_lib->getInfo($anchorid, 'islive');
        $liveinfo['pull'] = Func::PrivateKeyA('rtmp', $liveinfo['stream'], 0);

        $this->assign("liveinfo", $liveinfo);
        $this->assign("liveinfoj", json_encode($liveinfo));

        $nowtime = time();
        if ($uid > 0) {
            //判断该用户是否被禁用此IP
            //获取当前的IP
            $currentIP = $_SERVER['REMOTE_ADDR'];
            $Model = M("users_disable_ips");
            $count = $Model->where("uid={$uid}")->count();
            if ($count > 0) {
                $lists = $Model->where("uid={$uid}")->select();
                $isLimitIP = 0;
                foreach ($lists as $k => $v) {
                    if ($currentIP == $v['begin_ip']) {//符合单独IP被限制
                        $isLimitIP = 1;
                        break;
                    }

                    $info = $Model->where("uid={$uid} and inet_aton('{$currentIP}')  between inet_aton('{$v["begin_ip"]}') and inet_aton('{$v["end_ip"]}')")->count();
                    $count1 = intval($info);
                    if ($count1 > 0) {
                        $isLimitIP = 1;
                        break;
                    }
                }

                if ($isLimitIP == 1) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error('您的IP已经被封禁,请联系管理员解封');
                }
            }

            /*是否踢出房间*/
            $redis = connectionRedis();
            $iskick = $redis->hGet($anchorinfo['id'] . 'kick', $uid);
            $nowtime = time();
            if ($iskick > $nowtime) {
                $surplus = $iskick - $nowtime;
                $this->assign('jumpUrl', __APP__);
                $this->error('您已被踢出房间，剩余' . $surplus . '秒');
            } else {
                $redis->hdel($anchorinfo['id'] . 'kick', $uid);
            }

            //判断该房间是否被禁言
            $arr = $redis->hKeys('channelShutUp');
            $ischannelShutUp = 0;
            if ($arr) {
                foreach ($arr as $v) {
                    if ($v == $anchorid) {
                        $ischannelShutUp = 1;
                        break;
                    }
                }
            }

            $this->assign("ischannelShutUp", $ischannelShutUp);
            /*身份判断*/
            $getisadmin = getIsAdmin($uid, $anchorinfo['id']);
            /*该主播是否被禁用*/

            $isBan = isBan($anchorinfo['id']);
            if ($isBan == 0) {
                $this->assign('jumpUrl', __APP__);
                $this->error('该主播已经被禁止直播');
            }
            $isBan = isBan($uid);
            if ($isBan == 0) {
                $this->assign('jumpUrl', __APP__);
                $this->error('你的账号已经被禁用');
            }

            /*进入房间设置redis*/
            $userinfo = M('users')->where("id=" . $uid)->field("id,issuper")->find();
            if ($userinfo['issuper'] == 1) {
                $redis->hset('super', $userinfo['id'], '1');
            } else {
                $redis->hDel('super', $userinfo['id']);
            }

        } else {
            $getisadmin = 10;
        }

        $this->assign('identity', $getisadmin);
        /* 是否关注 */
        $isattention = isAttention($uid, $anchorinfo['id']);
        $this->assign("isattention", $isattention);
        $attention_type = $isattention ? "已关注" : "+关注";
        $this->assign("attention_type", $attention_type);
        $this->assign("anchorid", $anchorid);
        /* 礼物信息 */
        $giftinfo = M('gift')->field("*")->order("orderno asc")->select();
        $this->assign("giftinfoj", json_encode($giftinfo));
        //读取马甲信息
        $vests = M('users_vest')->order("id")->select();
        $this->assign('vests', $vests);

        $vestsj = [];
        foreach ($vests as $k => $v) {
            $vestsj[$v['id']] = $v;
        }

        $this->assign("vestsj", json_encode($vestsj));

        //读取后台配置的直播间轮播图效果
        $slideLists = M("slide")->where("slide_cid=2 and slide_status=1")->select();
        $this->assign('slidelists', $slideLists);

        //答题活动
        $activityJson = Activity::getInstance()->getActivityBaseInfo(Activity::getInstance()->getActivityIdInLive(0, $anchorid));
        if($activityJson){
            $activityJson['current_question'] = Activity::getInstance()->getCurrentQuestionByPlayer($activityJson['id']);
        }
        $this->assign('activityJson', json_encode((object)$activityJson));

        $configpri = M("config_private")->where("id=1")->find();
        /* 判断 播流还是推流 */
        $isplay = 0;
        if ($uid == $anchorinfo['id']) {//主播
            $checkToken = checkToken($uid, $token);
            if ($checkToken == 700) {
                $this->assign('jumpUrl', __APP__);
                $this->error('登陆过期，请重新登陆');
            }

            if ($configpri['auth_islimit'] == 1) {

                $auth = M("users_auth")->field("status")->where("uid='{$uid}'")->find();
                if (!$auth || $auth['status'] != 1) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error("请先进行身份认证");
                }
            }

            if ($configpri['level_islimit'] == 1) {
                if ($anchorinfo['level'] < $configpri['level_limit']) {
                    $this->assign('jumpUrl', __APP__);
                    $this->error('等级小于' . $configpri['level_limit'] . '级，不能直播');
                }
            }

            $token = getUserToken($uid);
            $this->assign('token', $token);
            /* 流地址 */
            $push = PrivateKeyA('rtmp', $stream, 1);
            $this->assign('push', $push);
            $this->assign('pushj', json_encode($push));
            $isplay = 1;
            $this->assign('isplay', $isplay);
            /*主播礼物PK没有信息，置为空数组start*/
            $giftPkArr = [];
            $this->assign('giftPkInfo', json_encode($giftPkArr));
            /*主播礼物PK没有信息，置为空数组end*/
            /*抢板凳没有信息，置为空数组start*/
            $grabBenchArr = [];
            $this->assign('grabBenchInfo', json_encode($grabBenchArr));
            /*抢板凳没有信息，置为空数组end*/
            /*守护没有信息，置为空数组start*/
            $guardInfo = [];
            $this->assign("guardInfoj", json_encode($guardInfo));
            /*守护没有信息，置为空数组end*/
            $sendMsg_Num = "";
            $this->assign("sendMsg_Num", $sendMsg_Num);
        } else {
            $giftPkArr = [];
            $grabBenchArr = [];
            if ($uid) {
                //判断用户的马甲
                $isGapAll = 0;    //默认定义房间禁言权限为0
                $currentUserInfo = M('users')->where("id={$uid}")->find();
                if ($currentUserInfo['vest_id'] > 1) {
                    //获取用户马甲身份权限
                    $currentUserVestInfo = M("users_vest")->where("id='{$currentUserInfo['vest_id']}'")->find();
                    if ($currentUserVestInfo['gap_all'] == 1) {//有频道禁言权限
                        $isGapAll = 1;
                    }
                } else {
                    //从房间马甲列表里查询马甲
                    $currentUserVestInfo = M("users_vest_lists")->where("uid={$uid} and liveid='{$anchorinfo['id']}'")->find();
                    if ($currentUserVestInfo) {
                        $currentUserVestInfo = M("users_vest")->where("id='{$currentUserVestInfo['vestid']}'")->find();
                        if ($currentUserVestInfo['gap_all'] == 1) {//有频道禁言权限
                            $isGapAll = 1;
                        }
                    }
                }

                $this->assign("isGapAll", $isGapAll);
                $redis = connectionRedis();
                /*******获取礼物PK的redis信息start********/
                $giftPkInfo = $redis->get("giftPK_" . $anchorid);
                if ($giftPkInfo) {
                    $giftPkArr = json_decode($giftPkInfo, true);//转换为数组
                    if ($giftPkArr['isEnd'] == 0) {
                        //到PK结束的剩余时间
                        $effectiveTime = $giftPkArr['lastStopTime'] - time();
                        if ($effectiveTime <= 0) {//主播未正常停止游戏造成时间失效
                            $redis->delete("giftPK_" . $anchorid);
                        } else {
                            $giftPkArr['effectiveTime'] = $effectiveTime;
                            $guestUserInfo = getUserInfo($giftPkArr['guestID']);
                            $giftPkArr['guestAvatar'] = $guestUserInfo['avatar'];//客队头像
                            $giftPkArr['guestName'] = $guestUserInfo['user_nicename'];//客队昵称

                            $masterUserInfo = getUserInfo($giftPkArr['uid']);
                            $giftPkArr['masterAvatar'] = $masterUserInfo['avatar'];//主队头像
                            $giftPkArr['masterName'] = $masterUserInfo['user_nicename'];//主队昵称

                            $guestGiftInfo = getGiftInfo($giftPkArr['guestGiftID']);
                            $giftPkArr['guestGiftImg'] = $guestGiftInfo['gifticon'];//客队礼物图片

                            $masterGiftInfo = getGiftInfo($giftPkArr['masterGiftID']);
                            $giftPkArr['masterGiftImg'] = $masterGiftInfo['gifticon'];//主队礼物图片
                        }
                    }
                } else {
                    $giftPkArr = [];
                }

                /*******获取礼物PK的redis信息end********/
                /***********获取抢板凳游戏的redis信息start************/
                $grabBenchInfo = $redis->get("grabbench_" . $anchorid);
                if ($grabBenchInfo) {
                    $grabBenchArr = json_decode($grabBenchInfo, true);//解析成数组
                    if ($grabBenchArr['isEnd'] == 1) {//活动已经结束
                        $redis->delete("grabbench_" . $anchorid);//将redis删除
                        $info['grabbench'] = [];
                    } else {
                        //获取后台配置的连续点击时间间隔
                        $hits_space = $configpri['grabbench_hits_space'];
                        $effectiveTime = $grabBenchArr['effectiveTime'] - time();//到活动结束的剩余时间
                        if ($effectiveTime <= 0) {
                            $redis->delete("grabbench_" . $anchorid);
                            $grabBenchArr = [];
                        } else {
                            $grabBenchArr['hits_space'] = $hits_space;
                            $grabBenchArr['effectiveTime'] = strval($effectiveTime);
                        }
                    }
                } else {
                    $grabBenchArr = [];
                }
                /***********获取抢板凳游戏的redis信息end************/
                $userinfo = getUserPrivateInfo($uid);
                if ($userinfo['iswhite'] == 1) {//用户在白名单中
                    $sendMsg_Num = "";
                } else {
                    if ($uid) {
                        //判断该用户是否守护主播
                        $guardInfo = M("users_guard_lists")->where("uid={$uid} and liveuid={$anchorid} and effectivetime>{$nowtime}")->find();
                    }
                    if ($guardInfo) {   //守护
                        $sendMsg_Num = $config['guard_digits_num'];
                    } else if ($userinfo['chat_num'] > 0) {
                        $sendMsg_Num = $userinfo['chat_num'];
                    } else {
                        $info = M('users_live')->where("islive=1 and uid={$anchorid}")->order("starttime desc")->find();
                        if ($info['chat_num']) {
                            $sendMsg_Num = $info['chat_num'];
                        } else {
                            $sendMsg_Num = 30;
                        }
                    }
                }
            }

            $this->assign('giftPkInfo', json_encode($giftPkArr));
            $this->assign('grabBenchInfo', json_encode($grabBenchArr));
            $this->assign('isplay', $isplay);
            $push = PrivateKeyA('rtmp', $stream, 1);
            $this->assign('push', $push);
            $this->assign('pushj', json_encode($push));
            $this->assign("sendMsg_Num", $sendMsg_Num);
            $this->assign("guardInfoj", json_encode($guardInfo));
        }

        $this->display('sidebar');
    }

    /**
     * 切换直播列表
     */
    public function getSwitchLive()
    {
        $liveId = (int)I('liveid');
        if (!$liveId) {
            return $this->renderJson([], 400, '缺liveid参数');
        }

        $switchList = M('live_switch')->where(['liveid' => $liveId])->order('orderno ASC')
            ->select() ?: [];
        $liveList = $switchList ? M('users_live')
            ->where(['uid' => ['IN', array_column($switchList, 'child_liveid')]])
            ->select() ?: [] : [];
        $liveList = index($liveList, 'uid');
        $list = [];
        foreach ($switchList as $value) {
            if (!empty($liveList[$value['child_liveid']])) {
                $list[] = $liveList[$value['child_liveid']];
            }
        }

        $this->renderJson($list);
    }

    /**
     * 设置切换直播排序
     */
    public function setSwitchLiveSort()
    {
        $liveId = (int)I('liveid');
        $switchLiveId = (int)I('switch_liveid');
        if (!$liveId) {
            return $this->renderJson([], 400, '缺liveid参数');
        }

        $maxSort = M('live_switch')->where(['liveid' => $liveId])->max('orderno');
        M('live_switch')->where(['liveid' => $liveId, 'child_liveid' => $switchLiveId])->save(['orderno' => $maxSort + 1]);

        $this->renderJson();
    }

}
