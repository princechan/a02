<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace Home\Controller;

use Common\Controller\HomebaseController;
use Common\Lib\Users\User;
use Common\Lib\Users\UsersLive;
use Common\Lib\Helpers\Func;
use Common\Lib\Gift\GiftGroup;
use Common\Lib\Live\LiveSwitch;
use Common\Lib\Activity\Activity;
/**
 * 分类页
 */
class TestController extends HomebaseController
{
    const SIDE_BAR_ROUTE = '/index.php?g=home&m=member&a=sidebar&init=1';

    /**
     * 标识首页
     */
    public function index()
    {
        $this->display('/Test/index');
    }

    public function login()
    {
        $username   = I('username');
        $password   = I('password');
        $level      = I('level');
        $roomid     = I('roomid');
        $token      = I('token');
        $redirect   = trim(I('redirect'));
        $source     = trim(I('source'));

        $authcode   = 'rCt52pF2cnnKNB3Hkp';
        $password  = "###".md5(md5($authcode.$password));

        //增加用户来源拓展参数
        $extend = [
            'source' => $source,
        ];

        $user = User::getInstance()->login($username, $password);
        if($user){
            //增加用户来源拓展参数
            $extend = [
                'source' => $source,
            ];

            User::getInstance()->setExtUsers($user['id'], $extend);
        }

        //侧边栏特殊处理
        if (!$roomid) {
            if ($redirect == 'side_bar') {
                if (ENV_DEV) {
                    $roomid = 279;
                } else {
                    $roomid = 5;
                }
            } else {
                $Live = M('users_live');
                $liveid = M("channel")->field("liveid")->order('orderno asc')->find();
                if ($liveid) {
                    $info = $Live
                        ->field("uid")
                        ->where("islive=1 and uid={$liveid['liveid']}")
                        ->order("starttime desc")
                        ->find();

                }
                if (!$info) {
                    $info = $Live
                        ->field("uid")
                        ->where("islive=1")
                        ->order("starttime desc")
                        ->find();

                }
                if ($info) {
                    $roomid = $info['uid'];
                } else {
                    $roomid = $liveid['liveid'];
                }
            }
        }

        $configpub = getConfigPub();
        $href = $configpub['site'] . '/' . $roomid;
        if ($redirect == 'side_bar') {
            $forceSsl = (bool)I('ssl');
            $href = getHost($forceSsl) . self::SIDE_BAR_ROUTE . "&roomid={$roomid}";
        }

        header("Location:{$href}");
        exit;
    }




}


