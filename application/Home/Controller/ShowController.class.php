<?php
/**
 * 直播间
 */
namespace Home\Controller;
use Common\Controller\HomebaseController;

use Common\Lib\Activity\Activity;
use Common\Lib\Helpers\Func;
use Common\Lib\Users\User;
use Common\Lib\Helpers\CRedis;


/**
 * 直播页面
 */
class ShowController extends HomebaseController {
  //首页
	public function index() {

		$uid    = User::getInstance()->getUserId();
		$token  = User::getInstance()->getToken();

		$config=$this->config;
		$getConfigPub=getConfigPub();
        $getConfigPub['app_api_host'] = sp_get_host().'/api/public/';

		if($getConfigPub['maintain_switch']==1){
			$this->assign('jumpUrl',__APP__);
			$this->error(nl2br($getConfigPub['maintain_tips']));
		}

		$getConfigPri = Func::getPrivateConfig();//getConfigPri();

		$this->assign("configj",json_encode($config));
		$this->assign("ConfigPub",json_encode($getConfigPub));
		$this->assign("getConfigPri",$getConfigPri);
		$this->assign("getConfigPub",$getConfigPub);

		/*读取礼物组*/
		$giftGroupLists=M("gift_group")->order("orderno")->select();

		$this->assign("giftGroupLists",$giftGroupLists);

		$User=M('users');
		$Gift=M('gift');
		$Live=M('users_live');
		$Vest=M("users_vest");
		$nowtime=time();
		/* 主播信息 */
		$anchorid=(int)I("roomnum");
		if(!$anchorid){
			$liveid=M("channel")->field("liveid")->order('orderno asc')->find();
			if($liveid){
				$info=$Live
					->field("uid")
					->where("islive=1 and uid={$liveid['liveid']}")
					->order("starttime desc")
					->find();

			}
			if(!$info){
				$info=$Live
					->field("uid")
					->where("islive=1")
					->order("starttime desc")
					->find();
			}
			if($info){
				$anchorid=$info['uid'];
			}else{
				$anchorid=$liveid['liveid'];
			}
		}

		$anchorinfo=getUserInfo($anchorid);

		if(!$anchorinfo){
			$this->error("主播不存在");
		}


		$anchorinfo['level']=getLevel($anchorinfo['consumption']);
		$anchorinfo['follows']=getFollownums($anchorinfo['id']);
		$anchorinfo['fans']=getFansnums($anchorinfo['id']);

		/*设置stream*/
		if($uid==$anchorinfo['id']){

			$stream=$anchorid."_".time();
		}
		else{

			$stream=$anchorid."_".$anchorid;
		}

		$anchorinfo['stream']=$stream;
		$this->assign("anchorinfo",$anchorinfo);
		$this->assign("anchorinfoj",json_encode($anchorinfo) );
		$liveinfo=$Live->where("uid='{$anchorinfo['id']}' and islive=1")->order("islive desc")->limit(1)->find();

		/*if($uid!=$anchorinfo['id']){
			if(!$liveinfo){
				$this->error('主播未开播','/',5);
			}
		}*/

		$liveinfo['pull'] = $liveinfo ? PrivateKeyA('rtmp',$liveinfo['stream'],0) : null;
		$this->assign("liveinfo",$liveinfo);
		$this->assign("liveinfoj",json_encode($liveinfo));

		if($uid>0){
			//判断该用户是否被禁用此IP
			//获取当前的IP
			$currentIP=$_SERVER['REMOTE_ADDR'];
			$Model=M("users_disable_ips");
			$count=$Model->where("uid={$uid}")->count();
			if($count>0){
				$lists=$Model->where("uid={$uid}")->select();
				$isLimitIP=0;
				foreach ($lists as $k => $v) {
					if($currentIP==$v['begin_ip']){//符合单独IP被限制
						$isLimitIP=1;
						break;
					}

					$info=$Model->where("uid={$uid} and inet_aton('{$currentIP}')  between inet_aton('{$v["begin_ip"]}') and inet_aton('{$v["end_ip"]}')")->count();
					$count1=intval($info);
					if($count1>0){
						$isLimitIP=1;
						break;
					}
				}

				if($isLimitIP==1){
					$this->assign('jumpUrl',__APP__);
					$this->error('您的IP已经被封禁,请联系管理员解封');
				}
			}

			/*是否踢出房间*/
			$redis = connectionRedis();
			$iskick=$redis  -> hGet($anchorinfo['id'].'kick',$uid);
			$nowtime=time();
			if($iskick>$nowtime){
				$surplus=$iskick-$nowtime;
				$this->assign('jumpUrl',__APP__);
				$this->error('您已被踢出房间，剩余'.$surplus.'秒');
			}else{
				$redis  -> hdel($anchorinfo['id'].'kick',$uid);
			}

			//判断该房间是否被禁言
			$arr=$redis->hKeys('channelShutUp');

			$ischannelShutUp=0;

	    	if($arr){
	    		foreach ($arr as $v) {
		    		if($v==$anchorid){
		    			$ischannelShutUp=1;
		    			break;
		    		}

	    		}

	    	}


	    	$this->assign("ischannelShutUp",$ischannelShutUp);

			/*身份判断*/
			$getisadmin=getIsAdmin($uid,$anchorinfo['id']);
			/*该主播是否被禁用*/

			$isBan=isBan($anchorinfo['id']);
			if($isBan==0)
			{
				$this->assign('jumpUrl',__APP__);
				$this->error('该主播已经被禁止直播');
			}
			$isBan=isBan($uid);
			if($isBan==0){
				$this->assign('jumpUrl',__APP__);
				$this->error('你的账号已经被禁用');
			}

			/*进入房间设置redis*/
			$userinfo=$User->where("id=".$uid)->field("id,issuper")->find();
			if($userinfo['issuper']==1){
				$redis  -> hset('super',$userinfo['id'],'1');
			}else{
				$redis  -> hDel('super',$userinfo['id']);
			}
		}else{
			$getisadmin=10;
		}
		$this->assign('identity',$getisadmin);

		/* 是否关注 */
		$isattention=isAttention($uid,$anchorinfo['id']);
		$this->assign("isattention",$isattention);
		$attention_type = $isattention ? "已关注" : "+关注" ;
		$this->assign("attention_type",$attention_type);
		$this->assign("anchorid",$anchorid);
		/* 礼物信息 */
		$giftinfo=$Gift->field("*")->order("orderno asc")->select();

		$this->assign("giftinfo",$giftinfo);
		$giftinfoj=array();
		foreach($giftinfo as $k=>$v){
			$giftinfoj[$v['id']]=$v;
		}

		$this->assign("giftinfoj",json_encode($giftinfoj, JSON_UNESCAPED_UNICODE));

		//读取马甲信息
		$vests=$Vest->order("id")->select();
		$this->assign('vests',$vests);

		$vestsj=array();
		foreach($vests as $k=>$v){
			$vestsj[$v['id']]=$v;
		}

		$this->assign("vestsj",json_encode($vestsj));

		//读取后台配置的直播间轮播图效果
		$slideLists=M("slide")->where("slide_cid=2 and slide_status=1")->select();
		$this->assign('slidelists',$slideLists);

        //答题活动
        $activityJson = Activity::getInstance()->getActivityBaseInfo(Activity::getInstance()->getActivityIdInLive(0, $anchorid));
        if($activityJson){
            $activityJson['current_question'] = Activity::getInstance()->getCurrentQuestionByPlayer($activityJson['id']);
        }
        $this->assign('activityJson', json_encode((object)$activityJson));

		$configpri=M("config_private")->where("id=1")->find();
		/* 判断 播流还是推流 */
		$isplay=0;

		if($uid==$anchorinfo['id']){ //播流(主播)

			$checkToken = User::getInstance()->checkToken($uid, $token);
			if($checkToken==700){
				$this->assign('jumpUrl',__APP__);
				$this->error('登陆过期，请重新登陆');
			}

			 if($configpri['auth_islimit']==1){

				$auth=M("users_auth")->field("status")->where("uid='{$uid}'")->find();
				if(!$auth || $auth['status']!=1)
				{
					$this->assign('jumpUrl',__APP__);
					$this->error("请先进行身份认证");
				}
			}

			if($configpri['level_islimit']==1){
				if($anchorinfo['level']<$configpri['level_limit'])
				{
					$this->assign('jumpUrl',__APP__);
					$this->error('等级小于'.$configpri['level_limit'].'级，不能直播');
				}
			}

			$token=getUserToken($uid);
			$this->assign('token',$token);
			/* 流地址 */
			$push=PrivateKeyA('rtmp',$stream,1);

			//var_dump($push);
			$this->assign('push',$push);

			$this->assign('pushj',json_encode($push));

			$isplay=1;

			//M("users_connect_video")->where("liveid={$anchorinfo['id']} and (status=1 or status=2)")->setField("status",3);

			/*主播礼物PK没有信息，置为空数组start*/
			$giftPkArr=array();

			$this->assign('giftPkInfo',json_encode($giftPkArr));

			/*主播礼物PK没有信息，置为空数组end*/

			/*抢板凳没有信息，置为空数组start*/
			$grabBenchArr=array();

			$this->assign('grabBenchInfo',json_encode($grabBenchArr));

			/*抢板凳没有信息，置为空数组end*/

			/*守护没有信息，置为空数组start*/
			$guardInfo=array();
			$this->assign("guardInfoj",json_encode($guardInfo));
			/*守护没有信息，置为空数组end*/
			$sendMsg_Num="";

			$this->assign("sendMsg_Num",$sendMsg_Num);

//			$this->display('index');
		} else{
			$giftPkArr=array();
			$grabBenchArr=array();

			if($uid){

				//判断用户的马甲
				$isGapAll=0;    //默认定义房间禁言权限为0

		    	$currentUserInfo=$User->where("id={$uid}")->find();
		    	if($currentUserInfo['vest_id']>1){

		    		//获取用户马甲身份权限
		    		$currentUserVestInfo=M("users_vest")->where("id='{$currentUserInfo['vest_id']}'")->find();

		    		if($currentUserVestInfo['gap_all']==1){//有频道禁言权限
		    			$isGapAll=1;
		    		}
		    	}else{
		    		//从房间马甲列表里查询马甲
		    		$currentUserVestInfo=M("users_vest_lists")->where("uid={$uid} and liveid='{$anchorinfo['id']}'")->find();

		    		if($currentUserVestInfo){
		    			$currentUserVestInfo=M("users_vest")->where("id='{$currentUserVestInfo['vestid']}'")->find();

			    		if($currentUserVestInfo['gap_all']==1){//有频道禁言权限
			    			$isGapAll=1;
			    		}
		    		}
		    	}

		    	$this->assign("isGapAll",$isGapAll);

				$redis = connectionRedis();

				/*******获取礼物PK的redis信息start********/
				$giftPkInfo=$redis->get("giftPK_".$anchorid);


				if($giftPkInfo){

					$giftPkArr=json_decode($giftPkInfo,true);//转换为数组


					if($giftPkArr['isEnd']==0){
						//到PK结束的剩余时间
						$effectiveTime=$giftPkArr['lastStopTime']-time();

						if($effectiveTime<=0){//主播未正常停止游戏造成时间失效
							$redis->delete("giftPK_".$anchorid);

						}else{
							$giftPkArr['effectiveTime']=$effectiveTime;

							$guestUserInfo=getUserInfo($giftPkArr['guestID']);

							$giftPkArr['guestAvatar']=$guestUserInfo['avatar'];//客队头像
							$giftPkArr['guestName']=$guestUserInfo['user_nicename'];//客队昵称

							$masterUserInfo=getUserInfo($giftPkArr['uid']);
							$giftPkArr['masterAvatar']=$masterUserInfo['avatar'];//主队头像
							$giftPkArr['masterName']=$masterUserInfo['user_nicename'];//主队昵称

							$guestGiftInfo=getGiftInfo($giftPkArr['guestGiftID']);
							$giftPkArr['guestGiftImg']=$guestGiftInfo['gifticon'];//客队礼物图片

							$masterGiftInfo=getGiftInfo($giftPkArr['masterGiftID']);
							$giftPkArr['masterGiftImg']=$masterGiftInfo['gifticon'];//主队礼物图片

							/*unset($giftPkArr['uid']);
							unset($giftPkArr['guestID']);
							*/
						}
					}
				}else{
					$giftPkArr=array();
				}

				//注意后面的assign
				/*******获取礼物PK的redis信息end********/
				/***********获取抢板凳游戏的redis信息start************/

				$grabBenchInfo=$redis->get("grabbench_".$anchorid);

				if($grabBenchInfo){

					$grabBenchArr=json_decode($grabBenchInfo,true);//解析成数组

					//var_dump($grabBenchArr);

					if($grabBenchArr['isEnd']==1){//活动已经结束
						$redis->delete("grabbench_".$anchorid);//将redis删除

						$info['grabbench']=array();

					}else{

						$grabbenchID=$grabBenchArr['grabbenchID'];

						//获取后台配置的连续点击时间间隔
						$hits_space=$configpri['grabbench_hits_space'];
						$effectiveTime=$grabBenchArr['effectiveTime']-time();//到活动结束的剩余时间

						//var_dump($effectiveTime);

						if($effectiveTime<=0){

							$redis->delete("grabbench_".$anchorid);
							$grabBenchArr=array();

						}else{
							$grabBenchArr['hits_space']=$hits_space;
							$grabBenchArr['effectiveTime']=strval($effectiveTime);
						}
					}
				}else{
					$grabBenchArr=array();
				}

				//注意后面的assign

				//var_dump($grabBenchArr);

				/***********获取抢板凳游戏的redis信息end************/

				$userinfo=getUserPrivateInfo($uid);

				if($userinfo['iswhite']==1){//用户在白名单中

					$sendMsg_Num="";
				}else{
					if($uid){
						//判断该用户是否守护主播
						$guardInfo=M("users_guard_lists")->where("uid={$uid} and liveuid={$anchorid} and effectivetime>{$nowtime}")->find();
					}

					if($guardInfo){   //守护
						//var_dump($config);
						$sendMsg_Num=$config['guard_digits_num'];

					}else if($userinfo['chat_num']>0){
						$sendMsg_Num=$userinfo['chat_num'];
					}else{
						$info=$Live->where("islive=1 and uid={$anchorid}")->order("starttime desc")->find();
						if($info['chat_num']){
							$sendMsg_Num=$info['chat_num'];
						}else{
							$sendMsg_Num=30;
						}
					}
				}
			}

			$this->assign('giftPkInfo',json_encode($giftPkArr));

			$this->assign('grabBenchInfo',json_encode($grabBenchArr));

			/* 流地址 */
			$push=PrivateKeyA('rtmp',$stream,1);
			$this->assign('push',$push);
			$this->assign('pushj',json_encode($push));

			$this->assign("sendMsg_Num",$sendMsg_Num);

			$this->assign("guardInfoj",json_encode($guardInfo));

//			if($liveinfo['ispc']==1){
//				$this->display("index");
//			}else{
//				$this->display("indexApp");
//			}
		}
        $this->assign('isplay',$isplay);

        $this->display("index");
  }

	/*
	二维码=====
	value 二维码连接地址
	*/
	function qrcode(){
		$roomid=$_GET["roomid"];
		include 'simplewind/Lib/Util/phpqrcode.php';
		$a= new \QRcode();
		$value = "http://".$_SERVER['SERVER_NAME'].'/wxshare/index.php/Share/show?roomnum='.$roomid;
		$errorCorrectionLevel = 'L';//容错级别
		$matrixPointSize = 6;//生成图片大小
		//生成二维码图片
		$a->png($value, 'qrcode.png', $errorCorrectionLevel, $matrixPointSize, 2);
		$logo = 'jb51.png';//准备好的logo图片
		$QR = 'qrcode.png';//已经生成的原始二维码图
		if ($logo !== FALSE) {
			$QR = imagecreatefromstring(file_get_contents($QR));
			$logo = imagecreatefromstring(file_get_contents($logo));
			$QR_width = imagesx($QR);//二维码图片宽度
			$QR_height = imagesy($QR);//二维码图片高度
			$logo_width = imagesx($logo);//logo图片宽度
			$logo_height = imagesy($logo);//logo图片高度
			$logo_qr_width = $QR_width / 5;
			$scale = $logo_width/$logo_qr_width;
			$logo_qr_height = $logo_height/$scale;
			$from_width = ($QR_width - $logo_qr_width) / 2;
			//重新组合图片并调整大小
			imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,
			$logo_qr_height, $logo_width, $logo_height);
		}
		//输出图片
		Header("Content-type: image/png");
		ImagePng($QR);
	}

    /**
     * 用户进入 写缓存  50本房间主播 60超管 40管理员 30观众 10为游客(判断当前用户身份)
     */
    public function setNodeInfo()
    {
        /* 当前用户信息 */
        $live_type  = (int)I('live_type');
        $show_id    = (int)I('showid');
        $stream     = (int)I('stream');
        $this->jsonFormat(User::getInstance()->setNodeInfo($live_type, $show_id, $stream));
    }

	//开播设置
	public function createRoom()
    {
		$token=I("token");
		$stream=I("stream");
		$uid=I("uid");
		$title=I("title");
		$type=I("type");
		$type_val=I("stand");
		$thumb=I("thumb");
		$sendMsgNum=I("sendMsgNum");
		$sendMsgFrequency=I("sendMsgFrequency");
		if($thumb){
			$thumb=get_upload_path($thumb);
		}
		$User=M("users");
		$live=M("users_live");
		$userinfo= $User->field('coin,token,expiretime,user_nicename,avatar,avatar_thumb')->where("id='{$uid}'")->find();
		if($userinfo['token']!=User::getInstance()->getToken() || $userinfo['expiretime']<time())
		{
			echo '{"state":"1002","data":"","msg":"Token以过期，请重新登录"}';
			exit;
		}
		else
		{
			$getConfigPri=getConfigPri();
			$orderid=explode("_",$stream);
			$showid=$orderid[1];
			$users_live=$live->field("uid")->where('uid='.$uid)->find();
			$data=array(
				"avatar"=>get_upload_path($userinfo['avatar']),
				"avatar_thumb"=>get_upload_path($userinfo['avatar_thumb']),
				"showid"=>$showid,
				"user_nicename"=>$userinfo['user_nicename'],
				"islive"=>"1",
				"starttime"=>$showid,
				"title"=>$title,
				"province"=>"",
				"city"=>"好像在火星",
				"stream"=>$stream,
				"pull"=>"rtmp://".$getConfigPri['pull_url']."/5showcam/".$stream,
				"lng"=>"",
				"lat"=>"",
				"topicid"=>"",
				"type"=>$type,
				"thumb"=>$thumb,
				"type_val"=>$type_val,
				"ispc"=>1,
				"chat_num"=>$sendMsgNum,
				"chat_frequency"=>$sendMsgFrequency
			);
			if($users_live){
				/* 更新 */
				$rs=$live->where('uid='.$uid)->save($data);
			}else{
				/*新增*/
				$data['uid']=$uid;
				$rs=$live->add($data);
			}

			if($rs)
			{
			    /* 这东西不知道有什么用，废业务逻辑
				$result['city']="好像在火星";
				$result['sign'] = md5($uid.'_'.$uid);
				$redis = connectionRedis();
				$redis->set($token,json_encode($result));
			    */

				echo '{"state":"0","data":"","msg":""}';
			}
			else
			{
				echo '{"state":"1000","data":"","msg":"直播信息处理失败"}';
			}

		}
	}
	/*
	用户列表弹出信息
	uid_admin 50本房间主播 60超管 40管理员 30观众 10为游客(判断当前用户身份)
	touid_admin 50本房间主播 60超管 40管理员 30观众 10为游客(判断当前点击用户身份)
	*/
	public function popupInfo()
    {
		$uid = User::getInstance()->getUserId();
		$touid=I('touid');
		$roomid=I('roomid');
		$users=M("Users");
		$info=$users->field("id,avatar,avatar_thumb,user_nicename,iswhite")->where("id='{$touid}'")->find();

		if($uid>0 &&$uid!=null)
		{
			$uid_admin=getIsAdmin($uid,$roomid);
			$isBlack=isBlack($uid,$touid);
		}
		else
		{
			$uid_admin=10;
			$isBlack="";
		}
		if($touid>0 &&$touid!=null)
		{
			$touid_admin=getIsAdmin($touid,$roomid);
		}
		else
		{
			$touid_admin=10;
            $info = CRedis::getInstance()->get(User::getInstance()->getVisitorKey($touid));
            $info = $info ? json_decode($info, true) : null;
		}

		$popupInfo=array(
			"state"=>"0",
			"uid_admin"=>$uid_admin,
			"touid_admin"=>$touid_admin,
			"info"=>$info,
			"isBlack"=>$isBlack
		);


		//获取当前用户的马甲
		$vestid=M("users_vest_lists")->where("uid='{$uid}' and liveid='{$roomid}'")->getField("vestid");
		if($vestid){
			$popupInfo['uid_vestid']=$vestid;
		}else{
			$popupInfo['uid_vestid']=1;//默认白马甲
		}

		//判断后台是否设置紫马或黑马
		$userVestID=$users->where("id='{$uid}'")->getField("vest_id");
		if($userVestID>1){
			$popupInfo['uid_vestid']=$userVestID;
		}


		//获取touid用户的马甲
		$vestid=M("users_vest_lists")->where("uid='{$touid}' and liveid='{$roomid}'")->getField("vestid");
		if($vestid){
			$popupInfo['touid_vestid']=$vestid;
		}else{
			$popupInfo['touid_vestid']=1;//默认白马甲
		}

		//判断后台是否设置紫马或黑马
		$userVestID=$users->where("id='{$touid}'")->getField("vest_id");
		if($userVestID>1){
			$popupInfo['touid_vestid']=$userVestID;
		}


		$popupInfo=json_encode($popupInfo);
		echo $popupInfo;
	}
	public function live()
	{
		$uid=I('uid');
		$liveinfo=M("users_live")->where("uid='{$uid}' and islive='1'")->find();
		$data=array(
			'error'=>0,
			'data'=>$liveinfo,
			'msg'=>'',
		);
		echo json_encode($data);
		exit;
	}
		/* 排行榜 */
		public function rank(){
			$touid=I('touid');
			$showid=I('showid');
			$list=array();

			if(!$touid){
				echo json_encode($list);
					exit;
			}

			//获取后台的钻石直播券兑换比例
			$live_coin_percent=M("config_private")->where("id=1")->getField("live_coin_percent");



			$Coinrecord=M('users_coinrecord');
			//本房间魅力榜


			$sql="select uid,sum(totalcoin) as totalcoin from cmf_users_coinrecord where type='expend' and touid='{$touid}' group by uid order by totalcoin desc limit 0,10";

			$now=$Coinrecord->query($sql);

			foreach ($now as $k => $v) {
				$userinfo=getUserInfo($v['uid']);
				$now[$k]['userinfo']=$userinfo;
			}


			$list['now']=$now;



			//全站魅力榜



			$sql="select uid,sum(totalcoin) as totalcoin from cmf_users_coinrecord where type='expend' group by uid order by totalcoin desc limit 0,10";
			$all=$Coinrecord->query($sql);

			foreach ($all as $k => $v) {
				$userinfo=getUserInfo($v['uid']);
				$all[$k]['userinfo']=$userinfo;
			}

			$list['all']=$all;


			echo json_encode($list);


		}






		/*进入直播间检查房间类型*/
		public function checkLive(){
			$rs = array('code' => 0, 'msg' => '', 'info' => array());
			$uid=User::getInstance()->getUserId();
			$token=User::getInstance()->getToken();
			$liveuid=I("liveuid");
			$stream=I("stream");
			$config=$this->config;
			$rs['land']=0;
			$islive=M('users_live')->field("islive,type,type_val,starttime")->where("uid='{$liveuid}' and stream='{$stream}'")->find();
			if($islive['type']==2)
			{
				if($uid>0){
					$rs['land']=0;
				}else{
					$rs['land']=1;
					$rs['type']=$islive['type'];
					$rs['type_msg']='当前房间为付费房间，请先登陆';
					echo json_encode($rs);
					exit;
				}
			}
			if(!$islive ||$islive['islive']==0)
			{
				$rs['code'] = 1005;
				$rs['msg'] = '直播已结束，请刷新';
				echo json_encode($rs);
				exit;
			}
			else
			{
				$rs['type']=$islive['type'];
				$rs['type_msg']='';
				if($islive['type']==1){
					$rs['type_msg']=$islive['type_val'];
				}
				else if($islive['type']==2)
				{
					$rs['type_msg']='本房间为收费房间，需支付'.$islive['type_val'].' '.$config['name_coin'];
					$isexist=M("users_coinrecord")->field('id')->where("uid=".$uid." and touid=".$liveuid." and showid=".$islive['starttime']." and action='roomcharge' and type='expend'")->find();
					if($isexist)
					{
						$rs['type']='0';
						$rs['type_msg']='';
					}
				}
				else if($islive['type']==3)
				{
					$rs['type_msg']='本房间为计时房间，每分钟支付需支付'.$islive['type_val'].' '.$config['name_coin'];
				}
			}
			echo  json_encode($rs);
		}
		/* 直播/回放 结束后推荐 */
		public function endRecommend(){
			/* 推荐列表 */

			$list=M("users_live")->where("islive='1'")->order("rand()")->limit(0,3)->select();
			foreach($list as $k=>$v){
				$list[$k]['userinfo']=getUserInfo($v['uid']);
			}
			$data=array(
					'error'=>0,
					'data'=>$list,
			 );
			echo  json_encode($data);
		}
		/* 关注 */
		public function attention(){
			/* 推荐列表 */
			$uid=User::getInstance()->getUserId();
			if($uid == 0 || $uid == '0' || $uid == ""){
				$data=array(
					'error'=> 1,
					'msg'=> "请登录",
					'data'=> "请登录",
				);

			}else{
				$anchorid=(int)I("roomnum");
				if($uid == $anchorid){
					$data=array(
						'error'=> 1,
						'msg'=> "不能关注自己",
						'data'=> "不能关注自己"
					);
				}else{
					$add=array(
						'uid'	=>	$uid,
						'touid'	=>	$anchorid
					);
					if(isAttention($uid,$anchorid)){
						$check=M('users_attention')->where($add)->delete();
						if($check !== false){
							$data=array(
								'error'=> 0,
								'msg'=> "+关注",
								'data'=> getFansnums($anchorid)
							);
						}else{
							$data=array(
								'error'=> 1,
								'data'=> '',
								'msg'=> "取消关注失败",
							);
						}
					}else{
						$check=M('users_attention')->add($add);
						$black=M('users_black')->where($add)->delete();
						if($check !== false){
							$data=array(
								'error'=> 0,
								'msg'=> "已关注",
								'data'=> getFansnums($anchorid)
							);
						}else{
							$data=array(
								'error'=> 1,
								'msg'=> "关注失败",
								'data'=> "",
							);
						}
					}
				}
			}
			echo  json_encode($data);
		}
		/* 获取粉丝数量 */
		public function getattentionnums(){
			$anchorid=I('anchorid');
			$data=array(
				'error'=> 0,
				'msg'=> "",
				'data'=> getFansnums($anchorid)
			);
			echo  json_encode($data);
		}
		/*主播页面特殊直播弹窗*/
		public function selectplay()
		{
			$this->display();
		}
		public function settimes()
		{

			$this->display();
		}
		public function getUserList(){

			$show_id = (int)I("showid");
			$rs = User::getInstance()->getOnlineUsers($show_id);
            $this->jsonFormat($rs);
		}

	public function getHall(){

		$p=I('times');
		$pnum=I('defnums');
		$start=($p-1)*$pnum;

		$nowtime=time();
		$result=M("channel c")
				->field("l.uid,l.avatar,l.avatar_thumb,l.user_nicename,l.title,l.city,l.stream,l.pull,l.thumb,l.ispc,l.starttime")
				->join("left join __USERS_LIVE__ l on l.uid=c.liveid")
				->where("l.islive= '1'")
				->order("c.orderno asc,starttime desc")
				->limit($start,$pnum)
				->select();
		$redis = connectionRedis();
		foreach($result as $k=>$v){
			$nums=$redis->hlen('userlist_'.$v['stream']);
			$result[$k]['nums']=(string)$nums;

			if(!$v['thumb']){
				$result[$k]['thumb']=$v['avatar'];
			}
			$result[$k]['length']=datelong($nowtime-$v['starttime']);
		}
		$redis -> close();
		echo  json_encode($result);
	}


	/*检测用户是否是主播的管理员*/
	public function checkManager(){
		$liveuid=I("liveuid");
		$uid=I("uid");
		$result=M("users_livemanager")
		->where("liveuid={$liveuid} and uid={$uid}")
		->find();


		$rs=array();
		if($result){
			$rs['code']=0;
			$rs['msg']="是管理员";
		}else{
			$rs['code']=1;
			$rs['msg']="不是管理员";
		}

		echo json_encode($rs);
	}


	public function checkSuper(){
		$uid=I("uid");
		$userinfo=M("users")->where("id={$uid}")->find();

		if($userinfo['issuper']==1){
			$rs['code']=0;
			$rs['msg']="是超管";
		}else{
			$rs['code']=1;
			$rs['msg']="不是超管";
		}

		echo json_encode($rs);
	}


	/*转盘游戏判断用户是否存在*/
	public function checkUser(){
		$uid=I("uid");
		$stream=I('stream');
		$info=M("users")->where("id={$uid}")->find();

		$rs=array('code'=>0,'msg'=>'','info'=>array());

		if(!$info){

			$rs['code']=1001;
			$rs['msg']='该用户不存在';
			echo json_encode($rs);
			exit;

		}else{

			if($info['iszombie']==1){

				$rs['code']=1002;
				$rs['msg']='该用户无法开启游戏';
				echo json_encode($rs);
				exit;
			}

			$redis = connectionRedis();

			$list=$redis -> hVals('userlist_'.$stream);
				//var_dump($list);

				//判断该用户是否在本房间内
				$inRoom=0;
				foreach ($list as $k => $v) {

					$arr=json_decode($v,true);

					if(intval($arr['id'])==$uid){
						$inRoom=1;
						break;
					}
				}


				if($inRoom==1){ //在房间内


					$info=M("users_carouse")
					->where("id=1")
					->find();


					if($info){
						$randNum=rand(1,$info['nums']);
						$info['randNum']=strval($randNum);
						$info['imgUrl']=get_upload_path($info['url']);
						unset($info['url']);
						unset($info['addtime']);

					}else{
						$info=-1;
					}

					if($info==-1){
						$rs['msg']='暂无转盘游戏';
						$rs['code']=1001;
						echo json_encode($rs);
						exit;
					}else{

						$rs['code']=0;
						$rs['info']=$info;
						echo json_encode($rs);
						exit;
					}


				}else{ //不在房间内

					$rs['code']=1001;
					$rs['msg']="该用户不在本房间";

					echo json_encode($rs);
					exit;


				}
		}


		echo json_encode($rs);
	}



	function getRandNum(){
		$info=M("users_carouse")
		->where("id=1")
		->find();

		$rs=array();

		if($info){
			$randNum=rand(1,$info['nums']);

			$rs['code']=0;
			$rs['randNum']=$randNum;

		}else{
			$rs['code']=1001;
		}

		echo json_encode($rs);
	}

	function changeStatus(){
		$senduID=I("sendUid");
		$recordID=I("recordID");
		$status=I("status");
		$result=M("users_connect_video")->where("id={$recordID}")->setField('status',$status);
        $uid = User::getInstance()->getUserId();

		 if($result !== false){
		 	$stream=$uid."_".$senduID."_".time();
		 	$data=array('code'=>0,'msg'=>'修改成功','stream'=>$stream);
            echo json_encode($data);
            exit;
        }else{
            $data=array('code'=>1001,'msg'=>'修改失败');
             echo json_encode($data);
             exit;
        }
	}



	function checkChannelShupUp(){

		$showid=I("showid");
		$redis = connectionRedis();
		$arr=$redis->hKeys('channelShutUp');
		$rs=array('code'=>0,'msg'=>'','info'=>0);

    	$ischannelShupUp=0;
    	foreach ($arr as $v) {
    		if(intval($v)==$showid){
    			$ischannelShupUp=1;
    			break;
    		}
    		
    	} 	

    	if($ischannelShupUp==1){
    		$rs['msg']="该房间已经被禁言";
    		$rs['info']=1;
    	}

    	echo json_encode($rs);
    	exit;


	}


	//从users_vest_lists表中检查用户的vestid
	public function checkVest(){
		$uid=I("uid");
		$touid=I("touid");
		$roomid=I("roomid");

		$userVestID=0;
		$touidVestID=0;

		//判断当前用户是否被设置成黑马或紫马
		$VestID=M("users")->where("id='{$uid}'")->getField("vest_id");
		if($VestID>1){
			$userVestID=$VestID;
		}else{
			//从房间，马甲列表中查找
			$VestID=M("users_vest_lists")->where("uid='{$uid}' and liveid='{$roomid}'")->getField("vestid");
			if($VestID){
				$userVestID=$VestID;
			}else{
				$userVestID=1;//默认白马甲
			}
		}

		//判断touid的马甲id
		$VestID=M("users")->where("id='{$touid}'")->getField("vest_id");

		if($VestID>1){
			$touidVestID=$VestID;
		}else{
			//从房间，马甲列表中查找
			$VestID=M("users_vest_lists")->where("uid='{$touid}' and liveid='{$roomid}'")->getField("vestid");
			if($VestID){
				$touidVestID=$VestID;
			}else{
				$touidVestID=1;//默认白马甲
			}
		}



		$rs=array('code'=>0,'userVestID'=>0,'touidVestID'=>0);

		$rs['userVestID']=$userVestID;
		$rs['touidVestID']=$touidVestID;

		echo json_encode($rs);
		exit;
	}



	public function selectvest(){
		$uid=I("uid");
		$touid=I("touid");
		$type=I("type");
		$vestLists=I("vestLists");
		$this->assign("vestLists",$vestLists);
		$this->assign("touid",$touid);
		$this->display();
	}

	//更换马甲

	public function changeVest(){
		$roomid=I("roomid");
		$touid=I("touid");
		$vestid=I("vestid");
		$stream=I("stream");

		//获取被更换马甲的昵称
		$user_nicename=M("users")->where("id={$touid}")->getField("user_nicename");

		$rs=array('code'=>0,'msg'=>'','info'=>'');
		if($roomid==""||$touid==""){
			$rs['code']=1001;
			$rs['msg']="参数错误";
			echo json_encode($rs);

		}else{

			$vestLists=M("users_vest_lists");
			$vestInfo=$vestLists->where("uid={$touid} and liveid={$roomid}")->find();
			if($vestInfo){
				$data=array(
					'vestid'=>$vestid,
					'addtime'=>time(),
				);
				$res=$vestLists->where("uid={$touid} and liveid={$roomid}")->save($data);
				if($res!==false){

					//更新用户列表缓存
					
					$redis = connectionRedis();
					$userStr=$redis->hGet('userlist_'.$stream,md5($roomid.'_'.$touid));

					//file_put_contents("1.txt", $userStr);
					if($userStr){
						$userArr=json_decode($userStr,true);
						$userArr['vestid']=$vestid;
						//获取用户的性别
						$sex=M("users")->where("id={$touid}")->getField("sex");
						if($sex==0){
							$sex=1;
						}

						if($sex==1){
							$vestIcon=M("users_vest")->where("id={$vestid}")->getField("vest_man_url");
						}

						if($sex==2){
							$vestIcon=M("users_vest")->where("id={$vestid}")->getField("vest_woman_url");
						}



						
						$userArr['vestIcon']=$vestIcon;
						$redis->hSet('userlist_'.$stream,md5($roomid.'_'.$touid),json_encode($userArr));
					}

					//$userStr=$redis->hGet('userlist_'.$stream,md5($roomid.'_'.$touid));
					//file_put_contents("2.txt", $userStr);

					$rs['msg']="更换马甲成功";
					$rs['info']=$user_nicename;
				}else{
					$rs['code']=1001;
					$rs['msg']="更换马甲失败";
				}

			}else{
				$data=array(
					'uid'=>$touid,
					'liveid'=>$roomid,
					'vestid'=>$vestid,
					'addtime'=>time(),
				);

				$res=$vestLists->add($data);
				if($res){
					$rs['msg']="更换马甲成功";
					$rs['info']=$user_nicename;
				}else{
					$rs['code']=1001;
					$rs['msg']="更换马甲失败";
				}

			}



		}

		echo json_encode($rs);

		exit;
	}



	function checkIsLive(){
		$uid=I("uid");
		$liveuid=I("liveuid");
		$rs=array();


		$liveInfo=M("users_live")->where("uid={$liveuid} and islive=1")->find();
		if(!$liveInfo){
			$rs['code']=1001;
			$rs['msg']="当前主播未直播,无法发起连麦";

			echo json_encode($rs);
			exit;
		}else{
			$userInfo=M("users")->where("id={$uid}")->find();
			if($userInfo){

				//判断当前直播间是否有用户在连麦
				$Lianmai=M("users_connect_video");

				$connectLists=$Lianmai->where("liveid={$liveuid} and (status=0 or status=1) ")->select();
				$connectNum=count($connectLists);
				if($connectNum>0){


					$iscurrentUser=0;
					$isConnectVideo=0;
					//判断当前连麦的用户是否是该用户
					foreach ($connectLists as $k => $v) {
						if($v['uid']==$uid){//当前连麦是当前用户

							$iscurrentUser=1;
							$isConnectVideo=$v['status'];
							break;
						}
					}


					if($iscurrentUser==1){

						$rs['code']=1001;
						if($isConnectVideo==1){
							$rs['msg']="您正在跟主播连麦";
						}else{
							$rs['msg']="您已经申请连麦";
						}
						
						echo json_encode($rs);

					}else{

						$rs['code']=1001;
						$rs['msg']="当前主播正在连麦";
						echo json_encode($rs);
					}



					
					exit;

				}else{

					

					//判断是否有该用户和主播的连麦记录
					$info=$Lianmai->where("liveid={$liveuid} and uid={$uid} ")->find();

					if($info){
						$data=array(
							'status'=>0,
							'addtime'=>time()
						);

						$result=$Lianmai->where("uid={$uid} and liveid={$liveuid}")->save($data);

					}else{

						$data=array(
							'uid'=>$uid,
							'liveid'=>$liveuid,
							'status'=>0,
							'addtime'=>time()
						);

						$result=$Lianmai->add($data);
					}

					$user_nicename=$userInfo['user_nicename'];
					
					if($result!==false){
						$rs['code']=0;
						$rs['msg']='';
						$rs['info']=$user_nicename;
						$rs['recordID']=$result;
						//$rs['stream']=$liveuid."_".$uid."_".time();
					}else{
						$rs['code']=1002;
						$rs['msg']="连麦失败啦";
					}

					

					echo json_encode($rs);
					exit;



				}

				

			}else{
				$rs['code']=1002;
				$rs['msg']="连麦失败啦,请确认您的身份";
				echo json_encode($rs);
				exit;
			}
		}
	}



	public function checkLianmaiStatus(){
		$userid=I("userid");
		$roomid=I("roomid");
		$rs=array('code'=>0,'msg'=>'','info'=>'');

		$info=M("users_connect_video")->where("uid={$userid} and liveid={$roomid}")->find();

		if($info['status']==0){//主播一直没有操作

			$rs['code']=0;
			$rs['msg']="主播未操作";
			//将对应记录状态改为连麦结束
			/*$data=array(
				"status"=>2,
			);
			M("users_connect_video")->where("uid={$userid} and liveid={$roomid}")->save($data);*/
		}else if($info['status']==1){//正在连麦
			$rs['code']=1001;
			$rs['msg']="您当前正在连麦";
		}else if($info['status']==2){//拒绝
			$rs['code']=1002;
			$rs['msg']="您的连麦已经被拒绝";
		}else if($info['status']==3){//您的连麦已经结束了
			$rs['code']=1003;
			$rs['msg']="您的连麦已经结束了";
		}

		echo json_encode($rs);
		exit;

	}

	//向直播记录里添加连麦用户的stream

	public function changeLianmaiStream(){
		$liveid=I("liveid");
		$stream=I("stream");

		$data=array(
			'lianmai_stream'=>$stream,
		);

		$res=M("users_live")->where("uid={$liveid}")->save($data);

		if($res!==false){
			$rs=array(
				'code'=>0,
				'msg'=>'更新连麦用户stream成功',
			);

		}else{
			$rs=array(
				'code'=>1001,
				'msg'=>'更新连麦用户stream失败',
			);
		}
		


		echo json_encode($rs);
	}


	public function getLianmaiStream(){
		$stream=I("stream");

		$rs=array('code'=>0,'msg'=>'','info'=>'');

		$url=PrivateKeyA('rtmp',$stream,0);

		$rs['info']=$url;

		echo json_encode($rs);

	}


	/*私信聊天判断用户是否被对方拉黑*/
	public function currentChatUid(){
		$uid=I("uid");
		$touid=I("touid");
		$info=M("users_black")->where("uid={$touid} and touid={$uid}")->find();

		$rs=array('code'=>0,'msg'=>'','info'=>'');

		if($info){
			$rs['msg']="被对方拉黑";
		}else{
			$rs['code']=1001;
			$rs['msg']="未被对方拉黑";
		}


		echo json_encode($rs);



	}



	public function getUserPrivateChat(){

		$uid=I("uid");
		$liveid=I("liveid");

		$rs=array('code'=>0,'msg'=>'','info'=>'');

		$vestID=M("users")->where("id={$uid}")->getField("vest_id");

		if($vestID>1){//紫马或黑马
			$vestInfo=M("users_vest")->where("id={$vestID}")->find();
			if($vestInfo['private_chat']==1){

				$rs['msg']="有私信权限";
			}else{
				$rs['code']=1001;
				$rs['msg']="没有私信权限";
			}

		}else{ //普通马甲

			//从房间马甲列表查询
			$vestListInfo=M("users_vest_lists")->where("uid={$uid} and liveid={$liveid}")->find();

			if($vestListInfo){
				$vestID=$vestListInfo['vestid'];
				//根据马甲id获取马甲权限
				$vestInfo=M("users_vest")->where("id={$vestID}")->find();
				if($vestInfo['private_chat']==1){
					$rs['code']=0;
					$rs['msg']="有私信权限";
				}else{
					$rs['code']=1001;
					$rs['msg']="没有私信权限";
				}


			}else{ //默认白马
				$vestInfo=M("users_vest")->where("id=1")->find();
				if($vestInfo){
					if($vestInfo['private_chat']==1){ //白马有私聊权限
						$rs['code']=0;
						$rs['msg']="有私信权限";
					}else{
						$rs['code']=1001;
						$rs['msg']="没有私信权限";
					}

				}else{
					$rs['code']=1001;
					$rs['msg']="没有私信权限";
				}
			}


		}

		echo json_encode($rs);



	}


	public function chackIsBlack(){
		$uid=I("uid");
		$touid=I("touid");

		$rs=array();
		$info=M("users_black")->where("uid={$touid} and touid={$uid}")->find();
		if($info){
			$rs['code']=0;
		}else{
			$rs['code']=1001;
		}


		echo json_encode($rs);

	}


	public function stopLianmai(){

		$liveid=I("liveid");

		//将直播记录里的连麦信息删掉
		M("users_live")->where("uid={$liveid} and islive=1")->setField("lianmai_stream","");

		//将连麦列表的数据修改
		M("users_connect_video")->where("liveid={$liveid} and (status=0 or status=1)")->setField("status",3);

		$rs=array('code'=>0,'msg'=>'','info'=>'');
		
		echo json_encode($rs);


	}


	/*通过用户昵称获取用户ID*/
	public function searchInfoByNicename(){

		$rs=array("code"=>0,"msg"=>'','info'=>'');

		$user_nicename=I("user_nicename");
		$where=array();
		$where['user_nicename']=array("like","%".$user_nicename."%");
		$where['isvirtual']=0;

		$lists=M("users")->where($where)->select();


		if($lists){
			$rs['code']=0;
			$rs['info']=$lists;
		}else{
			$rs['code']=1001;
			$rs['msg']="未查询到相关数据";
		}


		echo json_encode($rs);

	}



}


