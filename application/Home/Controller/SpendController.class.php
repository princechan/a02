<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Common\Controller\HomebaseController;
use Common\Lib\Gift\Gift;
use Common\Lib\Helpers\CRedis;
use Common\Lib\Helpers\Curl;
use Common\Lib\Helpers\Func;
use Common\Lib\Users\User;
use Common\Lib\Users\UsersLive;
use Common\Lib\Users\UsersCoinrecord;
use Common\Lib\Users\UsersSendGiftWin;
use Common\Lib\Users\UsersLiveManager;
use Common\Lib\Gift\GiftPkList;


/**
 * 消费相关
 */
class SpendController extends HomebaseController {
	/* 送礼物 */
	public function sendGift()
    {

        $user_lib       = User::getInstance();
		$uid            = $user_lib->getUserId();
		$token          = $user_lib->getToken();
		$touid          = I('touid');
		$giftid         = I('giftid');
		$giftGroupNum   = I("giftGroupNum");
		$sendType       = I("sendType");
		$giftcount      = 1;

		if($uid == $touid){
			echo '{"errno":"1000","data":"","msg":"不允许给自己送礼物"}';
			exit;
		}

		$config             = $this->config;
		$configpri          = $this->configpri;
		$live_coin_percent  = $configpri['live_coin_percent'];//直播券和钻石兑换比例

		$userinfo = $user_lib->getUserInfo($uid);
		if($userinfo['token']!=$token || $userinfo['expiretime']<time()){
			echo '{"errno":"1002","data":"","msg":"Token以过期，请重新登录"}';
			exit;
		}

		/* 获取余额 */
        $userinfo['coin'] = $this->getZhiBoBalance($userinfo['user_login'], $userinfo['user_pass']);

		//获取主播信息
        $user_live_lib  = UsersLive::getInstance();
        $liveInfo       = $user_live_lib->getInfo($touid);
		$stream         = $liveInfo['stream'];
		$liveuid        = $touid;

		/* 礼物信息 */
		$gift_lib = Gift::getInstance();
        $giftinfo = $gift_lib->getInfo($giftid);
		$giftinfo['gifticon'] = get_upload_path($giftinfo['gifticon']);

		$total          = abs($giftinfo['needcoin']*$giftcount*intval($giftGroupNum));
		$total_livecoin = $total*$live_coin_percent;

		$users_live = $user_live_lib->getInfo($touid);
        $showid     = $users_live ? $users_live['starttime'] : 0;

		if($sendType==0){//未提示用户直播券不足

			//判断用户的直播券是否足够支付该礼物价格
            //用户有直播券且足够支付该礼物的价格【直接扣除用户的直播券】
			if($userinfo['livecoin']>0&&($userinfo['livecoin']-$total_livecoin>=0)){

				//$total=0;//将总coin值改为0【发送礼物token时，直播间主播映票直接加0】
                $user_lib->setDec($uid, 'livecoin', $total_livecoin);   // 用户直播券扣除
                $user_lib->setInc($touid, 'livecoin', $total_livecoin); // 主播直播券增加【主播映票不增加】
                $user_lib->setInc($uid, 'consumption', $total);         //用户增加经验值

				//向消费记录表里添加一条记录
                $arr_coin_record = [
                    'type'              => 'expend',
                    'action'            => 'sendgift',
                    'uid'               => $uid,
                    'touid'             => $touid,
                    'giftid'            => $giftid,
                    'giftcount'         => $giftcount,
                    'totalcoin'         => $total,
                    'giftgroup_num'     => $giftGroupNum,
                    'spend_type'        => 2,
                    'total_livecoin'    => $total_livecoin,
                    'showid'            => $showid,
                    'addtime'           => time(),
                ];
                UsersCoinrecord::getInstance()->add($arr_coin_record);

			}else if($userinfo['livecoin']>0&&($userinfo['livecoin']-$total*$live_coin_percent<0)){
			    //用户有直播券，但不够支付该礼物的价格【提示用户是否使用钻石支付】

				//判断用户的coin是否足够支付该礼物
				if($userinfo['coin'] < $total){
					/* 余额不足 */
					echo '{"errno":"1001","data":"","msg":"余额不足"}';
					exit;
				}else{
				    //足够，提示用户将扣除coin
					echo '{"errno":"1003","data":"","coin_name":"'.$config['name_coin'].'","msg":"您的直播券不足，是否使用'.$config['name_coin'].'支付？"}';
					exit;
				}

			}else{
                //用户没有直播券，直接判断用户的coin,扣除coin即可，不用提示用户是否确认钻石支付
				//判断用户的coin是否足够支付该礼物
				if($userinfo['coin'] < $total){
					/* 余额不足 */
					echo '{"errno":"1001","data":"","msg":"余额不足"}';
					exit;
				}else{
					echo '{"errno":"1003","data":"","coin_name":"'.$config['name_coin'].'","msg":"您的直播券不足，是否使用'.$config['name_coin'].'支付？"}';
					exit;
				}
			}
		}else if($sendType==1){
		    //确认 [直播券不够，直接用钻石支付]

			/* 扣费 */
            $response2 = $this->transferZhiBoBalance($userinfo['user_login'], $userinfo['user_pass'], $total);

			if($response2['status']==-3){
				/* 余额不足 */
				echo '{"errno":"1001","data":"","msg":"余额不足"}';
				exit;
			}else if($response2['status']==1){
				echo '{"errno":"1001","data":"","msg":"操作失败，请重试"}';
				exit;
			}else if($response2['status']==999){
				echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
				exit;
			}

			//主播增加映票, 累计映票, 用户增加经验值
            $user_lib->setInc($touid, 'votes', $total);
            $user_lib->setInc($touid, 'votestotal', $total);
            $user_lib->setInc($uid, 'consumption', $total);

			//向消费记录表里添加一条记录
            $arr_coin_record = [
                'type'              => 'expend',
                'action'            => 'sendgift',
                'uid'               => $uid,
                'touid'             => $touid,
                'giftid'            => $giftid,
                'giftcount'         => $giftcount,
                'totalcoin'         => $total,
                'giftgroup_num'     => $giftGroupNum,
                'spend_type'        => 1,
                'total_livecoin'    => 0,
                'showid'            => $showid,
                'addtime'           => time(),
            ];
            UsersCoinrecord::getInstance()->add($arr_coin_record);
		}
		/*******判断中奖礼物start********/

        //默认不中奖 | 中奖钻石数
		$iswin = $winCoin = 0;

		//判断该礼物是不是中奖礼物

		if(intval($giftinfo['is_win_gift'])==1){

			//获取后台配置信息
            $configPub = Func::getPublicConfig();

			//获取后台配置的中奖几率
			$gift_win_percent   = $configPub['gift_win_percent'];

			//获取中奖倍率
			$gift_win_multiple  = $configPub['gift_win_multiple'];

            $gift_win_percent = ($gift_win_percent < 0) ? 0 : (($gift_win_percent >= 100) ? 100 : $gift_win_percent);
			if($gift_win_percent > 0 && $gift_win_percent<100){

				$randNum = rand(1,99);
				if($randNum<=$gift_win_percent){//中奖了

					//计算中奖的金额
					$winCoin = $gift_win_multiple*$total;
					//计算中奖的直播券
					$winLiveCoin = $live_coin_percent*$winCoin;

					//向中奖纪录表里添加数据
					$data = [
						'uid'               => $uid,
						'liveuid'           => $liveuid,
						'stream'            => $stream,
						'giftid'            => $giftid,
						'gift_coin'         => $giftinfo['needcoin'],
						'gift_win_multiple' => floatval($gift_win_multiple),
						'win_coin'          => $winCoin,
						'addtime'           => time(),
						'win_livecoin'      => $winLiveCoin
					];
                    UsersSendGiftWin::getInstance()->add($data);
					$iswin = 1;
				}

			}else if($gift_win_percent==100){//直接中奖

				//计算中奖的金额 | 计算中奖的直播券
				$winCoin        = $gift_win_multiple*$total;
                $winLiveCoin    = $live_coin_percent*$winCoin;

				//向中奖纪录表里添加数据
                $data = [
                    'uid'               => $uid,
                    'liveuid'           => $liveuid,
                    'stream'            => $stream,
                    'giftid'            => $giftid,
                    'gift_coin'         => $giftinfo['needcoin'],
                    'gift_win_multiple' => floatval($gift_win_multiple),
                    'win_coin'          => $winCoin,
                    'addtime'           => time(),
                    'win_livecoin'      => $winLiveCoin
                ];
                UsersSendGiftWin::getInstance()->add($data);
                $iswin=1;
			}
		}

		/*******判断中奖礼物end********/


		//判断用户如果中奖，给用户加直播券
		if($iswin==1){
		    $user_lib->setInc($uid, 'livecoin', $winLiveCoin); // 用户直播券增加
		}

		$redis = CRedis::getInstance();

		/*******主播发起送礼物PK时，用户送礼物给相应主播加数据start********/

		$giftPKinfo = $redis->get('giftPK_'.$liveuid);

		//判断redis数据是否存在
		if($giftPKinfo){
			$pkArr = json_decode($giftPKinfo,true);//将json转为数组

			//获取pk的状态数据
			$pkIsEnd = $pkArr['isEnd'];

			if($pkIsEnd == 1){//代表PK已经结束
				$redis->delete("giftPK_".$liveuid);

			}else{
			    //pk未结束

				$pkID           = $pkArr['pkID'];            //获取礼物PK数据的id
				$masterGiftID   = $pkArr['masterGiftID'];    //获取pk redis里的主队礼物id
				$guestGiftID    = $pkArr['guestGiftID'];     //获取pk redis里的客队礼物id
				$guestID        = $pkArr['guestID'];         //客队人id

				if($masterGiftID == $giftid){//赠送礼物同主队礼物相同
					$pkArr['masterGiftNum']         += $giftcount*$giftGroupNum;
					$pkArr['masterGiftTotalCoin']   += $total;

					//写入数据库pk赠送记录表中
					$dataMsg = [
                        'uid'           => $uid,
                        'liveuid'       => $liveuid,
                        'giftid'        => $giftid,
                        'num'           => $giftcount,
                        'group_num'     => $giftGroupNum,
                        'coin'          => $total,
                        'guestid'       => $guestID,
                        'send_type'     => 0,
                        'pk_id'         => $pkID,
                        'addtime'       => time()
                    ];
                    GiftPkList::getInstance()->add($dataMsg);
				}

				if($guestGiftID == $giftid){
                    //赠送的礼物同客队礼物相同

					$pkArr['guestGiftNum']          += $giftcount*$giftGroupNum;
					$pkArr['guestGiftTotalCoin']    += $total;

					//写入数据库pk赠送记录表中
					$dataMsg = [
						'uid'               => $uid,
						'liveuid'           => $liveuid,
						'giftid'            => $giftid,
						'num'               => $giftcount,
						'group_num'         => $giftGroupNum,
						'coin'              => $total,
						'guestid'           => $guestID,
						'send_type'         => 1,
						'pk_id'             => $pkID,
						'addtime'           => time()
					];
                    GiftPkList::getInstance()->add($dataMsg);
				}

				//将数组重新打包成json串
				$giftPKinfo = json_encode($pkArr);
				$redis->set("giftPK_".$liveuid,$giftPKinfo);
			}
		}

		/*******主播发起送礼物PK时，用户送礼物给相应主播加数据end********/


		/* 更新用户余额 消费 */
		//M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total} where id='{$uid}'");
		/* 更新直播 映票 累计映票 */
		//M()->execute("update __PREFIX__users set votes=votes+{$total},votestotal=votestotal+{$total} where id='{$touid}'");

		$userinfo2  = $user_lib->getUserInfo($uid);
		$level      = $user_lib->getLevel($userinfo2['consumption']);

		/* 获取余额 */
		$userinfo2['coin'] = $this->getZhiBoBalance($userinfo['user_login'], $userinfo['user_pass']);

		/* 清除缓存 */
		$redis->del("userinfo_".$uid);
		$redis->del("userinfo_".$touid);

		$gifttoken=md5(md5('sendGift' . $uid . $touid . $giftid . $giftcount . $total . $showid . time()));

		$result = [
            "uid"           => (int)$uid,
            "giftid"        => (int)$giftid,
            "giftcount"     => (int)$giftcount,
            "totalcoin"     => $total,
            'giftGroupNum'  => $giftGroupNum,
            "giftname"      => $giftinfo['giftname'],
            "gifticon"      => $giftinfo['gifticon'],
            "level"         => $level,
            "coin"          => $userinfo2['coin'],
            "votestotal"    => $userinfo2['votestotal'],
            "iswin"         => $iswin,
            "winCoin"       => $winCoin,
            "winLiveCoin"   => $winLiveCoin,
            "livecoin"      => $userinfo2['livecoin']
        ];
		$redis->set($gifttoken,json_encode($result));

        $evensend = $giftinfo['type']==1 ? 'y' : 'n';

		echo '{"errno":"0","uid":"' . $uid .'","level":"' . $level . '","charge_msg":"' . "送礼扣费￥{$total}" .
            '","giftname":"' . $giftinfo['giftname'] . '","gifticon":"' . $giftinfo['gifticon'] .
            '","giftGroupNum":"' . $giftGroupNum . '","iswin":"'. $iswin . '","winLiveCoin":"' .
            $winLiveCoin . '","evensend":"' . $evensend . '","coin":"' . $userinfo2['coin'] . '","gifttoken":"' .
            $gifttoken .'","msg":"赠送成功"}';
		exit;
	}

	/* 弹幕 */
	public function sendHorn(){
		$rs = array('code' => 0, 'msg' => '', 'info' =>array());
		$users=M("users");
		$uid=User::getInstance()->getUserId();
		$liveuid=I("liveuid");
		$content=I("content");
		$stream=I("stream");
		$userinfo=$users->field('user_login,user_pass,coin,token,expiretime,user_nicename,avatar,livecoin')->where("id =".$uid)->find();


		if($userinfo['token']!=User::getInstance()->getToken() || $userinfo['expiretime']<time()){
			echo '{"errno":"1002","data":"","msg":"Token以过期，请重新登录"}';
			exit;
		}


		//通过三方接口获取用户的余额
		$userinfo['coin'] = $this->getZhiBoBalance($userinfo['user_login'], $userinfo['user_pass']);

		$configpri=getConfigPri();
		$giftid=1;
		$giftcount=1;
		$giftinfo=array(
			"giftname"=>'弹幕',
			"gifticon"=>'',
			"needcoin"=>$configpri['barrage_fee'],
		);

		$total= $giftinfo['needcoin']*$giftcount;

		$addtime=time();
		$type='expend';
		$action='sendbarrage';
		if($userinfo['coin'] < $total){
			$rs['code']=1001;
			$rs['msg']='余额不足';
			echo  json_encode($rs);
			exit;
			/* 余额不足 */
		}



		/*// 更新用户余额 消费
		M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total} where id='{$uid}'");
		// 更新直播主播 映票 累计映票
		M()->execute("update __PREFIX__users set votes=votes+{$total},votestotal=votestotal+{$total} where id='{$liveuid}'");*/


		/* 扣费 */
        $response2 = $this->transferZhiBoBalance($userinfo['user_login'], $userinfo['user_pass'], $total);
		if($response2['status']==-3){
			/* 余额不足 */
			echo '{"errno":"1001","data":"","msg":"余额不足"}';
			exit;
		}else if($response2['status']==1){
			echo '{"errno":"1001","data":"","msg":"操作失败，请重试"}';
			exit;
		}else if($response2['status']==999){
			echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
			exit;
		}



		$stream2=explode('_',$stream);
		$showid=$stream2[1];

		$insert=array("type"=>$type,"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime );
		$isup=M("users_coinrecord")->add($insert);

		$userinfo2 =$users->field('consumption,coin')->where("id=".$uid)->find();
		/*获取当前用户的等级*/
		$level=getLevel($userinfo2['consumption']);

		/* 清除缓存 */
		delCache("userinfo_".$uid);
		delCache("userinfo_".$liveuid);
		/*获取主播影票*/
		$votestotal=$users->field('votestotal,coin')->where("id=".$liveuid)->find();

		$barragetoken=md5(md5($action.$uid.$liveuid.$giftid.$giftcount.$total.$showid.$addtime.rand(100,999)));

		//通过三方接口获取用户的余额
		$userinfo['coin'] = $this->getZhiBoBalance($userinfo['user_login'], $userinfo['user_pass']);

		$result=array("uid"=>$uid,"content"=>$content,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"giftname"=>$giftinfo['giftname'],"gifticon"=>$giftinfo['gifticon'],"level"=>$level,"coin"=>$userinfo['coin'],"votestotal"=>$votestotal['votestotal'],"barragetoken"=>$barragetoken);
		$rs['info']=$result;
		/*写入 redis*/
		unset($result['barragetoken']);
		$redis =connectionRedis();
		$redis -> set($barragetoken,json_encode($result));
		$redis -> close();

		echo json_encode($rs);

	}
	/*设置 取消 管理员*/
	public function cancel()
	{
		$rs = array('code' => 0, 'msg' => '', 'info' =>'操作成功');
		$uid=User::getInstance()->getUserId();
		$touid=I("touid");
		$showid=I("roomid");
		$users_livemanager=M("users_livemanager");
		if($uid!=$showid)
		{
			$rs['code']=1001;
			$rs['msg']='不是该房间主播';
			echo  json_encode($rs);
			exit;
		}
		if($uid==$touid)
		{
			$rs['code']=1002;
			$rs['msg']='自己无法管理自己';
			echo  json_encode($rs);
			exit;
		}
		$admininfo=$users_livemanager->where("uid='{$touid}' and liveuid='{$showid}'")->find();
		$rs=M("users")->field("id,avatar,avatar_thumb,user_nicename")->where("id=".$touid)->find();
		if($admininfo)
		{
			$users_livemanager->where("uid='{$touid}' and liveuid='{$showid}'")->delete();
			$rs['isadmin']=0;
		}
		else
		{
			$count =$users_livemanager->where("liveuid='{$showid}'")->count();
			if($count>=5)
			{
				$rs['code']=1004;
				$rs['msg']='最多设置5个管理员';
				echo  json_encode($rs);
				exit;
			}
			$users_livemanager->add( array("uid"=>$touid,"liveuid"=>$showid));
			$rs['isadmin']=1;
		}
		$rs['msg']="设置成功";
		echo  json_encode($rs);
		exit;
	}
	/*禁言*/
	public function gag(){
		$rs = array('code' => 0, 'msg' => '', 'info' => '禁言成功');
		$uid=User::getInstance()->getUserId();
		$touid=I("touid");
		$showid=I("roomid");
		$gagTime=I("gagTime");
    	$uidtype = getIsAdmin($uid,$showid);

		/*if($uidtype==30 ){
			$rs["code"]=1001;
			$rs["info"]='你不是主播或者管理员';
			$rs["msg"]='你不是主播或者管理员';
			echo  json_encode($rs);
			exit;
		}*/

    	$touidtype = getIsAdmin($touid,$showid);

		if($touidtype==50){
			$rs["code"]=1002;
			$rs["info"]='对方是主播，不能禁言';
			$rs["msg"]='对方是主播，不能禁言';
			echo  json_encode($rs);
			exit;
		}/*else if($touidtype==40 ){
			$rs["code"]=1002;
			$rs["info"]='对方是管理员，不能禁言';
			$rs["msg"]='对方是管理员，不能禁言';
			echo  json_encode($rs);
			exit;
		}*/
		else if($touidtype==60 )
		{
			$rs["code"]=1002;
			$rs["info"]='对方是超管，不能禁言';
			$rs["msg"]='对方是超管，不能禁言';
			echo  json_encode($rs);
			exit;
		}

		$nowtime=time();
    $redis = connectionRedis();
    $result=$redis -> hGet($showid . 'shutup',$touid);
		if($result){
			if($nowtime<$result){
				$rs["code"]=1003;
				$rs["info"]='对方已被禁言';
				$rs["msg"]='对方已被禁言';
				echo  json_encode($rs);
				exit;
			}
		}
		if($gagTime){
			$time=$nowtime+$gagTime*60;
		}else{
			$time=$nowtime+10*60;
		}

    $redis -> hSet($showid . 'shutup',$touid,$time);
		$redis -> close();
    echo  json_encode($rs);
		exit;
	}



	public function isShutUp() {
   		$uid=User::getInstance()->getUserId();
		$showid=I("showid");
		$rs = array('code' => 0, 'msg' => '', 'info' => '0');
		$nowtime=time();
		if($uid){

			//获取用户是否在白名单里

			$admin = getIsAdmin($uid,$showid);
			$rs['admin']=$admin;
			$redis = connectionRedis();
			$result=$redis -> hGet($showid . 'shutup',$uid);

			if($result){

				if($nowtime>$result)
				{
					$result=$redis -> hDel($showid . 'shutup',$uid);
					$rs['info']=0;
				}else{
						$rs['info']=1;
				}

			}else{

				$rs['info']=0;
			}
			$redis -> close();
		}
		echo  json_encode($rs);
		exit;
  }



	/*踢人*/
	public function tiren(){
		$rs         = array('code' => 0, 'msg' => '', 'info' =>'操作成功');
		$uid        = User::getInstance()->getUserId();
		$touid      = $this->getGet("touid");
		$showid     = $this->getGet("roomid");
		$stream     = $this->getGet("stream");
	  	$user_type  = UsersLiveManager::getInstance()->getIsAdmin($uid,$showid);

	  	if($stream == ""){
	  		$rs['code'] = 1000;
			$rs['msg']  = '踢出房间失败啦';
			echo  json_encode($rs);
			exit;
	  	}
		/*if($user_type==30)
		{
			$rs['code']=1000;
			$rs['msg']='您不是管理员，无权操作';
			echo  json_encode($rs);
			exit;
		}*/
		$touser_type = UsersLiveManager::getInstance()->getIsAdmin($touid,$showid);
		if($touser_type==50 )
		{
			$rs['code'] = 1001;
			$rs['msg']  = '对方是主播，不能被踢出';
			echo  json_encode($rs);
			exit;
		}
		/*else if($touser_type==40)
		{
			$rs['code']=1002;
			$rs['msg']='对方是管理员，不能被踢出';
			echo  json_encode($rs);
			exit;
		}*/
		else if($touser_type==60)
		{
			$rs["code"] = 1002;
			$rs["info"] = '对方是超管，不能被踢出';
			$rs["msg"]  = '对方是超管，不能被踢出';
			echo  json_encode($rs);
			exit;
		}
		$nowtime    = time();
		$time       = $nowtime+60*0.5;
		$redis      = CRedis::getInstance();
		$redis->hset($showid.'kick',$touid,$time);
		$lists      = $redis->hVals('userlist_'.$stream);

		foreach ($lists as $k => $v) {
		    $online_user = json_decode($v, true);
			if($online_user['id'] == $touid){
				$redis->hDel('userlist_'.$stream,$online_user['token']);
				break;
			}
		}
	    echo json_encode($rs);
		exit;
	}



	/*加入/取消 黑名单*/
	public function black()
	{
		$rs   = array('code' => 0, 'msg' => '', 'info' =>'操作成功');
		$uid  = User::getInstance()->getUserId();
		$touid=I("touid");
		if($uid==$touid)
		{
			$rs['code']=0;
			$rs['msg']='无法将自己拉黑';
			echo  json_encode($rs);
			exit;
		}
		$users_black=M(users_black);
		$isexist=$users_black->where("uid=".$uid." and touid=".$touid)->find();
		if($isexist)
		{
			$black=$users_black->where("uid=".$uid." and touid=".$touid)->delete();
			if($black)
			{
				$rs['code']=0;
				$rs['msg']='已将该用户移除黑名单';
				echo  json_encode($rs);
				exit;
			}
			else
			{
				$rs['code']=1000;
				$rs['msg']='移除黑名单失败';
				echo  json_encode($rs);
				exit;
			}
		}
		else
		{
			M('users_attention')->where("uid=".$uid." and touid=".$touid)->delete();
			$black=$users_black->add(array("uid"=>$uid,"touid"=>$touid));
			if($black)
			{
				$rs['code']=0;
				$rs['msg']='已将该用户添加到黑名单';
				echo  json_encode($rs);
				exit;
			}
			else
			{
				$rs['code']=1000;
				$rs['msg']='添加黑名单失败';
				echo  json_encode($rs);
				exit;
			}

		}
	}
	/*举报*/
	public function report()
	{
		$rs = array('code' => 0, 'msg' => '', 'info' =>'操作成功');
		$uid=User::getInstance()->getUserId();
		$tlleuid=I("tlleuid");
		$token=I("token");
		$liveuid=I("liveuid");
		$content=I("content");
		$users=M("users");
		if($uid!=$tlleuid)
		{
			$rs['code']=1000;
			$rs['msg']='未知信息错误';
			echo  json_encode($rs);
			exit;
		}
		$checkToken=checkToken($uid,$token);
		if($checkToken==700)
		{
			$rs['code']=$checkToken;
			$rs['msg']='登录信息过期，请重新登录';
			echo  json_encode($rs);
			exit;
		}
		if($content=="")
		{
			$rs['code']=1001;
			$rs['msg']='举报内容不能为空';
			echo  json_encode($rs);
			exit;
		}
		$data=array(
			"uid"=>$uid,
			"touid"=>$liveuid,
			'content'=>$content,
			'addtime'=>time()
		);
		$users_report=M("users_report")->add($data);
		if($users_report)
		{
			$rs['code']=0;
			$rs['msg']='举报成功';
			echo  json_encode($rs);
			exit;
		}
		else
		{
			$rs['code']=1003;
			$rs['msg']='举报失败';
			echo  json_encode($rs);
			exit;
		}

	}
	/* 星星 */
	public function sendStar(){
		$config=$this->config;
		$User=M("users");
		$uid=User::getInstance()->getUserId();
		$touid=I('touid');
		$giftid=I('giftid');
		$giftcount=I('giftcount');

	}
	/*收费房间扣费*/
	public function roomCharge()
	{
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$liveuid=I("liveuid");
		$stream=I("stream");
		$uid=User::getInstance()->getUserId();
		$token=User::getInstance()->getToken();
		$islive=M("users_live")->field("islive,type,type_val,starttime")->where("uid='{$liveuid}' and stream='{$stream}'")->find();
		if(!$islive || $islive['islive']==0){
			$rs['code'] = 1005;
			$rs['msg'] = '直播已结束';
			echo json_encode($rs);
			exit;
		}
		if($islive['type']==0 || $islive['type']==1 ){
			$rs['code'] = 1006;
			$rs['msg'] = '该房间非扣费房间';
			echo json_encode($rs);
			exit;
		}
		$userinfo=M("users")->where("id='{$uid}'")->find();
		if($userinfo['token']!=$token || $userinfo['expiretime']<time()){
			$rs['code'] = 700;
			$rs['msg'] = 'Token错误或已过期，请重新登录';
			echo json_encode($rs);
			exit;
		}
		$total=$islive['type_val'];
		if($total<=0){
			$rs['code'] = 1007;
			$rs['msg'] = '房间费用有误';
			echo json_encode($rs);
			exit;
		}


		//通过三方接口获取用户的余额
		$userinfo['coin'] = $this->getZhiBoBalance($userinfo['user_login'], $userinfo['user_pass']);
		if($userinfo['coin'] < $total){
			$rs['code'] = 1008;
			$rs['msg'] = '余额不足';
			echo json_encode($rs);
			exit;
		}
		$action='roomcharge';
		if($islive['type']==2){
			$action='roomcharge';
		}
		$giftid=0;
		$giftcount=0;
		$showid=$islive['starttime'];
		$addtime=time();
		/* 更新用户余额 消费 */
		/*M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total} where id='{$uid}'");*/


		/* 扣费 */
        $response2 = $this->transferZhiBoBalance($userinfo['user_login'], $userinfo['user_pass'], $total);
		if($response2['status']==-3){
			/* 余额不足 */
			echo '{"errno":"1001","data":"","msg":"余额不足"}';
			exit;
		}else if($response2['status']==1){
			echo '{"errno":"1001","data":"","msg":"操作失败，请重试"}';
			exit;
		}else if($response2['status']==999){
			echo '{"errno":"1001","data":"","msg":"操作异常，请重试"}';
			exit;
		}


		/* 更新直播 映票 累计映票 */
		M()->execute("update __PREFIX__users set votes=votes-{$total},votestotal=votestotal+{$total} where id='{$liveuid}'");
		/* 更新直播 映票 累计映票 */
		M("users_coinrecord")->add(array("type"=>'expend',"action"=>$action,"uid"=>$uid,"touid"=>$liveuid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime ));
		$userinfo2=M("users")->field('coin')->where("id='{$uid}'")->find();
		$rs['coin']=$userinfo2['coin'];
		echo json_encode($rs);
	}
	/* 守护 */
	public function buyKeeper(){
		$config=$this->config;
		$User=M("users");
		$uid=User::getInstance()->getUserId();
		$touid=I('touid');
		$giftid=I('giftid');
		$buytype=I('buytype');
		$giftcount=I('buytime');
		$showid=I('showid');



		$userinfo= $User->field('coin,token,expiretime,user_nicename,avatar')->where("id='{$uid}'")->find();

		if($userinfo['token']!=User::getInstance()->getToken() || $userinfo['expiretime']<time()){

			$data['errno']=1001;
			$data['data']='';
			$data['msg']='Token过期，请重新登录';
			echo json_encode($data);
			exit;
		}
		if($touid==$uid){
			$data['errno']=1003;
			$data['data']='';
			$data['msg']='自己不能守护自己';
			echo json_encode($data);
			exit;
		}
		if($buytype=='month'){
			$giftid=2;
			$type=0;
			$add=60*60*24*30*$giftcount;
		}else{
			$giftid=3;
			$type=1;
			$add=60*60*24*30*12*$giftcount;
		}
		$action='buytool_'.$giftid;
		$keeperinfo=M("tools")->where("id='{$giftid}'")->find();

		$total=$giftcount * $keeperinfo['needcoin'];
		$totalexperience=$total*$config['experience_rate'];

		$addtime=time();

		if($userinfo['coin'] < $total){
			/* 余额不足 */
			$data['errno']=1002;
			$data['data']='';
			$data['msg']='余额不足';
			echo json_encode($data);
			exit;
		}

		/* 更新用户余额 消费 */
		M()->execute("update __PREFIX__users set coin=coin-{$total},consumption=consumption+{$total},experience=experience+{$totalexperience} where id='{$uid}'");

		/* 更新直播 映票 累计映票 */
		M()->execute("update __PREFIX__users set votes=votes+{$total},votestotal=votestotal+{$total} where id='{$touid}'");

		/* 消费记录 */

		M("users_coinrecord")->add(array("type"=>'expend',"action"=>$action,"uid"=>$uid,"touid"=>$touid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime ));
		/* 守护记录 */
		$keeperinfo=M("keeper")->where("uid='{$uid}' and touid='{$touid}'")->find();
		if($keeperinfo){
			if($keeperinfo['endtime']>$addtime){
				/* 未过期 */
				M()->execute("update __PREFIX__keeper set endtime=endtime+{$add},type='{$type}' where id='{$keeperinfo['id']}'");
			}else{
				/* 已过期 */
				$endtime=$addtime+$add;
				M()->execute("update __PREFIX__keeper set endtime='{$endtime}',type='{$type}' where id='{$keeperinfo['id']}'");
			}
		}else{
			/* 未守护 */
			$endtime=$addtime+$add;
			M("keeper")->add(array("uid"=>$uid,"touid"=>$touid,"buytime"=>$addtime,"endtime"=>$endtime,"type"=>$type ));
		}

			$userinfo2=$User->field("experience,coin")->where("id='{$uid}'")->find();

			$level=getLevel($userinfo2['experience']);

		 $gifttoken=md5(md5($action.$uid.$touid.$giftid.$giftcount.$total.$showid.$addtime));

		 $result=array("type"=>'expend',"action"=>$action,"uid"=>$uid,"touid"=>$touid,"giftid"=>$giftid,"giftcount"=>$giftcount,"totalcoin"=>$total,"showid"=>$showid,"addtime"=>$addtime,"gifttoken"=>$gifttoken,"giftname"=>$keeperinfo['title'],"nicename"=>$userinfo['user_nicename'],"avatar"=>$userinfo['avatar'],"level"=>$level);


		$redis = connectionRedis();

		$redis  -> set($result['gifttoken'],json_encode($result));

		$redis -> close();


		$data['errno']=0;
		$data['data']=array(
			'level'=>$level,
			'coin'=>$userinfo2['coin'],
			'gifttoken'=>$gifttoken,
		);
		$data['msg']='购买成功';
		echo json_encode($data);
		exit;
	}

	/*判断用户是否是守护*/

	public function isGuard() {
   		$uid=User::getInstance()->getUserId();
		$showid=I("showid");
		$rs = array('code' => 0, 'msg' => '', 'isGurd' => '0');

		//获取后台配置的守护发言文字
		$guard_digitsNum=M("config")->where("id=1")->getField("guard_digitsNum");

		$rs['guard_digitsNum']=$guard_digitsNum;

		$nowtime=time();
		if($uid){

			$info=M("users_guard_lists")->where("uid={$uid} and liveuid={$showid} and effectivetime>{$nowtime}")->find();
			if($info){
				$rs['isGuard']=1;
			}else{
				$rs['isGuard']=0;
			}
		}

		echo  json_encode($rs);
		exit;
    }


    public function channelShutUp(){

    	$userID=I("userID");
    	$roomid=I("roomid");

    	/*if($userID==""||$roomid==""){
    		$rs['code']=1001;
    		$rs['msg']="参数错误";
    		echo json_encode($rs);
    		exit;
    	}*/

    	$rs=array('code'=>1001,'msg'=>'您没有房间禁言权限','info'=>'');

    	$redis = connectionRedis();

    	$arr=$redis->hKeys('channelShutUp');

    	$ischannelShutUp=0;

    	if($arr){
    		foreach ($arr as $v) {
	    		if($v==$roomid){
	    			$ischannelShutUp=1;
	    			break;
	    		}

    		}

    	}

    	if($ischannelShutUp==1){//房间被禁言
    		$rs['msg']="该房间已经被禁言";
    		echo json_encode($rs);
			exit;

    	}else{

    		//判断当前用户是否具备频道禁言权限
    		//判断用户是否是超管
    		$User=M("users");
    		$userInfo=$User->where("id='{$userID}'")->find();

    		if($userInfo['issuper']==1){//超管
    			$redis  -> hSet('channelShutUp',$roomid,$userID);
    			$rs['code']=0;
				$rs['msg']='频道禁言成功';
				$redis -> close();
				echo json_encode($rs);
				exit;
    		}else if($userID==$roomid){ //主播
				$redis  -> hSet('channelShutUp',$roomid,$userID);
				$rs['code']=0;
				$rs['msg']='频道禁言成功';
				$redis -> close();
				echo json_encode($rs);
				exit;

    		}else{//普通用户
    			$Vests=M("users_vest");
    			$VestLists=M("users_vest_lists");
    			//判断用户的马甲
    			if($userInfo['vest_id']==6||$userInfo['vest_id']==7){//用户在后台被设置了紫马或黑马
    				//判断紫马或黑马权限
    				$vestInfo=$Vests->where("id='{$userInfo['vest_id']}'")->find();
    				if($vestInfo['gap_all']==1){
    					$redis  -> hSet('channelShutUp',$roomid,$userID);
    					$rs['code']=0;
						$rs['msg']='频道禁言成功';
						$rs['vestName']=$vestInfo['vest_name'];
						$redis -> close();
						echo json_encode($rs);
						exit;
    				}
    			}else{

    				//判断用户的房间马甲权限
    				$vestListInfo=$VestLists->where("uid='{$userID}' and liveid='{$roomid}'")->find();

    				if($vestListInfo){
    					//获取马甲权限
    					$vestInfo=$Vests->where("id='{$vestListInfo['vestid']}'")->find();
    					if($vestInfo['gap_all']==1){
	    					$redis  -> hSet('channelShutUp',$roomid,$userID);
	    					$rs['code']=0;
							$rs['msg']='频道禁言成功';
							$rs['vestName']=$vestInfo['vest_name'];
							$redis -> close();
							echo json_encode($rs);
						exit;
    					}
    				}
    			}
    		}


    	}

		echo json_encode($rs);
		exit;



    }




    public function delChannelShutUp(){
    	$userID=I("userID");
    	$roomid=I("roomid");

    	if($userID==""||$roomid==""){
    		$rs['code']=1001;
    		$rs['msg']="参数错误";
    		echo json_encode($rs);
    		exit;
    	}

    	$rs=array('code'=>1001,'msg'=>'','info'=>'','type'=>0);

    	$redis = connectionRedis();

    	$arr=$redis->hKeys('channelShutUp');

    	$ischannelShutUp=0;

    	if($arr){
    		foreach ($arr as $v) {
	    		if($v==$roomid){
	    			$ischannelShutUp=1;
	    			break;
	    		}

    		}

    	}



    	if($ischannelShutUp==1){//房间被禁言
    		$User=M("users");
    		$channelShutUpUID=$redis->hGet('channelShutUp',$roomid);//获取禁言的用户ID



			//判断禁言房间用户是否是超管
			$issuper=$User->where("id='{$channelShutUpUID}'")->getField("issuper");


			//判断当前用户是否是超管
			$user_issuper=$User->where("id='{$userID}'")->getField("issuper");


			if($issuper==1){//超管禁言


				if($user_issuper==1){//是超管
					$redis  -> hDel('channelShutUp',$roomid);
					$redis -> close();
					$rs['code']=0;
					$rs['msg']='房间解禁成功';
					echo json_encode($rs);
					exit;
				}else{
					$rs['code']=1001;
					$rs['msg']='您的等级不够,无法解除禁言';
					echo json_encode($rs);
					exit;
				}

			}else{


				if($channelShutUpUID==$roomid){//是主播禁言

					//判断当前用户是否是超管
					if($user_issuper==1){
						$redis  -> hDel('channelShutUp',$roomid);
						$redis -> close();
						$rs['code']=0;
						$rs['msg']='房间解禁成功';
						echo json_encode($rs);
						exit;

					}else if($userID==$roomid){//判断当前用户是否是主播
						$redis  -> hDel('channelShutUp',$roomid);
						$redis -> close();
						$rs['code']=0;
						$rs['msg']='房间解禁成功';
						echo json_encode($rs);
						exit;

					}else{
						$rs['code']=1001;
						$rs['msg']='您的等级不够,无法解除禁言';
						echo json_encode($rs);
						exit;
					}


				}else{ //马甲用户禁言

					//判断当前用户是否是超管
					if($user_issuper==1){
						$redis  -> hDel('channelShutUp',$roomid);
						$redis -> close();
						$rs['code']=0;
						$rs['msg']='房间解禁成功';
						echo json_encode($rs);
						exit;

					}else if($userID==$roomid){//判断当前用户是否是主播

						$redis  -> hDel('channelShutUp',$roomid);
						$redis -> close();
						$rs['code']=0;
						$rs['msg']='房间解禁成功';
						echo json_encode($rs);
						exit;

					}else{

						//获取当前用户的马甲
						$Vest=M("users_vest");
						$vestLists=M("users_vest_lists");


						//判断当前用户的马甲等级
						$currentUserInfo=$User->where("id={$userID}")->find();



						if($currentUserInfo['vest_id']>1){

							$currentUserVestID=$currentUserInfo['vest_id'];

						}else{

							$vestListUserInfo=$vestLists->where("uid={$userID} and liveid={$roomid}")->find();

							if($vestListUserInfo){
								$currentUserVestID=$vestListUserInfo['vestid'];
							}else{
								$currentUserVestID=1;//默认白马甲
							}
						}

						//判断禁言用户的马甲ID
						$gagUserInfo=$User->where("id='{$channelShutUpUID}'")->find();



						if($gagUserInfo['vest_id']>1){

							$gagUserVestID=$gagUserInfo['vest_id'];

						}else{


							//从房间马甲列表里读取
							$gagListUserInfo=$vestLists->where("uid={$channelShutUpUID} and liveid={$roomid}")->find();

							if($gagListUserInfo){
								$gagUserVestID=$gagListUserInfo['vestid'];
							}else{
								$gagUserVestID=1;//默认白马甲
							}



						}


						//判断当前用户的马甲权限级别是否比禁言用户的马甲权限等级高
						if($currentUserVestID>$gagUserVestID){
							//判断当前用户马甲等级是否有频道禁言权限
							$vestInfo=$Vest->where("id='{$currentUserVestID}'")->find();
							if($vestInfo['gap_all']==1){//当前用户马甲有频道禁言权限
								$redis  -> hDel('channelShutUp',$roomid);
								$redis -> close();
								$rs['code']=0;
								$rs['msg']='房间解禁成功';
								$rs['vestName']=$vestInfo['vest_name'];
								echo json_encode($rs);
								exit;
							}

						}else if($currentUserVestID==$gagUserVestID){//马甲等级相同

							if($userID==$channelShutUpUID){//当前用户是频道禁言用户

								//判断当前用户马甲等级是否有频道禁言权限
								$vestInfo=$Vest->where("id='{$currentUserVestID}'")->find();

								if($vestInfo['gap_all']==1){//当前用户马甲有频道禁言权限
									$redis  -> hDel('channelShutUp',$roomid);
									$redis -> close();
									$rs['code']=0;
									$rs['msg']='房间解禁成功';
									$rs['vestName']=$vestInfo['vest_name'];
									echo json_encode($rs);
									exit;
								}
							}
						}else{
							$rs['code']=1001;
							$rs['msg']='您的等级不够,无法解除禁言';
							echo json_encode($rs);
							exit;
						}

					}


				}
			}


    	}else{

    		$rs['code']=1002;
			$rs['msg']='该房间未被禁言';

			echo json_encode($rs);


    	}


    	exit;

    }

    /**
     * 第三方接口
     */
    private static $debugUsername = 'ckf09080002';
    private static $debugPassword = 'd8be69dda476a353bc3e554aec6dc0a3';

    private static function getApiHost()
    {
        return ENV_DEV ? 'http://www.a02phpapi.com' : 'http://api.k8531.com';
    }

    private function transferZhiBoBalance($username, $password, $amount)
    {
        if(!empty($_REQUEST['debug'])) {
            $username = self::$debugUsername;
            $password = self::$debugPassword;
        }

        $data = [
            'username' => $username,
            'password' => $password,
            'timestamp' => time(),
            'amount' => $amount,
            'token' => '',
        ];
        $url = self::getApiHost() . '/api-transfer-zhibo-balance.htm';
        $response = Curl::getInstance()->post($url, $data);
        return $response;
    }

    private function getZhiBoBalance($username, $password, $debug = false)
    {
        if(!empty($_REQUEST['debug'])) {
            $username = self::$debugUsername;
            $password = self::$debugPassword;
        }

        $data = [
            'username' => $username,
            'password' => $password,
            'timestamp' => time(),
            'token' => '',
        ];
        $url = self::getApiHost() . '/api-get-zhibo-balance.htm';
        $response = Curl::getInstance()->post($url, $data);
        return  $debug ? $response : @(float)$response['data']['balance'];
    }

    public function testBalance() {
        $username=I('username');
        $user = M('users')->where(['user_login'=>$username])->find();
        $password = $user['user_pass'];
        echo '<pre>';var_dump($user,$this->getZhiBoBalance($username, $password, true));
    }

}
