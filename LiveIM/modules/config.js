/**
 * Current Version For LiveIM A02
 * Author:Clement
 */
module.exports= {
  "name":"a02",
  "redis":{
       "host":"localhost",
        "password":"tieweishivps",
        "port":6380
  },
   "https":{
       "enabled":true,
       "cert": '/data/IM_A02/keys/www.a02zhibo.com.crt',//'D:/Program Files/PHPHome/nginx/conf/vhost/keys/www.live.com.crt',
       "key": '/data/IM_A02/keys/www.a02zhibo.com.key'//'D:/Program Files/PHPHome/nginx/conf/vhost/keys/www.live.com.key'
   },
  "port":3000,
  "iphash":false,                                                              /*IPhash*/
  "token":'1234567',
  "api":"https://www.a02zhibo.com/api/public/index.php",                     /*API URL*/
  "io_options":{                                                               /*IO OPTIONS*/
    "pingTimeout": 20000,
    "pingInterval": 15000,
    "transports": [
      "websocket"
        //"polling",
    ]
  },
    "keys":{                                                                       /*Redus 在线用户前缀*/
        "online":"ONLINE:"
    },
  "configure":{
    "appenders": {
      "console": {
        "type": "console"
      },
      "trace": {
        "type": "file",
        "filename": "logs/server-access.log",
        "maxLogSize" : 31457280
      },
      "http": {
        "type": "logLevelFilter",
        "appender": "trace",
        "level": "trace",
        "maxLevel": "trace"
      },
      "info": {
        "type": "dateFile",
        "filename": "logs/server-info.log",
        "pattern": ".yyyy-MM-dd",
        "layout": {
          "type": "pattern",
          "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
        },
        "compress": true
      },
      "maxInfo": {
        "type": "logLevelFilter",
        "appender": "info",
        "level": "debug",
        "maxLevel": "info"
      },
      "error": {
        "type": "dateFile",
        "filename": "logs/server-error.log",
        "pattern": ".yyyy-MM-dd",
        "layout": {
          "type": "pattern",
          "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
        },
        "compress": true
      },
      "minError": {
        "type": "logLevelFilter",
        "appender": "error",
        "level": "error"
      }
    },
    "categories": {
      "default": {
        "appenders": [
          "console",
          "http",
          "maxInfo",
          "minError"
        ],
        "level": "all"
      }
    }
  }
};