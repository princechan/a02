CREATE TABLE `cmf_activity_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '活动名称',
  `start_at` int(11) NOT NULL DEFAULT '0' COMMENT '活动开始时间',
  `question_list_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联题单ID',
  `base_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '奖金数额',
  `extra_bonus_type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0-无加奖,1-有加奖',
  `extra_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '加奖总额',
  `extra_bonus_by_ranking` varchar(1000) NOT NULL DEFAULT '' COMMENT '排名奖金(多个奖金数值集合(多个奖金数值间以英文逗号间隔存储))',
  `desct` text NOT NULL COMMENT '活动描述',
  `join_requirements` varchar(400) NOT NULL DEFAULT '' COMMENT '报名要求json',
  `remind` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否启用预约提醒？是：1；否：0  默认0',
  `live_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0-单直播，1-多直播',
  `live_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播间ID',
  `location_live_id` varchar(1000) DEFAULT '' COMMENT '发布位置(输入房间ID，多个ID用英文半角逗号隔开)',
  `countdown_at` int(11) NOT NULL DEFAULT '0' COMMENT '活动准备倒计时',
  `answer_countdown_at` int(11) NOT NULL COMMENT '每道答题倒计时',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '活动状态：0-活动未发布,1-活动发布,2-活动准备,3-活动进行中,4-活动结束,5-活动删除',
  `icon` varchar(250) DEFAULT '' COMMENT '活动图片',
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建者ID',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE COMMENT '活动名称',
  KEY `start_at` (`start_at`) USING BTREE,
  KEY `question_list_id` (`question_list_id`) USING BTREE COMMENT '关联题库ID',
  KEY `live_room_id` (`live_id`) USING BTREE COMMENT '直播间ID',
  KEY `status` (`status`) USING BTREE COMMENT '活动进行状态？活动开始：1；答题准备：2；开始答题：3；关闭活动：4.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动百万答题活动表';

CREATE TABLE `cmf_activity_finished_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键自增长',
  `activity_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '关联活动ID',
  `join_total_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '总参加人数',
  `issue_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '分配金额',
  `winner_num` int(11) NOT NULL COMMENT '中奖人数',
  `everyone_get_bonuses` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '每个人获得奖金',
  `restraint_extend` text NOT NULL COMMENT '匹配条件(限制)(json格式化存储)',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '创时间',
  `updated_at` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `activity_id` (`activity_id`) USING BTREE COMMENT '关联活动ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动结束日志表';

CREATE TABLE `cmf_activity_user_detail_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '活动ID',
  `live_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '直播房间ID',
  `question_name` varchar(500) NOT NULL DEFAULT '' COMMENT '题目名称',
  `right_answer` varchar(30) NOT NULL DEFAULT '' COMMENT '正确答案',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0-打错，1-答对',
  `u_answer` varchar(30) NOT NULL DEFAULT '' COMMENT '玩家答案',
  `u_answer_at` varchar(30) NOT NULL DEFAULT '0' COMMENT '玩家答题时间',
  `u_answer_used_at` varchar(30) NOT NULL DEFAULT '0' COMMENT '玩家答题耗时',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT '0' COMMENT '更改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='玩家活动详情记录日志表';

CREATE TABLE `cmf_activity_user_total_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `activity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '活动ID',
  `live_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '直播房间号',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `base_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '平分奖金',
  `extra_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '加奖总额',
  `ranking_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '排名奖金',
  `total_bonus_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '总奖金数额',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='玩家活动奖金记录日志表';

CREATE TABLE `cmf_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '题库ID',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '题库类型    1:3-1;2:4-1',
  `name` varchar(500) NOT NULL DEFAULT '' COMMENT '题目',
  `choose_answer` text COMMENT '多个答案集合json',
  `right_answer` varchar(50) NOT NULL DEFAULT '' COMMENT '正确答案',
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建者ID',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE COMMENT '题目',
  KEY `type` (`type`) USING BTREE COMMENT '题目类型    1:3-1;2:4-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='问题题目';

CREATE TABLE `cmf_question_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '题单编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '题单名称',
  `question_ids` varchar(200) NOT NULL DEFAULT '' COMMENT '题目id',
  `admin_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建者ID',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated_at` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE COMMENT '题库名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='题库表';



INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/default',NULL,'活动管理','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/queswareroommanager',NULL,'活动管理','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/index',NULL,'答题活动管理','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/queslistmanager',NULL,'题单管理','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/quesmanager',NULL,'题目管理','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/quesinfo',NULL,'编辑题目','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/quesdel',NULL,'题目删除','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/queslistmanagerajax',NULL,'题单管理Ajax','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/addexistqueslistajax',NULL,'已存在题单添加','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/queslistadd',NULL,'题单添加','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/getqueslistinfo',NULL,'题单编辑修改','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/question/queslistdel',NULL,'题单删除','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/activity/index',NULL,'百万答题','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/activity/edit',NULL,'百万答题添加编辑','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/activity/delete',NULL,'百万答题删除','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/activity/activityanswerlogs',NULL,'答题记录','1','');
INSERT INTO `cmf_auth_rule` (`module`, `type`, `name`, `param`, `title`, `status`, `condition`) VALUES('Admin','admin_url','admin/activity/filterplayerstatus',NULL,'过滤/恢复','1','');




insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('320','0','Admin','Question','default','','1','1','活动管理','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('321','320','Admin','Question','quesWareroomManager','','1','1','答题活动(题库)管理','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('322','321','Admin','Question','quesListManager','','1','0','题单管理','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('323','321','Admin','Question','quesManager','','1','0','题目管理','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('324','323','Admin','Question','quesInfo','','1','0','编辑题目','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('325','323','Admin','Question','quesDel','','1','0','题目删除','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('326','322','Admin','Question','quesListManagerAjax','','1','0','题单管理Ajax','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('327','322','Admin','Question','addExistQuesListAjax','','1','0','已存在题单添加','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('328','322','Admin','Question','quesListAdd','','1','0','题单添加','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('329','322','Admin','Question','getQuesListInfo','','0','0','题单编辑修改','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('330','322','Admin','Question','quesListDel','','1','0','题单删除','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('331','320','Admin','Activity','index','','1','1','百万答题','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('332','331','Admin','Activity','edit','','1','0','百万答题添加编辑','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('333','331','Admin','Activity','delete','','1','0','百万答题删除','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('334','331','Admin','Activity','activityAnswerLogs','','1','0','答题记录','','','0');
insert into `cmf_menu` (`id`, `parentid`, `app`, `model`, `action`, `data`, `type`, `status`, `name`, `icon`, `remark`, `listorder`) values('335','331','Admin','Activity','filterPlayerStatus','','1','0','过滤/恢复','','','0');


