CREATE TABLE `cmf_live_switch` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `liveid` int(10) DEFAULT '0' COMMENT '直播ID',
  `child_liveid` int(10) DEFAULT '0' COMMENT '子直播ID',
  `orderno` int(10) DEFAULT '0' COMMENT '序号',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='直播切换表';

ALTER TABLE `cmf_config` ADD COLUMN sidebar_unlive_notice VARCHAR(255) NOT NULL COMMENT '侧边栏无直播提示语';

alter table cmf_users_guard_lists modify guard_level bigint(20) DEFAULT '1' COMMENT '守护等级';
alter table cmf_users_guard_lists modify level_endtime bigint(20) DEFAULT '0';
