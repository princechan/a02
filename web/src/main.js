import Vue from "vue";
import App from "./App";
// import App2 from './activityContainer'
// import App3 from './activityEntrance'
import VueBus from "./plugins/vue-bus";
import store from "./store";
Vue.use(VueBus);
import Event from "src/utils/Event";
import getUrlParameter from "src/utils/getUrlParameter";
import gtag from "src/utils/gtag";

var vm;
const initLiveVideo = function() {
  if (typeof vm == "undefined") {
    vm = new Vue({
      // router,
      // render: h => h(App),
      store,
      render: h => h(App),
      methods: {}
    }).$mount("#app");
    // new Vue({
    //   replace: false,
    //   store,
    //   render: h => h(App2),
    //   methods: {}
    // }).$mount('#answerContainer')
    
    
    // new Vue({
    //   replace: false,
    //   store,
    //   render: h => h(App3),
    //   methods: {}
    // }).$mount('#answerEntranceContainer')
    
  }
};

var init = getUrlParameter("init");
var source = decodeURIComponent(getUrlParameter("source"));
if (init == 1) {
  initLiveVideo();
}

window.sourceUrl = "";
if (source && source != "undefined") {
  sourceUrl = source;
} else {
  sourceUrl = "";
}

window.hideSliderLive = () => {};

window.refreshSliderLive = () => {};

window.onmessage = function(event) {
  sourceUrl = event.origin;
  if (event.data == "initLiveVideo") {
    initLiveVideo();
    gtag.open(); //统计代码
  } else if (event.data == "playLiveVideo") {
    vm.$bus.emit(Event.VIDEO_EVENT, { _method_: "playLiveVideo" });
  }

  hideSliderLive = function() {
    event.source.postMessage("hideSliderLive", event.origin);
    gtag.close(); //统计代码
  };

  refreshSliderLive = () => {
    event.source.postMessage("refreshSliderLive", event.origin);
  };
};
