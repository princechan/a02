import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'
import getters from './getters'

Vue.use(Vuex);

// console.log(_DATA);
// console.log('_DATA.user为: ' + JSON.stringify(_DATA.user, 2, 2));
// console.log('_DATA.config为: ' + JSON.stringify(_DATA.config, 2, 2));
// console.log('_DATA.anchor为: ' + JSON.stringify(_DATA.anchor, 2, 2));
// console.log('_DATA.live为: ' + JSON.stringify(_DATA.live, 2, 2));

// 这里对游客进行临时处理
if (!_DATA.user) {
  _DATA.user = {
    "id": "temp_5a939a164bc51",
    "avatar": "/public/home/images/icon-avatar.png",
    "user_nicename": "游客",
    "token": "eabd9dafe8060018e45d182d999952d4",
    "logged": 0,
    "issuper": 0,
    "iswhite": 0,
    "sex": 0,
    "signature": "",
    "consumption": 0,
    "votestotal": 0,
    "province": "",
    "city": "",
    "level": 0
  }
} else {
  _DATA.user.logged = 1;
}

// 对直播进行处理
// if(!_DATA.live || _DATA.live.islive != 1) {
  // _DATA.live = {
  //   "uid": "",
  //   "user_nicename": "",
  //   "avatar": "./default.jpg",
  //   "avatar_thumb": "./default_thumb.jpg",
  //   "showid": "",
  //   "islive": "0",
  //   "starttime": "",
  //   "title": "",
  //   "province": "",
  //   "city": "好像在火星",
  //   "stream": "",
  //   "thumb": "",
  //   "pull": "",
  //   "lng": "",
  //   "lat": "",
  //   "topicid": "0",
  //   "type": "0",
  //   "type_val": "",
  //   "ispc": "1",
  //   "chat_num": "30",
  //   "chat_frequency": "0",
  //   "lianmai_stream": null
  // }
// }


/**
 * 获取房间信息
 */
let getRoom = () => {
  var room = {};
  // if (_DATA.live && _DATA.live.uid) {
    room.typeId = 0;
    room.typeName = 'single'; // 单直播间： 'single'  多直播间： 'multi'
    room.id = _DATA.live.uid; // 房间的id
    room.userIsAnchor = (_DATA.user.id == _DATA.live.uid);             // 当前用户是否是此房间的主播
    // this.userIsDj =   // 当前用户是否是此房间的麦手
    room.userIsLive = room.userIsAnchor || (_DATA.live.islive == 1);    // 当前用户是否在直播
    room.chat_num = parseInt(_DATA.live.chat_num);             // 聊天的字数
    room.chat_frequency = parseInt(_DATA.live.chat_frequency);             // 聊天的频率
    room.isLive = (_DATA.live.islive == 1);             // 当前房间是否在直播
    room.ispc = (_DATA.live.ispc == 1);
    // room.anchorId = _DATA.anchor.id;
    // room.anchorStream = _DATA.anchor.stream
    room.streamUrl = _DATA.live.pull;
/*  } else if (_DATA.multiLive && _DATA.multiLive.id) {
    room.typeId = 1;
    room.typeName = 'multi'; // 单直播间： 'single'  多直播间： 'multi'
    room.id = _DATA.multiLive.id; // 房间的id
    room.userIsAnchor = _DATA.multiLive.ami_anchor; // 当前用户是否是此房间的主播
    room.userIsDj = _DATA.multiLive.ami_dj; // 当前用户是否是此房间的麦手
    // room.userIsLive =  (!$.isEmptyObject(rtc.liveStreams) && !!rtc.liveStreams[_DATA.user.id]); // 当前用户是否在直播
    room.chat_num = parseInt(_DATA.multiLive.chat_num || 30);   // 聊天的字数
    room.chat_frequency = parseInt(_DATA.multiLive.chat_frequency || 0); // 聊天的频率
    // room.isLive = !$.isEmptyObject(rtc.liveStreams);  // 当前房间是否在直播
    // this.ispc = ;
  }*/
  room.userIsSuper = (_DATA.user && _DATA.user.issuper && _DATA.user.issuper == 1); //用户是否是超管
  room.userIsLogged = (_DATA.user && _DATA.user.logged == 1);
  return room;
}

// 0-活动未发布,1-活动发布,2-活动准备(倒计时),3-活动进行中（先进来的用户）,4-活动结束,5-活动删除 6:排行版  7： 答题结束 8:活动准备（开始答题）  9:活动进行中（后进来的用户）
if(_DATA.activity.status == 3) {
  _DATA.activity.status= 9;
}

const state = {
  // userInfo: _DATA.user, //用户信息
  // gifts: _DATA.gift, //礼物列表

  config: _DATA.config,
  getConfigPub: _DATA.getConfigPub,
  anchor: _DATA.anchor,
  live: _DATA.live,
  gift: _DATA.gift,
  user: _DATA.user,
  giftPkInfo: _DATA.giftPkInfo,
  grabBenchInfo: _DATA.grabBenchInfo,
  guardInfo: _DATA.guardInfo,
  vests: _DATA.vests,
  thumb: _DATA.thumb,
  obs: _DATA.obs,
  push: _DATA.push,
  enterChat: _DATA.enterChat,
  room: getRoom(),
  userActivityStatus: -1,  //字符串,-1:出局；1，可继续答题 0：未报名
  // 百万答题相关
  activity: _DATA.activity,
  subjects: [],
  currentSubject: {},
  subjectTotal: 0,
  isShowActivity: false, // 是否显示答题区域
  isShowRank: false
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
})
