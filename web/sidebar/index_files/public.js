var KF = KF || {};
KF.public = (function($) {
	"use strict";
	var opt = {};
	var nowtime = 0;
	var callFlag = false;
	var phone;
	var init = function(args) {
		opt = $.extend(opt, args);
		if (opt.customerType == '0') {
			$('.mid-head-right-wrapper a:not(.gold-text)').on('click', function(event) {
				event.preventDefault();
				notice('此功能未对试玩玩家开放，请点击立即开户，立即开启您的专属账户');
			});
		}

		for(var key in opt.games){
			if (opt.games[key].gameAnnounment == "") {
				opt.games[key].gameAnnounment = '游戏维护中';
			}
		}

		$('.mid-head-right .login, #activity_popbtn, .not_login, .promo_header .login').on('click', function(event) {
			event.preventDefault();
			popLogin();
		});

		$(document).on('click.lightbox-close', '.lightbox-close, .lightbox-confirm', function(event) {
			event.preventDefault();
			$(this).closest('.lightbox').prev().remove();
			$(this).closest('.lightbox').remove();
		});

		getNowDate();

		$(document).on('click', '.customer-service, #registerok-cs,.openchat, .open-online-chat', function(event) {
			event.preventDefault();
			openOnlineChat();
		});

        $(document).on('click', '.open-vip-online-chat', function(event) {
            event.preventDefault();
            openVipOnlineChat();
        });

		$(document).on('click', '#captcha-img, .register-captcha', function(event) {
			event.preventDefault();
			var date = new Date();
			$(this).attr('src', 'genCaptcha.htm?'+date.getTime());
		});

		$(document).on('click', '.refresh.register-sprite', function(event) {
			event.preventDefault();
			$(".register-captcha").trigger("click");
		});

		$(document).on('submit', '#login-form', function(event) {

			//登入
			event.preventDefault();
			var $name = $("#login-form #username").val();
			var $pwd = $("#login-form #password").val();
			var $captcha =$("#login-form #captcha").val();
			var error_message = "";
			var $reload = $('#login-form .button.cover-sprite').data('reload');
			if(typeof is_promo_page != "undefined" && is_promo_page){
                $reload = 1;
			}

			if ($name == "" ){
                show_login_notice("游戏账号或手机号不能为空！");
				return false;
			}
            if($pwd == ""){
                show_login_notice("密码不能为空或长度不够！");
                return false;
            }

            var reg = /^[0-9a-zA-Z]{5,12}$/ ;
            if(!reg.test($name)){
                show_login_notice("帐号由c开头的5-12位数字或字母组成！");
                return false;
            }

            var reg = /^1\d{10,11}$/ ;
            if(reg.test($name)){
                login_by_phone($name, $pwd, $captcha, $reload);
			}else {
                login_by_username($name, $pwd, $captcha, $reload);
			}

			function  login_by_phone($name, $pwd, $captcha, $reload) {
				loading();
				$.ajax({
					type:'POST',
					url:"createVerifyCodeForLogin.htm",
					data:{'function':'1','smscode':$pwd,'operate':'2','type':'6','sendto':$name},
					success:function(data){
						loadingcancel();

						if(typeof(data)!='undefined'){
							if(data.isSucc=='1'){
								if(data.wsnum=='0'){
									if($reload == 1) {
                                        if (typeof waiting_obj == "object"){
                                            loginGame(waiting_obj);
                                            waiting_obj = '';
                                        }
										window.location.reload();
									} else {
										window.location.href='login_game_branch.htm';
									}
									return;
								}else if(data.wsnum=='1'){
									$(".lightbox-overlay").remove();
									$('#login-lightbox').remove();

									$('.pop-mask-mobile').fadeIn();
									$('.pop-wrap-mobile').fadeIn();

									$("#loginphonemore").html(data.returnhtml);
								}else if(data.wsnum=='2'){
									error_message = '尊敬的VIP会员，请使用贵宾版本登录，详情请联系客服';
									notice(error_message, callback_openOnlineChat);
								}else if(data.wsnum=='-1'){
									error_message = "没有找到与手机对应的账号";
								}
							}else if(data.isSucc=='-2'){
								error_message = "验证码校码次数过多，请重新发送验证码校验";
							}else if(data.isSucc=='-1'){
								error_message = "验证码错误，请重新输入";
							}else if(data.isSucc=='-3'){
								error_message = "验证码失效，请重新发送";
							}else{
								error_message = "验证码或密码不正确！";
							}
						}else{
							error_message = "网络异常，请稍后再试！";
						}

						$("#captcha-img").trigger("click");
                        show_login_notice(error_message);
					}
				});
			}
			
			function show_login_notice($error_message) {
                $(".lightbox-notice").show();
                $("#lightbox-notice-text").text($error_message);
            }

			function  login_by_username($name, $pwd, $captcha, $reload) {
            	var try_account = ['a0216414', 'a0216415'];

                if(jQuery.inArray($name, try_account) == -1 && $name.substr(0,1).toLowerCase() != 'c'){
                    show_login_notice("帐号由c开头的5-12位数字或字母组成！");
                    return false;
				}

                var pattern = /^[a-zA-Z0-9]{6,16}$/;//验证密码的正则

				/*判断是否有空格*/
                if ($.trim($pwd) == '' || ! pattern.test($pwd)) {
                    error_message = '密码由6-16位数字或字母组成，不能有空白或符号';
                    $("#captcha-img").trigger("click");
                    $(".lightbox-notice").show();
                    $("#lightbox-notice-text").text(error_message);
                    return false;
                }

				loading();
				$.ajax({
					type:'POST',
					url:"realdologin_retrycount.htm",
					data: {'username':$name,'password':$pwd,'captcha':$captcha,'from':'normal'},
					success:function(data){
						loadingcancel();
						if (data.isLoginSucc == 1){
							if($reload == 1) {
								if (typeof waiting_obj == "object"){
                                    loginGame(waiting_obj);
                                    waiting_obj = '';
								}
								window.location.reload();
							} else {
								window.location.href='login_game_branch.htm';
							}
							return;
						}else if (data.isLoginSucc == 0){
							$("#captcha").val("");
							error_message = "您输入的账号或密码不正确,请重新输入!";
						}else if (data.isLoginSucc == 2){
							error_message = '原密码已过期，请自助修改或联系客服修改';
							notice(error_message, callback_goLoginbranch);
						}else if (data.isLoginSucc == 3){
							error_message = "很抱歉，您输入的验证码不正确！";
						}else if(data.isLoginSucc == 5){
							error_message = '尊敬的VIP会员，请使用贵宾版本登录，详情请联系客服';
							notice(error_message, callback_openOnlineChat);
						}else if(data.isLoginSucc == 6){
                            error_message = '很抱歉，您不是VIP账户，请使用普通版登录，详情请联系客服';
                            notice(error_message, callback_openOnlineChat);
                        }else if(data.isLoginSucc == 11){
							error_message = "密码输错3次，会员账号已被锁住，请五分钟后再试或联系客服！";
							notice(error_message, callback_LocationReload);
						}else if(data.isLoginSucc == 12){
							error_message = "该账号已停用，请联系客服！";
						}else if(data.isLoginSucc == 13){
							error_message = "比特币账号已停用，请联系客服！";
						}else if(data.isLoginSucc == 10){
							overLogin=0;
							error_message = "您已输错游戏账号或密码两次，登录需要输入验证码！";
							notice(error_message, generateCpatchaField);
						}else if(data.isLoginSucc == 17){
							window.location.href=data.toUrl;
							return;
						}else if(data.isLoginSucc == 9){
                            error_message = "输入参数中含有不合法的字符";
                        }else if(data.isLoginSucc == 99){
							error_message = "域名登录有误，请联系客服！";
						}else{
							error_message = "很抱歉，服务器内部错误，请稍后再试或者联系在线客服"+data.isLoginSucc;
						}
						$("#captcha-img").trigger("click");
						$(".lightbox-notice").show();
						$("#lightbox-notice-text").text(error_message);
					}
				});
			}
		});

		//取款优化
		$.ajax({
			type:'POST',
			url:"newviewwithdrawlogs_index.htm",
			async:true,
			dataType:'json',
			success:function(data){
				$("#index_withdraw_count, #top_index_withdraw_count, #withdraw_page_count").html(data.count);
				$("#index_withdraw_time, #top_index_withdraw_time, #withdraw_page_time").html(data.time);
				$("#index_withdraw_today_count,#top_index_withdraw_today_count,#withdraw_page_today_count").html(data.today_count);
			}
		});

		$(document).on('keyup', '#username', function(event) {
			event.preventDefault();
			if($(this).val() !== ""){
				$("#login-lightbox .clear-input.cover-sprite").show();
			}else {
				$("#login-lightbox .clear-input.cover-sprite").hide();
			}
		});
		
		$(document).on('blur', '#username', function(event) {
			event.preventDefault();
			change_sendbutton();
		});
		
		$(document).on('click', "#login-lightbox .clear-input.cover-sprite", function(event) {
			event.preventDefault();
			$("#login-lightbox #username").val("");
			$("#login-lightbox .clear-input.cover-sprite").hide();
		});

        $(document).on('click', "#login-lightbox .eye.cover-sprite", function(event) {
            event.preventDefault();
            var $password_value = $('#lightbox-input-password-wrapper input[name=password]').val();
            $('#lightbox-input-password-wrapper').html('<div class="password cover-sprite"></div>'+
                '<div class="lightbox-input-line"></div>'+
                '<input type="password" id="password" name="password" placeholder="密码/验证码" class="lightbox-input-password">'+
                '<div class="close-eye cover-sprite"></div>');
            $('#lightbox-input-password-wrapper input[name=password]').val($password_value);

            //IE8 不支持 attr
            // $('.lightbox-input-password').attr('type', 'password');
			// $(this).removeClass('eye').addClass('close-eye');
        });

        $(document).on('click', "#login-lightbox .close-eye.cover-sprite", function(event) {
            event.preventDefault();
            var $password_value = $('#lightbox-input-password-wrapper input[name=password]').val();
            $('#lightbox-input-password-wrapper').html('<div class="password cover-sprite"></div>'+
                '<div class="lightbox-input-line"></div>'+
                '<input type="text" id="password" name="password" placeholder="密码/验证码" class="lightbox-input-password">'+
                '<div class="eye cover-sprite"></div>');
            $('#lightbox-input-password-wrapper input[name=password]').val($password_value);

            //IE8 不支持 attr
            //     $('.lightbox-input-password').attr('type', 'text');
            //     $(this).removeClass('close-eye').addClass('eye');
        });

		$(document).on('click', '.start-game', function(event) {
			event.preventDefault();
			var game_id = $(this).data('game-id');
			var gametype = $(this).data('gametype');
			var tablecode = $(this).data('tablecode');
			popAll(game_id, gametype,tablecode);
		});
		
		$(document).on('click', '.start-slot-game', function(event) {
			event.preventDefault();
			var game_provider = $(this).attr('attr-gameplat');
			var gameid = $(this).attr('attr-gameid');
            var game_name = $(this).attr('attr-gamename');
			
			popslotin(gameid, game_provider, game_name);
		});

		$(document).on('click', '.banner', function(event) {
			event.preventDefault();
			var type = $(this).data("type"),
			detail = $(this).data("detail"),
			target = $(this).data("target");
			bannerclick(type+"", detail, target+"");
		});

		$(document).on('change', '.top-head-left select', function () {
			$('option', this).prop('selected', false);
        });

		$('.notice-content a, .banner-button-wrapper a, .banner-more a').on('click', function(event) {
			event.stopPropagation();
		});

		$(document).on('click', '.bookmark', function(event) {
			event.preventDefault();
			bookmark('凯发娱乐');
		});

		$(document).on('click', '.set-home', function(event) {
			event.preventDefault();
			SetHome(window, window.location);
		});

		// $(document).on('mouseover', '.top-username', function(event) {
		// 	$(this).find('.triangle').addClass('inverted');
		// 	$('.menu-1-wrapper').show();
		// });
		// $(document).on('mouseout', '.menu-1-wrapper', function(event) {
		// 	$(this).parent().find('.triangle').removeClass('inverted');
		// 	$('.menu-1-wrapper').hide();
		// });
		
		$('.top-username').hover(function () {
	       	$(this).find('.triangle').addClass('inverted');
        	$('.menu-1-wrapper').show();
	    }, function () {
	    	$(this).parent().find('.triangle').removeClass('inverted');
		 	$('.menu-1-wrapper').hide();
	    });

		$(document).on('click', '.callback, .open-callback', function(event) {
			// 免费电话
			event.preventDefault();
			if (opt.customerType == '-1') {
				// 未登陆
				var template = $("#callback-lightbox-1-template").html();
				$("body").append(_.template(template));
			} else {
                phone = phone_call;
				var template = $("#callback-lightbox-2-template").html();
				$("body").append(_.template(template)({phone:phone}));
			}
			$(document).on('click', '.wrap-callback, .pop-callback .callback-close', function(event) {
				// 关闭弹窗
				fadeOutLightbox(this, '.wrap-callback');
			});
            $(document).on('click', '.pop-callback', function(event) {
                return false;
            });

			$('#callback-telephone-form a.btn').on('click', function(event) {
				// 输入号码
				event.preventDefault();
				var telephone = $('#callback-telephone').val();
				if (telephone == "") {
					notice("电话号码不能为空");
					return;
				}
				if (!/\d{11,12}/.test(telephone)) {
					notice("电话号码长度应为11-12位数字");
					return;
				}
				if (!/^(13|14|15|17|18|0)\d/.test(telephone)) {
					notice("请输入有效的电话号码");
					return;
				}
				if(callFlag){
					notice("您已回拨过，请30秒后再试");
					return;
				}
				callFlag = true;
				$.ajax({
					url: 'callBackTelService.htm',
					type: 'POST',
					dataType: 'json',
					data: {phone: $('#callback-telephone').val()},
					success: callback_callbackSuccess
				});
			});
			$('#callback-default-form a.btn').on('click', function(event) {
				// 已绑定号码
				event.preventDefault();
				if(callFlag){
					notice("您已回拨过，请30秒后再试");
					return;
				}
				callFlag = true;
				$.ajax({
					url: 'callBackTelService.htm',
					type: 'POST',
					dataType: 'json',
					success: callback_callbackSuccess
				});
			});
		});
		$('#header select[name=lang]').on('change', function(event) {
			if ($(this).val() != "") {
				window.open($(this).val());
			}
		});

		$('.mid-head-right .refresh').on('click', function(event) {
			event.preventDefault();
			$(this).addClass('rotating');
			$.ajax({
				url: 'serverhomeAjax.htm',
				type: 'GET',
				dataType: 'json',
				success: function(data){
					$('#localcredit').html(data.totalCredit);
					$('.mid-head-right .refresh').removeClass('rotating');
				}
			});
			
		});

		$(window).on('scroll', function(event) {
			if ($(this).scrollTop() > 200) {
				$(".top-icon").fadeIn();
			} else {
				$(".top-icon").stop().fadeOut();
			}
		});
		$(".top-icon").on('click', function(event){
			$('html, body').animate({scrollTop: 0}, "slow"); 
		});

		$(document).on('click', '.trial-game', function() {
            var template = $("#trial-template").html();
            var $gameTitle = $(this).data('game-title');
            var $gameId = $(this).data('game-id');
            var $gameType = $(this).data('game-type');
            var $gameCredit = $(this).data('game-credit');
            
            //游戏关厅
    		if(opt.games[$gameId].isEnable == 2){
    			notice(opt.games[$gameId].gameAnnounment);
    			return ;
    		}
            
            
            $("body").append(_.template(template)({gameTitle:$gameTitle,gameId:$gameId,gameType:$gameType,gameCredit:$gameCredit}));
            $('.lightbox-overlay').css({'width':$(window).width(), 'height':$(document).height()});
		});

        $(document).on('click', '.trial-now', function() {
            var $gameId = $(this).data('game-id');
            var $gameType = $(this).data('game-type');
            var $captcha = $(this).parents().find('input[name=captcha]').val();

            if($captcha=='') {
                KF.public.notice('验证码不能为空，请重新输入');
            	return;
			}

            KF.public.loading();
            var tmpurl = "createTrialAccounttoGame.htm?gmCode="+$gameId+"&gmType="+$gameType+"&captcha="+$captcha;
            window.open(tmpurl);
            window.location.reload();
            //$('#captcha-img').trigger("click");
            // $.post('/api-verify-captcha.htm', {
            //     captcha: $captcha,
            // }, function (result) {
            //     if (result.status == 1) {
            //         var tmpurl = "createTrialAccounttoGame.htm?gmCode="+$gameId+"&gmType="+$gameType+"&captcha="+$captcha;
            //         window.open(tmpurl);
            //         window.location.href='index.htm';
            //     } else if(result.status == 0) {
            //         KF.public.loadingcancel();
            //         $('#captcha-img').trigger("click");
            //         KF.public.notice('验证码不正确,请重新输入');
            //     }
            // }, 'json')
            //     .fail(function () {
            //         KF.public.loadingcancel();
            //         KF.public.notice('网络异常，请稍后尝试');
            //         return;
            //     });
        });
        
        //退出按钮
        $(document).on('click', '.global_logout', function(event) {
			event.preventDefault();
			/* Act on the event */
			$('#backdrop').show();
			$('.pop-wrapper-logout').fadeIn();
		});
        $(document).on('click', '.pop-close-event', function(event) {
			event.preventDefault();
			/* Act on the event */
			closePopUp();
		});
        function closePopUp() {
    		$('#backdrop').fadeOut();
    		$('.pop-wrapper').fadeOut();
    		$('.pop-wrapper-logout').fadeOut();
    	}
	};

	var generateCpatchaField = function(){
		var template = $("#login-captcha-template").html();
		$(".login-captcha-wrapper").html(_.template(template)({default_captcha_address:opt.default_captcha_address}));
	}

	var callback_callbackSuccess = function(data){
		setTimeout(setFlag,30000);
		if(data.result){
			$('.pop-callback .callback-close').trigger('click');
			var template = $("#callback-lightbox-3-template").html();
			if(getParameterByName('phone', "?"+this.data)){
				phone = getParameterByName('phone', "?"+this.data);
			}
			$("body").append(_.template(template)({phone:phone}));
		}else{
			 $('.pop-callback .callback-close').trigger('click');

            if(typeof data.msg != "undefined"){
                notice(data.msg);
            }else{
                notice("连接失败，请稍后重试");
            }
		}
	}

	var setFlag = function(){
		callFlag = false;
	}

	// 关闭弹窗
	var fadeOutLightbox = function(element, wrapper_class){
		$(element).closest(wrapper_class).prev().fadeOut(function() {
			$(this).remove();
		});
		$(element).closest(wrapper_class).fadeOut(function() {
			$(this).remove();
		});
	}
	// 查query string的值
	function getParameterByName(name, url) {
		if (!url) {
			url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var openOnlineChat = function(){
		var info = '';
		if(typeof opt.infoValue != "undefined" && opt.infoValue != ''){
		   info = "&info="+opt.infoValue;
		}
					
		var url = 'https://www.k8-livechat.com/chat/chatClient/chatbox.jsp?companyID=8984&configID=41&skillId=27'+info;
		var left = (screen.width/2)-(810/2);
		var top = (screen.height/2)-(530/2);
		window.open(url,
				"DescriptiveWindowName",
				"resizable,scrollbars=yes,status=1,width=810, height=530, top="+top+", left="+left);
	}

    var openVipOnlineChat = function(){
        var info = '';
        if(typeof opt.infoValue != "undefined" && opt.infoValue != ''){
            info = "&info="+opt.infoValue;
        }

        var url = 'https://www.k8-livechat.com/chat/chatClient/chatbox.jsp?companyID=8984&configID=41&skillId=29'+info;
        var left = (screen.width/2)-(810/2);
        var top = (screen.height/2)-(530/2);
        window.open(url,
            "DescriptiveWindowName",
            "resizable,scrollbars=yes,status=1,width=810, height=530, top="+top+", left="+left);
    }

	var getNowDate = function(){
		var now = new Date();
		var hour = now.getHours();
		var minute = now.getMinutes();
		var second = now.getSeconds();
		var timezone = now.getTimezoneOffset()/60 * -1;

		if( timezone >= 0  && timezone < 10){
			timezone = "+0"+timezone+":00";
		}else if(timezone >= 10){
			timezone = "+"+timezone +":00";
		}else if(timezone < 0  && timezone > -10){
			timezone = "-0"+(timezone*-1)+":00";
		}else if(timezone <= -10){
			timezone = "-"+(timezone*-1)+":00";
		}

		if(hour < 10){
			hour = "0"+hour;
		}
		if(minute < 10){
			minute = "0"+minute;
		}
		if(second < 10){
			second = "0"+second;
		}


		var year=now.getFullYear();
		var month=now.getMonth();
		var day=now.getDate();
		month=month+1;
		if(year<10){
			year = "0"+year;
		}
		if(month<10){
			month = "0"+month;
		}
		if(day<10){
			day="0"+day;
		}
		var utcHour=now.getUTCHours();
		var utcMinute=now.getUTCMinutes();
		var utcSecond=now.getUTCSeconds();
		if(utcHour < 10){
			utcHour = "0"+utcHour;
		}
		if(utcMinute < 10){
			utcMinute = "0"+utcMinute;
		}
		if(utcSecond < 10){
			utcSecond = "0"+utcSecond;
		}

		var datetext = [];
		datetext.push("(GMT ");
		datetext.push(timezone);
		datetext.push(") ");
		datetext.push(hour);
		datetext.push(":");
		datetext.push(minute);
		datetext.push(":");
		datetext.push(second);

		var text = datetext.join("");
		if($("#time")){
			$("#time").html(text);
			setTimeout(getNowDate,1000);
		}

	}

	var popAll = function(detail, gametype, videoID){

		if(opt.customerType == "2"){
			notice('您好，合作伙伴账号无法进入游戏，请您通过游戏账号进入，谢谢');
			return ;
		}


		//游戏关厅
		if(opt.games[detail].isEnable == 2){
			notice(opt.games[detail].gameAnnounment);
			return ;
		}


		//PT只支持真钱的游戏
		if(detail== 'A02039' &&　opt.customerType != 1){
            // pop3({title:"提示", message:"很抱歉，PT游戏只支持真钱游戏", button1_text:"立即开户", button2_text:"游戏参观", txt_center: 1}, function(){
            //     $('.lightbox-button-2.lightbox-button-3-1').one('click', function(event) {
            //         location.href = "register.htm";
            //     });
            //     $('.lightbox-button-2.lightbox-button-2-2').one('click', function(event) {
            //         popvisit(detail, gametype, videoID);
            //     });
            // });
            notice('很抱歉，PT游戏只支持真钱游戏');
            return ;
        }

        //MG只支持真钱的游戏
        if(detail== 'A02035' &&　opt.customerType != 1){
            pop3({title:"提示", message:"很抱歉，MG游戏只支持真钱游戏", button1_text:"立即开户", button2_text:"游戏参观", txt_center: 1}, function(){
                $('.lightbox-button-2.lightbox-button-3-1').one('click', function(event) {
                    location.href = "register.htm";
                });
                $('.lightbox-button-2.lightbox-button-2-2').one('click', function(event) {
                    popvisit(detail, gametype, videoID);
                });
            });
            // notice('很抱歉，MG游戏只支持真钱游戏');
            return ;
        }

        //EA只支持真钱的游戏
        if(detail== 'A02009' &&　opt.customerType != 1){
            pop3({title:"提示", message:"很抱歉，EA游戏只支持真钱游戏", button1_text:"立即开户", button2_text:"游戏参观", txt_center: 1}, function(){
                $('.lightbox-button-2.lightbox-button-3-1').one('click', function(event) {
                    location.href = "register.htm";
                });
                $('.lightbox-button-2.lightbox-button-2-2').one('click', function(event) {
                    popvisit(detail, gametype, videoID);
                });
            });
            // notice('很抱歉，MG游戏只支持真钱游戏');
            return ;
        }

		//OPUS只支持真钱的游戏
		if(detail== 'A02043' &&　opt.customerType != 1){
			notice('很抱歉，OPUS游戏只支持真钱游戏');
			return ;
		}
		
		//虚拟体育，直接弹登录框
		if(detail== 'A02046' &&　opt.customerType != 1){
			popLogin();
			return ;
		}

		if(nowtime==0){
			nowtime = new Date().getTime();
		}else{
			if(parseInt(new Date().getTime())-parseInt(nowtime)<parseInt("15000")){
				notice("请不要重复点击，15秒后再试！");
				return ;
			}else{
				nowtime = new Date().getTime();
			}
		}
		//真钱进游戏
		if(opt.isMoneyCustomer ==1){

			popgamein(detail, gametype,videoID);

		}
		//试玩进游戏
		else{
			popvisit(detail, gametype, videoID);
		}

	}

    var popgamein = function ($gameid, $gametype,$videoID) {
        if ($gameid == 'A02035') {
            var url = 'loginGame.htm?gameCode=A02035&gameType=1';
        } else if ($gameid == 'A02039') {
            var data = {
                'game_code': 'PT',
                'game_kind': 5,
                'game_id': 'bal',
                'login_website': location.hostname
            };

            $.post('/visitedgame.htm', {
                'data': {
                    'game_code': data.game_code,
                    'game_kind': data.game_kind,
                    'game_id': data.game_id,
                    'login_website': data.login_website
                }
            }, function (result) {
            }, 'json');

            var url = 'loginslotgame.htm?gameId=bal?PT?gameName=';
        } else {
        	if(typeof($gametype) != "undefined" && $gametype != "" && typeof $videoID !== "undefined" && $videoID != ""){
        		var url = 'loginGame.htm?gameCode=' + $gameid + '&gameType=' + $gametype + "&videoID="+$videoID;
			}else if (typeof($gametype) != "undefined" && $gametype != "") {
                var url = 'loginGame.htm?gameCode=' + $gameid + '&gameType=' + $gametype;
            } else {
                var url = 'loginGame.htm?gameCode=' + $gameid;
            }
        }
        window.open(url, '_blank');
    }

	var popvisit = function($gameid,$gameType, $videoID){
		var url;
        var $gameType = typeof($gametype) == "undefined" ? '' : '&gmType='+$gameType;
		if($gameid=='A02003'){
            if($gameType != "undefined" && $gameType != "" && typeof $videoID !== "undefined" && $videoID != ""){
                url = "createVisitorAccount.htm?gmCode=" + $gameid+$gameType+"&videoID="+$videoID;
            } else {
                url = "createVisitorAccount.htm?gmCode=" + $gameid+$gameType;
            }
		}else{
			if($gameType != "undefined" && $gameType != "" && typeof $videoID !== "undefined" && $videoID != ""){
				url = "createVisitorAccount.htm?gmCode=" + $gameid+$gameType+"&videoID="+$videoID;
			}else if($gameType != "undefined" && $gameType != ""){
				url = "createVisitorAccount.htm?gmCode=" + $gameid+$gameType;
			}else{
				url = "createVisitorAccount.htm?gmCode=" + $gameid;
			}
		}
		window.open(url, "_blank");
	}
	
	var popslotin = function(gameid, game_provider, game_name){
		var game_provider = game_provider.toUpperCase();
		var game_arr = {'PNG':'A02052','TTG':'A02027','AG':'A02026','MG':'A02035','PT':'A02039','OPUS':'A02043','GPI':'A02048','BS':'A02051','AS':'A02064'};
		var game_code =eval("game_arr."+game_provider);
        var game_name = game_name || '';
		
	    //游戏关厅
		if(opt.games[game_code].isEnable == 2){
			notice(opt.games[game_code].gameAnnounment);
			return ;
		}

        var window_name = game_provider == 'PNG' ? "png_game_window" : "_blank"  ;
		if(opt.customerType == "-1"){

            var url = 'visitslotgame.htm?gameId='+gameid+"?"+game_provider+"?gameName="+game_name;
		    window.open(url, window_name);
		    
		}else{
			var map = new ParamsMap();
			sendAjaxRequest("checkLoginDetails.htm", map, function(results){
				if (results.isLogin != "false"){
					window.open('loginslotgame.htm?gameId='+gameid+"?"+game_provider, window_name);
				}else{
					notice('尊敬的会员，您的登陆已超时，请您重新登陆！',KF.public.callback_goIndexPage);
				}
			},false);
		}
	}
	var bannerclick = function($type,$detail,target){

		if($detail == '') return;

		//type =1 直接跳页面
		if ($type == 1){

			if(target.indexOf('javascript:')>-1){
				eval('"'+target+'"');
			}

			//新开窗口
			if(target == 1){
				window.open($detail);
			}else{
				window.location.href = $detail;
			}

		}
		//type=2时，表示进入游戏
		if ($type == 2){
			var $arr = $detail.split('&');
			if($arr.length>1){
				//进入电子游戏
				loginGameByCode($arr[0],$arr[1]);
			}else{//进入游戏大厅  游戏代码&游戏类型
				popAll($detail);
			}
		}
		//type=4时，表示只允许进入游戏
		if ($type == 4){
			
			if(opt.customerType != '1'){
				popLogin();
				return false;
			}
			var $arr = $detail.split('&');
			if($arr.length>1){
				//进入电子游戏
				loginGameByCode($arr[0],$arr[1]);
			}else{//进入游戏大厅  游戏代码&游戏类型
				popAll($detail);
			}
		}
	}

	var bookmark = function(title, url){
		if(url != null){
			var BookmarkURL = url;
		}else{
			var BookmarkURL="http://" + window.location.hostname;
		}

		if(navigator.appName=='Microsoft Internet Explorer'){
			window.external.AddFavorite(BookmarkURL, title);
		}
		else {
			//google not support bookmark by javascript
			notice("请点击 Ctrl + d 加入收藏");
			// window.sidebar.addPanel(title, BookmarkURL, '');
		}
	}

	var SetHome = function(obj,url){
		try{
			obj.style.behavior='url(#default#homepage)';
			obj.setHomePage(url);
		}catch(e){
			if(window.netscape){
				try{
					netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
				}catch(e){
					notice("抱歉，此操作被浏览器拒绝，请更换其他浏览器尝试。");
				}
			}else{
				notice("抱歉，您所使用的浏览器不支持此操作，请手动设置。");
			}
		}
	}

	var pop = function(param, callback){
		var title = param.title,
		message = param.message;

		var template = $("#pop-template").html();
		$("body").append(_.template(template)({title:title, message:message}));
		adjust_lightbox_css();
		if (callback && typeof callback === "function") {
			callback();
		}
	}

	var pop2 = function(param, callback){
		var template = $("#pop-template2").html();
		$("body").append(_.template(template)(param));
		adjust_lightbox_css();
		if (callback && typeof callback === "function") {
			callback();
		}
	}

    var pop3 = function(param, callback){
        var template = $("#pop-template3").html();
        $("body").append(_.template(template)(param));
        adjust_lightbox_css();
        if (callback && typeof callback === "function") {
            callback();
        }
    }
    
    var popLogin = function(){
    	
    	var template = $("#login-lightbox-template").html();
    	if($(this).data('reload')) {
            $("body").append(_.template(template)({reload:true}));
    	} else {
            $("body").append(_.template(template));
    	}
    	$('.lightbox-overlay').css({'width':$(window).width(), 'height':$(document).height()});
    	$('input').placeholder();
    	if (overLogin == '0') {
    		generateCpatchaField();
    	}
    }
    
    

	var adjust_lightbox_css = function(){
		if ($('.lightbox').length>1) {
			var prev_lightbox_z_index = $('.lightbox:eq(0)').css('z-index');
			$('.lightbox-overlay').css({'z-index':prev_lightbox_z_index++});
			$('#pop-lightbox').css({'z-index':prev_lightbox_z_index++});
		}
		if ($('.pop-wrapper').length>1) {
			var prev_pop_wrapper_z_index = $('.pop-wrapper:eq(0)').css('z-index');
			$('.lightbox-overlay').css({'z-index':prev_pop_wrapper_z_index++});
			$('#pop-lightbox').css({'z-index':prev_pop_wrapper_z_index++});
		}
		$('.lightbox-overlay').css({'width':$(window).width(), 'height':$(document).height()});
	}

	var notice = function(message, callback){
		pop({title:'提示', message:message});
		if (callback && typeof callback === "function") {
			callback();
		}
	}

	var callback_LocationReload = function(){
		$(".lightbox-confirm, .lightbox-close").one('click', function(event) {
			location.reload();
		});
	}

    var callback_pageLocationReload = function(){
        $(".lightbox-confirm, .lightbox-close").one('click', function(event) {
        	loading();
            location.reload();
        });
    }

	var callback_goLoginbranch = function(){
		$(".lightbox-confirm, .lightbox-close").one('click', function(event) {
			location.href = "Loginbranch.htm";
		});
	}

    var callback_goIndexPage = function(){
        $(".lightbox-confirm, .lightbox-close").one('click', function(event) {
            location.href = "index.htm";
        });
    }

	var callback_openOnlineChat = function(){
		$(".lightbox-confirm").one('click', function(event) {
			openOnlineChat();
		});
	}

	var loading = function(){
		var template = $("#loading-template").html();
		$("body").append(_.template(template));
		$('.lightbox-overlay').css({'width':$(window).width(), 'height':$(document).height()});
	
		//loading animation
		var supports = (function() {
			var 
			div = document.createElement('div'),
			vendors = 'Khtml Ms O Moz Webkit'.split(' '),
			len = vendors.length;
	
			return function(prop) {
			if ( prop in div.style ) return true;
	
			prop = prop.replace(/^[a-z]/, function(val) {
			  return val.toUpperCase();
			});
	
			while(len--) {
			  if ( vendors[len] + prop in div.style ) {
			  // browser supports box-shadow. Do what you need.
			  // Or use a bang (!) to test if the browser doesn't.
			    return true;
			  } 
			}
			return false;
			}
		})();
	
		var
			logo_step = 12,
			logo_steppos = 83,
			logo_delay = 100
			;
		/**
		* @param  {jquery dom} 目标元素
		* @param  {number} 每帧距离
		* @param  {number} 帧数
		* @param  {ms} 毫秒数
		* @return {null}
		*/
		var animation = function($target, steppos, step, delay){
			var idx = 1;
			var _animation = function(step){
			$target.css('background-position', -(idx-1) * steppos + 'px 0');
			};
			var timer = setInterval(function(){
			idx += 1;
			if( idx > step ){
			  idx = 1;
			}
			_animation(idx);
			}, delay);
		};
	
		if ( supports('animation') ){
			$('.loading-sprite').addClass('animated');
			}else{
			animation($('.loading-sprite'), logo_steppos, logo_step, logo_delay);
		}
	}
	
	var loadingcancel = function(){
		$('.loading.lightbox-overlay').remove();
		$('#loading-lightbox').remove();
	}

	var aboutk8_branch = function(){
		$('.main-left-title, .expand-all').on('click', function(event) {
			event.preventDefault();
			$('.main-left-title').removeClass('highlight');
			$(this).addClass('highlight');
			var selector = $(this).data('filter');
			$('.cell-wrapper').isotope({
				filter: selector,
				itemSelector: ".cell",
				layoutMode: 'fitRows'
			});
			if ($(this).data('filter') == ".cell:lt(7)") {
				// 全部风采
				$('.expand-all').show();
			} else if ($(this).data('filter') == "*") {
				// 展开全部
				$(this).hide();
				$('.main-left-title:eq(0)').addClass('highlight');
			} else {
				$('.expand-all').hide();
			}
		});

		$('.main-left-title:eq(0)').trigger('click');
		$('.banner').on('click', function(event) {
			event.preventDefault();
			event.stopPropagation();
			location.href = "manchester_schalke01.htm";
		});
	}
	
	
	// 专属经理专线
	var customer_exclusive_line = function(){
		
		if (opt.customerType == '-1') {
			return false;
		}
		
		if (is_customer_exclusive_line != '1') {
			return false;
		}
		
		var template = $("#vip-callback-lightbox-2-template").html();
		$("body").append(_.template(template)());
		
		phone = $('#vip-customer-phone').html();
		
			
		// 关闭弹窗
		$(document).on('click', '.pop-callback .callback-close', function(event) {
			fadeOutLightbox(this, '.wrap-callback');
		});
		
		$('#vip-callback-telephone-form a.btn').on('click', function(event) {
			// 输入号码
			event.preventDefault();
			var telephone = $('#vip-callback-telephone').val();
			if (telephone == "") {
				notice("电话号码不能为空");
				return;
			}
			if (!/\d{11,12}/.test(telephone)) {
				notice("电话号码长度应为11-12位数字");
				return;
			}
			if (!/^(13|14|15|17|18|0)\d/.test(telephone)) {
				notice("请输入有效的电话号码");
				return;
			}
			if(callFlag){
				notice("您已回拨过，请30秒后再试");
				return;
			}
			callFlag = true;
			$.ajax({
				url: 'callBackTelService.htm',
				type: 'POST',
				dataType: 'json',
				data: {phone: $('#vip-callback-telephone').val(), is_vip: 1},
				success: vip_callback_callbackSuccess
			});
		});
		$('#vip-callback-default-form a.btn').on('click', function(event) {
			// 已绑定号码
			event.preventDefault();
			if(callFlag){
				notice("您已回拨过，请30秒后再试");
				return;
			}
			callFlag = true;
			$.ajax({
				url: 'callBackTelService.htm',
				type: 'POST',
				dataType: 'json',
				data: {is_vip: 1},
				success: vip_callback_callbackSuccess
			});
		});
	}
	
	var vip_callback_callbackSuccess = function(data){
		setTimeout(setFlag,30000);
		if(data.result){
			$('.pop-callback .callback-close').trigger('click');
			var template = $("#vip-callback-lightbox-3-template").html();
			if(getParameterByName('phone', "?"+this.data)){
				phone = getParameterByName('phone', "?"+this.data);
			}
			$("body").append(_.template(template)({phone:phone}));
		}else{
			 $('.pop-callback .callback-close').trigger('click');
			 if(typeof data.msg != "undefined"){
				 notice(data.msg);
			 }else{
				 notice("连接失败，请稍后重试");
			 }
			 
		}
	}
	
	var formError = function($obj, $msg){
		if(typeof $obj == "string"){
			$obj = $($obj);
		}
		$obj.parents('.form-group').find('.error-message span').text($msg);
		$obj.parents('.form-group').find('.error-message').show();
	}

	return {
		init: init,
		bannerclick: bannerclick,
		notice: notice,
		loading: loading,
		loadingcancel:loadingcancel,
		callback_LocationReload:callback_LocationReload,
        callback_pageLocationReload:callback_pageLocationReload,
        callback_goIndexPage:callback_goIndexPage,
        pop: pop,
		pop2: pop2,
		pop3: pop3,
		popLogin: popLogin,
		openOnlineChat: openOnlineChat,
		aboutk8_branch: aboutk8_branch,
		customer_exclusive_line: customer_exclusive_line,
		popAll: popAll,
		formError: formError,
        bookmark:bookmark
	};
})(jQuery);

/* HTML5 Placeholder jQuery Plugin - v2.3.1
 * Copyright (c)2015 Mathias Bynens
 * 2015-12-16
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof module&&module.exports?require("jquery"):jQuery)}(function(a){function b(b){var c={},d=/^jQuery\d+$/;return a.each(b.attributes,function(a,b){b.specified&&!d.test(b.name)&&(c[b.name]=b.value)}),c}function c(b,c){var d=this,f=a(this);if(d.value===f.attr(h?"placeholder-x":"placeholder")&&f.hasClass(n.customClass))if(d.value="",f.removeClass(n.customClass),f.data("placeholder-password")){if(f=f.hide().nextAll('input[type="password"]:first').show().attr("id",f.removeAttr("id").data("placeholder-id")),b===!0)return f[0].value=c,c;f.focus()}else d==e()&&d.select()}function d(d){var e,f=this,g=a(this),i=f.id;if(!d||"blur"!==d.type||!g.hasClass(n.customClass))if(""===f.value){if("password"===f.type){if(!g.data("placeholder-textinput")){try{e=g.clone().prop({type:"text"})}catch(j){e=a("<input>").attr(a.extend(b(this),{type:"text"}))}e.removeAttr("name").data({"placeholder-enabled":!0,"placeholder-password":g,"placeholder-id":i}).bind("focus.placeholder",c),g.data({"placeholder-textinput":e,"placeholder-id":i}).before(e)}f.value="",g=g.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id",g.data("placeholder-id")).show()}else{var k=g.data("placeholder-password");k&&(k[0].value="",g.attr("id",g.data("placeholder-id")).show().nextAll('input[type="password"]:last').hide().removeAttr("id"))}g.addClass(n.customClass),g[0].value=g.attr(h?"placeholder-x":"placeholder")}else g.removeClass(n.customClass)}function e(){try{return document.activeElement}catch(a){}}var f,g,h=!1,i="[object OperaMini]"===Object.prototype.toString.call(window.operamini),j="placeholder"in document.createElement("input")&&!i&&!h,k="placeholder"in document.createElement("textarea")&&!i&&!h,l=a.valHooks,m=a.propHooks,n={};j&&k?(g=a.fn.placeholder=function(){return this},g.input=!0,g.textarea=!0):(g=a.fn.placeholder=function(b){var e={customClass:"placeholder"};return n=a.extend({},e,b),this.filter((j?"textarea":":input")+"["+(h?"placeholder-x":"placeholder")+"]").not("."+n.customClass).not(":radio, :checkbox, [type=hidden]").bind({"focus.placeholder":c,"blur.placeholder":d}).data("placeholder-enabled",!0).trigger("blur.placeholder")},g.input=j,g.textarea=k,f={get:function(b){var c=a(b),d=c.data("placeholder-password");return d?d[0].value:c.data("placeholder-enabled")&&c.hasClass(n.customClass)?"":b.value},set:function(b,f){var g,h,i=a(b);return""!==f&&(g=i.data("placeholder-textinput"),h=i.data("placeholder-password"),g?(c.call(g[0],!0,f)||(b.value=f),g[0].value=f):h&&(c.call(b,!0,f)||(h[0].value=f),b.value=f)),i.data("placeholder-enabled")?(""===f?(b.value=f,b!=e()&&d.call(b)):(i.hasClass(n.customClass)&&c.call(b),b.value=f),i):(b.value=f,i)}},j||(l.input=f,m.value=f),k||(l.textarea=f,m.value=f),a(function(){a(document).delegate("form","submit.placeholder",function(){var b=a("."+n.customClass,this).each(function(){c.call(this,!0,"")});setTimeout(function(){b.each(d)},10)})}),a(window).bind("beforeunload.placeholder",function(){var b=!0;try{"javascript:void(0)"===document.activeElement.toString()&&(b=!1)}catch(c){}b&&a("."+n.customClass).each(function(){this.value=""})}))});

function myBrowser(){
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器
    if (isIE) {
        var IE5 = IE55 = IE6 = IE7 = IE8 = false;
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        IE55 = fIEVersion == 5.5;
        IE6 = fIEVersion == 6.0;
        IE7 = fIEVersion == 7.0;
        IE7 = fIEVersion == 8.0;
        if (IE55 || IE6 || IE7 || IE8) {
            return true;
        }
    }
}

function toThousands(num){
    return (num || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
}