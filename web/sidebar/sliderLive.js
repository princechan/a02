var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
};


var liveVideo = {
    el: document.getElementById('sliderLiveIframe'),
    isInit: false,
    show: function() {
        if(!this.isInit) {
            this.isInit = true;
            this.el.contentWindow.postMessage('initLiveVideo','*'); //初始化
            setTimeout(function() {
              $("#sliderLiveIframe").css({
                width: 263,
                opacity: 1
              });
            }, 800);
        } else {
            this.el.contentWindow.postMessage('playLiveVideo','*'); //播放视频
        }
        $(this.el).css({
          visibility: 'visible'
        });
    },
    hide: function() {
        this.el.contentWindow.postMessage('pauseLiveVideo','*'); //暂停视频
        $(this.el).css({
          visibility: 'hidden'
        })
    }
}

window.onmessage = function(event) {
    if(event.data === 'hideSliderLive') { //关闭侧边栏
        liveVideo.hide();
    } else if(event.data == 'refreshSliderLive') {
      setTimeout(function() { //侧边栏刷新后得再次初始化
        liveVideo.el.contentWindow.postMessage('initLiveVideo','*'); //初始化
      }, 5000)
    }
};


// 侧边栏对接
$(function () {

  var url = "https://www.a02zhibo.com/member/login?username="+getUrlParameter("username")+"&password="+getUrlParameter('password')+"&token="+(getUrlParameter('token') || 123) + "&level=" + (getUrlParameter("level") || 0);
  if(getUrlParameter("roomid")) {
    url += "&roomid=" + getUrlParameter("roomid");
  } else {
    url += "&roomid=" + 279
  }
  url += "&redirect=side_bar";
  $("#sliderLiveIframe").attr("src", url)

    $("#showSliderLive").click(function(e) {
        e.preventDefault();
        $('.nav-open .close').trigger('click');
        liveVideo.show();
    });
});
