
	
	
	function pro_start(pageName){
		var uid=$("#uid").val();
		var start=$("#start").val();
		var end=$("#end").val();
		var count=$("#count").val();
		var token=$("#token").val();
		if(parseInt(start)<=0)
		{
			layer.msg("这是首页");
		}
		else
		{
			var starte=parseInt(start)-20;
			var ende=parseInt(end)-20;
			window.location.href="./index.php?g=User&m=record&a="+pageName+"&uid="+uid+"&start="+starte+"&end="+ende+"&token="+token;
		}
	}


	function pro_end(pageName){
		var uid=$("#uid").val();
		var start=$("#start").val();
		var end=$("#end").val();
		var count=$("#count").val();
		var token=$("#token").val();

		if(parseInt(end)>=parseInt(count))
		{
			layer.msg("这已经是最后一页啦");
		}
		else
		{
			var starte=parseInt(start)+20;
			var ende=parseInt(end)+20;
			window.location.href="./index.php?g=User&m=record&a="+pageName+"&uid="+uid+"&start="+starte+"&end="+ende+"&token="+token;
		}
	}


	

	function submitChange(OriginalNum,uid){

		var nowNum=$(".changePercent").val();//当前填写的新数字
		//判断当前数字类型
		if(nowNum<=0||nowNum>=100||isNaN(nowNum)||nowNum==""){
			layer.msg("请填写正确的数字格式");
			return;
		}
		if(parseInt(OriginalNum)==parseInt(nowNum)){
			layer.msg("请填写新的数字");
			return;
		}else{
			//var agentid=$("#agentid").val();//当前经纪人id
			$.ajax({
				url: './index.php?g=User&m=Agent&a=changeUserAgentPercent',
				type: 'POST',
				dataType: 'json',
				data: {uid:uid,OriginalNum:nowNum},
				success:function(data){

					if(data.code==0){

						layer.closeAll();
						layer.msg('修改成功', {icon: 1});
						window.setTimeout(function(){
							location.reload();
						},2000);
						
						return;
					}else{
						layer.closeAll();
						layer.msg("修改失败");
						
						return;
					}

					
				},
				error:function(data){
					layer.msg("修改失败");
					layer.closeAll();
				}
			});
					

		}

	}

	//关闭弹窗
	function cancel(){
		layer.closeAll();
	}
	

