$(function () {
  $("#answerBox").css({
    height: $("#giftRecord").height() + $("#LF-chat-gift").height() 
  });

  $(window).on("resize", function () {
    $("#answerBox").css({
      height: $("#giftRecord").height() + $("#LF-chat-gift").height()
    });
  })

  $("#answerEntrance").bind('click', function () {
    $("#answerBox").toggle();
  });

  (function () {
    if (_DATA.activity.status == 0) {
      $("#answerEntrance").hide();
    // } 
    // else if (_DATA.activity.status == 4 && $("#answerBox").is(":hidden")) {
      // layer.msg("活动已结束");
    } else if (_DATA.activity && parseInt(_DATA.activity.status) > 0) {
      $("#answerEntrance").show();
    }
  })();

});

// window.test = function () {
//   // /index.php?g=Api&m=Activity&a=setActivityStatusFinished&activity_id=1
//   $.ajax({
//     type: "post",
//     url: "./index.php?g=Api&m=Activity&a=setActivityStatusTest",
//     data: {
//       activity_id: _DATA.activity.id, //1,2,3,4
//       // question_id: 
//     },
//     success: function (data) {
//       console.log('setActivityStatusFinished', data)
//     }
//   });
// }

// window.enroll = function () {
//   $.ajax({
//     type: "post",
//     url: "./index.php?g=Api&m=Activity&a=enroll",
//     data: {
//       activity_id: _DATA.activity.id, 
//     },
//     success: function (data) {
//       console.log('报名成功', data)
//     }
//   });
// }


  // _DATA.activity =  {
  //   "id": "1",
  //   "name": "20180406测试",
  //   "start_at": "1522997100",
  //   "base_bonus_amount": "1000000.00",
  //   "desct": "这是一个测试活动N\r\n这是一个测试活动2\r\n这是一个测试活动3\r\n这是一个测试活动4\r\n这是一个测试活动5\r\n这是一个测试活动1\r\n这是一个测试活动2\r\n这是一个测试活动3\r\n这是一个测试活动4\r\n这是一个测试活动5\r\n这是一个测试活动1\r\n这是一个测试活动2\r\n这是一个测试活动3\r\n这是一个测试活动4\r\n这是一个测试活动5",
  //   "status": "3",
  //   "icon": "http://testzhibo.com/data/upload/20180406/5ac70b570d6f2.jpg",
  //   "answer_countdown_time": "15",
  //   "enroll_num": 1,
  //   "server_time": 1523007549,
  //   "current_question": {
  //     "id": 1,
  //     "content": {
  //       "question": "“都是时辰的错”这句话出自哪里？",
  //       "answer": [
  //         {
  //           "key": 1,
  //           "text": "Fate/Zero"
  //         },
  //         {
  //           "key": 2,
  //           "text": "海贼王"
  //         },
  //         {
  //           "key": 3,
  //           "text": "高达seed"
  //         },
  //         {
  //           "key": 4,
  //           "text": "刀剑神域"
  //         }
  //       ]
  //     },
  //     "timestamp": 1523007549.6651,
  //     "total_num": 10
  //   }
  // }
  // {
  //   "id": "1",
  //   "name": "活动",
  //   "start_at": new Date('2018/04/02 10:30:50').getTime() / 1000, //开始时间戳
  //   "base_bonus_amount": "111.00",
  //   "desct": "地方撒旦飞洒发",
  //   "status": "2",
  //   "icon": "http://www.live.com/public/images/logo.png",
  //   "enroll_num": 0,
  //   "server_time": new Date('2018/04/05 10:30:50').getTime() / 1000,
  // }
