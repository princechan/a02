
export default class Event {
  // 向sockt服务器发送事件
  static EMIT_EVENT = 'EMIT_EVENT';
  
  static ACTIVITY_NEXT_EVENT = 'ACTIVITY_NEXT_EVENT';
  static ACTIVITY_RESULT_EVENT = 'ACTIVITY_RESULT_EVENT';
  static SET_SUBJECT_TOTAL = 'SET_SUBJECT_TOTAL';

}
