import fetch from '../config/fetch'
import {getStore} from './mUtils'



/**
 * 
 */
export const signUpActivity = (activity_id) => fetch(
  "./index.php?g=Api&m=Activity&a=enroll",
  {
    activity_id,
    // uid: 580 //todo test , 待删除
  },
  'POST'
);

export const isSignUpActivity = (activity_id, live_id) => fetch(
  "./index.php?g=Api&m=Activity&a=isEnroll",
  {
    activity_id,
    live_id,

    // uid: 580  //todo test , 待删除
  },
  'POST'
);

/**
 * 获取活动的详情
 */
export const getActivity = (activity_id, live_id, live_token, uid) => fetch(
  "./index.php?g=Api&m=Activity&a=getCurrentQuestion",
  {
    user_id
  },
  'POST'
);


/**
 * 取当前问题及备选答案API
 */
export const getCurrentQuestion = (activity_id, live_id, uid) => fetch(
  "./index.php?g=Api&m=Activity&a=getCurrentQuestion",
  {
    activity_id,
    live_id,
    uid,
  },
  'POST'
);

export const getRightAnswerNum = (activity_id, live_id) => fetch(
  "./index.php?g=Api&m=Activity&a=getRightAnswerNum",
  {
    activity_id,
    live_id,
  },
  'POST'
);


export const getAnswerTotalNum = (activity_id, live_id, uid) => fetch(
  "./index.php?g=Api&m=Activity&a=answerTotalNum",
  {
    activity_id,
    live_id,
    uid,
  },
  'POST'
);


/**
 * 用户提交答案并验证返回结果API
 */
export const pushVerifyAnswer = (activity_id, live_id, answer) => fetch(
  "./index.php?g=Api&m=Activity&a=pushVerifyAnswer",
  {
    activity_id,
    live_id,
    answer,
  },
  'POST'
);

export const getPlayerStatus = (activity_id, live_id) => fetch(
  "./index.php?g=Api&m=Activity&a=getPlayerStatus",
  {
    activity_id,
    live_id,
  },
  'POST'
);

// {"code":400,"message":"您已参与过","data":[]}

/**
 * 抢答排行榜
 */
export const getPrizeList = (activity_id, page, limit) => fetch(
  // "./index.php?g=api&m=activity&a=getPrizeList",
  "./index.php",
  {
    g: 'api',
    m: 'activity',
    a: 'getPrizeList',
    activity_id,
    page,
    limit
  },
  'GET'
);


export const getExtPrizeList = (activity_id, page, limit) => fetch(
  "./index.php",
  {
    g: 'api',
    m: 'activity',
    a: 'getExtPrizeList',
    activity_id,
    page,
    limit
  },
  'GET'
);



export const setActivityStatus = (activity_id, live_id, uid, status) => fetch(
  "./index.php?g=Api&m=Activity&a=setActivityStatus",
  {
    activity_id,
    live_id,
    uid,
    status
  },
  'POST'
);

export const getServerTimeAjax = () => fetch(
  "./index.php?g=Api&m=Activity&a=getServerTime",
  {
   
  },
  'GET'
);