/**
 * 配置编译环境和线上环境之间的切换
 *
 * baseUrl: 域名地址
 * imUrl: im
 * imgBaseUrl: 图片所在域名地址
 *
 */

//测试
let baseUrl = '';


if (process.env.NODE_ENV == 'development') {
  baseUrl = 'http://127.0.0.1';
}else if(process.env.NODE_ENV == 'production'){
}

export {
	baseUrl,
}
