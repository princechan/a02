import Vue from 'vue'
import App from './App'
import App2 from './App2'
import VueBus from "./plugins/vue-bus";
import infiniteScroll from 'vue-infinite-scroll';
import store from './store';
Vue.use(VueBus);
Vue.use(infiniteScroll)
import Event from 'src/utils/Event';

new Vue({
  replace: false,
  store,
  render: h => h(App),
  methods: {}
}).$mount('#answerContainer')


new Vue({
  replace: false,
  store,
  render: h => h(App2),
  methods: {}
}).$mount('#answerEntranceContainer')
