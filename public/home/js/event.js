var JsInterface = {
  giftEffects: 1,
  roomNumObj: $(".MR-count .online .info cite"),
  userListNum: $(".MR-online .user cite"),
  userCite: $(".MR-online .nav-tab cite"),
  chatFromSocket: function (data) {
    var data = WlTools.strTojson(data);
    var msgObject = data.msg[0];
    var msgtype = msgObject.msgtype;
    var msgaction = msgObject.action;
    var msgmethod = msgObject._method_;
    if (msgmethod == 'SendMsg') { //聊天信息
      this.sendMsg(msgObject);
    } else if (msgmethod == 'SendGift') { //赠送礼物
      this.sendGift(msgObject);
    } else if (msgmethod == 'SendHorn') { //喇叭
      this.sendHorn(msgObject);
    } else if (msgmethod == 'SystemNot' || msgmethod == 'ShutUpUser') { //系统信息//禁言 踢人
      this.systemNot(msgObject);
    } else if (msgmethod == 'StartEndLive') { //开关播
      this.showEndRecommend(msgObject);
    } else if (msgmethod == 'disconnect') { //关播
      this.disconnect(msgObject);
      /* 新增 用户掉线直播间--在线用户列表该掉线用户没有下线*/
      if (msgObject.action == 'flushOnlineList') { //用户下线,要刷新用户列表
        User.getOnline();
      }
    } else if (msgmethod == 'requestFans') { //关播
      var nums = msgObject.ct.data.info.nums;
    } else if (msgmethod == 'KickUser') { //踢人
      this.KickUser(msgObject);
    } else if (msgmethod == 'SendBarrage') { //弹幕
      this.sendBigHorn(msgObject);
    } else if (msgmethod == 'light') { //弹幕
      this.setLight(msgObject);
    } else if (msgmethod == 'clearScreen') { //清屏
      this.clearScreen(msgObject);
    } else if (msgmethod == 'showuserinfo') {
      this.showuserinfo(msgObject);
    } else if (msgmethod == 'SystemAdvertise') { //系统推送文字广告
      this.systemAdvertise(msgObject);
    } else if (msgmethod == 'SendRedPack') { //发红包
      this.SendRedPack(msgObject);
    } else if (msgmethod == 'giftPK') { //礼物PK
      this.giftPK(msgObject);
    } else if (msgmethod == 'GrabBench') { //抢板凳活动
      this.GrabBench(msgObject);
    } else if (msgmethod == 'Carouse') { //转盘游戏
      this.Carouse(msgObject);
    } else if (msgmethod == 'lianmai') { //PC端连麦
      this.Lianmai(msgObject);
    } else if (msgmethod == "channel_gap") { //直播间禁言
      this.channelGap(msgObject);
    } else if (msgmethod == "changeVest") { //更换马甲
      this.changeVest(msgObject);
    } else if (msgmethod == 'startEndLiveEven') { //连麦连通
      this.startEndLiveEven(msgObject);
    }
  },
  createRoom: function (create, choice) {
    var str = JSON.parse(choice);
    var data = Object.assign(str, create);
    //获取主播设置的发言字数

    if (sendMsgNum) {
      if (isNaN(sendMsgNum)) {
        layer.msg("发言字数必须填写数字");
        $("#sendMsgNum").val('');
        return;
      }

      if (sendMsgNum < 0 || sendMsgNum > 100) {
        layer.msg("发言字数必须大于0小于100");
        $("#sendMsgNum").val('');
        return;
      }

      if (sendMsgNum % 1 !== 0) {
        layer.msg("发言字数必须是整数");
        $("#sendMsgNum").val('');
        return;
      }
    } else {
      sendMsgNum = 30;
    }
    if (sendMsgFrequency) {
      if (isNaN(sendMsgFrequency)) {
        layer.msg("发言频率必须填写数字");
        $("#sendMsgFrequency").val('');
        return;
      }

      if (sendMsgFrequency < 1 || sendMsgFrequency > 60) {
        layer.msg("发言频率必须在1-60秒之间");
        $("#sendMsgFrequency").val('');
        return;
      }

      if (sendMsgFrequency % 1 !== 0) {
        layer.msg("发言频率必须是整数");
        $("#sendMsgFrequency").val('');
        return;
      }
    } else {
      sendMsgFrequency = 0;
    }

    //设置主播开播时的发言字数和发言频率
    data.sendMsgNum = sendMsgNum;
    data.sendMsgFrequency = sendMsgFrequency;

    $.ajax({
      type: "GET",
      url: "./index.php?m=show&a=createRoom",
      data: data,
      success: function (data) {
        var result = JSON.parse(data);
        if (result.state == 0) {
          if (_DATA.obs == "1") {
            alertMessage("OBS推流成功");
          } else {

            gotoPlayVideo();

          }
          /* 更新直播信息 */
          $.ajax({
            url: './index.php?m=show&a=live',
            data: {
              uid: _DATA.anchor.id
            },
            dataType: 'json',
            success: function (data) {
              if (data.error == 0) {
                Interface.startEndLive(2);
                _DATA.live = data.data;
                Rank.adddate();
                //$(".MR-talk .speaker cite").text(sendMsgNum);主播不做字数限制
                //Chat.chat_max_text_len=sendMsgNum;
                //console.log(Chat.chat_max_text_len);
              } else {
                alert(data.msg);
              }
            }
          })
        } else {
          alertMessage("接口请求失败" + result.msg);
        }
      },
      error: function (data) {
        alertMessage("接口请求失败");
      }
    });
  },

  createRoomEven: function (create, choice) {
    console.log(choice);
    /*var str = JSON.parse(choice);
    var data=Object.assign(str,create);*/


    if (choice == 1) {
      alertLianMaiMessage("OBS推流成功");
    } else {
      gotoLianMaiPlayVideo();
    }

    //更新直播信息
    /*var stream=data.stream;
    $.ajax({
    	url: './index.php?m=show&a=changeLianmaiStream',
    	type: 'POST',
    	dataType: 'json',
    	data: {liveid:_DATA.live.uid,stream:stream},
    	success:function(data){
    		if(data.code==0){
    			Interface.startEndLiveEven(2,stream);//发送连麦socket
    		}else{
    			layer.msg(data.msg);
    		}
    	},

    });*/



  },



  stopRoom: function (data) {
    $.ajax({
      type: "GET",
      url: "./api/public/?service=Live.stopRoom",
      data: data,
      success: function (data) {
        var result = data;
        if (result.ret == 200 && result.data.code == 0) {
          stopPublish();
        } else {
          alertMessage("接口请求失败:" + result.data.msg);
        }
      },
      error: function (data) {
        alertMessage("接口请求失败");
      }
    });
  },
  /*超管关闭房间*/
  superStopRoom: function () {
    setTimeout("window.location.href='/'", 5000) //秒后执行
    layer.alert("该直播间涉嫌违规，已被停播", {
      skin: 'layui-layer-molv' //样式类名
        ,
      closeBtn: 0,
      shift: 5,
      icon: 2
    }, function () {
      window.location.href = "./";
    });
  },
  sendMsg: function (data) {
    var msgtype = data.msgtype;
    var isimg = data.isimg;
    var msgaction = data.action;
    var _method_ = data._method_;
    if (msgtype == 0) {
      //console.log("调用进房间：");
      this.enterRoom(data);
    } else if (msgtype == 2) {
      if (isimg) {
        this.sendImg(data);

      } else {
        this.sendChat(data);
      }

    } else if (msgtype == 3) {
      this.virtualenterRoom(data);
    }
  },
  sendChat: function (data) {
    // 右侧聊天区发送修改显示提示窗 noah.g
    //html='<li><span class="time">'+data.timestamp+'</span>'
    var timestamp = +new Date();
    var html = '<li>';
    html += '<span class="ICON-noble-level ICON-nl-' + data.level + '"></span>';
    //html+='<i class="ICON-medal" title="消费等级"><img class="medal-img" src="http://static.youku.com/ddshow/img/zhou/gold_v2/gold_02.png"></i>';
    //html+='<i class="ICON-active-level-bg" title="活跃等级"><img src="http://static.youku.com/ddshow/img/zhou/active_v2/active_14.png">恋夏</i>';
    //html+='<i class="ICON-medal" title="旋风卡"><img class="medal-img" src="http://static.youku.com/ddshow/img/v5icons/xuan_new.png"></i>';
    html += '<span class="user-name" onclick=User.chatMsgAreaPopup(event,"' + data.uid + '","' + timestamp + '") popup-data-name="' + data.uname + '" data-name="' + data.uname + '" popup-data-uid="' + data.uid + '" data-id="' + data.uid + '" data-time="' + timestamp + '" id="js-rightmsg-' + data.uid + '-' + timestamp + '">' + data.uname + '</span>：' + twemoji.parse(data.ct);
    html += '</li>';
    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    Chat.resetsH();
  },

  sendImg: function (data) {
    var html = '<li>';
    html += '<span class="ICON-noble-level ICON-nl-' + data.level + '"></span>';
    html += '<span class="user-name" data-name="' + data.uname + '" data-id="' + data.uid + '">' + data.uname + '</span>：<img src="' + data.ct + '" class="sendmsgimg" />';
    html += '</li>';
    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    Chat.resetsH();
  },

  setLight: function () {
    var e = $("#player-praises .bubble"),
      t = e.width(),
      r = e.height(),
      i = 32,
      s = 26,
      o = 80,
      u = ["FF5D31", "FF7043", "FF9800", "F9A825", "F57F17", "FFCA28"],
      a = '<svg viewBox="-1 -1 27 27"><path class="svgpath" style="fill:$fill$;stroke: #FFF; stroke-width: 1px;" d="M11.29,2C7-2.4,0,1,0,7.09c0,4.4,4.06,7.53,7.1,9.9,2.11,1.63,3.21,2.41,4,3a1.72,1.72,0,0,0,2.12,0c0.79-.64,1.88-1.44,4-3,3.09-2.32,7.1-5.55,7.1-9.94,0-6-7-9.45-11.29-5.07A1.15,1.15,0,0,1,11.29,2Z"/></svg>',
      f = function () {
        if (e.find("svg").length > o) return;
        var n = u[Math.floor(Math.random() * u.length)],
          r = $(a.replace("$fill$", "#" + n));
        this.startx = t / 2 - 10,
          this.pos = Math.random() * Math.PI,
          this.hz = Math.random() * 20 + 10,
          this.zf = Math.random() * 15 + 10,
          this.opacityStart = Math.random() * 10 + 10,
          this.y = 0,
          this.$el = r,
          this.setStyle(),
          e.append(r),
          this.run()
      };
    f.prototype.setStyle = function () {
        var e = this.startx + Math.sin(this.pos + this.y / this.hz) * this.zf,
          t = 1 - Math.max((this.y - this.opacityStart) / (r - this.opacityStart), 0),
          n = Math.min(this.y * 2 / r * (i - s) + s, i);
        this.$el.css({
          left: e,
          bottom: this.y,
          opacity: t
        }).width(n).height(n)
      },
      f.prototype.run = function () {
        var e = this,
          t = Math.random() * 20 + 10,
          n = $.now(),
          i = setInterval(function () {
              var s = $.now();
              e.y += Math.round((s - n) / t),
                n = s,
                e.setStyle(),
                e.y >= r && (e.$el.remove(), clearTimeout(i))
            },
            t)
      }
    new f;
  },
  enterRoom: function (data) {
    //console.log("进房间了：");
    var html = '<li class="enter">欢迎<span class="all-name"><span class="user-name" data-name="' + data.ct.user_nicename + '" data-id="' + data.ct.id + '">' + data.ct.user_nicename + '</span></span>进入频道</li>';
    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    Chat.resetsH();

    //console.log("刷新用户列表：");	
    User.getOnline(); //刷新用户列表	
    //console.log("刷新用户列表完毕：");
  },
  disconnect: function (data) {},
  sendHorn: function (data) {
    var action = data.ct.action;
    if (action == 'sendsmallhorn') {
      this.sendSmallHorn(data);
    } else if (action == 'sendbighorn') {
      this.sendBigHorn(data);
    }
  },
  sendSmallHorn: function (data) {

  },
  sendBigHorn: function (data) {
    var html = '<a class="notice-horn" href="/' + data.touid + '" target="_blank">\
				<span class="name">' + data.uname + '</span>\
				<span class="mid">：</span>\
				<span class="say">' + data.ct.content + '</span>\
				<span class="link">[' + _DATA.anchor.user_nicename + '的直播频道]</span>\
			</a>';
    $(".MR-msg-notice .msg-content").html(html);
    $(".MR-msg-notice").show();
    setTimeout(function () {
      $(".MR-msg-notice").hide();
      $(".MR-msg-notice .msg-content").html('');
    }, 5000)
  },

  showEndRecommend: function (data) {
    var msgmethod = data.action;
    if (msgmethod == 18) {
      layer.msg("直播结束", {}, function () {
        //location.href='/';
        $(".channel-player-v2").html(''); //直播画面变黑
      })
      return !1;
      Video.endRecommend();
    } else {
      Video.statRecommend();
    }
  },

  KickUser: function (data) {
    this.systemNot(data);
    if(data.touid==_DATA.user.id)
    {
      setTimeout("window.location.href='/'", 5000) //秒后执行
      layer.alert("你已经被踢出房间", {
        skin: 'layui-layer-molv' //样式类名
          ,
        closeBtn: 0,
        shift: 5,
        icon: 2
      }, function () {
        window.location.href = "./";
      });
    }

    $(".MR-online .MR-chat .clearfix #anchor_" + data.touid + "").remove(); //将用户从列表删除
    var num = $(".MR-online .nav-tab cite").text();
    if (num > 0) {
      $(".MR-online .nav-tab cite").text(num - 1);
    }
  },
  systemNot: function (data) {

    var isimg = data.isimg;
    if (isimg == 1) {

      this.systemNotImg(data);
      return;
    }
    var html = '<li><span class="system_a">系统消息：</span><span class="system_name">' + data.ct + '</span></li>';
    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    //$('#LF-chat-msg-area .MR-chat .boarder').scrollTop( $('#LF-chat-msg-area .MR-chat .boarder')[0].scrollHeight );
    Chat.resetsH();
    if (data.ct.indexOf('关注了主播') >= 0) {
      User.setAttentionNums();
    }
  },


  systemNotImg: function (data) {
    var html = '<li><span class="system_a">系统消息：</span><img src="' + data.ct + '" class="sendmsgimg" /></li>';
    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    Chat.resetsH();
    if (data.ct.indexOf('关注了主播') >= 0) {
      User.setAttentionNums();
    }
  },


  /*系统推送文字广告*/
  systemAdvertise: function (data) {
    if (data.url != '') {
      var html = '<li><span class="system_a">系统推送：</span><span class="system_name"><a style="color:#F9963E;text-decoration:underline;" target="_blank" href="' + data.url + '">' + data.ct + '</a></span></li>';
    } else {
      var html = '<li><span class="system_a">系统推送：</span><span class="system_name">' + data.ct + '</span></li>';
    }

    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    Chat.resetsH();
  },

  sendGift: function (data) {

    var html = '<li><span class="ICON-noble-level ICON-nl-' + data.level + '"></span>';
    html += '<span class="user-name" data-name="' + data.uname + '" data-id="' + data.uid + '">' + data.uname + '</span>';
    html += '<i class="mlr-5">赠送</i>';
    html += data.ct.giftname + ' X ';
    html += data.ct.giftGroupNum;

    html += '<img src="' + data.ct.gifticon + '">';
    data.ct.giftcount > 1 ? html += '(' + data.ct.giftname + '*' + data.ct.giftcount + ')' : '';
    html += '</li>';

    //console.log(html);
    $(".msg-gift .MR-chat .boarder ul").append(html);
    //$('.MR-msg .MR-chat .boarder').scrollTop( $('.MR-msg .MR-chat .boarder')[0].scrollHeight );
    Giftlist.resetH();
    window.HJ_PopBox.gift(data);

    //即时刷新排行榜
    Rank.adddate();


    /*礼物PK处理*/
    if (isGiftPK == 1) {

      var masterCoin = 0,
        guestCoin = 0;

      if (giftPkMasterGiftID == data.ct.giftid) {



        masterCoin = parseFloat($(".giftPkNumsL").text());


        //console.log("原来礼物总数：");
        //console.log(masterCoin);

        //console.log("赠送礼物价值：");
        //console.log(data.ct.totalcoin);
        //console.log("新的总数：");
        //console.log(masterCoin+data.ct.totalcoin);


        $(".giftPkNumsL").text((masterCoin + data.ct.totalcoin).toFixed(2));
        //计算双方占比
        masterCoin = parseFloat($(".giftPkNumsL").text());
        guestCoin = parseFloat($(".giftPkNumsR").text());

        //console.log("主队礼物主队总数：");
        //console.log(masterCoin);
        //console.log(typeof(masterCoin));


        //console.log("主队礼物客队总数：");
        //console.log(guestCoin);
        //console.log(typeof(guestCoin));

        //console.log("主队礼物：主队：");

        //console.log((masterCoin/(masterCoin+guestCoin)*100).toFixed(2));

        $(".masterGiftPercent").css('width', (masterCoin / (masterCoin + guestCoin) * 100).toFixed(2) + '%');
        //console.log("主队礼物：客队：");
        //console.log((guestCoin/(masterCoin+guestCoin)*100).toFixed(2));
        $(".guestGiftPercent").css('width', (guestCoin / (masterCoin + guestCoin) * 100).toFixed(2) + '%');
      }

      if (giftPkGuestGiftID == data.ct.giftid) {

        guestCoin = parseFloat($(".giftPkNumsR").text());
        $(".giftPkNumsR").text((guestCoin + data.ct.totalcoin).toFixed(2));

        //计算双方占比
        masterCoin = parseFloat($(".giftPkNumsL").text());

        guestCoin = parseFloat($(".giftPkNumsR").text());

        //console.log("客队礼物：主队：");
        //console.log((masterCoin/(masterCoin+guestCoin)*100).toFixed(2));

        //console.log("客队礼物：客队：");
        //console.log((guestCoin/(masterCoin+guestCoin)*100).toFixed(2));

        $(".masterGiftPercent").css('width', (masterCoin / (masterCoin + guestCoin) * 100).toFixed(2) + '%');
        $(".guestGiftPercent").css('width', (guestCoin / (masterCoin + guestCoin) * 100).toFixed(2) + '%');
      }
    }

  },
  giftExecuteQueue: function (data) { //执行队列
    var giftId = data.ct.giftid;
    var giftinfo = _DATA.gift[giftId];
    var runTime = 0,
      type = 0;
    //记录礼物信息
    if (giftinfo['swf'] != '' && giftinfo['swf'] != null && giftinfo['swftime'] != '') {
      runTime = giftinfo['swftime'] * 1000;
      type = 1;
    } else if (data.ct.giftcount > 1) {
      runTime = 5 * 1000;
    } else {
      return !1;
    }

    var giftQueueItem = [];
    giftQueueItem['time'] = Date.parse(new Date());
    giftQueueItem['data'] = data;
    giftQueueItem['type'] = type;
    giftQueueItem['giftPlayTime'] = 0; //Date.parse(new Date());
    giftQueueItem['runTime'] = runTime;

    giftQueue.unshift(giftQueueItem);

    if (giftPlayState == 0) //如果队列未在执行创建一个队列
    {
      giftPlayState = 1;
      this.giftQueueStart();
    } else if (giftPlayState == 2) {
      //等待队列结束
      var queueStart = this.giftQueueStart;
      var interID = setInterval(function () {
        if (giftPlayState == 0) {
          clearInterval(interID);
          giftPlayState = 1;
          queueStart();
        }
      }, 10);
    } else {
      //console.log("队列正在执行，等待执行中");
    }

  },
  giftQueueStart: function () {
    //获取到执行时间
    var data = giftQueue.pop();
    if (typeof (data) == "undefined") return 0;
    if (data['type'] == 1) {
      /* Flash */
      this.giftShowswf(data['data']);
    } else {
      /* 普通礼物 */
      this.giftShowFlash(data['data']);
    }
    //判断下一个 有没有  什么时间放
    //当前一个 播放完之后检测 是否 有下一个  有 继续播放  没有 标注队列 状态为 0
    setTimeout(function () {
      if (giftQueue.length != 0) {
        JsInterface.giftQueueStart();
      } else {
        giftPlayState = 0; //准备停止队列
      }
    }, data['runTime'])
  },
  giftShowFlash: function (data) { //礼物展示
    var data = data.ct;
    if (this.giftEffects == 0) {
      return 0;
    }
    var giftIcon = data.gifticon;
    var giftcount = data.giftcount;
    if (giftcount >= 3344) {
      var effectId = 9;
    } else if (giftcount >= 1314) {
      var effectId = 8;
    } else if (giftcount >= 520) {
      var effectId = 7;
    } else if (giftcount >= 188) {
      var effectId = 5;
    } else if (giftcount >= 99) {
      var effectId = 3;
    } else if (giftcount >= 66) {
      var effectId = 2;
    } else if (giftcount >= 11) {
      var effectId = 0;
    } else if (giftcount > 1) {
      var effectId = 0;
    } else {
      var effectId = -1;
    }

    //-1一个 0三角形 1不显示 2六字形 3嘴形 4元宝 5心形 7 ILOVEYOU 8一生一世 9海枯石烂
    if (giftcount > 1) {
      // 一次 多个礼物赠送的展示
      $('#LF-gift-container').css({
        "width": "672px",
        "height": "353px"
      });

      var aa = parseInt(Math.random() * 10000);
      swfobject.getObjectById("LF-gift-flash").playEffect(giftIcon, effectId, 200, aa);
      setTimeout(
        function () {
          swfobject.getObjectById("LF-gift-flash").clearDuoEffect(aa);
          $('#LF-gift-container').css({
            "width": "1px",
            "height": "1px"
          });
        }, 5000
      );
    }
  },
  giftShowswf: function (data) { //有swf 礼物展示
    var data = data.ct;
    var giftId = data["giftid"];
    var giftinfo = _DATA.gift[giftId]
    if (this.giftEffects == 0) {
      return 0;
    }
    var giftIcon = giftinfo['swf'];
    var effectId = -2;

    $('#LF-gift-container').css({
      "width": "672px",
      "height": "353px"
    });

    swfobject.getObjectById("LF-gift-flash").playEffect(giftIcon, effectId, 200);
    setTimeout(
      function () {
        swfobject.getObjectById("LF-gift-flash").clearEffect();
        swfobject.getObjectById("LF-gift-flash").playEffect("", "", 200);
        $('#LF-gift-container').css({
          "width": "1px",
          "height": "1px"
        });
      }, giftinfo['swftime'] * 1000
    );
  },
  clearScreen: function (data) {
    $("#LF-chat-msg-area .MR-chat .clearfix").html("");
    $("#LF-chat-msg-area .MR-chat .clearfix").html('<li><span class="system_a">系统消息：</span><span class="system_name">发言列表被清除</span></li>');
  },
  showuserinfo: function (data) {
    //显示在线用户
    //console.log(data);
    //console.log("刷新555555");
    var list = data.usinfo.list,
      len = list.length,
      html = '';
    var n = 0;
    // 旗舰厅标识 手机端标识图片 noah.g
    var piclist = ['/public/home/images/ulist-ag-flagship.png', '/public/home/images/ulist-ag-international.png', '/public/home/images/ulist-mobile.png'];
    for (var i = 0; i < len; i++) {
      var userinfo = list[i];
      n = n + 1;
      html += '<li data-cardid="' + userinfo.id + '" id="anchor_' + userinfo.id + '" class="anchor"><a class="xinxi" onclick=User.popup("' + userinfo.id + '")>';
      html += '<span class="ICON-noble-level ICON-nl-' + userinfo.level + '"></span>';
      //html+='<i class="ICON-medal" title="消费等级"><img class="medal-img" src=""></i>';
      //html+='<i class="ICON-active-level-bg" title="活跃等级"><img src="">普通</i>';
      html += '<span class="name" title="' + userinfo.user_nicename + '">' + userinfo.user_nicename + '</span>';
      /* 游客试玩普通账号标识处理 noah.g
       * is_trial_play 1为试玩账号 0 为游客普通账号
       */
      html += '<span class="isGuard">';
      if (userinfo.is_trial_play !== 1) {
        if (userinfo.vestIcon) {
          html += '<img src="' + userinfo.vestIcon + '" style="width:20px;height:20px;min-width:20px;">';
        } else {
          html += '<img src="/public/images/vest-icon-youke.png" style="width:15px;height:15px;min-width:15px;">';
        }
      }
      // 旗舰厅标识 手机端标识noah.g Start
      if (userinfo.source === 'A02_AG') {
        html += '<img src="' + piclist[0] + '" style="width:20px;height:20px;min-width:20px;">';
      }
      if (userinfo.source === 'A02_AGIN') {
        html += '<img src="' + piclist[1] + '" style="width:20px;height:20px;min-width:20px;">';
      }
      if (userinfo.os === 'mobile') {
        html += '<img src="' + piclist[2] + '" style="width:12px;height:20px;min-width:12px;">';
      }
      html += '</span>'; //马甲图标

      if (userinfo.isGuard == 1) {
        html += '<span class="isGuard">';
        if (userinfo.guardLevel == 1) {
          html += '<img src="/public/home/show/images/guardBg1.png" style="width:20px;height:20px;">'; //守护1图标
        } else if (userinfo.guardLevel == 2) {
          html += '<img src="/public/home/show/images/guardBg2.png" style="width:20px;height:20px;">'; //守护2图标
        } else if (userinfo.guardLevel == 3) {
          html += '<img src="/public/home/show/images/guardBg3.png" style="width:20px;height:20px;">'; //守护3图标
        } else if (userinfo.guardLevel == 4) {
          html += '<img src="/public/home/show/images/guardBg4.png" style="width:20px;height:20px;">'; //守护4图标
        } else if (userinfo.guardLevel == 5) {
          html += '<img src="/public/home/show/images/guardBg5.png" style="width:20px;height:20px;">'; //守护5图标
        }
        html += '</span>';
      }

      html += '<div class="yanjing"><div class="yanjing_img"></div></div>';
      html += '</a></li>';
    }
    if (User.user_isfirst == 1) {
      User.userCon.html(html);
      User.userCite.html(data.nums);

    } else {
      User.userCon.append(html);
      User.userCite.html(data.nums);

    }
    if (_DATA.live.ispc != 1) {
      User.resetuserssH();
    }
    /*if(len < User.defnums){
    	User.moreListBtn.hide();
    }else{
    	User.moreListBtn.show();
    }*/
  },

  /*发红包*/
  SendRedPack: function (data) {
    console.log('红包来啦', data);
    var redPacketType = data.redPacketType;

    if (redPacketType == 0) {
      PacketType = "普通";
    } else if (redPacketType == 1) {
      PacketType = "口令";
    }

    //console.log("红包来啦");
    var html = '<li class="enter" onclick="openRedPacket(' + data.sendRedUid + ',' + data.liveuid + ',' + data.sendtime + ',' + data.redPacketType + ',\'' + data.title + '\',\'' + data.avatar + '\',\'' + data.user_nicename + '\')" ><span class="ICON-noble-level ICON-nl-' + data.level + '"></span><span class="system_a">' + data.user_nicename + '发了一个' + PacketType + '红包，点击领取</span><img src="/public/home/images/redBag.png" width="25" height="25" /></li>';
    $("#LF-chat-msg-area .MR-chat .boarder ul").append(html);
    Chat.resetsH();

  },

  giftPK: function (data) {

    if (data.action == 0) {

      /*改变公共变量*/
      giftPkMasterGiftID = data.masterGiftID;
      giftPkGuestGiftID = data.guestGiftID;
      isGiftPK = 1;

      //console.log(giftPkMasterGiftID);
      //console.log(giftPkGuestGiftID);
      //console.log(isGiftPK);

      /*清除上一局的结果*/
      $(".masterGiftPercent").css('width', '50%');
      $(".guestGiftPercent").css('width', '50%');
      $(".giftPkNumsL").text(0);
      $(".giftPkNumsR").text(0);


      var masterUserImg = data.masterAvatar;
      $(".masterUserImg").attr('src', masterUserImg);
      var masterName = data.masterName;
      $(".masterName").text(masterName);
      var masterGiftImg = data.masterGiftImg;
      $(".masterGiftImg img").attr('src', masterGiftImg);


      var guestUserImg = data.guestAvatar;
      $(".guestUserImg").attr('src', guestUserImg);
      var guestName = data.guestName;
      $(".guestName").text(guestName);
      var guestGiftImg = data.guestGiftImg;
      $(".guestGiftImg img").attr('src', guestGiftImg);

      $("#giftPKIngs").show();

      /*倒计时显示*/
      $("#giftPkClock").show();

      var effectiveTime = data.effectiveTime;

      var interval = window.setInterval(function () {

        if (effectiveTime == 0) {
          window.clearInterval(interval);
          $(".giftPkClockNum").height(60);
          $(".giftPkClockNum").css('line-height', '65px');
          $(".giftPkClockNum").text("wating");
          $(".giftPkUnit").text('');
          $("#giftPKIngs").hide();


          /*执行倒计时结束后的关闭*/

          if (_DATA.anchor.id == _DATA.user.id) { //判断是否是主播方

            stopGiftPK(data.pkID); //调用礼物PK结束函数【giftPK.js】

          }
          return;
        }

        $(".giftPkClockNum").text(effectiveTime);
        effectiveTime--;


      }, 1000);
    }


    if (data.action == 1) { //PK结束返回PK结果展示给房间内的用户
      var winType = data.winType;
      $("#giftPkClock").hide(); //倒计时消失

      /*改变公共变量*/
      giftPkMasterGiftID = 0;
      giftPkGuestGiftID = 0;
      isGiftPK = 0;

      /*清除结果*/
      $(".masterGiftPercent").css('width', '50%');
      $(".guestGiftPercent").css('width', '50%');
      $(".giftPkNumsL").text(0);
      $(".giftPkNumsR").text(0);

      //console.log("winType*****Start");
      //console.log(winType);
      //console.log("winType*****End");

      if (winType == 0) {

        $("#giftPkResult").css("background-image", "url(/public/home/show/images/giftPkEqual.png)");
        $(".giftPkResultStatus").text("双方平局");

        if (data.masterFirstUserNicename != "") {
          $(".masterSendUserName").text(data.masterFirstUserNicename);
          $(".masterSendUserCoin").text("贡献:" + data.masterFirstUserCoin);
        } else {
          $(".masterSendUserName").text('');
          $(".masterSendUserCoin").text("(主队)贡献:0");
        }

        if (data.guestFirstUserNicename != "") {
          $(".guestSendUserName").text(data.guestFirstUserNicename);
          $(".guestSendUserCoin").text("贡献:" + data.guestFirstUserCoin);
        } else {
          $(".guestSendUserName").text('');
          $(".guestSendUserCoin").text("(客队)贡献:0");
        }



      } else if (winType == 1) {

        $("#giftPkResult").css("background-image", "url(/public/home/show/images/giftPkWin.png)");
        $(".giftPkResultStatus").text(data.masterUserNicename);
        $(".masterSendUserName").text(data.masterFirstUserNicename);
        $(".masterSendUserCoin span").text(data.masterFirstUserCoin);
        $(".masterSendUser").width(286);
        $(".guestSendUser").hide();


      } else if (winType == 2) {

        $("#giftPkResult").css("background-image", "url(/public/home/show/images/giftPkWin.png)");
        $(".giftPkResultStatus").text(data.guestUserNicename);
        $(".guestSendUserName").text(data.guestFirstUserNicename);
        $(".guestSendUserCoin span").text(data.guestFirstUserCoin);
        $(".guestSendUser").width(286).css('marginLeft', 7);
        $(".masterSendUser").hide();
      }



      $("#giftPkResult").show();
      setTimeout(function () {
        $("#giftPkResult").hide();
        $(".masterSendUser").width(143);
        $(".guestSendUser").width(143).css('marginLeft', 0);
        $(".masterSendUser").show();
        $(".guestSendUser").show();
      }, 10000);



    }


  },

  GrabBench: function (data) {


    if (data.action == 0) { //抢板凳活动开始

      if (_DATA.user.id != _DATA.anchor.id) {
        $("#grabBenchIngs").show();
      }


      grabBenchID = data.grabbenchID;

      grabBenchHits_Space = data.hits_space; //用户抢板凳时间间隔

      isGrabBench = 1;

      //console.log("开始游戏");
      //console.log("grabBenchHits_Space"+grabBenchHits_Space);
      //console.log("isGrabBench"+isGrabBench);

      var effectiveTime = data.effectiveTime; //抢板凳活动倒计时

      var interval = window.setInterval(function () {

        if (effectiveTime == 0) {
          window.clearInterval(interval); //将倒计时清空
          if (hitsInterval) {
            window.clearInterval(hitsInterval); //将用户点击倒计时清空【grabBench.js中】
          }

          $(".grabBenchClockNum").height(60);
          $(".grabBenchClockNum").css('line-height', '65px');
          $(".grabBenchClockNum").text("wating");
          $(".grabBenchUnit").text('');
          $("#grabBenchIngs").hide();


          /*执行倒计时结束后的关闭*/

          if (_DATA.anchor.id == _DATA.user.id) { //判断是否是主播方

            stopGrabBench(data.grabbenchID); //调用抢板凳结束函数【grabBench.js中】

          }
          return;
        }

        $(".grabBenchClockNum").text(effectiveTime);
        effectiveTime--;


      }, 1000);

      /*活动倒计时显示*/
      $("#grabBenchClock").show();
      $("#grabWinNums").text(''); //将用户选择号码显示区域置空
      $('.grabNums').text(''); //将用户抢到号码置空
    }

    if (data.action == 1) { //抢板凳活动结束

      var winArray = data.winArray;

      //console.log(typeof(winArray));
      var str = '<li><div class="grabBenthWinNum">中奖号码</div><div class="grabBenthUserName">中奖人</div><div class="clearboth"></div></li>';

      $("#grabBenchClock").hide();
      $("#grabBenchIngs").hide();

      for (var i = 0; i < winArray.length; i++) {　
        str += '<li><div class="grabBenthWinNum">' + winArray[i].num + '</div>';
        str += '<div class="grabBenthUserName">' + winArray[i].user_nicename + '</div>';
        str += '<div class="clearboth"></div>';
        str += '</li>';　　
      }

      $(".grabWinNums").html(str);





      if (_DATA.user.id != _DATA.anchor.id) {

        isHitsInterval = 0; //将倒计时可点击改为0【未倒计时，可点击】


        if (grabBenchNumsArr.length > 0) {
          var winNUmStr = "我的号码:";
          for (var i = 0; i < grabBenchNumsArr.length; i++) {
            winNUmStr += grabBenchNumsArr[i] + ",";
          }



          winNUmStr = winNUmStr.substr(0, winNUmStr.length - 1);


          $(".grabNums").text(winNUmStr);

        } else {

          $(".grabNums").text("您没有抢到号码");
        }

      } else {
        $(".grabNums").text();
      }


      $("#grabBenchRes").show();

      //十秒结果自动消失
      window.setTimeout(function () {
        grabBenchNumsArr = []; //将抢到号码数组置空
        $("#grabWinNums").text(''); //将用户选择号码显示区域置空
        $('.grabNums').text(''); //将用户抢到号码置空
        $("#grabBenchRes").hide();
      }, 10000);

    }

  },

  Carouse: function (data) {


    if (data.action == 0) { //发起转盘游戏时
      $("#turntableImg").css('background-image', 'url(' + data.imgUrl + ')');
      $("#turntableImg").css('backgroundSize', '250px 250px');
      $("#turntableImg").css('background-repeat', 'no-repeat');

      turntableTotalNum = data.nums;
      turntableRandNum = data.randNum;

      if (_DATA.user.id == data.startGameUid) {

        $("#turntableImg").after('<div class="turntableStartBtn" onclick="startTurnTable()">启动转盘</div>');
      }

      setTimeout(function () {
        if (isStartTurnTable == 0) {
          $(".turntableStartBtn").remove();
        }
      }, 10000);

      /*角度置0*/
      $("#turntableImg").css({
        rotate: '0deg'
      });



      $("#turntableIngs").show();
    }

    if (data.action == 1) { //指定用户开启

      /*角度置0*/
      $("#turntableImg").css({
        rotate: '0deg'
      });


      var jiaodu = 0;
      isStartTurnTable = 1;
      //获取总数
      var nums = turntableTotalNum;
      var randNum = turntableRandNum;
      var count = 10; //圈数

      //console.log(randNum);

      var res = 360 * count - (randNum - 1) * (360 / nums) + jiaodu + 15;

      //console.log(res);

      $("#turntableImg").transition({ //调用home/js/jquery.transit.js实现动画，参考http://code.ciaoca.com/jquery/transit/
        rotate: res,
        duration: 6000,
        easing: 'easeInOutQuad',
        complete: function () {
          setTimeout(function () {


            isStartTurnTable = 0;
            if (_DATA.user.id == _DATA.anchor.id) {
              $("#turntableIngs").hide();
              layer.confirm('是否再次发起转盘游戏？', {
                btn: ['确定', '取消'] //按钮
              }, function () {
                layer.closeAll();
                $(".turntableOpenUser").val('');
                $("#turntableArea").show();

              }, function () {
                // layer.close();
                CancelTurnTable();
              });
            }


          }, 5000);
        }
      });
    }
    if (data.action == 2) { //结束
      if (_DATA.user.id != _DATA.anchor.id) {

        if (data.msgtype == 1) { //超时未开启
          if (_DATA.user.id == data.startGameUid) { //当前发起用户
            layer.msg('您未按时启动游戏！主播将重新发起');
            $(".turntableStartBtn").remove();
            $("#turntableIngs").hide();
          } else {
            layer.msg('用户未按时启动游戏！将重新发起');
            $("#turntableIngs").hide();
          }
        } else if (data.msgtype == 0) {
          layer.msg('主播关闭了转盘游戏');
          $("#turntableIngs").hide();
        }
      } else { //主播
        if (data.msgtype == 2) {
          layer.msg('用户启动游戏失败啦,将重新发起');
          $("#turntableIngs").hide();
          $("#turntableArea").show();
        }

        /*if(data.msgtype==0){
        	layer.msg('你关闭了转盘游戏,将重新发起');
        	
        	//$("#turntableArea").show();
        }*/
      }
    }
  },
  Lianmai: function (data) {
    //console.log(data);
    if (data.action == 0) { //申请连麦

      if (data.liveUid == _DATA.user.id) { //主播

        layer.confirm(data.user_nicename + '申请连麦，是否同意？', {
          btn: ['同意', '拒绝'] //按钮
        }, function () {
          AgreeLianmai(data.sendUid, data.recordID);
          layer.closeAll();
        }, function () {
          RefuseLianmai(data.sendUid, data.recordID);
        });

      }

      if (data.sendUid == _DATA.user.id) {

        layer.msg('连麦申请成功请等待主播回复', {
          icon: 1
        });

        window.setTimeout(function () {
          TenMinutesConnectVideoCheck(data.sendUid, data.liveUid);
        }, 10000);



      }


    } else if (data.action == 1) { //主播拒绝
      if (data.sendUid == _DATA.user.id) {
        layer.msg('你的连麦申请被主播拒绝啦');
        return;
      }


    } else if (data.action == 2) { //主播同意连麦

      //console.log("主播同意连麦后信息");
      //console.log(data);

      if (_DATA.anchor.id == _DATA.user.id) {

        $(".lianmaiCancelIcon").show(); //主播显示断麦图标
      }
      if (data.sendUid == _DATA.user.id) {

        isConnectVideo = 1; //改变模板上的公共变量

        layer.msg('主播已经同意连麦', {
          icon: 1
        });
        $(".lianmaiIcon").hide();
        $(".lianmaiCancelIcon").show(); //用户显示断麦图标
        // console.log(data.stream);

        var videoData = new Object();
        videoData.rtmpUrl = "rtmp://video-center.alivecdn.com/5showcam/" + data.stream + "?vhost=xiangganglive.yunbaozhibo.com"; //rtmp
        console.log("rtmp://video-center.alivecdn.com/5showcam/" + data.stream + "?vhost=xiangganglive.yunbaozhibo.com");
        videoData.videoWidth = 200; //宽
        videoData.videoHeight = 200; //高
        getSwf("playerzmblbkjP").publishVideo(videoData); //推流

        sendConnectMsg(data.stream, data.sendUid);

        //用户加载推流界面
        /*var  script_text= "swfobject.embedSWF(\"/public/home/js/5ShowCamLivePlayer.swf?uid="+_DATA.anchor.id+"&token="+_DATA.user.token+"&roomId="+data.stream+"&stream="+data.stream+"&cdn="+_DATA.push.cdn+"&keyframe="+_DATA.config.keyframe+"&fps="+_DATA.config.fps+"&bandwidth=0&width=200&height=200&quality="+_DATA.config.quality+"\" ,\"lianmaiPlayer\",\"100%\",\"100%\",\"10.0\", \"\",{},{quality:\"high\",wmode:\"opaque\",allowscriptaccess:\"always\"})";
        console.log(script_text);
        $("#lianmaiPlayer").html("<script>"+script_text+"<\/script>");
        $("#lianmaiPlayer").css({"width":"200px","height":"200px"});
        $("#lianmaiPlayer").show();*/
      }



    } else if (data.action == 3) { //十秒后主播未同意或拒绝用户连麦

      if (_DATA.anchor.id == _DATA.user.id) {

        layer.closeAll(); //关闭主播端连麦申请弹窗
      }

    } else if (data.action == 4) { //播放
      console.log(data);
      if (_DATA.user.id == _DATA.anchor.id) { //主播

        var mike = '<object type=\"application/x-shockwave-flash\" data=\"/public/swf/monitor.swf?roomId=' + data.stream + '&cdn=xiangganglive.yunbaozhibo.com\" width=\"200\" height=\"200\" id=\"39\" style=\"visibility: visible;\"><param name=\"wmode\" value=\"transparent\"><param name=\"allowscriptaccess\" value=\"always\"></object>';
        $("#lianmaiPlayer").html(mike);
        //document.getElementById("lianmaiPlayer").innerHTML = mike;

      } else if (_DATA.user.id == data.sendUid) { //连麦人

      } else {
        console.log("普通用户");



        var videoData = new Object();
        videoData.touid = data.sendUid;
        videoData.rtmpUrl = "rtmp://xiangganglive.yunbaozhibo.com/5showcam/" + data.stream; //rtmp
        videoData.videoWidth = 200;
        videoData.videoHeight = 200;
        getSwf("playerzmblbkjP").addConnectedVideo(videoData); //连麦
      }

    } else if (data.action == 5) {

      var sendUid = data.sendUid;

      if (_DATA.user.id != sendUid && _DATA.anchor.id != sendUid) { //非主播非连麦人

        $("#playerzmblbkjP").remove();
      }

    }

  },

  channelGap: function (data) {
    //console.log(data);
    if (data.action == 0) { //禁言

      if ($("#ChannelShutUp")) {
        $("#ChannelShutUp").text("解除禁言");
      }

      this.systemNot(data);

    } else if (data.action == 1) { //解除禁言
      if ($("#ChannelShutUp")) {
        $("#ChannelShutUp").text("频道禁言");
      }

      this.systemNot(data);
    }
  },


  changeVest: function (data) {
    this.systemNot(data);
    if (data.touid == _DATA.user.id) {

      if (data.gap_all == 1) {
        //console.log("准备添加频道禁言按钮");
        var str = '<a class="info identity" data-type="0" style="margin-top: 10px;margin-left: 10px;" id="ChannelShutUp" onclick="roomShutUp()">频道禁言</a>';
        $(".MR-count").append(str);

        //console.log("频道禁言按钮添加成功");
      } else {
        //console.log("准备删除频道禁言按钮");
        if ($("#ChannelShutUp")) {
          $("#ChannelShutUp").remove();
          //console.log("频道禁言按钮删除成功");
        }
      }
    }

  },

  startEndLiveEven: function (data) {
    var msgmethod = data.action;

    if (msgmethod == 18) { //连麦断麦

      Video.endRecommend();

    } else { //连麦

      var stream = data.stream;
      //将stream分割
      $arr = explode("_", $str);
      $liveid = $arr[0];
      $touid = $arr[1];

      console.log("直播间id：");
      console.log($liveid);
      console.log("连麦用户id：");
      console.log($touid);
      if ($touid != _DATA.user.id) { //除连麦用户之外的其他人加载连麦人画面
        $.ajax({
          url: '/index.php?g=Home&m=Show&a=getLianmaiStream',
          type: 'POST',
          dataType: 'json',
          data: {
            stream: stream
          },
          success: function (data) {
            var pull = data.info;
            /*jwplayer('lianmaiplayerzmblbkjP').setup({
            	provider: "rtmp",
            	flashplayer:"public/swf/jwplayer.flash.swf",
            	file: pull,
            	image: "public/home/images/loading.png",
            	autostart:true,
            	'stretching': 'bestfit',
            	height: '200px',
            	width: '200px'
            });	*/
            var mike = '<object type=\"application/x-shockwave-flash\" data=\"/public/swf/monitor.swf?roomId=' + stream + '&cdn=xiangganglive.yunbaozhibo.com\" width=\"200\" height=\"200\" id=\"39\" style=\"visibility: visible;\"><param name=\"wmode\" value=\"transparent\"><param name=\"allowscriptaccess\" value=\"always\"></object>';
            document.getElementById("zhubotan").innerHTML = mike;
          },
          error: function (data) {
            layer.msg("连麦失败啦");
          }
        });
      }
    }
  }
}